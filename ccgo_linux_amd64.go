// Code generated for linux/amd64 by 'generator --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -I/tmp/libmpc/mpc-1.3.1/ccgo -o libmpc.go --package-name libmpc src/.libs/libmpc.a -lgmp -lmpfr', DO NOT EDIT.

//go:build linux && amd64

package libmpc

import (
	"reflect"
	"unsafe"

	"modernc.org/libc"
	"modernc.org/libgmp"
	"modernc.org/libmpfr"
)

var _ reflect.Type
var _ unsafe.Pointer

const m_ARG_MAX = 131072
const m_BC_BASE_MAX = 99
const m_BC_DIM_MAX = 2048
const m_BC_SCALE_MAX = 99
const m_BC_STRING_MAX = 1000
const m_BITS_PER_MP_LIMB = "mp_bits_per_limb"
const m_CHARCLASS_NAME_MAX = 14
const m_CHAR_BIT = 8
const m_CHAR_MAX = 255
const m_CHAR_MIN = 0
const m_COLL_WEIGHTS_MAX = 2
const m_DELAYTIMER_MAX = 0x7fffffff
const m_EXIT_FAILURE = 1
const m_EXIT_SUCCESS = 0
const m_EXPR_NEST_MAX = 32
const m_FILESIZEBITS = 64
const m_GMP_LIMB_BITS = 64
const m_GMP_NAIL_BITS = 0
const m_GMP_NUMB_MAX = "GMP_NUMB_MASK"
const m_GMP_RNDD = "MPFR_RNDD"
const m_GMP_RNDN = "MPFR_RNDN"
const m_GMP_RNDU = "MPFR_RNDU"
const m_GMP_RNDZ = "MPFR_RNDZ"
const m_HAVE_COMPLEX_H = 1
const m_HAVE_CONFIG_H = 1
const m_HAVE_DLFCN_H = 1
const m_HAVE_DUP = 1
const m_HAVE_DUP2 = 1
const m_HAVE_GETRUSAGE = 1
const m_HAVE_GETTIMEOFDAY = 1
const m_HAVE_INTTYPES_H = 1
const m_HAVE_LIBM = 1
const m_HAVE_LOCALECONV = 1
const m_HAVE_MEMORY_H = 1
const m_HAVE_SETLOCALE = 1
const m_HAVE_STDINT_H = 1
const m_HAVE_STDLIB_H = 1
const m_HAVE_STRINGS_H = 1
const m_HAVE_STRING_H = 1
const m_HAVE_SYS_STAT_H = 1
const m_HAVE_SYS_TIME_H = 1
const m_HAVE_SYS_TYPES_H = 1
const m_HAVE_UNISTD_H = 1
const m_HOST_NAME_MAX = 255
const m_INT16_MAX = 0x7fff
const m_INT32_MAX = 0x7fffffff
const m_INT64_MAX = 0x7fffffffffffffff
const m_INT8_MAX = 0x7f
const m_INTMAX_MAX = "INT64_MAX"
const m_INTMAX_MIN = "INT64_MIN"
const m_INTPTR_MAX = "INT64_MAX"
const m_INTPTR_MIN = "INT64_MIN"
const m_INT_FAST16_MAX = "INT32_MAX"
const m_INT_FAST16_MIN = "INT32_MIN"
const m_INT_FAST32_MAX = "INT32_MAX"
const m_INT_FAST32_MIN = "INT32_MIN"
const m_INT_FAST64_MAX = "INT64_MAX"
const m_INT_FAST64_MIN = "INT64_MIN"
const m_INT_FAST8_MAX = "INT8_MAX"
const m_INT_FAST8_MIN = "INT8_MIN"
const m_INT_LEAST16_MAX = "INT16_MAX"
const m_INT_LEAST16_MIN = "INT16_MIN"
const m_INT_LEAST32_MAX = "INT32_MAX"
const m_INT_LEAST32_MIN = "INT32_MIN"
const m_INT_LEAST64_MAX = "INT64_MAX"
const m_INT_LEAST64_MIN = "INT64_MIN"
const m_INT_LEAST8_MAX = "INT8_MAX"
const m_INT_LEAST8_MIN = "INT8_MIN"
const m_INT_MAX = 0x7fffffff
const m_IOV_MAX = 1024
const m_LINE_MAX = 4096
const m_LLONG_MAX = 0x7fffffffffffffff
const m_LOGIN_NAME_MAX = 256
const m_LONG_BIT = 64
const m_LONG_MAX = "__LONG_MAX"
const m_LT_OBJDIR = ".libs/"
const m_MB_LEN_MAX = 4
const m_MPC_CC = "gcc"
const m_MPC_GCC = "yes"
const m_MPC_GCC_VERSION = "12"
const m_MPC_VERSION_MAJOR = 1
const m_MPC_VERSION_MINOR = 3
const m_MPC_VERSION_PATCHLEVEL = 1
const m_MPC_VERSION_STRING = "1.3.1"
const m_MPFR_FLAGS_DIVBY0 = 32
const m_MPFR_FLAGS_ERANGE = 16
const m_MPFR_FLAGS_INEXACT = 8
const m_MPFR_FLAGS_NAN = 4
const m_MPFR_FLAGS_OVERFLOW = 2
const m_MPFR_FLAGS_UNDERFLOW = 1
const m_MPFR_PREC_MIN = 1
const m_MPFR_USE_C99_FEATURE = 1
const m_MPFR_VERSION_MAJOR = 4
const m_MPFR_VERSION_MINOR = 2
const m_MPFR_VERSION_PATCHLEVEL = 0
const m_MPFR_VERSION_STRING = "4.2.0"
const m_MQ_PRIO_MAX = 32768
const m_MUL_KARATSUBA_THRESHOLD = 23
const m_NAME_MAX = 255
const m_NDEBUG = 1
const m_NGROUPS_MAX = 32
const m_NL_ARGMAX = 9
const m_NL_LANGMAX = 32
const m_NL_MSGMAX = 32767
const m_NL_NMAX = 16
const m_NL_SETMAX = 255
const m_NL_TEXTMAX = 2048
const m_NZERO = 20
const m_PACKAGE = "mpc"
const m_PACKAGE_BUGREPORT = "mpc-discuss@inria.fr"
const m_PACKAGE_NAME = "mpc"
const m_PACKAGE_STRING = "mpc 1.3.1"
const m_PACKAGE_TARNAME = "mpc"
const m_PACKAGE_URL = ""
const m_PACKAGE_VERSION = "1.3.1"
const m_PAGESIZE = 4096
const m_PAGE_SIZE = "PAGESIZE"
const m_PATH_MAX = 4096
const m_PIPE_BUF = 4096
const m_PTHREAD_DESTRUCTOR_ITERATIONS = 4
const m_PTHREAD_KEYS_MAX = 128
const m_PTHREAD_STACK_MIN = 2048
const m_PTRDIFF_MAX = "INT64_MAX"
const m_PTRDIFF_MIN = "INT64_MIN"
const m_RAND_MAX = 0x7fffffff
const m_RE_DUP_MAX = 255
const m_SCHAR_MAX = 127
const m_SEM_NSEMS_MAX = 256
const m_SEM_VALUE_MAX = 0x7fffffff
const m_SHRT_MAX = 0x7fff
const m_SIG_ATOMIC_MAX = "INT32_MAX"
const m_SIG_ATOMIC_MIN = "INT32_MIN"
const m_SIZE_MAX = "UINT64_MAX"
const m_SSIZE_MAX = "LONG_MAX"
const m_STDC_HEADERS = 1
const m_SYMLOOP_MAX = 40
const m_TTY_NAME_MAX = 32
const m_TZNAME_MAX = 6
const m_UCHAR_MAX = 255
const m_UINT16_MAX = 0xffff
const m_UINT32_MAX = "0xffffffffu"
const m_UINT64_MAX = "0xffffffffffffffffu"
const m_UINT8_MAX = 0xff
const m_UINTMAX_MAX = "UINT64_MAX"
const m_UINTPTR_MAX = "UINT64_MAX"
const m_UINT_FAST16_MAX = "UINT32_MAX"
const m_UINT_FAST32_MAX = "UINT32_MAX"
const m_UINT_FAST64_MAX = "UINT64_MAX"
const m_UINT_FAST8_MAX = "UINT8_MAX"
const m_UINT_LEAST16_MAX = "UINT16_MAX"
const m_UINT_LEAST32_MAX = "UINT32_MAX"
const m_UINT_LEAST64_MAX = "UINT64_MAX"
const m_UINT_LEAST8_MAX = "UINT8_MAX"
const m_UINT_MAX = 4294967295
const m_USHRT_MAX = 65535
const m_VERSION = "1.3.1"
const m_WINT_MAX = "UINT32_MAX"
const m_WINT_MIN = 0
const m_WNOHANG = 1
const m_WORD_BIT = 32
const m_WUNTRACED = 2
const m__GNU_SOURCE = 1
const m__LP64 = 1
const m__MPC_H_HAVE_INTMAX_T = 1
const m__MPFR_EXP_FORMAT = "_MPFR_PREC_FORMAT"
const m__MPFR_H_HAVE_INTMAX_T = 1
const m__MPFR_PREC_FORMAT = 3
const m__POSIX2_BC_BASE_MAX = 99
const m__POSIX2_BC_DIM_MAX = 2048
const m__POSIX2_BC_SCALE_MAX = 99
const m__POSIX2_BC_STRING_MAX = 1000
const m__POSIX2_CHARCLASS_NAME_MAX = 14
const m__POSIX2_COLL_WEIGHTS_MAX = 2
const m__POSIX2_EXPR_NEST_MAX = 32
const m__POSIX2_LINE_MAX = 2048
const m__POSIX2_RE_DUP_MAX = 255
const m__POSIX_AIO_LISTIO_MAX = 2
const m__POSIX_AIO_MAX = 1
const m__POSIX_ARG_MAX = 4096
const m__POSIX_CHILD_MAX = 25
const m__POSIX_CLOCKRES_MIN = 20000000
const m__POSIX_DELAYTIMER_MAX = 32
const m__POSIX_HOST_NAME_MAX = 255
const m__POSIX_LINK_MAX = 8
const m__POSIX_LOGIN_NAME_MAX = 9
const m__POSIX_MAX_CANON = 255
const m__POSIX_MAX_INPUT = 255
const m__POSIX_MQ_OPEN_MAX = 8
const m__POSIX_MQ_PRIO_MAX = 32
const m__POSIX_NAME_MAX = 14
const m__POSIX_NGROUPS_MAX = 8
const m__POSIX_OPEN_MAX = 20
const m__POSIX_PATH_MAX = 256
const m__POSIX_PIPE_BUF = 512
const m__POSIX_RE_DUP_MAX = 255
const m__POSIX_RTSIG_MAX = 8
const m__POSIX_SEM_NSEMS_MAX = 256
const m__POSIX_SEM_VALUE_MAX = 32767
const m__POSIX_SIGQUEUE_MAX = 32
const m__POSIX_SSIZE_MAX = 32767
const m__POSIX_SS_REPL_MAX = 4
const m__POSIX_STREAM_MAX = 8
const m__POSIX_SYMLINK_MAX = 255
const m__POSIX_SYMLOOP_MAX = 8
const m__POSIX_THREAD_DESTRUCTOR_ITERATIONS = 4
const m__POSIX_THREAD_KEYS_MAX = 128
const m__POSIX_THREAD_THREADS_MAX = 64
const m__POSIX_TIMER_MAX = 32
const m__POSIX_TRACE_EVENT_NAME_MAX = 30
const m__POSIX_TRACE_NAME_MAX = 8
const m__POSIX_TRACE_SYS_MAX = 8
const m__POSIX_TRACE_USER_EVENT_MAX = 32
const m__POSIX_TTY_NAME_MAX = 9
const m__POSIX_TZNAME_MAX = 6
const m__STDC_PREDEF_H = 1
const m__XOPEN_IOV_MAX = 16
const m__XOPEN_NAME_MAX = 255
const m__XOPEN_PATH_MAX = 1024
const m___ATOMIC_ACQUIRE = 2
const m___ATOMIC_ACQ_REL = 4
const m___ATOMIC_CONSUME = 1
const m___ATOMIC_HLE_ACQUIRE = 65536
const m___ATOMIC_HLE_RELEASE = 131072
const m___ATOMIC_RELAXED = 0
const m___ATOMIC_RELEASE = 3
const m___ATOMIC_SEQ_CST = 5
const m___BIGGEST_ALIGNMENT__ = 16
const m___BIG_ENDIAN = 4321
const m___BYTE_ORDER = 1234
const m___BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const m___CCGO__ = 1
const m___CHAR_BIT__ = 8
const m___DBL_DECIMAL_DIG__ = 17
const m___DBL_DIG__ = 15
const m___DBL_HAS_DENORM__ = 1
const m___DBL_HAS_INFINITY__ = 1
const m___DBL_HAS_QUIET_NAN__ = 1
const m___DBL_IS_IEC_60559__ = 2
const m___DBL_MANT_DIG__ = 53
const m___DBL_MAX_10_EXP__ = 308
const m___DBL_MAX_EXP__ = 1024
const m___DEC128_EPSILON__ = 1e-33
const m___DEC128_MANT_DIG__ = 34
const m___DEC128_MAX_EXP__ = 6145
const m___DEC128_MAX__ = "9.999999999999999999999999999999999E6144"
const m___DEC128_MIN__ = 1e-6143
const m___DEC128_SUBNORMAL_MIN__ = 0.000000000000000000000000000000001e-6143
const m___DEC32_EPSILON__ = 1e-6
const m___DEC32_MANT_DIG__ = 7
const m___DEC32_MAX_EXP__ = 97
const m___DEC32_MAX__ = 9.999999e96
const m___DEC32_MIN__ = 1e-95
const m___DEC32_SUBNORMAL_MIN__ = 0.000001e-95
const m___DEC64_EPSILON__ = 1e-15
const m___DEC64_MANT_DIG__ = 16
const m___DEC64_MAX_EXP__ = 385
const m___DEC64_MAX__ = "9.999999999999999E384"
const m___DEC64_MIN__ = 1e-383
const m___DEC64_SUBNORMAL_MIN__ = 0.000000000000001e-383
const m___DECIMAL_BID_FORMAT__ = 1
const m___DECIMAL_DIG__ = 17
const m___DEC_EVAL_METHOD__ = 2
const m___ELF__ = 1
const m___FINITE_MATH_ONLY__ = 0
const m___FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const m___FLT128_DECIMAL_DIG__ = 36
const m___FLT128_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const m___FLT128_DIG__ = 33
const m___FLT128_EPSILON__ = 1.92592994438723585305597794258492732e-34
const m___FLT128_HAS_DENORM__ = 1
const m___FLT128_HAS_INFINITY__ = 1
const m___FLT128_HAS_QUIET_NAN__ = 1
const m___FLT128_IS_IEC_60559__ = 2
const m___FLT128_MANT_DIG__ = 113
const m___FLT128_MAX_10_EXP__ = 4932
const m___FLT128_MAX_EXP__ = 16384
const m___FLT128_MAX__ = "1.18973149535723176508575932662800702e+4932"
const m___FLT128_MIN__ = 3.36210314311209350626267781732175260e-4932
const m___FLT128_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const m___FLT16_DECIMAL_DIG__ = 5
const m___FLT16_DENORM_MIN__ = 5.96046447753906250000000000000000000e-8
const m___FLT16_DIG__ = 3
const m___FLT16_EPSILON__ = 9.76562500000000000000000000000000000e-4
const m___FLT16_HAS_DENORM__ = 1
const m___FLT16_HAS_INFINITY__ = 1
const m___FLT16_HAS_QUIET_NAN__ = 1
const m___FLT16_IS_IEC_60559__ = 2
const m___FLT16_MANT_DIG__ = 11
const m___FLT16_MAX_10_EXP__ = 4
const m___FLT16_MAX_EXP__ = 16
const m___FLT16_MAX__ = 6.55040000000000000000000000000000000e+4
const m___FLT16_MIN__ = 6.10351562500000000000000000000000000e-5
const m___FLT16_NORM_MAX__ = 6.55040000000000000000000000000000000e+4
const m___FLT32X_DECIMAL_DIG__ = 17
const m___FLT32X_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const m___FLT32X_DIG__ = 15
const m___FLT32X_EPSILON__ = 2.22044604925031308084726333618164062e-16
const m___FLT32X_HAS_DENORM__ = 1
const m___FLT32X_HAS_INFINITY__ = 1
const m___FLT32X_HAS_QUIET_NAN__ = 1
const m___FLT32X_IS_IEC_60559__ = 2
const m___FLT32X_MANT_DIG__ = 53
const m___FLT32X_MAX_10_EXP__ = 308
const m___FLT32X_MAX_EXP__ = 1024
const m___FLT32X_MAX__ = 1.79769313486231570814527423731704357e+308
const m___FLT32X_MIN__ = 2.22507385850720138309023271733240406e-308
const m___FLT32X_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const m___FLT32_DECIMAL_DIG__ = 9
const m___FLT32_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const m___FLT32_DIG__ = 6
const m___FLT32_EPSILON__ = 1.19209289550781250000000000000000000e-7
const m___FLT32_HAS_DENORM__ = 1
const m___FLT32_HAS_INFINITY__ = 1
const m___FLT32_HAS_QUIET_NAN__ = 1
const m___FLT32_IS_IEC_60559__ = 2
const m___FLT32_MANT_DIG__ = 24
const m___FLT32_MAX_10_EXP__ = 38
const m___FLT32_MAX_EXP__ = 128
const m___FLT32_MAX__ = 3.40282346638528859811704183484516925e+38
const m___FLT32_MIN__ = 1.17549435082228750796873653722224568e-38
const m___FLT32_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const m___FLT64X_DECIMAL_DIG__ = 36
const m___FLT64X_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const m___FLT64X_DIG__ = 33
const m___FLT64X_EPSILON__ = 1.92592994438723585305597794258492732e-34
const m___FLT64X_HAS_DENORM__ = 1
const m___FLT64X_HAS_INFINITY__ = 1
const m___FLT64X_HAS_QUIET_NAN__ = 1
const m___FLT64X_IS_IEC_60559__ = 2
const m___FLT64X_MANT_DIG__ = 113
const m___FLT64X_MAX_10_EXP__ = 4932
const m___FLT64X_MAX_EXP__ = 16384
const m___FLT64X_MAX__ = "1.18973149535723176508575932662800702e+4932"
const m___FLT64X_MIN__ = 3.36210314311209350626267781732175260e-4932
const m___FLT64X_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const m___FLT64_DECIMAL_DIG__ = 17
const m___FLT64_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const m___FLT64_DIG__ = 15
const m___FLT64_EPSILON__ = 2.22044604925031308084726333618164062e-16
const m___FLT64_HAS_DENORM__ = 1
const m___FLT64_HAS_INFINITY__ = 1
const m___FLT64_HAS_QUIET_NAN__ = 1
const m___FLT64_IS_IEC_60559__ = 2
const m___FLT64_MANT_DIG__ = 53
const m___FLT64_MAX_10_EXP__ = 308
const m___FLT64_MAX_EXP__ = 1024
const m___FLT64_MAX__ = 1.79769313486231570814527423731704357e+308
const m___FLT64_MIN__ = 2.22507385850720138309023271733240406e-308
const m___FLT64_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const m___FLT_DECIMAL_DIG__ = 9
const m___FLT_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const m___FLT_DIG__ = 6
const m___FLT_EPSILON__ = 1.19209289550781250000000000000000000e-7
const m___FLT_EVAL_METHOD_TS_18661_3__ = 0
const m___FLT_EVAL_METHOD__ = 0
const m___FLT_HAS_DENORM__ = 1
const m___FLT_HAS_INFINITY__ = 1
const m___FLT_HAS_QUIET_NAN__ = 1
const m___FLT_IS_IEC_60559__ = 2
const m___FLT_MANT_DIG__ = 24
const m___FLT_MAX_10_EXP__ = 38
const m___FLT_MAX_EXP__ = 128
const m___FLT_MAX__ = 3.40282346638528859811704183484516925e+38
const m___FLT_MIN__ = 1.17549435082228750796873653722224568e-38
const m___FLT_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const m___FLT_RADIX__ = 2
const m___FUNCTION__ = "__func__"
const m___FXSR__ = 1
const m___GCC_ASM_FLAG_OUTPUTS__ = 1
const m___GCC_ATOMIC_BOOL_LOCK_FREE = 2
const m___GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const m___GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const m___GCC_ATOMIC_CHAR_LOCK_FREE = 2
const m___GCC_ATOMIC_INT_LOCK_FREE = 2
const m___GCC_ATOMIC_LLONG_LOCK_FREE = 2
const m___GCC_ATOMIC_LONG_LOCK_FREE = 2
const m___GCC_ATOMIC_POINTER_LOCK_FREE = 2
const m___GCC_ATOMIC_SHORT_LOCK_FREE = 2
const m___GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const m___GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const m___GCC_CONSTRUCTIVE_SIZE = 64
const m___GCC_DESTRUCTIVE_SIZE = 64
const m___GCC_HAVE_DWARF2_CFI_ASM = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const m___GCC_IEC_559 = 2
const m___GCC_IEC_559_COMPLEX = 2
const m___GMP_CC = "gcc"
const m___GMP_CFLAGS = "-DNDEBUG -mlong-double-64"
const m___GMP_HAVE_HOST_CPU_FAMILY_power = 0
const m___GMP_HAVE_HOST_CPU_FAMILY_powerpc = 0
const m___GMP_INLINE_PROTOTYPES = 1
const m___GMP_LIBGMP_DLL = 0
const m___GMP_MP_SIZE_T_INT = 0
const m___GNUC_EXECUTION_CHARSET_NAME = "UTF-8"
const m___GNUC_MINOR__ = 2
const m___GNUC_PATCHLEVEL__ = 0
const m___GNUC_STDC_INLINE__ = 1
const m___GNUC_WIDE_EXECUTION_CHARSET_NAME = "UTF-32LE"
const m___GNUC__ = 12
const m___GNU_MP_VERSION = 6
const m___GNU_MP_VERSION_MINOR = 3
const m___GNU_MP_VERSION_PATCHLEVEL = 0
const m___GNU_MP__ = 6
const m___GXX_ABI_VERSION = 1017
const m___HAVE_SPECULATION_SAFE_VALUE = 1
const m___INT16_MAX__ = 0x7fff
const m___INT32_MAX__ = 0x7fffffff
const m___INT32_TYPE__ = "int"
const m___INT64_MAX__ = 0x7fffffffffffffff
const m___INT8_MAX__ = 0x7f
const m___INTMAX_MAX__ = 0x7fffffffffffffff
const m___INTMAX_WIDTH__ = 64
const m___INTPTR_MAX__ = 0x7fffffffffffffff
const m___INTPTR_WIDTH__ = 64
const m___INT_FAST16_MAX__ = 0x7fffffffffffffff
const m___INT_FAST16_WIDTH__ = 64
const m___INT_FAST32_MAX__ = 0x7fffffffffffffff
const m___INT_FAST32_WIDTH__ = 64
const m___INT_FAST64_MAX__ = 0x7fffffffffffffff
const m___INT_FAST64_WIDTH__ = 64
const m___INT_FAST8_MAX__ = 0x7f
const m___INT_FAST8_WIDTH__ = 8
const m___INT_LEAST16_MAX__ = 0x7fff
const m___INT_LEAST16_WIDTH__ = 16
const m___INT_LEAST32_MAX__ = 0x7fffffff
const m___INT_LEAST32_TYPE__ = "int"
const m___INT_LEAST32_WIDTH__ = 32
const m___INT_LEAST64_MAX__ = 0x7fffffffffffffff
const m___INT_LEAST64_WIDTH__ = 64
const m___INT_LEAST8_MAX__ = 0x7f
const m___INT_LEAST8_WIDTH__ = 8
const m___INT_MAX__ = 0x7fffffff
const m___INT_WIDTH__ = 32
const m___LDBL_DECIMAL_DIG__ = 17
const m___LDBL_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const m___LDBL_DIG__ = 15
const m___LDBL_EPSILON__ = 2.22044604925031308084726333618164062e-16
const m___LDBL_HAS_DENORM__ = 1
const m___LDBL_HAS_INFINITY__ = 1
const m___LDBL_HAS_QUIET_NAN__ = 1
const m___LDBL_IS_IEC_60559__ = 2
const m___LDBL_MANT_DIG__ = 53
const m___LDBL_MAX_10_EXP__ = 308
const m___LDBL_MAX_EXP__ = 1024
const m___LDBL_MAX__ = 1.79769313486231570814527423731704357e+308
const m___LDBL_MIN__ = 2.22507385850720138309023271733240406e-308
const m___LDBL_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const m___LITTLE_ENDIAN = 1234
const m___LONG_DOUBLE_64__ = 1
const m___LONG_LONG_MAX__ = 0x7fffffffffffffff
const m___LONG_LONG_WIDTH__ = 64
const m___LONG_MAX = 9223372036854775807
const m___LONG_MAX__ = 0x7fffffffffffffff
const m___LONG_WIDTH__ = 64
const m___LP64__ = 1
const m___MMX_WITH_SSE__ = 1
const m___MMX__ = 1
const m___MPC_DECLSPEC = "__GMP_DECLSPEC"
const m___MPFR_DECLSPEC = "__GMP_DECLSPEC"
const m___NO_INLINE__ = 1
const m___ORDER_BIG_ENDIAN__ = 4321
const m___ORDER_LITTLE_ENDIAN__ = 1234
const m___ORDER_PDP_ENDIAN__ = 3412
const m___PIC__ = 2
const m___PIE__ = 2
const m___PRAGMA_REDEFINE_EXTNAME = 1
const m___PRETTY_FUNCTION__ = "__func__"
const m___PTRDIFF_MAX__ = 0x7fffffffffffffff
const m___PTRDIFF_WIDTH__ = 64
const m___SCHAR_MAX__ = 0x7f
const m___SCHAR_WIDTH__ = 8
const m___SEG_FS = 1
const m___SEG_GS = 1
const m___SHRT_MAX__ = 0x7fff
const m___SHRT_WIDTH__ = 16
const m___SIG_ATOMIC_MAX__ = 0x7fffffff
const m___SIG_ATOMIC_TYPE__ = "int"
const m___SIG_ATOMIC_WIDTH__ = 32
const m___SIZEOF_DOUBLE__ = 8
const m___SIZEOF_FLOAT128__ = 16
const m___SIZEOF_FLOAT80__ = 16
const m___SIZEOF_FLOAT__ = 4
const m___SIZEOF_INT128__ = 16
const m___SIZEOF_INT__ = 4
const m___SIZEOF_LONG_DOUBLE__ = 8
const m___SIZEOF_LONG_LONG__ = 8
const m___SIZEOF_LONG__ = 8
const m___SIZEOF_POINTER__ = 8
const m___SIZEOF_PTRDIFF_T__ = 8
const m___SIZEOF_SHORT__ = 2
const m___SIZEOF_SIZE_T__ = 8
const m___SIZEOF_WCHAR_T__ = 4
const m___SIZEOF_WINT_T__ = 4
const m___SIZE_MAX__ = 0xffffffffffffffff
const m___SIZE_WIDTH__ = 64
const m___SSE2_MATH__ = 1
const m___SSE2__ = 1
const m___SSE_MATH__ = 1
const m___SSE__ = 1
const m___STDC_HOSTED__ = 1
const m___STDC_IEC_559_COMPLEX__ = 1
const m___STDC_IEC_559__ = 1
const m___STDC_IEC_60559_BFP__ = 201404
const m___STDC_IEC_60559_COMPLEX__ = 201404
const m___STDC_ISO_10646__ = 201706
const m___STDC_UTF_16__ = 1
const m___STDC_UTF_32__ = 1
const m___STDC_VERSION__ = 201710
const m___STDC__ = 1
const m___UINT16_MAX__ = 0xffff
const m___UINT32_MAX__ = 0xffffffff
const m___UINT64_MAX__ = 0xffffffffffffffff
const m___UINT8_MAX__ = 0xff
const m___UINTMAX_MAX__ = 0xffffffffffffffff
const m___UINTPTR_MAX__ = 0xffffffffffffffff
const m___UINT_FAST16_MAX__ = 0xffffffffffffffff
const m___UINT_FAST32_MAX__ = 0xffffffffffffffff
const m___UINT_FAST64_MAX__ = 0xffffffffffffffff
const m___UINT_FAST8_MAX__ = 0xff
const m___UINT_LEAST16_MAX__ = 0xffff
const m___UINT_LEAST32_MAX__ = 0xffffffff
const m___UINT_LEAST64_MAX__ = 0xffffffffffffffff
const m___UINT_LEAST8_MAX__ = 0xff
const m___USE_TIME_BITS64 = 1
const m___VERSION__ = "12.2.0"
const m___WCHAR_MAX__ = 0x7fffffff
const m___WCHAR_TYPE__ = "int"
const m___WCHAR_WIDTH__ = 32
const m___WINT_MAX__ = 0xffffffff
const m___WINT_MIN__ = 0
const m___WINT_WIDTH__ = 32
const m___amd64 = 1
const m___amd64__ = 1
const m___code_model_small__ = 1
const m___gnu_linux__ = 1
const m___inline = "inline"
const m___k8 = 1
const m___k8__ = 1
const m___linux = 1
const m___linux__ = 1
const m___pic__ = 2
const m___pie__ = 2
const m___restrict = "restrict"
const m___restrict_arr = "restrict"
const m___unix = 1
const m___unix__ = 1
const m___x86_64 = 1
const m___x86_64__ = 1
const m__mpq_cmp_si = "__gmpq_cmp_si"
const m__mpq_cmp_ui = "__gmpq_cmp_ui"
const m__mpz_cmp_si = "__gmpz_cmp_si"
const m__mpz_cmp_ui = "__gmpz_cmp_ui"
const m__mpz_realloc = "__gmpz_realloc"
const m_alloca = "__builtin_alloca"
const m_gmp_asprintf = "__gmp_asprintf"
const m_gmp_errno = "__gmp_errno"
const m_gmp_fprintf = "__gmp_fprintf"
const m_gmp_fscanf = "__gmp_fscanf"
const m_gmp_obstack_printf = "__gmp_obstack_printf"
const m_gmp_obstack_vprintf = "__gmp_obstack_vprintf"
const m_gmp_printf = "__gmp_printf"
const m_gmp_randclear = "__gmp_randclear"
const m_gmp_randinit = "__gmp_randinit"
const m_gmp_randinit_default = "__gmp_randinit_default"
const m_gmp_randinit_lc_2exp = "__gmp_randinit_lc_2exp"
const m_gmp_randinit_lc_2exp_size = "__gmp_randinit_lc_2exp_size"
const m_gmp_randinit_mt = "__gmp_randinit_mt"
const m_gmp_randinit_set = "__gmp_randinit_set"
const m_gmp_randseed = "__gmp_randseed"
const m_gmp_randseed_ui = "__gmp_randseed_ui"
const m_gmp_scanf = "__gmp_scanf"
const m_gmp_snprintf = "__gmp_snprintf"
const m_gmp_sprintf = "__gmp_sprintf"
const m_gmp_sscanf = "__gmp_sscanf"
const m_gmp_urandomb_ui = "__gmp_urandomb_ui"
const m_gmp_urandomm_ui = "__gmp_urandomm_ui"
const m_gmp_vasprintf = "__gmp_vasprintf"
const m_gmp_version = "__gmp_version"
const m_gmp_vfprintf = "__gmp_vfprintf"
const m_gmp_vfscanf = "__gmp_vfscanf"
const m_gmp_vprintf = "__gmp_vprintf"
const m_gmp_vscanf = "__gmp_vscanf"
const m_gmp_vsnprintf = "__gmp_vsnprintf"
const m_gmp_vsprintf = "__gmp_vsprintf"
const m_gmp_vsscanf = "__gmp_vsscanf"
const m_linux = 1
const m_mp_bits_per_limb = "__gmp_bits_per_limb"
const m_mp_get_memory_functions = "__gmp_get_memory_functions"
const m_mp_prec_t = "mpfr_prec_t"
const m_mp_rnd_t = "mpfr_rnd_t"
const m_mp_set_memory_functions = "__gmp_set_memory_functions"
const m_mpf_abs = "__gmpf_abs"
const m_mpf_add = "__gmpf_add"
const m_mpf_add_ui = "__gmpf_add_ui"
const m_mpf_ceil = "__gmpf_ceil"
const m_mpf_clear = "__gmpf_clear"
const m_mpf_clears = "__gmpf_clears"
const m_mpf_cmp = "__gmpf_cmp"
const m_mpf_cmp_d = "__gmpf_cmp_d"
const m_mpf_cmp_si = "__gmpf_cmp_si"
const m_mpf_cmp_ui = "__gmpf_cmp_ui"
const m_mpf_cmp_z = "__gmpf_cmp_z"
const m_mpf_div = "__gmpf_div"
const m_mpf_div_2exp = "__gmpf_div_2exp"
const m_mpf_div_ui = "__gmpf_div_ui"
const m_mpf_dump = "__gmpf_dump"
const m_mpf_eq = "__gmpf_eq"
const m_mpf_fits_sint_p = "__gmpf_fits_sint_p"
const m_mpf_fits_slong_p = "__gmpf_fits_slong_p"
const m_mpf_fits_sshort_p = "__gmpf_fits_sshort_p"
const m_mpf_fits_uint_p = "__gmpf_fits_uint_p"
const m_mpf_fits_ulong_p = "__gmpf_fits_ulong_p"
const m_mpf_fits_ushort_p = "__gmpf_fits_ushort_p"
const m_mpf_floor = "__gmpf_floor"
const m_mpf_get_d = "__gmpf_get_d"
const m_mpf_get_d_2exp = "__gmpf_get_d_2exp"
const m_mpf_get_default_prec = "__gmpf_get_default_prec"
const m_mpf_get_prec = "__gmpf_get_prec"
const m_mpf_get_si = "__gmpf_get_si"
const m_mpf_get_str = "__gmpf_get_str"
const m_mpf_get_ui = "__gmpf_get_ui"
const m_mpf_init = "__gmpf_init"
const m_mpf_init2 = "__gmpf_init2"
const m_mpf_init_set = "__gmpf_init_set"
const m_mpf_init_set_d = "__gmpf_init_set_d"
const m_mpf_init_set_si = "__gmpf_init_set_si"
const m_mpf_init_set_str = "__gmpf_init_set_str"
const m_mpf_init_set_ui = "__gmpf_init_set_ui"
const m_mpf_inits = "__gmpf_inits"
const m_mpf_inp_str = "__gmpf_inp_str"
const m_mpf_integer_p = "__gmpf_integer_p"
const m_mpf_mul = "__gmpf_mul"
const m_mpf_mul_2exp = "__gmpf_mul_2exp"
const m_mpf_mul_ui = "__gmpf_mul_ui"
const m_mpf_neg = "__gmpf_neg"
const m_mpf_out_str = "__gmpf_out_str"
const m_mpf_pow_ui = "__gmpf_pow_ui"
const m_mpf_random2 = "__gmpf_random2"
const m_mpf_reldiff = "__gmpf_reldiff"
const m_mpf_set = "__gmpf_set"
const m_mpf_set_d = "__gmpf_set_d"
const m_mpf_set_default_prec = "__gmpf_set_default_prec"
const m_mpf_set_prec = "__gmpf_set_prec"
const m_mpf_set_prec_raw = "__gmpf_set_prec_raw"
const m_mpf_set_q = "__gmpf_set_q"
const m_mpf_set_si = "__gmpf_set_si"
const m_mpf_set_str = "__gmpf_set_str"
const m_mpf_set_ui = "__gmpf_set_ui"
const m_mpf_set_z = "__gmpf_set_z"
const m_mpf_size = "__gmpf_size"
const m_mpf_sqrt = "__gmpf_sqrt"
const m_mpf_sqrt_ui = "__gmpf_sqrt_ui"
const m_mpf_sub = "__gmpf_sub"
const m_mpf_sub_ui = "__gmpf_sub_ui"
const m_mpf_swap = "__gmpf_swap"
const m_mpf_trunc = "__gmpf_trunc"
const m_mpf_ui_div = "__gmpf_ui_div"
const m_mpf_ui_sub = "__gmpf_ui_sub"
const m_mpf_urandomb = "__gmpf_urandomb"
const m_mpfr_cmp_abs = "mpfr_cmpabs"
const m_mpfr_custom_get_mantissa = "mpfr_custom_get_significand"
const m_mpfr_get_sj = "__gmpfr_mpfr_get_sj"
const m_mpfr_get_uj = "__gmpfr_mpfr_get_uj"
const m_mpfr_get_z_exp = "mpfr_get_z_2exp"
const m_mpfr_pow_sj = "__gmpfr_mpfr_pow_sj"
const m_mpfr_pow_uj = "__gmpfr_mpfr_pow_uj"
const m_mpfr_pown = "mpfr_pow_sj"
const m_mpfr_set_fr = "mpfr_set"
const m_mpfr_set_sj = "__gmpfr_set_sj"
const m_mpfr_set_sj_2exp = "__gmpfr_set_sj_2exp"
const m_mpfr_set_uj = "__gmpfr_set_uj"
const m_mpfr_set_uj_2exp = "__gmpfr_set_uj_2exp"
const m_mpq_abs = "__gmpq_abs"
const m_mpq_add = "__gmpq_add"
const m_mpq_canonicalize = "__gmpq_canonicalize"
const m_mpq_clear = "__gmpq_clear"
const m_mpq_clears = "__gmpq_clears"
const m_mpq_cmp = "__gmpq_cmp"
const m_mpq_cmp_z = "__gmpq_cmp_z"
const m_mpq_div = "__gmpq_div"
const m_mpq_div_2exp = "__gmpq_div_2exp"
const m_mpq_equal = "__gmpq_equal"
const m_mpq_get_d = "__gmpq_get_d"
const m_mpq_get_den = "__gmpq_get_den"
const m_mpq_get_num = "__gmpq_get_num"
const m_mpq_get_str = "__gmpq_get_str"
const m_mpq_init = "__gmpq_init"
const m_mpq_inits = "__gmpq_inits"
const m_mpq_inp_str = "__gmpq_inp_str"
const m_mpq_inv = "__gmpq_inv"
const m_mpq_mul = "__gmpq_mul"
const m_mpq_mul_2exp = "__gmpq_mul_2exp"
const m_mpq_neg = "__gmpq_neg"
const m_mpq_out_str = "__gmpq_out_str"
const m_mpq_set = "__gmpq_set"
const m_mpq_set_d = "__gmpq_set_d"
const m_mpq_set_den = "__gmpq_set_den"
const m_mpq_set_f = "__gmpq_set_f"
const m_mpq_set_num = "__gmpq_set_num"
const m_mpq_set_si = "__gmpq_set_si"
const m_mpq_set_str = "__gmpq_set_str"
const m_mpq_set_ui = "__gmpq_set_ui"
const m_mpq_set_z = "__gmpq_set_z"
const m_mpq_sub = "__gmpq_sub"
const m_mpq_swap = "__gmpq_swap"
const m_mpz_2fac_ui = "__gmpz_2fac_ui"
const m_mpz_abs = "__gmpz_abs"
const m_mpz_add = "__gmpz_add"
const m_mpz_add_ui = "__gmpz_add_ui"
const m_mpz_addmul = "__gmpz_addmul"
const m_mpz_addmul_ui = "__gmpz_addmul_ui"
const m_mpz_and = "__gmpz_and"
const m_mpz_array_init = "__gmpz_array_init"
const m_mpz_bin_ui = "__gmpz_bin_ui"
const m_mpz_bin_uiui = "__gmpz_bin_uiui"
const m_mpz_cdiv_q = "__gmpz_cdiv_q"
const m_mpz_cdiv_q_2exp = "__gmpz_cdiv_q_2exp"
const m_mpz_cdiv_q_ui = "__gmpz_cdiv_q_ui"
const m_mpz_cdiv_qr = "__gmpz_cdiv_qr"
const m_mpz_cdiv_qr_ui = "__gmpz_cdiv_qr_ui"
const m_mpz_cdiv_r = "__gmpz_cdiv_r"
const m_mpz_cdiv_r_2exp = "__gmpz_cdiv_r_2exp"
const m_mpz_cdiv_r_ui = "__gmpz_cdiv_r_ui"
const m_mpz_cdiv_ui = "__gmpz_cdiv_ui"
const m_mpz_clear = "__gmpz_clear"
const m_mpz_clears = "__gmpz_clears"
const m_mpz_clrbit = "__gmpz_clrbit"
const m_mpz_cmp = "__gmpz_cmp"
const m_mpz_cmp_d = "__gmpz_cmp_d"
const m_mpz_cmpabs = "__gmpz_cmpabs"
const m_mpz_cmpabs_d = "__gmpz_cmpabs_d"
const m_mpz_cmpabs_ui = "__gmpz_cmpabs_ui"
const m_mpz_com = "__gmpz_com"
const m_mpz_combit = "__gmpz_combit"
const m_mpz_congruent_2exp_p = "__gmpz_congruent_2exp_p"
const m_mpz_congruent_p = "__gmpz_congruent_p"
const m_mpz_congruent_ui_p = "__gmpz_congruent_ui_p"
const m_mpz_div = "mpz_fdiv_q"
const m_mpz_div_2exp = "mpz_fdiv_q_2exp"
const m_mpz_div_ui = "mpz_fdiv_q_ui"
const m_mpz_divexact = "__gmpz_divexact"
const m_mpz_divexact_ui = "__gmpz_divexact_ui"
const m_mpz_divisible_2exp_p = "__gmpz_divisible_2exp_p"
const m_mpz_divisible_p = "__gmpz_divisible_p"
const m_mpz_divisible_ui_p = "__gmpz_divisible_ui_p"
const m_mpz_divmod = "mpz_fdiv_qr"
const m_mpz_divmod_ui = "mpz_fdiv_qr_ui"
const m_mpz_dump = "__gmpz_dump"
const m_mpz_eor = "__gmpz_xor"
const m_mpz_export = "__gmpz_export"
const m_mpz_fac_ui = "__gmpz_fac_ui"
const m_mpz_fdiv_q = "__gmpz_fdiv_q"
const m_mpz_fdiv_q_2exp = "__gmpz_fdiv_q_2exp"
const m_mpz_fdiv_q_ui = "__gmpz_fdiv_q_ui"
const m_mpz_fdiv_qr = "__gmpz_fdiv_qr"
const m_mpz_fdiv_qr_ui = "__gmpz_fdiv_qr_ui"
const m_mpz_fdiv_r = "__gmpz_fdiv_r"
const m_mpz_fdiv_r_2exp = "__gmpz_fdiv_r_2exp"
const m_mpz_fdiv_r_ui = "__gmpz_fdiv_r_ui"
const m_mpz_fdiv_ui = "__gmpz_fdiv_ui"
const m_mpz_fib2_ui = "__gmpz_fib2_ui"
const m_mpz_fib_ui = "__gmpz_fib_ui"
const m_mpz_fits_sint_p = "__gmpz_fits_sint_p"
const m_mpz_fits_slong_p = "__gmpz_fits_slong_p"
const m_mpz_fits_sshort_p = "__gmpz_fits_sshort_p"
const m_mpz_fits_uint_p = "__gmpz_fits_uint_p"
const m_mpz_fits_ulong_p = "__gmpz_fits_ulong_p"
const m_mpz_fits_ushort_p = "__gmpz_fits_ushort_p"
const m_mpz_gcd = "__gmpz_gcd"
const m_mpz_gcd_ui = "__gmpz_gcd_ui"
const m_mpz_gcdext = "__gmpz_gcdext"
const m_mpz_get_d = "__gmpz_get_d"
const m_mpz_get_d_2exp = "__gmpz_get_d_2exp"
const m_mpz_get_si = "__gmpz_get_si"
const m_mpz_get_str = "__gmpz_get_str"
const m_mpz_get_ui = "__gmpz_get_ui"
const m_mpz_getlimbn = "__gmpz_getlimbn"
const m_mpz_hamdist = "__gmpz_hamdist"
const m_mpz_import = "__gmpz_import"
const m_mpz_init = "__gmpz_init"
const m_mpz_init2 = "__gmpz_init2"
const m_mpz_init_set = "__gmpz_init_set"
const m_mpz_init_set_d = "__gmpz_init_set_d"
const m_mpz_init_set_si = "__gmpz_init_set_si"
const m_mpz_init_set_str = "__gmpz_init_set_str"
const m_mpz_init_set_ui = "__gmpz_init_set_ui"
const m_mpz_inits = "__gmpz_inits"
const m_mpz_inp_raw = "__gmpz_inp_raw"
const m_mpz_inp_str = "__gmpz_inp_str"
const m_mpz_invert = "__gmpz_invert"
const m_mpz_ior = "__gmpz_ior"
const m_mpz_jacobi = "__gmpz_jacobi"
const m_mpz_kronecker = "mpz_jacobi"
const m_mpz_kronecker_si = "__gmpz_kronecker_si"
const m_mpz_kronecker_ui = "__gmpz_kronecker_ui"
const m_mpz_lcm = "__gmpz_lcm"
const m_mpz_lcm_ui = "__gmpz_lcm_ui"
const m_mpz_legendre = "mpz_jacobi"
const m_mpz_limbs_finish = "__gmpz_limbs_finish"
const m_mpz_limbs_modify = "__gmpz_limbs_modify"
const m_mpz_limbs_read = "__gmpz_limbs_read"
const m_mpz_limbs_write = "__gmpz_limbs_write"
const m_mpz_lucnum2_ui = "__gmpz_lucnum2_ui"
const m_mpz_lucnum_ui = "__gmpz_lucnum_ui"
const m_mpz_mdiv = "mpz_fdiv_q"
const m_mpz_mdiv_ui = "mpz_fdiv_q_ui"
const m_mpz_mdivmod = "mpz_fdiv_qr"
const m_mpz_mfac_uiui = "__gmpz_mfac_uiui"
const m_mpz_millerrabin = "__gmpz_millerrabin"
const m_mpz_mmod = "mpz_fdiv_r"
const m_mpz_mod = "__gmpz_mod"
const m_mpz_mod_2exp = "mpz_fdiv_r_2exp"
const m_mpz_mod_ui = "mpz_fdiv_r_ui"
const m_mpz_mul = "__gmpz_mul"
const m_mpz_mul_2exp = "__gmpz_mul_2exp"
const m_mpz_mul_si = "__gmpz_mul_si"
const m_mpz_mul_ui = "__gmpz_mul_ui"
const m_mpz_neg = "__gmpz_neg"
const m_mpz_nextprime = "__gmpz_nextprime"
const m_mpz_out_raw = "__gmpz_out_raw"
const m_mpz_out_str = "__gmpz_out_str"
const m_mpz_perfect_power_p = "__gmpz_perfect_power_p"
const m_mpz_perfect_square_p = "__gmpz_perfect_square_p"
const m_mpz_popcount = "__gmpz_popcount"
const m_mpz_pow_ui = "__gmpz_pow_ui"
const m_mpz_powm = "__gmpz_powm"
const m_mpz_powm_sec = "__gmpz_powm_sec"
const m_mpz_powm_ui = "__gmpz_powm_ui"
const m_mpz_prevprime = "__gmpz_prevprime"
const m_mpz_primorial_ui = "__gmpz_primorial_ui"
const m_mpz_probab_prime_p = "__gmpz_probab_prime_p"
const m_mpz_random = "__gmpz_random"
const m_mpz_random2 = "__gmpz_random2"
const m_mpz_realloc = "__gmpz_realloc"
const m_mpz_realloc2 = "__gmpz_realloc2"
const m_mpz_remove = "__gmpz_remove"
const m_mpz_roinit_n = "__gmpz_roinit_n"
const m_mpz_root = "__gmpz_root"
const m_mpz_rootrem = "__gmpz_rootrem"
const m_mpz_rrandomb = "__gmpz_rrandomb"
const m_mpz_scan0 = "__gmpz_scan0"
const m_mpz_scan1 = "__gmpz_scan1"
const m_mpz_set = "__gmpz_set"
const m_mpz_set_d = "__gmpz_set_d"
const m_mpz_set_f = "__gmpz_set_f"
const m_mpz_set_fr = "mpfr_get_z"
const m_mpz_set_q = "__gmpz_set_q"
const m_mpz_set_si = "__gmpz_set_si"
const m_mpz_set_str = "__gmpz_set_str"
const m_mpz_set_ui = "__gmpz_set_ui"
const m_mpz_setbit = "__gmpz_setbit"
const m_mpz_si_kronecker = "__gmpz_si_kronecker"
const m_mpz_size = "__gmpz_size"
const m_mpz_sizeinbase = "__gmpz_sizeinbase"
const m_mpz_sqrt = "__gmpz_sqrt"
const m_mpz_sqrtrem = "__gmpz_sqrtrem"
const m_mpz_sub = "__gmpz_sub"
const m_mpz_sub_ui = "__gmpz_sub_ui"
const m_mpz_submul = "__gmpz_submul"
const m_mpz_submul_ui = "__gmpz_submul_ui"
const m_mpz_swap = "__gmpz_swap"
const m_mpz_tdiv_q = "__gmpz_tdiv_q"
const m_mpz_tdiv_q_2exp = "__gmpz_tdiv_q_2exp"
const m_mpz_tdiv_q_ui = "__gmpz_tdiv_q_ui"
const m_mpz_tdiv_qr = "__gmpz_tdiv_qr"
const m_mpz_tdiv_qr_ui = "__gmpz_tdiv_qr_ui"
const m_mpz_tdiv_r = "__gmpz_tdiv_r"
const m_mpz_tdiv_r_2exp = "__gmpz_tdiv_r_2exp"
const m_mpz_tdiv_r_ui = "__gmpz_tdiv_r_ui"
const m_mpz_tdiv_ui = "__gmpz_tdiv_ui"
const m_mpz_tstbit = "__gmpz_tstbit"
const m_mpz_ui_kronecker = "__gmpz_ui_kronecker"
const m_mpz_ui_pow_ui = "__gmpz_ui_pow_ui"
const m_mpz_ui_sub = "__gmpz_ui_sub"
const m_mpz_urandomb = "__gmpz_urandomb"
const m_mpz_urandomm = "__gmpz_urandomm"
const m_mpz_xor = "__gmpz_xor"
const m_unix = 1

type t__builtin_va_list = uintptr

type t__predefined_size_t = uint64

type t__predefined_wchar_t = int32

type t__predefined_ptrdiff_t = int64

type Twchar_t = int32

type Tsize_t = uint64

type Tdiv_t = struct {
	Fquot int32
	Frem  int32
}

type Tldiv_t = struct {
	Fquot int64
	Frem  int64
}

type Tlldiv_t = struct {
	Fquot int64
	Frem  int64
}

type Tuintptr_t = uint64

type Tintptr_t = int64

type Tint8_t = int8

type Tint16_t = int16

type Tint32_t = int32

type Tint64_t = int64

type Tintmax_t = int64

type Tuint8_t = uint8

type Tuint16_t = uint16

type Tuint32_t = uint32

type Tuint64_t = uint64

type Tuintmax_t = uint64

type Tint_fast8_t = int8

type Tint_fast64_t = int64

type Tint_least8_t = int8

type Tint_least16_t = int16

type Tint_least32_t = int32

type Tint_least64_t = int64

type Tuint_fast8_t = uint8

type Tuint_fast64_t = uint64

type Tuint_least8_t = uint8

type Tuint_least16_t = uint16

type Tuint_least32_t = uint32

type Tuint_least64_t = uint64

type Tint_fast16_t = int32

type Tint_fast32_t = int32

type Tuint_fast16_t = uint32

type Tuint_fast32_t = uint32

type Tmax_align_t = struct {
	F__ll int64
	F__ld float64
}

type Tptrdiff_t = int64

type Tmp_limb_t = uint64

type Tmp_limb_signed_t = int64

type Tmp_bitcnt_t = uint64

type t__mpz_struct = struct {
	F_mp_alloc int32
	F_mp_size  int32
	F_mp_d     uintptr
}

type TMP_INT = struct {
	F_mp_alloc int32
	F_mp_size  int32
	F_mp_d     uintptr
}

type Tmpz_t = [1]t__mpz_struct

type Tmp_ptr = uintptr

type Tmp_srcptr = uintptr

type Tmp_size_t = int64

type Tmp_exp_t = int64

type t__mpq_struct = struct {
	F_mp_num t__mpz_struct
	F_mp_den t__mpz_struct
}

type TMP_RAT = struct {
	F_mp_num t__mpz_struct
	F_mp_den t__mpz_struct
}

type Tmpq_t = [1]t__mpq_struct

type t__mpf_struct = struct {
	F_mp_prec int32
	F_mp_size int32
	F_mp_exp  Tmp_exp_t
	F_mp_d    uintptr
}

type Tmpf_t = [1]t__mpf_struct

type Tgmp_randalg_t = int32

const _GMP_RAND_ALG_DEFAULT = 0
const _GMP_RAND_ALG_LC = 0

type t__gmp_randstate_struct = struct {
	F_mp_seed    Tmpz_t
	F_mp_alg     Tgmp_randalg_t
	F_mp_algdata struct {
		F_mp_lc uintptr
	}
}

type Tgmp_randstate_t = [1]t__gmp_randstate_struct

type Tmpz_srcptr = uintptr

type Tmpz_ptr = uintptr

type Tmpf_srcptr = uintptr

type Tmpf_ptr = uintptr

type Tmpq_srcptr = uintptr

type Tmpq_ptr = uintptr

type Tgmp_randstate_ptr = uintptr

type Tgmp_randstate_srcptr = uintptr

const _GMP_ERROR_NONE = 0
const _GMP_ERROR_UNSUPPORTED_ARGUMENT = 1
const _GMP_ERROR_DIVISION_BY_ZERO = 2
const _GMP_ERROR_SQRT_OF_NEGATIVE = 4
const _GMP_ERROR_INVALID_ARGUMENT = 8
const _GMP_ERROR_MPZ_OVERFLOW = 16

type Tmpfr_void = struct{}

type Tmpfr_int = int32

type Tmpfr_uint = uint32

type Tmpfr_long = int64

type Tmpfr_ulong = uint64

type Tmpfr_size_t = uint64

type Tmpfr_flags_t = uint32

type Tmpfr_rnd_t = int32

const _MPFR_RNDN = 0
const _MPFR_RNDZ = 1
const _MPFR_RNDU = 2
const _MPFR_RNDD = 3
const _MPFR_RNDA = 4
const _MPFR_RNDF = 5
const _MPFR_RNDNA = -1

type Tmpfr_prec_t = int64

type Tmpfr_uprec_t = uint64

type Tmpfr_sign_t = int32

type Tmpfr_exp_t = int64

type Tmpfr_uexp_t = uint64

type t__mpfr_struct = struct {
	F_mpfr_prec Tmpfr_prec_t
	F_mpfr_sign Tmpfr_sign_t
	F_mpfr_exp  Tmpfr_exp_t
	F_mpfr_d    uintptr
}

type Tmpfr_t = [1]t__mpfr_struct

type Tmpfr_ptr = uintptr

type Tmpfr_srcptr = uintptr

type Tmpfr_kind_t = int32

const _MPFR_NAN_KIND = 0
const _MPFR_INF_KIND = 1
const _MPFR_ZERO_KIND = 2
const _MPFR_REGULAR_KIND = 3

type Tmpfr_free_cache_t = int32

const _MPFR_FREE_LOCAL_CACHE = 1
const _MPFR_FREE_GLOBAL_CACHE = 2

type Tmpc_rnd_t = int32

type t__mpc_struct = struct {
	Fre Tmpfr_t
	Fim Tmpfr_t
}

type Tmpc_t = [1]t__mpc_struct

type Tmpc_ptr = uintptr

type Tmpc_srcptr = uintptr

type t__mpcr_struct = struct {
	Fmant Tint64_t
	Fexp  Tint64_t
}

type Tmpcr_t = [1]t__mpcr_struct

type Tmpcr_ptr = uintptr

type Tmpcr_srcptr = uintptr

type t__mpcb_struct = struct {
	Fc Tmpc_t
	Fr Tmpcr_t
}

type Tmpcb_t = [1]t__mpcb_struct

type Tmpcb_ptr = uintptr

type Tmpcb_srcptr = uintptr

// C documentation
//
//	/* the rounding mode is mpfr_rnd_t here since we return an mpfr number */
func Xmpc_abs(tls *libc.TLS, a Tmpfr_ptr, b Tmpc_srcptr, rnd Tmpfr_rnd_t) (r int32) {
	return libmpfr.Xmpfr_hypot(tls, a, b, b+32, rnd)
}

const m_BUFSIZ = 1024
const m_FILENAME_MAX = 4096
const m_FOPEN_MAX = 1000
const m_L_ctermid = 20
const m_L_cuserid = 20
const m_L_tmpnam = 20
const m_P_tmpdir = "/tmp"
const m_TMP_MAX = 10000
const m__GMP_H_HAVE_FILE = 1
const m__IOFBF = 0
const m__IOLBF = 1
const m__IONBF = 2
const m__MPFR_H_HAVE_FILE = 1
const m_mpfr_fpif_export = "__gmpfr_fpif_export"
const m_mpfr_fpif_import = "__gmpfr_fpif_import"
const m_mpfr_fprintf = "__gmpfr_fprintf"
const m_mpfr_inp_str = "__gmpfr_inp_str"
const m_mpfr_out_str = "__gmpfr_out_str"

type Tssize_t = int64

type Toff_t = int64

type Tva_list = uintptr

type t__isoc_va_list = uintptr

type Tfpos_t = struct {
	F__lldata [0]int64
	F__align  [0]float64
	F__opaque [16]int8
}

type T_G_fpos64_t = Tfpos_t

type Tcookie_io_functions_t = struct {
	Fread   uintptr
	Fwrite  uintptr
	Fseek   uintptr
	Fclose1 uintptr
}

type T_IO_cookie_io_functions_t = Tcookie_io_functions_t

func Xmpc_acos(tls *libc.TLS, rop Tmpc_ptr, op Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(160)
	defer tls.Free(160)
	var _p, _p1, _p2 Tmpfr_ptr
	var e1, e2, saved_emax, saved_emin Tmpfr_exp_t
	var inex, inex_im, inex_re, loop, ok, s_im, v1, v10, v11, v12, v13, v14, v15, v16, v17, v18, v19, v2, v20, v21, v22, v23, v24, v25, v26, v27, v28, v29, v3, v31, v32, v35, v36, v39, v4, v40, v41, v42, v43, v44, v6, v8, v9 int32
	var p, p_im, p_re, prec Tmpfr_prec_t
	var rnd1 Tmpc_rnd_t
	var rnd_im Tmpfr_rnd_t
	var v34, v37, v38 int64
	var _ /* minus_op_re at bp+128 */ Tmpfr_t
	var _ /* pi_over_2 at bp+64 */ Tmpfr_t
	var _ /* x at bp+96 */ Tmpfr_t
	var _ /* z1 at bp+0 */ Tmpc_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = _p, _p1, _p2, e1, e2, inex, inex_im, inex_re, loop, ok, p, p_im, p_re, prec, rnd1, rnd_im, s_im, saved_emax, saved_emin, v1, v10, v11, v12, v13, v14, v15, v16, v17, v18, v19, v2, v20, v21, v22, v23, v24, v25, v26, v27, v28, v29, v3, v31, v32, v34, v35, v36, v37, v38, v39, v4, v40, v41, v42, v43, v44, v6, v8, v9
	loop = 0
	inex_re = 0
	inex_im = 0
	/* special values */
	if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			if (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_sign < 0 {
				v1 = +libc.Int32FromInt32(1)
			} else {
				v1 = -int32(1)
			}
			libmpfr.Xmpfr_set_inf(tls, rop+32, v1)
			libmpfr.Xmpfr_set_nan(tls, rop)
		} else {
			if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				inex_re = Xset_pi_over_2(tls, rop, +libc.Int32FromInt32(1), rnd&libc.Int32FromInt32(0x0F))
				libmpfr.Xmpfr_set_nan(tls, rop+32)
			} else {
				libmpfr.Xmpfr_set_nan(tls, rop)
				libmpfr.Xmpfr_set_nan(tls, rop+32)
			}
		}
		if inex_re < 0 {
			v2 = int32(2)
		} else {
			if inex_re == 0 {
				v3 = 0
			} else {
				v3 = int32(1)
			}
			v2 = v3
		}
		return v2 | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2)
	}
	if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			if (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
					if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
						libmpfr.Xmpfr_set_erangeflag(tls)
					}
					v4 = libc.Int32FromInt32(0)
				} else {
					v4 = (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_sign
				}
				if v4 > 0 {
					inex_re = Xset_pi_over_2(tls, rop, +libc.Int32FromInt32(1), rnd&libc.Int32FromInt32(0x0F))
					libmpfr.Xmpfr_div_2ui(tls, rop, rop, uint64(1), int32(_MPFR_RNDN))
				} else {
					libmpfr.Xmpfr_init(tls, bp+96)
					prec = (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec
					p = prec
					for cond := true; cond; cond = ok == 0 {
						p += Xmpc_ceil_log2(tls, p)
						libmpfr.Xmpfr_set_prec(tls, bp+96, p)
						libmpfr.Xmpfr_const_pi(tls, bp+96, int32(_MPFR_RNDD))
						_ = libmpfr.Xmpfr_mul_ui(tls, bp+96, bp+96, libc.Uint64FromInt32(libc.Int32FromInt32(3)), int32(_MPFR_RNDD))
						ok = libmpfr.Xmpfr_can_round(tls, bp+96, p-int64(1), int32(_MPFR_RNDD), rnd&libc.Int32FromInt32(0x0F), prec+libc.BoolInt64(rnd&libc.Int32FromInt32(0x0F) == int32(_MPFR_RNDN)))
					}
					inex_re = libmpfr.Xmpfr_div_2ui(tls, rop, bp+96, uint64(2), rnd&libc.Int32FromInt32(0x0F))
					libmpfr.Xmpfr_clear(tls, bp+96)
				}
			} else {
				if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
					if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
						libmpfr.Xmpfr_set_erangeflag(tls)
					}
					v6 = libc.Int32FromInt32(0)
				} else {
					v6 = (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_sign
				}
				if v6 > 0 {
					{
						_p = rop
						(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign = int32(1)
						(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
						_ = int32(_MPFR_RNDN)
						v8 = 0
					}
					_ = v8
				} else {
					inex_re = libmpfr.Xmpfr_const_pi(tls, rop, rnd&libc.Int32FromInt32(0x0F))
				}
			}
		} else {
			inex_re = Xset_pi_over_2(tls, rop, +libc.Int32FromInt32(1), rnd&libc.Int32FromInt32(0x0F))
		}
		if (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_sign < 0 {
			v9 = +libc.Int32FromInt32(1)
		} else {
			v9 = -int32(1)
		}
		libmpfr.Xmpfr_set_inf(tls, rop+32, v9)
		if inex_re < 0 {
			v10 = int32(2)
		} else {
			if inex_re == 0 {
				v11 = 0
			} else {
				v11 = int32(1)
			}
			v10 = v11
		}
		return v10 | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2)
	}
	/* pure real argument */
	if (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		s_im = libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_sign < 0)
		if libmpfr.Xmpfr_cmp_ui_2exp(tls, op, libc.Uint64FromInt32(libc.Int32FromInt32(1)), 0) > 0 {
			if s_im != 0 {
				inex_im = libmpfr.Xmpfr_acosh(tls, rop+32, op, rnd>>libc.Int32FromInt32(4))
			} else {
				if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDU) {
					v12 = int32(_MPFR_RNDD)
				} else {
					if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDD) {
						v13 = int32(_MPFR_RNDU)
					} else {
						v13 = rnd >> libc.Int32FromInt32(4)
					}
					v12 = v13
				}
				inex_im = -libmpfr.Xmpfr_acosh(tls, rop+32, op, v12)
			}
			{
				_p1 = rop
				(*t__mpfr_struct)(unsafe.Pointer(_p1)).F_mpfr_sign = int32(1)
				(*t__mpfr_struct)(unsafe.Pointer(_p1)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
				_ = int32(_MPFR_RNDN)
				v14 = 0
			}
			_ = v14
		} else {
			if libmpfr.Xmpfr_cmp_si_2exp(tls, op, int64(-libc.Int32FromInt32(1)), 0) < 0 {
				(*(*Tmpfr_t)(unsafe.Pointer(bp + 128)))[0] = *(*t__mpfr_struct)(unsafe.Pointer(op))
				libmpfr.Xmpfr_neg(tls, bp+128, bp+128, int32(_MPFR_RNDN))
				if s_im != 0 {
					inex_im = libmpfr.Xmpfr_acosh(tls, rop+32, bp+128, rnd>>libc.Int32FromInt32(4))
				} else {
					if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDU) {
						v15 = int32(_MPFR_RNDD)
					} else {
						if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDD) {
							v16 = int32(_MPFR_RNDU)
						} else {
							v16 = rnd >> libc.Int32FromInt32(4)
						}
						v15 = v16
					}
					inex_im = -libmpfr.Xmpfr_acosh(tls, rop+32, bp+128, v15)
				}
				inex_re = libmpfr.Xmpfr_const_pi(tls, rop, rnd&libc.Int32FromInt32(0x0F))
			} else {
				inex_re = libmpfr.Xmpfr_acos(tls, rop, op, rnd&libc.Int32FromInt32(0x0F))
				{
					_p2 = rop + 32
					(*t__mpfr_struct)(unsafe.Pointer(_p2)).F_mpfr_sign = int32(1)
					(*t__mpfr_struct)(unsafe.Pointer(_p2)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
					_ = rnd >> libc.Int32FromInt32(4)
					v17 = 0
				}
				_ = v17
			}
		}
		if !(s_im != 0) {
			Xmpc_conj(tls, rop, rop, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
		}
		if inex_re < 0 {
			v18 = int32(2)
		} else {
			if inex_re == 0 {
				v19 = 0
			} else {
				v19 = int32(1)
			}
			v18 = v19
		}
		if inex_im < 0 {
			v20 = int32(2)
		} else {
			if inex_im == 0 {
				v21 = 0
			} else {
				v21 = int32(1)
			}
			v20 = v21
		}
		return v18 | v20<<int32(2)
	}
	/* pure imaginary argument */
	if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		inex_re = Xset_pi_over_2(tls, rop, +libc.Int32FromInt32(1), rnd&libc.Int32FromInt32(0x0F))
		if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDU) {
			v22 = int32(_MPFR_RNDD)
		} else {
			if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDD) {
				v23 = int32(_MPFR_RNDU)
			} else {
				v23 = rnd >> libc.Int32FromInt32(4)
			}
			v22 = v23
		}
		inex_im = -libmpfr.Xmpfr_asinh(tls, rop+32, op+32, v22)
		Xmpc_conj(tls, rop, rop, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
		if inex_re < 0 {
			v24 = int32(2)
		} else {
			if inex_re == 0 {
				v25 = 0
			} else {
				v25 = int32(1)
			}
			v24 = v25
		}
		if inex_im < 0 {
			v26 = int32(2)
		} else {
			if inex_im == 0 {
				v27 = 0
			} else {
				v27 = int32(1)
			}
			v26 = v27
		}
		return v24 | v26<<int32(2)
	}
	saved_emin = libmpfr.Xmpfr_get_emin(tls)
	saved_emax = libmpfr.Xmpfr_get_emax(tls)
	libmpfr.Xmpfr_set_emin(tls, libmpfr.Xmpfr_get_emin_min(tls))
	libmpfr.Xmpfr_set_emax(tls, libmpfr.Xmpfr_get_emax_max(tls))
	/* regular complex argument: acos(z) = Pi/2 - asin(z) */
	p_re = (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec
	p_im = (*t__mpfr_struct)(unsafe.Pointer(rop + 32)).F_mpfr_prec
	p = p_re
	Xmpc_init3(tls, bp, p, p_im) /* we round directly the imaginary part to p_im,
	   with rounding mode opposite to rnd_im */
	rnd_im = rnd >> libc.Int32FromInt32(4)
	/* the imaginary part of asin(z) has the same sign as Im(z), thus if
	   Im(z) > 0 and rnd_im = RNDZ, we want to round the Im(asin(z)) to -Inf
	   so that -Im(asin(z)) is rounded to zero */
	if rnd_im == int32(_MPFR_RNDZ) {
		if (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			if (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				libmpfr.Xmpfr_set_erangeflag(tls)
			}
			v29 = libc.Int32FromInt32(0)
		} else {
			v29 = (*t__mpfr_struct)(unsafe.Pointer(op + 32)).F_mpfr_sign
		}
		if v29 > 0 {
			v28 = int32(_MPFR_RNDD)
		} else {
			v28 = int32(_MPFR_RNDU)
		}
		rnd_im = v28
	} else {
		if rnd_im == int32(_MPFR_RNDU) {
			v31 = int32(_MPFR_RNDD)
		} else {
			if rnd_im == int32(_MPFR_RNDD) {
				v32 = int32(_MPFR_RNDU)
			} else {
				v32 = rnd_im
			}
			v31 = v32
		}
		rnd_im = v31
	} /* both RNDZ and RNDA map to themselves for -asin(z) */
	rnd1 = int32(_MPFR_RNDN) + rnd_im<<libc.Int32FromInt32(4)
	libmpfr.Xmpfr_init2(tls, bp+64, p)
	for {
		loop++
		if loop <= int32(2) {
			v34 = Xmpc_ceil_log2(tls, p) + int64(3)
		} else {
			v34 = p / int64(2)
		}
		p += v34
		libmpfr.Xmpfr_set_prec(tls, bp, p)
		libmpfr.Xmpfr_set_prec(tls, bp+64, p)
		Xset_pi_over_2(tls, bp+64, +libc.Int32FromInt32(1), int32(_MPFR_RNDN))
		e1 = int64(1)                       /* Exp(pi_over_2) */
		inex = Xmpc_asin(tls, bp, op, rnd1) /* asin(z) */
		if inex>>int32(2) == int32(2) {
			v35 = -int32(1)
		} else {
			if inex>>int32(2) == 0 {
				v36 = 0
			} else {
				v36 = int32(1)
			}
			v35 = v36
		}
		inex_im = v35 /* inex_im is in {-1, 0, 1} */
		e2 = (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp
		libmpfr.Xmpfr_sub(tls, bp, bp+64, bp, int32(_MPFR_RNDN))
		if !((*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))) {
			/* the error on x=Re(z1) is bounded by 1/2 ulp(x) + 2^(e1-p-1) +
			   2^(e2-p-1) */
			if e1 >= e2 {
				v37 = e1 + int64(1)
			} else {
				v37 = e2 + int64(1)
			}
			e1 = v37
			/* the error on x is bounded by 1/2 ulp(x) + 2^(e1-p-1) */
			e1 -= (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp
			/* the error on x is bounded by 1/2 ulp(x) [1 + 2^e1] */
			if e1 <= 0 {
				v38 = 0
			} else {
				v38 = e1
			}
			e1 = v38
			/* the error on x is bounded by 2^e1 * ulp(x) */
			libmpfr.Xmpfr_neg(tls, bp+32, bp+32, int32(_MPFR_RNDN)) /* exact */
			inex_im = -inex_im
			if libmpfr.Xmpfr_can_round(tls, bp, p-e1, int32(_MPFR_RNDN), int32(_MPFR_RNDZ), p_re+libc.BoolInt64(rnd&libc.Int32FromInt32(0x0F) == int32(_MPFR_RNDN))) != 0 {
				break
			}
		}
		goto _33
	_33:
	}
	inex = Xmpc_set(tls, rop, bp, rnd)
	if inex&int32(3) == int32(2) {
		v39 = -int32(1)
	} else {
		if inex&int32(3) == 0 {
			v40 = 0
		} else {
			v40 = int32(1)
		}
		v39 = v40
	}
	inex_re = v39
	Xmpc_clear(tls, bp)
	libmpfr.Xmpfr_clear(tls, bp+64)
	/* restore the exponent range, and check the range of results */
	libmpfr.Xmpfr_set_emin(tls, saved_emin)
	libmpfr.Xmpfr_set_emax(tls, saved_emax)
	inex_re = libmpfr.Xmpfr_check_range(tls, rop, inex_re, rnd&libc.Int32FromInt32(0x0F))
	inex_im = libmpfr.Xmpfr_check_range(tls, rop+32, inex_im, rnd>>libc.Int32FromInt32(4))
	if inex_re < 0 {
		v41 = int32(2)
	} else {
		if inex_re == 0 {
			v42 = 0
		} else {
			v42 = int32(1)
		}
		v41 = v42
	}
	if inex_im < 0 {
		v43 = int32(2)
	} else {
		if inex_im == 0 {
			v44 = 0
		} else {
			v44 = int32(1)
		}
		v43 = v44
	}
	return v41 | v43<<int32(2)
}

func Xmpc_acosh(tls *libc.TLS, rop Tmpc_ptr, op Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var inex, v1, v10, v11, v12, v13, v14, v15, v16, v17, v18, v19, v2, v20, v21, v22, v23, v24, v25, v26, v27, v28, v3, v4, v5, v6, v7, v8, v9 int32
	var tmp Tmpfr_t
	var _ /* a at bp+0 */ Tmpc_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = inex, tmp, v1, v10, v11, v12, v13, v14, v15, v16, v17, v18, v19, v2, v20, v21, v22, v23, v24, v25, v26, v27, v28, v3, v4, v5, v6, v7, v8, v9
	if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) && (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		libmpfr.Xmpfr_set_nan(tls, rop)
		libmpfr.Xmpfr_set_nan(tls, rop+32)
		return 0
	}
	/* Note reversal of precisions due to later multiplication by i or -i */
	Xmpc_init3(tls, bp, (*t__mpfr_struct)(unsafe.Pointer(rop+32)).F_mpfr_prec, (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec)
	if (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_sign < 0 {
		if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDU) {
			v1 = int32(_MPFR_RNDD)
		} else {
			if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDD) {
				v2 = int32(_MPFR_RNDU)
			} else {
				v2 = rnd >> libc.Int32FromInt32(4)
			}
			v1 = v2
		}
		inex = Xmpc_acos(tls, bp, op, v1+rnd&libc.Int32FromInt32(0x0F)<<libc.Int32FromInt32(4))
		/* change a to -i*a, i.e., -y+i*x to x+i*y */
		tmp[0] = *(*t__mpfr_struct)(unsafe.Pointer(bp))
		*(*t__mpfr_struct)(unsafe.Pointer(bp)) = *(*t__mpfr_struct)(unsafe.Pointer(bp + 32))
		*(*t__mpfr_struct)(unsafe.Pointer(bp + 32)) = tmp[0]
		libmpfr.Xmpfr_neg(tls, bp+32, bp+32, int32(_MPFR_RNDN))
		if inex>>int32(2) == int32(2) {
			v4 = -int32(1)
		} else {
			if inex>>int32(2) == 0 {
				v5 = 0
			} else {
				v5 = int32(1)
			}
			v4 = v5
		}
		if v4 < 0 {
			v3 = int32(2)
		} else {
			if inex>>int32(2) == int32(2) {
				v7 = -int32(1)
			} else {
				if inex>>int32(2) == 0 {
					v8 = 0
				} else {
					v8 = int32(1)
				}
				v7 = v8
			}
			if v7 == 0 {
				v6 = 0
			} else {
				v6 = int32(1)
			}
			v3 = v6
		}
		if inex&int32(3) == int32(2) {
			v10 = -int32(1)
		} else {
			if inex&int32(3) == 0 {
				v11 = 0
			} else {
				v11 = int32(1)
			}
			v10 = v11
		}
		if -v10 < 0 {
			v9 = int32(2)
		} else {
			if inex&int32(3) == int32(2) {
				v13 = -int32(1)
			} else {
				if inex&int32(3) == 0 {
					v14 = 0
				} else {
					v14 = int32(1)
				}
				v13 = v14
			}
			if -v13 == 0 {
				v12 = 0
			} else {
				v12 = int32(1)
			}
			v9 = v12
		}
		inex = v3 | v9<<int32(2)
	} else {
		if rnd&libc.Int32FromInt32(0x0F) == int32(_MPFR_RNDU) {
			v15 = int32(_MPFR_RNDD)
		} else {
			if rnd&libc.Int32FromInt32(0x0F) == int32(_MPFR_RNDD) {
				v16 = int32(_MPFR_RNDU)
			} else {
				v16 = rnd & libc.Int32FromInt32(0x0F)
			}
			v15 = v16
		}
		inex = Xmpc_acos(tls, bp, op, rnd>>libc.Int32FromInt32(4)+v15<<libc.Int32FromInt32(4))
		/* change a to i*a, i.e., y-i*x to x+i*y */
		tmp[0] = *(*t__mpfr_struct)(unsafe.Pointer(bp))
		*(*t__mpfr_struct)(unsafe.Pointer(bp)) = *(*t__mpfr_struct)(unsafe.Pointer(bp + 32))
		*(*t__mpfr_struct)(unsafe.Pointer(bp + 32)) = tmp[0]
		libmpfr.Xmpfr_neg(tls, bp, bp, int32(_MPFR_RNDN))
		if inex>>int32(2) == int32(2) {
			v18 = -int32(1)
		} else {
			if inex>>int32(2) == 0 {
				v19 = 0
			} else {
				v19 = int32(1)
			}
			v18 = v19
		}
		if -v18 < 0 {
			v17 = int32(2)
		} else {
			if inex>>int32(2) == int32(2) {
				v21 = -int32(1)
			} else {
				if inex>>int32(2) == 0 {
					v22 = 0
				} else {
					v22 = int32(1)
				}
				v21 = v22
			}
			if -v21 == 0 {
				v20 = 0
			} else {
				v20 = int32(1)
			}
			v17 = v20
		}
		if inex&int32(3) == int32(2) {
			v24 = -int32(1)
		} else {
			if inex&int32(3) == 0 {
				v25 = 0
			} else {
				v25 = int32(1)
			}
			v24 = v25
		}
		if v24 < 0 {
			v23 = int32(2)
		} else {
			if inex&int32(3) == int32(2) {
				v27 = -int32(1)
			} else {
				if inex&int32(3) == 0 {
					v28 = 0
				} else {
					v28 = int32(1)
				}
				v27 = v28
			}
			if v27 == 0 {
				v26 = 0
			} else {
				v26 = int32(1)
			}
			v23 = v26
		}
		inex = v17 | v23<<int32(2)
	}
	Xmpc_set(tls, rop, bp, rnd)
	Xmpc_clear(tls, bp)
	return inex
}

// C documentation
//
//	/* return 0 iff both the real and imaginary parts are exact */
func Xmpc_add(tls *libc.TLS, a Tmpc_ptr, b Tmpc_srcptr, c Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	var inex_im, inex_re, v1, v2, v3, v4 int32
	_, _, _, _, _, _ = inex_im, inex_re, v1, v2, v3, v4
	inex_re = libmpfr.Xmpfr_add(tls, a, b, c, rnd&libc.Int32FromInt32(0x0F))
	inex_im = libmpfr.Xmpfr_add(tls, a+32, b+32, c+32, rnd>>libc.Int32FromInt32(4))
	if inex_re < 0 {
		v1 = int32(2)
	} else {
		if inex_re == 0 {
			v2 = 0
		} else {
			v2 = int32(1)
		}
		v1 = v2
	}
	if inex_im < 0 {
		v3 = int32(2)
	} else {
		if inex_im == 0 {
			v4 = 0
		} else {
			v4 = int32(1)
		}
		v3 = v4
	}
	return v1 | v3<<int32(2)
}

// C documentation
//
//	/* return 0 iff both the real and imaginary parts are exact */
func Xmpc_add_fr(tls *libc.TLS, a Tmpc_ptr, b Tmpc_srcptr, c Tmpfr_srcptr, rnd Tmpc_rnd_t) (r int32) {
	var _p Tmpfr_srcptr
	var inex_im, inex_re, v1, v2, v3, v4, v5 int32
	_, _, _, _, _, _, _, _ = _p, inex_im, inex_re, v1, v2, v3, v4, v5
	inex_re = libmpfr.Xmpfr_add(tls, a, b, c, rnd&libc.Int32FromInt32(0x0F))
	{
		_p = b + 32
		v1 = libmpfr.Xmpfr_set4(tls, a+32, _p, rnd>>libc.Int32FromInt32(4), (*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign)
	}
	inex_im = v1
	if inex_re < 0 {
		v2 = int32(2)
	} else {
		if inex_re == 0 {
			v3 = 0
		} else {
			v3 = int32(1)
		}
		v2 = v3
	}
	if inex_im < 0 {
		v4 = int32(2)
	} else {
		if inex_im == 0 {
			v5 = 0
		} else {
			v5 = int32(1)
		}
		v4 = v5
	}
	return v2 | v4<<int32(2)
}

func Xmpc_add_si(tls *libc.TLS, rop Tmpc_ptr, op1 Tmpc_srcptr, op2 int64, rnd Tmpc_rnd_t) (r int32) {
	var _p Tmpfr_srcptr
	var inex_im, inex_re, v1, v2, v3, v4, v5 int32
	_, _, _, _, _, _, _, _ = _p, inex_im, inex_re, v1, v2, v3, v4, v5
	inex_re = libmpfr.Xmpfr_add_si(tls, rop, op1, op2, rnd&libc.Int32FromInt32(0x0F))
	{
		_p = op1 + 32
		v1 = libmpfr.Xmpfr_set4(tls, rop+32, _p, rnd>>libc.Int32FromInt32(4), (*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign)
	}
	inex_im = v1
	if inex_re < 0 {
		v2 = int32(2)
	} else {
		if inex_re == 0 {
			v3 = 0
		} else {
			v3 = int32(1)
		}
		v2 = v3
	}
	if inex_im < 0 {
		v4 = int32(2)
	} else {
		if inex_im == 0 {
			v5 = 0
		} else {
			v5 = int32(1)
		}
		v4 = v5
	}
	return v2 | v4<<int32(2)
}

// C documentation
//
//	/* return 0 iff both the real and imaginary parts are exact */
func Xmpc_add_ui(tls *libc.TLS, a Tmpc_ptr, b Tmpc_srcptr, c uint64, rnd Tmpc_rnd_t) (r int32) {
	var _p Tmpfr_srcptr
	var inex_im, inex_re, v1, v2, v3, v4, v5 int32
	_, _, _, _, _, _, _, _ = _p, inex_im, inex_re, v1, v2, v3, v4, v5
	inex_re = libmpfr.Xmpfr_add_ui(tls, a, b, c, rnd&libc.Int32FromInt32(0x0F))
	{
		_p = b + 32
		v1 = libmpfr.Xmpfr_set4(tls, a+32, _p, rnd>>libc.Int32FromInt32(4), (*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign)
	}
	inex_im = v1
	if inex_re < 0 {
		v2 = int32(2)
	} else {
		if inex_re == 0 {
			v3 = 0
		} else {
			v3 = int32(1)
		}
		v2 = v3
	}
	if inex_im < 0 {
		v4 = int32(2)
	} else {
		if inex_im == 0 {
			v5 = 0
		} else {
			v5 = int32(1)
		}
		v4 = v5
	}
	return v2 | v4<<int32(2)
}

func _mpc_agm_angle_zero(tls *libc.TLS, rop Tmpc_ptr, a Tmpc_srcptr, b Tmpc_srcptr, rnd Tmpc_rnd_t, cmp int32) (r int32) {
	bp := tls.Alloc(128)
	defer tls.Free(128)
	/* AGM for angle 0 between a and b, but they are neither real nor
	   purely imaginary. cmp is mpc_cmp_abs (a, b). */
	var inex int32
	var prec Tmpfr_prec_t
	var v1 int64
	var _ /* a0 at bp+64 */ Tmpfr_t
	var _ /* agm at bp+0 */ Tmpc_t
	var _ /* b0 at bp+96 */ Tmpfr_t
	_, _, _ = inex, prec, v1
	if (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(rop+32)).F_mpfr_prec {
		v1 = (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec
	} else {
		v1 = (*t__mpfr_struct)(unsafe.Pointer(rop + 32)).F_mpfr_prec
	}
	prec = v1
	Xmpc_init2(tls, bp, int64(2))
	libmpfr.Xmpfr_init2(tls, bp+64, int64(2))
	_ = libmpfr.Xmpfr_set_ui_2exp(tls, bp+64, libc.Uint64FromInt32(libc.Int32FromInt32(1)), 0, int32(_MPFR_RNDN))
	libmpfr.Xmpfr_init2(tls, bp+96, int64(2))
	for cond := true; cond; cond = !(libmpfr.Xmpfr_can_round(tls, bp, prec-int64(3), int32(_MPFR_RNDN), int32(_MPFR_RNDU), (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec+int64(1)) != 0) || !(libmpfr.Xmpfr_can_round(tls, bp+32, prec-int64(3), int32(_MPFR_RNDN), int32(_MPFR_RNDU), (*t__mpfr_struct)(unsafe.Pointer(rop+32)).F_mpfr_prec+int64(1)) != 0) {
		prec += int64(20)
		Xmpc_set_prec(tls, bp, prec)
		libmpfr.Xmpfr_set_prec(tls, bp+96, prec)
		if cmp >= 0 {
			libmpfr.Xmpfr_div(tls, bp+96, b, a, int32(_MPFR_RNDZ))
		} else {
			libmpfr.Xmpfr_div(tls, bp+96, a, b, int32(_MPFR_RNDZ))
		}
		libmpfr.Xmpfr_agm(tls, bp+96, bp+64, bp+96, int32(_MPFR_RNDN))
		if cmp >= 0 {
			Xmpc_mul_fr(tls, bp, a, bp+96, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
		} else {
			Xmpc_mul_fr(tls, bp, b, bp+96, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
		}
	}
	inex = Xmpc_set(tls, rop, bp, rnd)
	Xmpc_clear(tls, bp)
	libmpfr.Xmpfr_clear(tls, bp+64)
	libmpfr.Xmpfr_clear(tls, bp+96)
	return inex
}

func _mpc_agm_general(tls *libc.TLS, rop Tmpc_ptr, a Tmpc_srcptr, b Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(384)
	defer tls.Free(384)
	/* AGM for not extremely special numbers:
	   Finite, non-zero, and a != -b; if the angle is 0, then we are neither
	   in the real nor in the purely imaginary case.
	   We follow the strategy outlined in algorithms.tex. */
	var L, N, exp_diff, k1, kI, kR, prec Tmpfr_prec_t
	var cmp, equal, i, inex, n, okI, okR, v1 int32
	var exp_im_b0, exp_re_a1, exp_re_b0 Tmpfr_exp_t
	var v10, v12, v13, v14, v4, v5, v6, v7, v8, v9 int64
	var v3 bool
	var _ /* a1 at bp+64 */ Tmpc_t
	var _ /* an at bp+128 */ Tmpc_t
	var _ /* anp1 at bp+256 */ Tmpc_t
	var _ /* b0 at bp+0 */ Tmpc_t
	var _ /* bn at bp+192 */ Tmpc_t
	var _ /* bnp1 at bp+320 */ Tmpc_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = L, N, cmp, equal, exp_diff, exp_im_b0, exp_re_a1, exp_re_b0, i, inex, k1, kI, kR, n, okI, okR, prec, v1, v10, v12, v13, v14, v3, v4, v5, v6, v7, v8, v9
	/* Determine whether to compute AGM (1, b0) with b0 = a/b or b0 = b/a. */
	cmp = Xmpc_cmp_abs(tls, a, b)
	/* Compute an approximation k1 of the precision loss in the first
	   iteration. */
	Xmpc_init2(tls, bp, int64(2))
	Xmpc_init2(tls, bp+64, int64(2))
	prec = int64(1)
	for cond := true; cond; cond = exp_re_a1 == -prec {
		prec *= int64(2)
		Xmpc_set_prec(tls, bp, prec)
		Xmpc_set_prec(tls, bp+64, prec)
		if cmp >= 0 {
			Xmpc_div(tls, bp, b, a, int32(_MPFR_RNDZ)+int32(_MPFR_RNDZ)<<libc.Int32FromInt32(4))
		} else {
			Xmpc_div(tls, bp, a, b, int32(_MPFR_RNDZ)+int32(_MPFR_RNDZ)<<libc.Int32FromInt32(4))
		}
		if v3 = (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)); v3 {
			if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
					libmpfr.Xmpfr_set_erangeflag(tls)
				}
				v1 = libc.Int32FromInt32(0)
			} else {
				v1 = (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_sign
			}
		}
		if v3 && v1 > 0 {
			Xmpc_clear(tls, bp)
			Xmpc_clear(tls, bp+64)
			return _mpc_agm_angle_zero(tls, rop, a, b, rnd, cmp)
		}
		Xmpc_add_ui(tls, bp+64, bp, uint64(1), int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
		Xmpc_div_2ui(tls, bp+64, bp+64, uint64(1), int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
		exp_re_a1 = (*t__mpfr_struct)(unsafe.Pointer(bp + 64)).F_mpfr_exp
	}
	exp_re_b0 = (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp
	exp_im_b0 = (*t__mpfr_struct)(unsafe.Pointer(bp + 32)).F_mpfr_exp
	Xmpc_clear(tls, bp+64)
	Xmpc_clear(tls, bp)
	if int64(libc.Int32FromInt32(3)) > int64(-int32(2))*exp_re_a1-int64(2) {
		v4 = int64(libc.Int32FromInt32(3))
	} else {
		v4 = int64(-int32(2))*exp_re_a1 - int64(2)
	}
	k1 = v4
	/* Compute the number n of iterations and the target precision. */
	if (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(rop+32)).F_mpfr_prec {
		v5 = (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec
	} else {
		v5 = (*t__mpfr_struct)(unsafe.Pointer(rop + 32)).F_mpfr_prec
	}
	N = v5 + int64(20)
	/* 20 is an arbitrary safety margin. */
	for cond := true; cond; cond = !(okR != 0) || !(okI != 0) {
		/* With the notation of algorithms.tex, compute 2*L, which is
		   an integer; then correct this when taking the logarithm. */
		if exp_im_b0 <= int64(-int32(1)) {
			if exp_re_b0 <= int64(-int32(1)) {
				if exp_re_b0 > exp_im_b0 {
					v7 = exp_re_b0
				} else {
					v7 = exp_im_b0
				}
				if int64(libc.Int32FromInt32(6)) > -v7+int64(1) {
					v6 = int64(libc.Int32FromInt32(6))
				} else {
					if exp_re_b0 > exp_im_b0 {
						v8 = exp_re_b0
					} else {
						v8 = exp_im_b0
					}
					v6 = -v8 + int64(1)
				}
				L = v6
			} else {
				if exp_re_a1 <= int64(-int32(2)) {
					if exp_re_a1 > exp_im_b0-int64(1) {
						v9 = exp_re_a1
					} else {
						v9 = exp_im_b0 - int64(1)
					}
					L = int64(-int32(2))*v9 + int64(3)
				} else {
					L = int64(6)
				}
			}
		} else {
			L = int64(6)
		}
		if int64(libc.Int32FromInt32(1)) > Xmpc_ceil_log2(tls, L)-int64(1) {
			v10 = int64(libc.Int32FromInt32(1))
		} else {
			v10 = Xmpc_ceil_log2(tls, L) - int64(1)
		}
		L = v10
		n = int32(L + Xmpc_ceil_log2(tls, N+int64(4)) + int64(3))
		prec = N + (int64(n)+k1+int64(7)+int64(1))/int64(2)
		Xmpc_init2(tls, bp+128, prec)
		Xmpc_init2(tls, bp+192, prec)
		Xmpc_init2(tls, bp+256, prec)
		Xmpc_init2(tls, bp+320, prec)
		/* Compute the argument for AGM (1, b0) at the working precision. */
		if cmp >= 0 {
			Xmpc_div(tls, bp+192, b, a, int32(_MPFR_RNDZ)+int32(_MPFR_RNDZ)<<libc.Int32FromInt32(4))
		} else {
			Xmpc_div(tls, bp+192, a, b, int32(_MPFR_RNDZ)+int32(_MPFR_RNDZ)<<libc.Int32FromInt32(4))
		}
		Xmpc_set_ui(tls, bp+128, uint64(1), int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
		equal = 0
		/* In practice, a fixed point of the AGM operation is reached before
		   the last iteration, so we may stop when an==anp1 and bn==bnp1.
		   Also in practice one observes that often one iteration earlier one
		   has an==bn, which is also tested for an early abort strategy. */
		/* Execute the AGM iterations. */
		i = int32(1)
		for {
			if !(!(equal != 0) && i <= n) {
				break
			}
			Xmpc_add(tls, bp+256, bp+128, bp+192, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
			Xmpc_div_2ui(tls, bp+256, bp+256, uint64(1), int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
			Xmpc_mul(tls, bp+320, bp+128, bp+192, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
			Xmpc_sqrt(tls, bp+320, bp+320, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
			equal = libc.BoolInt32(Xmpc_cmp(tls, bp+128, bp+256) == 0 && Xmpc_cmp(tls, bp+192, bp+320) == 0 || Xmpc_cmp(tls, bp+256, bp+320) == 0)
			Xmpc_swap(tls, bp+128, bp+256)
			Xmpc_swap(tls, bp+192, bp+320)
			goto _11
		_11:
			;
			i++
		}
		/* Remultiply. */
		if cmp >= 0 {
			Xmpc_mul(tls, bp+128, bp+128, a, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
		} else {
			Xmpc_mul(tls, bp+128, bp+128, b, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
		}
		exp_diff = (*t__mpfr_struct)(unsafe.Pointer(bp+128+32)).F_mpfr_exp - (*t__mpfr_struct)(unsafe.Pointer(bp+128)).F_mpfr_exp
		if exp_diff+int64(1) > int64(libc.Int32FromInt32(0)) {
			v12 = exp_diff + int64(1)
		} else {
			v12 = int64(libc.Int32FromInt32(0))
		}
		kR = v12
		if -exp_diff+int64(1) > int64(libc.Int32FromInt32(0)) {
			v13 = -exp_diff + int64(1)
		} else {
			v13 = int64(libc.Int32FromInt32(0))
		}
		kI = v13
		/* Use the trick of asking mpfr_can_round whether it can do a directed
		   rounding at precision + 1; then the whole uncertainty interval is
		   contained in the upper or the lower half of the interval between two
		   representable numbers, and mpc_set reveals the inexact value
		   regardless of the rounding direction. */
		okR = libmpfr.Xmpfr_can_round(tls, bp+128, N-kR, int32(_MPFR_RNDN), int32(_MPFR_RNDU), (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec+int64(1))
		okI = libmpfr.Xmpfr_can_round(tls, bp+128+32, N-kI, int32(_MPFR_RNDN), int32(_MPFR_RNDU), (*t__mpfr_struct)(unsafe.Pointer(rop+32)).F_mpfr_prec+int64(1))
		if !(okR != 0) || !(okI != 0) {
			/* Until a counterexample is found, we assume that this happens
			   only once and increase the precision only moderately. */
			if kR > kI {
				v14 = kR
			} else {
				v14 = kI
			}
			N += v14
		}
	}
	inex = Xmpc_set(tls, rop, bp+128, rnd)
	Xmpc_clear(tls, bp+128)
	Xmpc_clear(tls, bp+192)
	Xmpc_clear(tls, bp+256)
	Xmpc_clear(tls, bp+320)
	return inex
}

func Xmpc_agm(tls *libc.TLS, rop Tmpc_ptr, a Tmpc_srcptr, b Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	var _p, _p1 Tmpfr_ptr
	var inex_im, inex_re, v1, v10, v12, v15, v16, v17, v18, v20, v23, v24, v25, v3, v5, v7 int32
	var v14, v22, v9 bool
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = _p, _p1, inex_im, inex_re, v1, v10, v12, v14, v15, v16, v17, v18, v20, v22, v23, v24, v25, v3, v5, v7, v9
	if !(libmpfr.Xmpfr_number_p(tls, a) != 0 && libmpfr.Xmpfr_number_p(tls, a+32) != 0) || !(libmpfr.Xmpfr_number_p(tls, b) != 0 && libmpfr.Xmpfr_number_p(tls, b+32) != 0) {
		libmpfr.Xmpfr_set_nan(tls, rop)
		libmpfr.Xmpfr_set_nan(tls, rop+32)
		return libc.Int32FromInt32(0) | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2)
	} else {
		if (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) && (*t__mpfr_struct)(unsafe.Pointer(a+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(b)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) && (*t__mpfr_struct)(unsafe.Pointer(b+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			return Xmpc_set_ui_ui(tls, rop, uint64(0), uint64(0), int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
		} else {
			if Xmpc_cmp(tls, a, b) == 0 {
				return Xmpc_set(tls, rop, a, rnd)
			} else {
				if (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
					if (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
						libmpfr.Xmpfr_set_erangeflag(tls)
					}
					v1 = libc.Int32FromInt32(0)
				} else {
					v1 = (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_sign
				}
				if (*t__mpfr_struct)(unsafe.Pointer(b)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
					if (*t__mpfr_struct)(unsafe.Pointer(b)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
						libmpfr.Xmpfr_set_erangeflag(tls)
					}
					v3 = libc.Int32FromInt32(0)
				} else {
					v3 = (*t__mpfr_struct)(unsafe.Pointer(b)).F_mpfr_sign
				}
				if v9 = v1 == -v3; v9 {
					if (*t__mpfr_struct)(unsafe.Pointer(a+32)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
						if (*t__mpfr_struct)(unsafe.Pointer(a+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
							libmpfr.Xmpfr_set_erangeflag(tls)
						}
						v5 = libc.Int32FromInt32(0)
					} else {
						v5 = (*t__mpfr_struct)(unsafe.Pointer(a + 32)).F_mpfr_sign
					}
					if (*t__mpfr_struct)(unsafe.Pointer(b+32)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
						if (*t__mpfr_struct)(unsafe.Pointer(b+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
							libmpfr.Xmpfr_set_erangeflag(tls)
						}
						v7 = libc.Int32FromInt32(0)
					} else {
						v7 = (*t__mpfr_struct)(unsafe.Pointer(b + 32)).F_mpfr_sign
					}
				}
				if v9 && v5 == -v7 && libmpfr.Xmpfr_cmpabs(tls, a, b) == 0 && libmpfr.Xmpfr_cmpabs(tls, a+32, b+32) == 0 {
					/* a == -b */
					return Xmpc_set_ui_ui(tls, rop, uint64(0), uint64(0), int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
				} else {
					if v14 = (*t__mpfr_struct)(unsafe.Pointer(a+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) && (*t__mpfr_struct)(unsafe.Pointer(b+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)); v14 {
						if (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
							if (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
								libmpfr.Xmpfr_set_erangeflag(tls)
							}
							v10 = libc.Int32FromInt32(0)
						} else {
							v10 = (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_sign
						}
						if (*t__mpfr_struct)(unsafe.Pointer(b)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
							if (*t__mpfr_struct)(unsafe.Pointer(b)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
								libmpfr.Xmpfr_set_erangeflag(tls)
							}
							v12 = libc.Int32FromInt32(0)
						} else {
							v12 = (*t__mpfr_struct)(unsafe.Pointer(b)).F_mpfr_sign
						}
					}
					if v14 && v10 == v12 {
						/* angle 0, real values */
						inex_re = libmpfr.Xmpfr_agm(tls, rop, a, b, rnd&libc.Int32FromInt32(0x0F))
						{
							_p = rop + 32
							(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign = int32(1)
							(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
							_ = int32(_MPFR_RNDN)
							v15 = 0
						}
						_ = v15
						if inex_re < 0 {
							v16 = int32(2)
						} else {
							if inex_re == 0 {
								v17 = 0
							} else {
								v17 = int32(1)
							}
							v16 = v17
						}
						return v16 | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2)
					} else {
						if v22 = (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) && (*t__mpfr_struct)(unsafe.Pointer(b)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)); v22 {
							if (*t__mpfr_struct)(unsafe.Pointer(a+32)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
								if (*t__mpfr_struct)(unsafe.Pointer(a+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
									libmpfr.Xmpfr_set_erangeflag(tls)
								}
								v18 = libc.Int32FromInt32(0)
							} else {
								v18 = (*t__mpfr_struct)(unsafe.Pointer(a + 32)).F_mpfr_sign
							}
							if (*t__mpfr_struct)(unsafe.Pointer(b+32)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
								if (*t__mpfr_struct)(unsafe.Pointer(b+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
									libmpfr.Xmpfr_set_erangeflag(tls)
								}
								v20 = libc.Int32FromInt32(0)
							} else {
								v20 = (*t__mpfr_struct)(unsafe.Pointer(b + 32)).F_mpfr_sign
							}
						}
						if v22 && v18 == v20 {
							/* angle 0, purely imaginary values */
							inex_im = libmpfr.Xmpfr_agm(tls, rop+32, a+32, b+32, rnd>>libc.Int32FromInt32(4))
							{
								_p1 = rop
								(*t__mpfr_struct)(unsafe.Pointer(_p1)).F_mpfr_sign = int32(1)
								(*t__mpfr_struct)(unsafe.Pointer(_p1)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
								_ = int32(_MPFR_RNDN)
								v23 = 0
							}
							_ = v23
							if inex_im < 0 {
								v24 = int32(2)
							} else {
								if inex_im == 0 {
									v25 = 0
								} else {
									v25 = int32(1)
								}
								v24 = v25
							}
							return 0 | v24<<int32(2)
						} else {
							return _mpc_agm_general(tls, rop, a, b, rnd)
						}
					}
				}
			}
		}
	}
	return r
}

func Xmpc_arg(tls *libc.TLS, a Tmpfr_ptr, b Tmpc_srcptr, rnd Tmpfr_rnd_t) (r int32) {
	return libmpfr.Xmpfr_atan2(tls, a, b+32, b, rnd)
}

// C documentation
//
//	/* Special case op = 1 + i*y for tiny y (see algorithms.tex).
//	   Return 0 if special formula fails, otherwise put in z1 the approximate
//	   value which needs to be converted to rop.
//	   z1 is a temporary variable with enough precision.
//	 */
func _mpc_asin_special(tls *libc.TLS, rop Tmpc_ptr, op Tmpc_srcptr, rnd Tmpc_rnd_t, z1 Tmpc_ptr) (r int32) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var ey Tmpfr_exp_t
	var inex int32
	var p Tmpfr_prec_t
	var _ /* abs_y at bp+0 */ Tmpfr_t
	_, _, _ = ey, inex, p
	ey = (*t__mpfr_struct)(unsafe.Pointer(op + 32)).F_mpfr_exp
	/* |Re(asin(1+i*y)) - pi/2| <= y^(1/2) */
	if ey >= 0 || -ey/int64(2) < (*t__mpfr_struct)(unsafe.Pointer(z1)).F_mpfr_prec {
		return 0
	}
	libmpfr.Xmpfr_const_pi(tls, z1, int32(_MPFR_RNDN))
	libmpfr.Xmpfr_div_2ui(tls, z1, z1, libc.Uint64FromInt32(libc.Int32FromInt32(1)), int32(_MPFR_RNDN)) /* exact */
	p = (*t__mpfr_struct)(unsafe.Pointer(z1)).F_mpfr_prec
	/* if z1 has precision p, the error on z1 is 1/2*ulp(z1) = 2^(-p) so far,
	   and since ey <= -2p, then y^(1/2) <= 1/2*ulp(z1) too, thus the total
	   error is bounded by ulp(z1) */
	if !(libmpfr.Xmpfr_can_round(tls, z1, p, int32(_MPFR_RNDN), int32(_MPFR_RNDZ), (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec+libc.BoolInt64(rnd&libc.Int32FromInt32(0x0F) == int32(_MPFR_RNDN))) != 0) {
		return 0
	}
	/* |Im(asin(1+i*y)) - y^(1/2)| <= (1/12) * y^(3/2) for y >= 0 (err >= 0)
	   |Im(asin(1-i*y)) + y^(1/2)| <= (1/12) * y^(3/2) for y >= 0 (err <= 0) */
	(*(*Tmpfr_t)(unsafe.Pointer(bp)))[0] = *(*t__mpfr_struct)(unsafe.Pointer(op + 32))
	if (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_sign < 0 {
		libmpfr.Xmpfr_neg(tls, bp, bp, int32(_MPFR_RNDN))
	}
	inex = libmpfr.Xmpfr_sqrt(tls, z1+32, bp, int32(_MPFR_RNDN)) /* error <= 1/2 ulp */
	if (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_sign < 0 {
		libmpfr.Xmpfr_neg(tls, z1+32, z1+32, int32(_MPFR_RNDN))
	}
	/* If z1 has precision p, the error on z1 is 1/2*ulp(z1) = 2^(-p) so far,
	   and (1/12) * y^(3/2) <= (1/8) * y * y^(1/2) <= 2^(ey-3)*2^p*ulp(y^(1/2))
	   thus for p+ey-3 <= -1 we have (1/12) * y^(3/2) <= (1/2) * ulp(y^(1/2)),
	   and the total error is bounded by ulp(z1).
	   Note: if y^(1/2) is exactly representable, and ends with many zeroes,
	   then mpfr_can_round below will fail; however in that case we know that
	   Im(asin(1+i*y)) is away from +/-y^(1/2) wrt zero. */
	if inex == 0 { /* enlarge im(z1) so that the inexact flag is correct */
		if (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_sign < 0 {
			libmpfr.Xmpfr_nextbelow(tls, z1+32)
		} else {
			libmpfr.Xmpfr_nextabove(tls, z1+32)
		}
		return int32(1)
	}
	p = (*t__mpfr_struct)(unsafe.Pointer(z1 + 32)).F_mpfr_prec
	if !(libmpfr.Xmpfr_can_round(tls, z1+32, p-int64(1), int32(_MPFR_RNDA), int32(_MPFR_RNDZ), (*t__mpfr_struct)(unsafe.Pointer(rop+32)).F_mpfr_prec+libc.BoolInt64(rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDN))) != 0) {
		return 0
	}
	return int32(1)
}

// C documentation
//
//	/* Put in s an approximation of asin(z) using:
//	   asin z = z + 1/2*z^3/3 + (1*3)/(2*4)*z^5/5 + ...
//	   Assume |Re(z)|, |Im(z)| < 1/2.
//	   Return non-zero if we can get the correct result by rounding s:
//	   mpc_set (rop, s, ...) */
func _mpc_asin_series(tls *libc.TLS, rop Tmpc_srcptr, s Tmpc_ptr, z Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(128)
	defer tls.Free(128)
	var e, ex, exp_x, exp_y, ey Tmpfr_exp_t
	var err, k, kx, ky uint64
	var p Tmpfr_prec_t
	var v1 int64
	var _ /* t at bp+64 */ Tmpc_t
	var _ /* w at bp+0 */ Tmpc_t
	_, _, _, _, _, _, _, _, _, _, _ = e, err, ex, exp_x, exp_y, ey, k, kx, ky, p, v1
	/* assume z = (x,y) with |x|,|y| < 2^(-e) with e >= 1, see the error
	   analysis in algorithms.tex */
	ex = (*t__mpfr_struct)(unsafe.Pointer(z)).F_mpfr_exp
	ey = (*t__mpfr_struct)(unsafe.Pointer(z + 32)).F_mpfr_exp
	if ex >= ey {
		v1 = ex
	} else {
		v1 = ey
	}
	e = v1
	e = -e
	/* now e >= 1 */
	p = (*t__mpfr_struct)(unsafe.Pointer(s)).F_mpfr_prec /* working precision */
	Xmpc_init2(tls, bp, p)
	Xmpc_init2(tls, bp+64, p)
	Xmpc_set(tls, s, z, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
	Xmpc_sqr(tls, bp, z, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
	Xmpc_set(tls, bp+64, z, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
	k = uint64(1)
	for {
		Xmpc_mul(tls, bp+64, bp+64, bp, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
		Xmpc_mul_ui(tls, bp+64, bp+64, (uint64(2)*k-uint64(1))*(uint64(2)*k-uint64(1)), int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
		Xmpc_div_ui(tls, bp+64, bp+64, uint64(2)*k*(uint64(2)*k+uint64(1)), int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
		exp_x = (*t__mpfr_struct)(unsafe.Pointer(s)).F_mpfr_exp
		exp_y = (*t__mpfr_struct)(unsafe.Pointer(s + 32)).F_mpfr_exp
		if (*t__mpfr_struct)(unsafe.Pointer(bp+64)).F_mpfr_exp < exp_x-p && (*t__mpfr_struct)(unsafe.Pointer(bp+64+32)).F_mpfr_exp < exp_y-p {
			/* Re(t) < 1/2 ulp(Re(s)) and Im(t) < 1/2 ulp(Im(s)),
			   thus adding t to s will not change s */
			break
		}
		Xmpc_add(tls, s, s, bp+64, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
		goto _2
	_2:
		;
		k++
	}
	Xmpc_clear(tls, bp)
	Xmpc_clear(tls, bp+64)
	/* check (2k-1)^2 is exactly representable */
	/* maximal absolute error on Re(s),Im(s) is:
	   (5k-3)k/2*2^(-1-p) for e=1
	   5k/2*2^(-e-p) for e >= 2 */
	if e == int64(1) {
		kx = (uint64(5)*k - uint64(3)) * k
	} else {
		kx = uint64(5) * k
	}
	kx = (kx + uint64(1)) / uint64(2) /* takes into account the 1/2 factor in both cases */
	/* now (5k-3)k/2 <= kx for e=1, and 5k/2 <= kx for e >= 2, thus
	   the maximal absolute error on Re(s),Im(s) is bounded by kx*2^(-e-p) */
	e = -e
	ky = kx
	/* for the real part, convert the maximal absolute error kx*2^(e-p) into
	   relative error */
	ex = (*t__mpfr_struct)(unsafe.Pointer(s)).F_mpfr_exp
	/* ulp(Re(s)) = 2^(ex+1-p) */
	err = uint64(0)
	/* invariant: the error will be kx*2^err */
	if ex+int64(1) > e { /* divide kx by 2^(ex+1-e) */
		for ex+int64(1) > e {
			kx = (kx + uint64(1)) / uint64(2)
			ex--
		}
	} else { /* multiply the error by 2^(e-(ex+1)), thus add e-(ex+1) to err */
		err += libc.Uint64FromInt64(e - (ex + int64(1)))
	}
	/* now the rounding error is bounded by kx*2^err*ulp(Re(s)), add the
	   mathematical error which is bounded by ulp(Re(s)): the first neglected
	   term is less than 1/2*ulp(Re(s)), and each term decreases by at least
	   a factor 2, since |z^2| <= 1/2. */
	kx++
	for {
		if !(kx > uint64(2)) {
			break
		}
		goto _3
	_3:
		;
		err++
		kx = (kx + libc.Uint64FromInt32(1)) / libc.Uint64FromInt32(2)
	}
	/* can we round Re(s) with error less than 2^(EXP(Re(s))-err) ? */
	if !(libmpfr.Xmpfr_can_round(tls, s, libc.Int64FromUint64(libc.Uint64FromInt64(p)-err), int32(_MPFR_RNDN), int32(_MPFR_RNDZ), (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec+libc.BoolInt64(rnd&libc.Int32FromInt32(0x0F) == int32(_MPFR_RNDN))) != 0) {
		return 0
	}
	/* same for the imaginary part */
	ey = (*t__mpfr_struct)(unsafe.Pointer(s + 32)).F_mpfr_exp
	/* we take for e the exponent of Im(z), which amounts to divide the error by
	   2^delta where delta is the exponent difference between Re(z) and Im(z)
	   (see algorithms.tex) */
	e = (*t__mpfr_struct)(unsafe.Pointer(z + 32)).F_mpfr_exp
	/* ulp(Im(s)) = 2^(ey+1-p) */
	if ey+int64(1) > e { /* divide ky by 2^(ey+1-e) */
		for ey+int64(1) > e {
			ky = (ky + uint64(1)) / uint64(2)
			ey--
		}
	} else { /* multiply ky by 2^(e-(ey+1)) */
		ky <<= libc.Uint64FromInt64(e - (ey + int64(1)))
	}
	/* now the rounding error is bounded by ky*ulp(Im(s)), add the
	   mathematical error which is bounded by ulp(Im(s)): the first neglected
	   term is less than 1/2*ulp(Im(s)), and each term decreases by at least
	   a factor 2, since |z^2| <= 1/2. */
	ky++
	err = uint64(0)
	for {
		if !(ky > uint64(2)) {
			break
		}
		goto _4
	_4:
		;
		err++
		ky = (ky + libc.Uint64FromInt32(1)) / libc.Uint64FromInt32(2)
	}
	/* can we round Im(s) with error less than 2^(EXP(Im(s))-err) ? */
	return libmpfr.Xmpfr_can_round(tls, s+32, libc.Int64FromUint64(libc.Uint64FromInt64(p)-err), int32(_MPFR_RNDN), int32(_MPFR_RNDZ), (*t__mpfr_struct)(unsafe.Pointer(rop+32)).F_mpfr_prec+libc.BoolInt64(rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDN)))
}

func _asin_taylor1(tls *libc.TLS, inex uintptr, rop Tmpc_ptr, z Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	/* Write z = x + i*y and assume |x| < 1/2 and |y| < 1/4, that is,
	   Exp (x) <= -1 and Exp (y) <= -2; this also implies |z| < 1.
	   The function computes the Taylor series of order 1 around x
	      asin (z) \approx asin (x) + i * y / sqrt (1 - x^2)
	   with error term bounded above by Pi/2 * beta^2 / (1 - beta)
	   where beta = |y| / (1 - |x|), see algorithms.tex.
	   If the result can be rounded in direction rnd to rop, the value is
	   stored in rop, the inexact value is stored in inex, and true is
	   returned; otherwise rop and inex are not changed, and false
	   is returned. */
	var _p, _p1, x, y Tmpfr_srcptr
	var err, es, ex, ey Tmpfr_exp_t
	var inex_im, inex_re, ok, v2, v3, v4, v5, v6, v7 int32
	var prec, prec_im, prec_re Tmpfr_prec_t
	var v1 int64
	var _ /* s at bp+0 */ Tmpfr_t
	var _ /* t at bp+32 */ Tmpfr_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = _p, _p1, err, es, ex, ey, inex_im, inex_re, ok, prec, prec_im, prec_re, x, y, v1, v2, v3, v4, v5, v6, v7
	/* We have asin (x) ~ x,
	   |y| <= |y| / sqrt (1 - x^2) <= sqrt (4/3) * |y|,
	   beta <= 2 * |y| < 1/2, 1 / 1 - beta < 2 and the error term is bounded
	   above by 4 * Pi * |y|^2 < 13 * |y|^2 < 16 * |y|^2.
	   So to have a chance to round the imaginary part, we need roughly
	   log_2 (error term) \approx 2 * Exp (y) + 4
	   <= log_2 (ulp (y)) \approx Exp (y) - prec (imag (rop)), or
	   Exp (y) <= -prec (imag (rop)) - 4.
	   For the real part, we need
	   2 * Exp (y) + 4 <= Exp (x) - prec (real (rop)), or
	   Exp (x) >= 2 * Exp (y) + 4 + prec (real (rop)).
	   Check this first. */
	x = z
	y = z + 32
	ex = (*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_exp
	ey = (*t__mpfr_struct)(unsafe.Pointer(y)).F_mpfr_exp
	prec_re = (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec
	prec_im = (*t__mpfr_struct)(unsafe.Pointer(rop + 32)).F_mpfr_prec
	if ey > -prec_im-int64(4) || ex < int64(2)*ey+int64(4)+prec_re {
		return 0
	}
	/* Real part. */
	prec = prec_re + int64(7)
	libmpfr.Xmpfr_init2(tls, bp, prec)
	libmpfr.Xmpfr_asin(tls, bp, x, int32(_MPFR_RNDN))
	/* The error is bounded above by 13*|y|^2 + 1/2 * 2^(Exp (s) - prec)
	   <= 2^(max (2 * Exp (y) + 5, Exp (s) - prec)). */
	es = (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp
	if int64(2)*ey+int64(5) > es-prec {
		v1 = int64(2)*ey + int64(5)
	} else {
		v1 = es - prec
	}
	err = v1
	ok = libmpfr.Xmpfr_can_round(tls, bp, es-err, int32(_MPFR_RNDN), int32(_MPFR_RNDZ), (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec+libc.BoolInt64(rnd&libc.Int32FromInt32(0x0F) == int32(_MPFR_RNDN)))
	if ok != 0 {
		/* Imaginary part. */
		prec = prec_im + int64(7)
		libmpfr.Xmpfr_init2(tls, bp+32, prec)
		libmpfr.Xmpfr_mul(tls, bp+32, x, x, int32(_MPFR_RNDU)) /* 0 < t <= 1/4, error 1 ulp */
		libmpfr.Xmpfr_ui_sub(tls, bp+32, uint64(1), bp+32, int32(_MPFR_RNDD))
		/* 3/4 <= t < 1, error 2 ulp, epsilon- = 0 since rounded down */
		libmpfr.Xmpfr_sqrt(tls, bp+32, bp+32, int32(_MPFR_RNDD))
		/* error 3 ulp: propagation error stable since epsilon- = 0,
		   1 ulp for rounding; see ssec:proprealsqrt in algorithms.tex */
		libmpfr.Xmpfr_div(tls, bp+32, y, bp+32, int32(_MPFR_RNDA))
		/* error 7 ulp: since denominator rounded down, previous error
		   multiplied by 2, 1 ulp additional rounding error */
		ok = libmpfr.Xmpfr_can_round(tls, bp+32, prec-int64(3), int32(_MPFR_RNDA), int32(_MPFR_RNDZ), (*t__mpfr_struct)(unsafe.Pointer(rop+32)).F_mpfr_prec+libc.BoolInt64(rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDN)))
		if ok != 0 {
			{
				_p = bp
				v2 = libmpfr.Xmpfr_set4(tls, rop, _p, rnd&libc.Int32FromInt32(0x0F), (*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign)
			}
			inex_re = v2
			{
				_p1 = bp + 32
				v3 = libmpfr.Xmpfr_set4(tls, rop+32, _p1, rnd>>libc.Int32FromInt32(4), (*t__mpfr_struct)(unsafe.Pointer(_p1)).F_mpfr_sign)
			}
			inex_im = v3
			if inex_re < 0 {
				v4 = int32(2)
			} else {
				if inex_re == 0 {
					v5 = 0
				} else {
					v5 = int32(1)
				}
				v4 = v5
			}
			if inex_im < 0 {
				v6 = int32(2)
			} else {
				if inex_im == 0 {
					v7 = 0
				} else {
					v7 = int32(1)
				}
				v6 = v7
			}
			*(*int32)(unsafe.Pointer(inex)) = v4 | v6<<int32(2)
		}
		libmpfr.Xmpfr_clear(tls, bp+32)
	}
	libmpfr.Xmpfr_clear(tls, bp)
	return ok
}

func Xmpc_asin(tls *libc.TLS, rop Tmpc_ptr, op Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(112)
	defer tls.Free(112)
	var _p Tmpfr_srcptr
	var _p1, _p2 Tmpfr_ptr
	var err, ex, ey, olderr, saved_emax, saved_emin, v24 Tmpfr_exp_t
	var inex_im, inex_re, inf_im, loop, s, s_im, v1, v10, v11, v12, v13, v14, v15, v16, v17, v18, v19, v2, v20, v21, v22, v3, v36, v37, v38, v39, v4, v40, v41, v42, v43, v5, v6, v7, v8, v9 int32
	var p, p_im, p_re Tmpfr_prec_t
	var rnd_im, rnd_re Tmpfr_rnd_t
	var v23, v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35 int64
	var _ /* inex at bp+64 */ int32
	var _ /* minus_op_re at bp+72 */ Tmpfr_t
	var _ /* z1 at bp+0 */ Tmpc_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = _p, _p1, _p2, err, ex, ey, inex_im, inex_re, inf_im, loop, olderr, p, p_im, p_re, rnd_im, rnd_re, s, s_im, saved_emax, saved_emin, v1, v10, v11, v12, v13, v14, v15, v16, v17, v18, v19, v2, v20, v21, v22, v23, v24, v25, v26, v27, v28, v29, v3, v30, v31, v32, v33, v34, v35, v36, v37, v38, v39, v4, v40, v41, v42, v43, v5, v6, v7, v8, v9
	loop = 0
	/* special values */
	if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			libmpfr.Xmpfr_set_nan(tls, rop)
			if (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_sign < 0 {
				v1 = -int32(1)
			} else {
				v1 = +libc.Int32FromInt32(1)
			}
			libmpfr.Xmpfr_set_inf(tls, rop+32, v1)
		} else {
			if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				{
					_p = op
					v2 = libmpfr.Xmpfr_set4(tls, rop, _p, int32(_MPFR_RNDN), (*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign)
				}
				_ = v2
				libmpfr.Xmpfr_set_nan(tls, rop+32)
			} else {
				libmpfr.Xmpfr_set_nan(tls, rop)
				libmpfr.Xmpfr_set_nan(tls, rop+32)
			}
		}
		return 0
	}
	if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			inf_im = libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)))
			if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_sign < 0 {
				v3 = -int32(1)
			} else {
				v3 = int32(1)
			}
			inex_re = Xset_pi_over_2(tls, rop, v3, rnd&libc.Int32FromInt32(0x0F))
			if (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_sign < 0 {
				v4 = -int32(1)
			} else {
				v4 = int32(1)
			}
			libmpfr.Xmpfr_set_inf(tls, rop+32, v4)
			if inf_im != 0 {
				libmpfr.Xmpfr_div_2ui(tls, rop, rop, uint64(1), int32(_MPFR_RNDN))
			}
		} else {
			if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_sign < 0 {
				v5 = -int32(1)
			} else {
				v5 = int32(1)
			}
			libmpfr.Xmpfr_set_zero(tls, rop, v5)
			inex_re = 0
			if (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_sign < 0 {
				v6 = -int32(1)
			} else {
				v6 = int32(1)
			}
			libmpfr.Xmpfr_set_inf(tls, rop+32, v6)
		}
		if inex_re < 0 {
			v7 = int32(2)
		} else {
			if inex_re == 0 {
				v8 = 0
			} else {
				v8 = int32(1)
			}
			v7 = v8
		}
		return v7 | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2)
	}
	/* pure real argument */
	if (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		s_im = libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_sign < 0)
		if libmpfr.Xmpfr_cmp_ui_2exp(tls, op, libc.Uint64FromInt32(libc.Int32FromInt32(1)), 0) > 0 {
			if s_im != 0 {
				if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDU) {
					v9 = int32(_MPFR_RNDD)
				} else {
					if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDD) {
						v10 = int32(_MPFR_RNDU)
					} else {
						v10 = rnd >> libc.Int32FromInt32(4)
					}
					v9 = v10
				}
				inex_im = -libmpfr.Xmpfr_acosh(tls, rop+32, op, v9)
			} else {
				inex_im = libmpfr.Xmpfr_acosh(tls, rop+32, op, rnd>>libc.Int32FromInt32(4))
			}
			if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_sign < 0 {
				v11 = -int32(1)
			} else {
				v11 = int32(1)
			}
			inex_re = Xset_pi_over_2(tls, rop, v11, rnd&libc.Int32FromInt32(0x0F))
			if s_im != 0 {
				Xmpc_conj(tls, rop, rop, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
			}
		} else {
			if libmpfr.Xmpfr_cmp_si_2exp(tls, op, int64(-libc.Int32FromInt32(1)), 0) < 0 {
				(*(*Tmpfr_t)(unsafe.Pointer(bp + 72)))[0] = *(*t__mpfr_struct)(unsafe.Pointer(op))
				libmpfr.Xmpfr_neg(tls, bp+72, bp+72, int32(_MPFR_RNDN))
				if s_im != 0 {
					if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDU) {
						v12 = int32(_MPFR_RNDD)
					} else {
						if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDD) {
							v13 = int32(_MPFR_RNDU)
						} else {
							v13 = rnd >> libc.Int32FromInt32(4)
						}
						v12 = v13
					}
					inex_im = -libmpfr.Xmpfr_acosh(tls, rop+32, bp+72, v12)
				} else {
					inex_im = libmpfr.Xmpfr_acosh(tls, rop+32, bp+72, rnd>>libc.Int32FromInt32(4))
				}
				if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_sign < 0 {
					v14 = -int32(1)
				} else {
					v14 = int32(1)
				}
				inex_re = Xset_pi_over_2(tls, rop, v14, rnd&libc.Int32FromInt32(0x0F))
				if s_im != 0 {
					Xmpc_conj(tls, rop, rop, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
				}
			} else {
				{
					_p1 = rop + 32
					(*t__mpfr_struct)(unsafe.Pointer(_p1)).F_mpfr_sign = int32(1)
					(*t__mpfr_struct)(unsafe.Pointer(_p1)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
					_ = rnd >> libc.Int32FromInt32(4)
					v15 = 0
				}
				inex_im = v15
				if s_im != 0 {
					libmpfr.Xmpfr_neg(tls, rop+32, rop+32, int32(_MPFR_RNDN))
				}
				inex_re = libmpfr.Xmpfr_asin(tls, rop, op, rnd&libc.Int32FromInt32(0x0F))
			}
		}
		if inex_re < 0 {
			v16 = int32(2)
		} else {
			if inex_re == 0 {
				v17 = 0
			} else {
				v17 = int32(1)
			}
			v16 = v17
		}
		if inex_im < 0 {
			v18 = int32(2)
		} else {
			if inex_im == 0 {
				v19 = 0
			} else {
				v19 = int32(1)
			}
			v18 = v19
		}
		return v16 | v18<<int32(2)
	}
	/* pure imaginary argument */
	if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		s = libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_sign < 0)
		{
			_p2 = rop
			(*t__mpfr_struct)(unsafe.Pointer(_p2)).F_mpfr_sign = int32(1)
			(*t__mpfr_struct)(unsafe.Pointer(_p2)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
			_ = int32(_MPFR_RNDN)
			v20 = 0
		}
		_ = v20
		if s != 0 {
			libmpfr.Xmpfr_neg(tls, rop, rop, int32(_MPFR_RNDN))
		}
		inex_im = libmpfr.Xmpfr_asinh(tls, rop+32, op+32, rnd>>libc.Int32FromInt32(4))
		if inex_im < 0 {
			v21 = int32(2)
		} else {
			if inex_im == 0 {
				v22 = 0
			} else {
				v22 = int32(1)
			}
			v21 = v22
		}
		return 0 | v21<<int32(2)
	}
	/* Try special code for |x| < 1/2 and |y| < 1/4. */
	if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp <= int64(-int32(1)) && (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp <= int64(-int32(2)) {
		if _asin_taylor1(tls, bp+64, rop, op, rnd) != 0 {
			return *(*int32)(unsafe.Pointer(bp + 64))
		}
	}
	saved_emin = libmpfr.Xmpfr_get_emin(tls)
	saved_emax = libmpfr.Xmpfr_get_emax(tls)
	libmpfr.Xmpfr_set_emin(tls, libmpfr.Xmpfr_get_emin_min(tls))
	libmpfr.Xmpfr_set_emax(tls, libmpfr.Xmpfr_get_emax_max(tls))
	/* regular complex: asin(z) = -i*log(i*z+sqrt(1-z^2)) */
	p_re = (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec
	p_im = (*t__mpfr_struct)(unsafe.Pointer(rop + 32)).F_mpfr_prec
	rnd_re = rnd & libc.Int32FromInt32(0x0F)
	rnd_im = rnd >> libc.Int32FromInt32(4)
	if p_re >= p_im {
		v23 = p_re
	} else {
		v23 = p_im
	}
	p = v23
	Xmpc_init2(tls, bp, p)
	v24 = libc.Int64FromInt32(0)
	err = v24
	olderr = v24 /* number of lost bits */
	for int32(1) != 0 {
		loop++
		p += err - olderr /* add extra number of lost bits in previous loop */
		olderr = err
		if loop <= int32(2) {
			v25 = Xmpc_ceil_log2(tls, p) + int64(3)
		} else {
			v25 = p / int64(2)
		}
		p += v25
		Xmpc_set_prec(tls, bp, p)
		/* try special code for 1+i*y with tiny y */
		if loop == int32(1) && libmpfr.Xmpfr_cmp_ui_2exp(tls, op, libc.Uint64FromInt32(libc.Int32FromInt32(1)), 0) == 0 && _mpc_asin_special(tls, rop, op, rnd, bp) != 0 {
			break
		}
		/* try special code for small z */
		if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp <= int64(-int32(1)) && (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp <= int64(-int32(1)) && _mpc_asin_series(tls, rop, bp, op, rnd) != 0 {
			break
		}
		/* z1 <- z^2 */
		Xmpc_sqr(tls, bp, op, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
		/* err(x) <= 1/2 ulp(x), err(y) <= 1/2 ulp(y) */
		/* z1 <- 1-z1 */
		ex = (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp
		libmpfr.Xmpfr_ui_sub(tls, bp, uint64(1), bp, int32(_MPFR_RNDN))
		libmpfr.Xmpfr_neg(tls, bp+32, bp+32, int32(_MPFR_RNDN))
		/* if Re(z1) = 0, we can't determine the relative error */
		if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			continue
		}
		ex = ex - (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp
		if ex <= 0 {
			v26 = 0
		} else {
			v26 = ex
		}
		ex = v26
		/* err(x) <= 2^ex * ulp(x) */
		ex = ex + (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp - p
		/* err(x) <= 2^ex */
		ey = (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp - p - int64(1)
		/* err(y) <= 2^ey */
		if ex >= ey {
			v27 = ex
		} else {
			v27 = ey
		}
		ex = v27 /* err(x), err(y) <= 2^ex, i.e., the norm
		   of the error is bounded by |h|<=2^(ex+1/2) */
		/* z1 <- sqrt(z1): if z1 = z + h, then sqrt(z1) = sqrt(z) + h/2/sqrt(t) */
		if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp >= (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp {
			v28 = (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp
		} else {
			v28 = (*t__mpfr_struct)(unsafe.Pointer(bp + 32)).F_mpfr_exp
		}
		ey = v28
		/* we have |z1| >= 2^(ey-1) thus 1/|z1| <= 2^(1-ey) */
		Xmpc_sqrt(tls, bp, bp, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
		ex = int64(2)*ex + int64(1) - int64(2) - (ey - int64(1)) /* |h^2/4/|t| <= 2^ex */
		ex = (ex + int64(1)) / int64(2)                          /* ceil(ex/2) */
		/* express ex in terms of ulp(z1) */
		if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp <= (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp {
			v29 = (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp
		} else {
			v29 = (*t__mpfr_struct)(unsafe.Pointer(bp + 32)).F_mpfr_exp
		}
		ey = v29
		ex = ex - ey + p
		/* take into account the rounding error in the mpc_sqrt call */
		if ex <= 0 {
			v30 = int64(1)
		} else {
			v30 = ex + int64(1)
		}
		err = v30
		/* err(x) <= 2^err * ulp(x), err(y) <= 2^err * ulp(y) */
		/* z1 <- i*z + z1 */
		ex = (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp
		ey = (*t__mpfr_struct)(unsafe.Pointer(bp + 32)).F_mpfr_exp
		libmpfr.Xmpfr_sub(tls, bp, bp, op+32, int32(_MPFR_RNDN))
		libmpfr.Xmpfr_add(tls, bp+32, bp+32, op, int32(_MPFR_RNDN))
		if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			continue
		}
		ex -= (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp      /* cancellation in x */
		ey -= (*t__mpfr_struct)(unsafe.Pointer(bp + 32)).F_mpfr_exp /* cancellation in y */
		if ex >= ey {
			v31 = ex
		} else {
			v31 = ey
		}
		ex = v31 /* maximum cancellation */
		err += ex
		if err <= 0 {
			v32 = int64(1)
		} else {
			v32 = err + int64(1)
		}
		err = v32 /* rounding error in sub/add */
		/* z1 <- log(z1): if z1 = z + h, then log(z1) = log(z) + h/t with
		   |t| >= min(|z1|,|z|) */
		ex = (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp
		ey = (*t__mpfr_struct)(unsafe.Pointer(bp + 32)).F_mpfr_exp
		if ex >= ey {
			v33 = ex
		} else {
			v33 = ey
		}
		ex = v33
		err += ex - p /* revert to absolute error <= 2^err */
		Xmpc_log(tls, bp, bp, int32(_MPFR_RNDN))
		err -= ex - int64(1) /* 1/|t| <= 1/|z| <= 2^(1-ex) */
		/* express err in terms of ulp(z1) */
		if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp <= (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp {
			v34 = (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp
		} else {
			v34 = (*t__mpfr_struct)(unsafe.Pointer(bp + 32)).F_mpfr_exp
		}
		ey = v34
		err = err - ey + p
		/* take into account the rounding error in the mpc_log call */
		if err <= 0 {
			v35 = int64(1)
		} else {
			v35 = err + int64(1)
		}
		err = v35
		/* z1 <- -i*z1 */
		libmpfr.Xmpfr_swap(tls, bp, bp+32)
		libmpfr.Xmpfr_neg(tls, bp+32, bp+32, int32(_MPFR_RNDN))
		if libmpfr.Xmpfr_can_round(tls, bp, p-err, int32(_MPFR_RNDN), int32(_MPFR_RNDZ), p_re+libc.BoolInt64(rnd_re == int32(_MPFR_RNDN))) != 0 && libmpfr.Xmpfr_can_round(tls, bp+32, p-err, int32(_MPFR_RNDN), int32(_MPFR_RNDZ), p_im+libc.BoolInt64(rnd_im == int32(_MPFR_RNDN))) != 0 {
			break
		}
	}
	*(*int32)(unsafe.Pointer(bp + 64)) = Xmpc_set(tls, rop, bp, rnd)
	Xmpc_clear(tls, bp)
	/* restore the exponent range, and check the range of results */
	libmpfr.Xmpfr_set_emin(tls, saved_emin)
	libmpfr.Xmpfr_set_emax(tls, saved_emax)
	if *(*int32)(unsafe.Pointer(bp + 64))&int32(3) == int32(2) {
		v36 = -int32(1)
	} else {
		if *(*int32)(unsafe.Pointer(bp + 64))&int32(3) == 0 {
			v37 = 0
		} else {
			v37 = int32(1)
		}
		v36 = v37
	}
	inex_re = libmpfr.Xmpfr_check_range(tls, rop, v36, rnd&libc.Int32FromInt32(0x0F))
	if *(*int32)(unsafe.Pointer(bp + 64))>>int32(2) == int32(2) {
		v38 = -int32(1)
	} else {
		if *(*int32)(unsafe.Pointer(bp + 64))>>int32(2) == 0 {
			v39 = 0
		} else {
			v39 = int32(1)
		}
		v38 = v39
	}
	inex_im = libmpfr.Xmpfr_check_range(tls, rop+32, v38, rnd>>libc.Int32FromInt32(4))
	if inex_re < 0 {
		v40 = int32(2)
	} else {
		if inex_re == 0 {
			v41 = 0
		} else {
			v41 = int32(1)
		}
		v40 = v41
	}
	if inex_im < 0 {
		v42 = int32(2)
	} else {
		if inex_im == 0 {
			v43 = 0
		} else {
			v43 = int32(1)
		}
		v42 = v43
	}
	return v40 | v42<<int32(2)
}

func Xmpc_asinh(tls *libc.TLS, rop Tmpc_ptr, op Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(128)
	defer tls.Free(128)
	var inex, v1, v10, v11, v12, v13, v14, v2, v3, v4, v5, v6, v7, v8, v9 int32
	var tmp Tmpfr_t
	var _ /* a at bp+64 */ Tmpc_t
	var _ /* z at bp+0 */ Tmpc_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = inex, tmp, v1, v10, v11, v12, v13, v14, v2, v3, v4, v5, v6, v7, v8, v9
	/* z = i*op */
	*(*t__mpfr_struct)(unsafe.Pointer(bp)) = *(*t__mpfr_struct)(unsafe.Pointer(op + 32))
	*(*t__mpfr_struct)(unsafe.Pointer(bp + 32)) = *(*t__mpfr_struct)(unsafe.Pointer(op))
	libmpfr.Xmpfr_neg(tls, bp, bp, int32(_MPFR_RNDN))
	/* Note reversal of precisions due to later multiplication by -i */
	Xmpc_init3(tls, bp+64, (*t__mpfr_struct)(unsafe.Pointer(rop+32)).F_mpfr_prec, (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec)
	if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDU) {
		v1 = int32(_MPFR_RNDD)
	} else {
		if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDD) {
			v2 = int32(_MPFR_RNDU)
		} else {
			v2 = rnd >> libc.Int32FromInt32(4)
		}
		v1 = v2
	}
	inex = Xmpc_asin(tls, bp+64, bp, v1+rnd&libc.Int32FromInt32(0x0F)<<libc.Int32FromInt32(4))
	/* if a = asin(i*op) = x+i*y, and we want y-i*x */
	/* change a to -i*a */
	tmp[0] = *(*t__mpfr_struct)(unsafe.Pointer(bp + 64))
	*(*t__mpfr_struct)(unsafe.Pointer(bp + 64)) = *(*t__mpfr_struct)(unsafe.Pointer(bp + 64 + 32))
	*(*t__mpfr_struct)(unsafe.Pointer(bp + 64 + 32)) = tmp[0]
	libmpfr.Xmpfr_neg(tls, bp+64+32, bp+64+32, int32(_MPFR_RNDN))
	Xmpc_set(tls, rop, bp+64, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4)) /* exact */
	Xmpc_clear(tls, bp+64)
	if inex>>int32(2) == int32(2) {
		v4 = -int32(1)
	} else {
		if inex>>int32(2) == 0 {
			v5 = 0
		} else {
			v5 = int32(1)
		}
		v4 = v5
	}
	if v4 < 0 {
		v3 = int32(2)
	} else {
		if inex>>int32(2) == int32(2) {
			v7 = -int32(1)
		} else {
			if inex>>int32(2) == 0 {
				v8 = 0
			} else {
				v8 = int32(1)
			}
			v7 = v8
		}
		if v7 == 0 {
			v6 = 0
		} else {
			v6 = int32(1)
		}
		v3 = v6
	}
	if inex&int32(3) == int32(2) {
		v10 = -int32(1)
	} else {
		if inex&int32(3) == 0 {
			v11 = 0
		} else {
			v11 = int32(1)
		}
		v10 = v11
	}
	if -v10 < 0 {
		v9 = int32(2)
	} else {
		if inex&int32(3) == int32(2) {
			v13 = -int32(1)
		} else {
			if inex&int32(3) == 0 {
				v14 = 0
			} else {
				v14 = int32(1)
			}
			v13 = v14
		}
		if -v13 == 0 {
			v12 = 0
		} else {
			v12 = int32(1)
		}
		v9 = v12
	}
	return v3 | v9<<int32(2)
}

// C documentation
//
//	/* set rop to
//	   -pi/2 if s < 0
//	   +pi/2 else
//	   rounded in the direction rnd
//	*/
func Xset_pi_over_2(tls *libc.TLS, rop Tmpfr_ptr, s int32, rnd Tmpfr_rnd_t) (r int32) {
	var inex, v1, v2, v3 int32
	_, _, _, _ = inex, v1, v2, v3
	if s < 0 {
		if rnd == int32(_MPFR_RNDU) {
			v2 = int32(_MPFR_RNDD)
		} else {
			if rnd == int32(_MPFR_RNDD) {
				v3 = int32(_MPFR_RNDU)
			} else {
				v3 = rnd
			}
			v2 = v3
		}
		v1 = v2
	} else {
		v1 = rnd
	}
	inex = libmpfr.Xmpfr_const_pi(tls, rop, v1)
	libmpfr.Xmpfr_div_2ui(tls, rop, rop, uint64(1), int32(_MPFR_RNDN))
	if s < 0 {
		inex = -inex
		libmpfr.Xmpfr_neg(tls, rop, rop, int32(_MPFR_RNDN))
	}
	return inex
}

func Xmpc_atan(tls *libc.TLS, rop Tmpc_ptr, op Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(272)
	defer tls.Free(272)
	var _p, _p1, _p2, _p3, _p4 Tmpfr_ptr
	var _p5 Tmpfr_srcptr
	var cmp_1, inex, inex_im, inex_re, ok, ok1, s_im, s_re, v1, v10, v11, v12, v13, v14, v15, v16, v17, v18, v2, v22, v23, v25, v26, v28, v3, v30, v37, v38, v39, v4, v40, v41, v42, v43, v44, v5, v6, v7, v8, v9 int32
	var err, err1, exp_a, expo, op_im_exp, op_re_exp, saved_emax, saved_emin Tmpfr_exp_t
	var p, p1, p_im, prec Tmpfr_prec_t
	var rnd1, rnd2, rnd_im Tmpfr_rnd_t
	var v19, v20, v32, v33, v34, v35, v36 int64
	var v21 bool
	var _ /* a at bp+64 */ Tmpfr_t
	var _ /* b at bp+96 */ Tmpfr_t
	var _ /* minus_op_re at bp+192 */ Tmpfr_t
	var _ /* x at bp+128 */ Tmpfr_t
	var _ /* y at bp+0 */ Tmpfr_t
	var _ /* y at bp+160 */ Tmpfr_t
	var _ /* z at bp+32 */ Tmpfr_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = _p, _p1, _p2, _p3, _p4, _p5, cmp_1, err, err1, exp_a, expo, inex, inex_im, inex_re, ok, ok1, op_im_exp, op_re_exp, p, p1, p_im, prec, rnd1, rnd2, rnd_im, s_im, s_re, saved_emax, saved_emin, v1, v10, v11, v12, v13, v14, v15, v16, v17, v18, v19, v2, v20, v21, v22, v23, v25, v26, v28, v3, v30, v32, v33, v34, v35, v36, v37, v38, v39, v4, v40, v41, v42, v43, v44, v5, v6, v7, v8, v9
	inex_re = 0
	inex_im = 0
	s_re = libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_sign < 0)
	s_im = libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_sign < 0)
	/* special values */
	if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			libmpfr.Xmpfr_set_nan(tls, rop)
			if (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				{
					_p = rop + 32
					(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign = int32(1)
					(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
					_ = int32(_MPFR_RNDN)
					v1 = 0
				}
				_ = v1
				if s_im != 0 {
					Xmpc_conj(tls, rop, rop, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
				}
			} else {
				libmpfr.Xmpfr_set_nan(tls, rop+32)
			}
		} else {
			if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				inex_re = Xset_pi_over_2(tls, rop, -s_re, rnd&libc.Int32FromInt32(0x0F))
				{
					_p1 = rop + 32
					(*t__mpfr_struct)(unsafe.Pointer(_p1)).F_mpfr_sign = int32(1)
					(*t__mpfr_struct)(unsafe.Pointer(_p1)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
					_ = int32(_MPFR_RNDN)
					v2 = 0
				}
				_ = v2
			} else {
				libmpfr.Xmpfr_set_nan(tls, rop)
				libmpfr.Xmpfr_set_nan(tls, rop+32)
			}
		}
		if inex_re < 0 {
			v3 = int32(2)
		} else {
			if inex_re == 0 {
				v4 = 0
			} else {
				v4 = int32(1)
			}
			v3 = v4
		}
		return v3 | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2)
	}
	if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		inex_re = Xset_pi_over_2(tls, rop, -s_re, rnd&libc.Int32FromInt32(0x0F))
		{
			_p2 = rop + 32
			(*t__mpfr_struct)(unsafe.Pointer(_p2)).F_mpfr_sign = int32(1)
			(*t__mpfr_struct)(unsafe.Pointer(_p2)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
			_ = int32(_MPFR_RNDN)
			v5 = 0
		}
		_ = v5
		if s_im != 0 {
			Xmpc_conj(tls, rop, rop, int32(_MPFR_RNDN))
		}
		if inex_re < 0 {
			v6 = int32(2)
		} else {
			if inex_re == 0 {
				v7 = 0
			} else {
				v7 = int32(1)
			}
			v6 = v7
		}
		return v6 | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2)
	}
	/* pure real argument */
	if (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		inex_re = libmpfr.Xmpfr_atan(tls, rop, op, rnd&libc.Int32FromInt32(0x0F))
		{
			_p3 = rop + 32
			(*t__mpfr_struct)(unsafe.Pointer(_p3)).F_mpfr_sign = int32(1)
			(*t__mpfr_struct)(unsafe.Pointer(_p3)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
			_ = int32(_MPFR_RNDN)
			v8 = 0
		}
		_ = v8
		if s_im != 0 {
			Xmpc_conj(tls, rop, rop, int32(_MPFR_RNDN))
		}
		if inex_re < 0 {
			v9 = int32(2)
		} else {
			if inex_re == 0 {
				v10 = 0
			} else {
				v10 = int32(1)
			}
			v9 = v10
		}
		return v9 | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2)
	}
	/* pure imaginary argument */
	if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		if s_im != 0 {
			cmp_1 = -libmpfr.Xmpfr_cmp_si_2exp(tls, op+32, int64(-libc.Int32FromInt32(1)), 0)
		} else {
			cmp_1 = libmpfr.Xmpfr_cmp_ui_2exp(tls, op+32, libc.Uint64FromInt32(+libc.Int32FromInt32(1)), 0)
		}
		if cmp_1 < 0 {
			/* atan(+0+iy) = +0 +i*atanh(y), if |y| < 1
			   atan(-0+iy) = -0 +i*atanh(y), if |y| < 1 */
			{
				_p4 = rop
				(*t__mpfr_struct)(unsafe.Pointer(_p4)).F_mpfr_sign = int32(1)
				(*t__mpfr_struct)(unsafe.Pointer(_p4)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
				_ = int32(_MPFR_RNDN)
				v11 = 0
			}
			_ = v11
			if s_re != 0 {
				libmpfr.Xmpfr_neg(tls, rop, rop, int32(_MPFR_RNDN))
			}
			inex_im = libmpfr.Xmpfr_atanh(tls, rop+32, op+32, rnd>>libc.Int32FromInt32(4))
		} else {
			if cmp_1 == 0 {
				/* atan(+/-0 +i) = +/-0 +i*inf
				   atan(+/-0 -i) = +/-0 -i*inf */
				if s_re != 0 {
					v12 = -int32(1)
				} else {
					v12 = +libc.Int32FromInt32(1)
				}
				libmpfr.Xmpfr_set_zero(tls, rop, v12)
				if s_im != 0 {
					v13 = -int32(1)
				} else {
					v13 = +libc.Int32FromInt32(1)
				}
				libmpfr.Xmpfr_set_inf(tls, rop+32, v13)
			} else {
				ok = 0
				rnd_im = rnd >> libc.Int32FromInt32(4)
				libmpfr.Xmpfr_init(tls, bp)
				libmpfr.Xmpfr_init(tls, bp+32)
				p_im = (*t__mpfr_struct)(unsafe.Pointer(rop + 32)).F_mpfr_prec
				p = p_im
				/* a = o(1/y)      with error(a) < ulp(a), rounded away
				   b = o(atanh(a)) with error(b) < ulp(b) + 1/|a^2-1|*ulp(a),
				   since if a = 1/y + eps, then atanh(a) = atanh(1/y) + eps * atanh'(t)
				   with t in (1/y, a). Since a is rounded away, we have 1/y <= a <= 1
				   if y > 1, and -1 <= a <= 1/y if y < -1, thus |atanh'(t)| =
				   1/|t^2-1| <= 1/|a^2-1|.
				   We round atanh(1/y) away from 0.
				*/
				for cond := true; cond; cond = ok == 0 {
					p += Xmpc_ceil_log2(tls, p) + int64(2)
					libmpfr.Xmpfr_set_prec(tls, bp, p)
					libmpfr.Xmpfr_set_prec(tls, bp+32, p)
					inex_im = libmpfr.Xmpfr_ui_div(tls, bp, uint64(1), op+32, int32(_MPFR_RNDA))
					exp_a = (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp
					/* FIXME: should we consider the case with unreasonably huge
					   precision prec(y)>3*exp_min, where atanh(1/Im(op)) could be
					   representable while 1/Im(op) underflows ?
					   This corresponds to |y| = 0.5*2^emin, in which case the
					   result may be wrong. */
					/* We would like to compute a rounded-up error bound 1/|a^2-1|,
					   so we need to round down |a^2-1|, which means rounding up
					   a^2 since |a|<1. */
					libmpfr.Xmpfr_sqr(tls, bp+32, bp, int32(_MPFR_RNDU))
					/* since |y| > 1, we should have |a| <= 1, thus a^2 <= 1 */
					/* in case z=1, we should try again with more precision */
					if libmpfr.Xmpfr_cmp_ui_2exp(tls, bp+32, libc.Uint64FromInt32(libc.Int32FromInt32(1)), 0) == 0 {
						continue
					}
					/* now z < 1 */
					libmpfr.Xmpfr_ui_sub(tls, bp+32, uint64(1), bp+32, int32(_MPFR_RNDZ))
					/* atanh cannot underflow: |atanh(x)| > |x| for |x| < 1 */
					inex_im |= libmpfr.Xmpfr_atanh(tls, bp, bp, int32(_MPFR_RNDA))
					/* the error is now bounded by ulp(b) + 1/z*ulp(a), thus
					   ulp(b) + 2^(exp(a) - exp(b) + 1 - exp(z)) * ulp(b) */
					err = exp_a - (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp + int64(1) - (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp
					if err >= 0 { /* 1 + 2^err <= 2^(err+1) */
						err = err + int64(1)
					} else {
						err = int64(1)
					} /* 1 + 2^err <= 2^1 */
					/* the error is bounded by 2^err ulps */
					ok = libc.BoolInt32(inex_im == 0 || libmpfr.Xmpfr_can_round(tls, bp, p-err, int32(_MPFR_RNDA), int32(_MPFR_RNDZ), p_im+libc.BoolInt64(rnd_im == int32(_MPFR_RNDN))) != 0)
				}
				inex_re = Xset_pi_over_2(tls, rop, -s_re, rnd&libc.Int32FromInt32(0x0F))
				{
					_p5 = bp
					v14 = libmpfr.Xmpfr_set4(tls, rop+32, _p5, rnd_im, (*t__mpfr_struct)(unsafe.Pointer(_p5)).F_mpfr_sign)
				}
				inex_im = v14
				libmpfr.Xmpfr_clear(tls, bp)
				libmpfr.Xmpfr_clear(tls, bp+32)
			}
		}
		if inex_re < 0 {
			v15 = int32(2)
		} else {
			if inex_re == 0 {
				v16 = 0
			} else {
				v16 = int32(1)
			}
			v15 = v16
		}
		if inex_im < 0 {
			v17 = int32(2)
		} else {
			if inex_im == 0 {
				v18 = 0
			} else {
				v18 = int32(1)
			}
			v17 = v18
		}
		return v15 | v17<<int32(2)
	}
	saved_emin = libmpfr.Xmpfr_get_emin(tls)
	saved_emax = libmpfr.Xmpfr_get_emax(tls)
	libmpfr.Xmpfr_set_emin(tls, libmpfr.Xmpfr_get_emin_min(tls))
	libmpfr.Xmpfr_set_emax(tls, libmpfr.Xmpfr_get_emax_max(tls))
	/* regular number argument */
	ok1 = 0
	libmpfr.Xmpfr_inits2(tls, int64(m_MPFR_PREC_MIN), bp+64, libc.VaList(bp+232, bp+96, bp+128, bp+160, libc.UintptrFromInt32(0)))
	/* real part: Re(arctan(x+i*y)) = [arctan2(x,1-y) - arctan2(-x,1+y)]/2 */
	(*(*Tmpfr_t)(unsafe.Pointer(bp + 192)))[0] = *(*t__mpfr_struct)(unsafe.Pointer(op))
	libmpfr.Xmpfr_neg(tls, bp+192, bp+192, int32(_MPFR_RNDN))
	op_re_exp = (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp
	op_im_exp = (*t__mpfr_struct)(unsafe.Pointer(op + 32)).F_mpfr_exp
	prec = (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec /* result precision */
	/* a = o(1-y)         error(a) < 1 ulp(a)
	   b = o(atan2(x,a))  error(b) < [1+2^{3+Exp(x)-Exp(a)-Exp(b)}] ulp(b)
	                                 = kb ulp(b)
	   c = o(1+y)         error(c) < 1 ulp(c)
	   d = o(atan2(-x,c)) error(d) < [1+2^{3+Exp(x)-Exp(c)-Exp(d)}] ulp(d)
	                                 = kd ulp(d)
	   e = o(b - d)       error(e) < [1 + kb*2^{Exp(b}-Exp(e)}
	                                    + kd*2^{Exp(d)-Exp(e)}] ulp(e)
	                      error(e) < [1 + 2^{4+Exp(x)-Exp(a)-Exp(e)}
	                                    + 2^{4+Exp(x)-Exp(c)-Exp(e)}] ulp(e)
	                      because |atan(u)| < |u|
	                               < [1 + 2^{5+Exp(x)-min(Exp(a),Exp(c))
	                                         -Exp(e)}] ulp(e)
	   f = e/2            exact
	*/
	/* p: working precision */
	if v21 = op_im_exp > 0; !v21 {
		if op_im_exp >= 0 {
			v20 = op_im_exp
		} else {
			v20 = -op_im_exp
		}
	}
	if v21 || prec > v20 {
		v19 = prec
	} else {
		v19 = prec - op_im_exp
	}
	p1 = v19
	if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			libmpfr.Xmpfr_set_erangeflag(tls)
		}
		v23 = libc.Int32FromInt32(0)
	} else {
		v23 = (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_sign
	}
	if v23 > 0 {
		v22 = int32(_MPFR_RNDD)
	} else {
		v22 = int32(_MPFR_RNDU)
	}
	rnd1 = v22
	if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			libmpfr.Xmpfr_set_erangeflag(tls)
		}
		v26 = libc.Int32FromInt32(0)
	} else {
		v26 = (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_sign
	}
	if v26 < 0 {
		v25 = int32(_MPFR_RNDU)
	} else {
		v25 = int32(_MPFR_RNDD)
	}
	rnd2 = v25
	for cond := true; cond; cond = ok1 == 0 {
		p1 += Xmpc_ceil_log2(tls, p1) + int64(2)
		libmpfr.Xmpfr_set_prec(tls, bp+64, p1)
		libmpfr.Xmpfr_set_prec(tls, bp+96, p1)
		libmpfr.Xmpfr_set_prec(tls, bp+128, p1)
		/* x = upper bound for atan (x/(1-y)). Since atan is increasing, we
		   need an upper bound on x/(1-y), i.e., a lower bound on 1-y for
		   x positive, and an upper bound on 1-y for x negative */
		libmpfr.Xmpfr_ui_sub(tls, bp+64, uint64(1), op+32, rnd1)
		if (*t__mpfr_struct)(unsafe.Pointer(bp+64)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			if (*t__mpfr_struct)(unsafe.Pointer(bp+64)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				libmpfr.Xmpfr_set_erangeflag(tls)
			}
			v28 = libc.Int32FromInt32(0)
		} else {
			v28 = (*t__mpfr_struct)(unsafe.Pointer(bp + 64)).F_mpfr_sign
		}
		if v28 == 0 { /* y is near 1, thus 1+y is near 2, and
			   expo will be 1 or 2 below */
			/* check for intermediate underflow */
			err1 = int64(2) /* ensures err will be expo below */
		} else {
			err1 = (*t__mpfr_struct)(unsafe.Pointer(bp + 64)).F_mpfr_exp
		} /* err = Exp(a) with the notations above */
		libmpfr.Xmpfr_atan2(tls, bp+128, op, bp+64, int32(_MPFR_RNDU))
		/* b = lower bound for atan (-x/(1+y)): for x negative, we need a
		   lower bound on -x/(1+y), i.e., an upper bound on 1+y */
		libmpfr.Xmpfr_add_ui(tls, bp+64, op+32, uint64(1), rnd2)
		/* if a is exactly zero, i.e., Im(op) = -1, then the error on a is 0,
		   and we can simply ignore the terms involving Exp(a) in the error */
		if (*t__mpfr_struct)(unsafe.Pointer(bp+64)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			if (*t__mpfr_struct)(unsafe.Pointer(bp+64)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				libmpfr.Xmpfr_set_erangeflag(tls)
			}
			v30 = libc.Int32FromInt32(0)
		} else {
			v30 = (*t__mpfr_struct)(unsafe.Pointer(bp + 64)).F_mpfr_sign
		}
		if v30 == 0 {
			/* check for intermediate underflow */
			expo = err1 /* will leave err unchanged below */
		} else {
			expo = (*t__mpfr_struct)(unsafe.Pointer(bp + 64)).F_mpfr_exp
		} /* expo = Exp(c) with the notations above */
		libmpfr.Xmpfr_atan2(tls, bp+96, bp+192, bp+64, int32(_MPFR_RNDD))
		if err1 < expo {
			v32 = err1
		} else {
			v32 = expo
		}
		err1 = v32 /* err = min(Exp(a),Exp(c)) */
		libmpfr.Xmpfr_sub(tls, bp+128, bp+128, bp+96, int32(_MPFR_RNDU))
		err1 = int64(5) + op_re_exp - err1 - (*t__mpfr_struct)(unsafe.Pointer(bp+128)).F_mpfr_exp
		/* error is bounded by [1 + 2^err] ulp(e) */
		if err1 < 0 {
			v33 = int64(1)
		} else {
			v33 = err1 + int64(1)
		}
		err1 = v33
		libmpfr.Xmpfr_div_2ui(tls, bp+128, bp+128, uint64(1), int32(_MPFR_RNDU))
		/* Note: using RND2=RNDD guarantees that if x is exactly representable
		   on prec + ... bits, mpfr_can_round will return 0 */
		ok1 = libmpfr.Xmpfr_can_round(tls, bp+128, p1-err1, int32(_MPFR_RNDU), int32(_MPFR_RNDD), prec+libc.BoolInt64(rnd&libc.Int32FromInt32(0x0F) == int32(_MPFR_RNDN)))
	}
	/* Imaginary part
	   Im(atan(x+I*y)) = 1/4 * [log(x^2+(1+y)^2) - log (x^2 +(1-y)^2)] */
	prec = (*t__mpfr_struct)(unsafe.Pointer(rop + 32)).F_mpfr_prec /* result precision */
	/* a = o(1+y)    error(a) < 1 ulp(a)
	   b = o(a^2)    error(b) < 5 ulp(b)
	   c = o(x^2)    error(c) < 1 ulp(c)
	   d = o(b+c)    error(d) < 7 ulp(d)
	   e = o(log(d)) error(e) < [1 + 7*2^{2-Exp(e)}] ulp(e) = ke ulp(e)
	   f = o(1-y)    error(f) < 1 ulp(f)
	   g = o(f^2)    error(g) < 5 ulp(g)
	   h = o(c+f)    error(h) < 7 ulp(h)
	   i = o(log(h)) error(i) < [1 + 7*2^{2-Exp(i)}] ulp(i) = ki ulp(i)
	   j = o(e-i)    error(j) < [1 + ke*2^{Exp(e)-Exp(j)}
	                               + ki*2^{Exp(i)-Exp(j)}] ulp(j)
	                 error(j) < [1 + 2^{Exp(e)-Exp(j)} + 2^{Exp(i)-Exp(j)}
	                               + 7*2^{3-Exp(j)}] ulp(j)
	                          < [1 + 2^{max(Exp(e),Exp(i))-Exp(j)+1}
	                               + 7*2^{3-Exp(j)}] ulp(j)
	   k = j/4       exact
	*/
	err1 = int64(2)
	p1 = prec /* working precision */
	for cond := true; cond; cond = ok1 == 0 {
		p1 += Xmpc_ceil_log2(tls, p1) + err1
		libmpfr.Xmpfr_set_prec(tls, bp+64, p1)
		libmpfr.Xmpfr_set_prec(tls, bp+96, p1)
		libmpfr.Xmpfr_set_prec(tls, bp+160, p1)
		/* a = upper bound for log(x^2 + (1+y)^2) */
		libmpfr.Xmpfr_add_ui(tls, bp+64, op+32, uint64(1), int32(_MPFR_RNDA))
		libmpfr.Xmpfr_sqr(tls, bp+64, bp+64, int32(_MPFR_RNDU))
		libmpfr.Xmpfr_sqr(tls, bp+160, op, int32(_MPFR_RNDU))
		libmpfr.Xmpfr_add(tls, bp+64, bp+64, bp+160, int32(_MPFR_RNDU))
		libmpfr.Xmpfr_log(tls, bp+64, bp+64, int32(_MPFR_RNDU))
		/* b = lower bound for log(x^2 + (1-y)^2) */
		libmpfr.Xmpfr_ui_sub(tls, bp+96, uint64(1), op+32, int32(_MPFR_RNDZ)) /* round to zero */
		libmpfr.Xmpfr_sqr(tls, bp+96, bp+96, int32(_MPFR_RNDZ))
		/* we could write mpfr_sqr (y, mpc_realref (op), MPFR_RNDZ) but it is
		   more efficient to reuse the value of y (x^2) above and subtract
		   one ulp */
		libmpfr.Xmpfr_nextbelow(tls, bp+160)
		libmpfr.Xmpfr_add(tls, bp+96, bp+96, bp+160, int32(_MPFR_RNDZ))
		libmpfr.Xmpfr_log(tls, bp+96, bp+96, int32(_MPFR_RNDZ))
		libmpfr.Xmpfr_sub(tls, bp+160, bp+64, bp+96, int32(_MPFR_RNDU))
		if (*t__mpfr_struct)(unsafe.Pointer(bp+160)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			/* FIXME: happens when x and y have very different magnitudes;
			   could be handled more efficiently                           */
			ok1 = 0
		} else {
			if (*t__mpfr_struct)(unsafe.Pointer(bp+64)).F_mpfr_exp > (*t__mpfr_struct)(unsafe.Pointer(bp+96)).F_mpfr_exp {
				v34 = (*t__mpfr_struct)(unsafe.Pointer(bp + 64)).F_mpfr_exp
			} else {
				v34 = (*t__mpfr_struct)(unsafe.Pointer(bp + 96)).F_mpfr_exp
			}
			expo = v34
			expo = expo - (*t__mpfr_struct)(unsafe.Pointer(bp+160)).F_mpfr_exp + int64(1)
			err1 = int64(3) - (*t__mpfr_struct)(unsafe.Pointer(bp+160)).F_mpfr_exp
			/* error(j) <= [1 + 2^expo + 7*2^err] ulp(j) */
			if expo <= err1 { /* error(j) <= [1 + 2^{err+1}] ulp(j) */
				if err1 < 0 {
					v35 = int64(1)
				} else {
					v35 = err1 + int64(2)
				}
				err1 = v35
			} else {
				if expo < 0 {
					v36 = int64(1)
				} else {
					v36 = expo + int64(2)
				}
				err1 = v36
			}
			libmpfr.Xmpfr_div_2ui(tls, bp+160, bp+160, uint64(2), int32(_MPFR_RNDN))
			/* FIXME: underflow. Since the main term of the Taylor series
			   in y=0 is 1/(x^2+1) * y, this means that y is very small
			   and/or x very large; but then the mpfr_zero_p (y) above
			   should be true. This needs a proof, or better yet,
			   special code.                                              */
			ok1 = libmpfr.Xmpfr_can_round(tls, bp+160, p1-err1, int32(_MPFR_RNDU), int32(_MPFR_RNDD), prec+libc.BoolInt64(rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDN)))
		}
	}
	inex = Xmpc_set_fr_fr(tls, rop, bp+128, bp+160, rnd)
	libmpfr.Xmpfr_clears(tls, bp+64, libc.VaList(bp+232, bp+96, bp+128, bp+160, libc.UintptrFromInt32(0)))
	/* restore the exponent range, and check the range of results */
	libmpfr.Xmpfr_set_emin(tls, saved_emin)
	libmpfr.Xmpfr_set_emax(tls, saved_emax)
	if inex&int32(3) == int32(2) {
		v37 = -int32(1)
	} else {
		if inex&int32(3) == 0 {
			v38 = 0
		} else {
			v38 = int32(1)
		}
		v37 = v38
	}
	inex_re = libmpfr.Xmpfr_check_range(tls, rop, v37, rnd&libc.Int32FromInt32(0x0F))
	if inex>>int32(2) == int32(2) {
		v39 = -int32(1)
	} else {
		if inex>>int32(2) == 0 {
			v40 = 0
		} else {
			v40 = int32(1)
		}
		v39 = v40
	}
	inex_im = libmpfr.Xmpfr_check_range(tls, rop+32, v39, rnd>>libc.Int32FromInt32(4))
	if inex_re < 0 {
		v41 = int32(2)
	} else {
		if inex_re == 0 {
			v42 = 0
		} else {
			v42 = int32(1)
		}
		v41 = v42
	}
	if inex_im < 0 {
		v43 = int32(2)
	} else {
		if inex_im == 0 {
			v44 = 0
		} else {
			v44 = int32(1)
		}
		v43 = v44
	}
	return v41 | v43<<int32(2)
	return r
}

func Xmpc_atanh(tls *libc.TLS, rop Tmpc_ptr, op Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(128)
	defer tls.Free(128)
	var inex, v1, v10, v11, v12, v13, v14, v2, v3, v4, v5, v6, v7, v8, v9 int32
	var tmp Tmpfr_t
	var _ /* a at bp+64 */ Tmpc_t
	var _ /* z at bp+0 */ Tmpc_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = inex, tmp, v1, v10, v11, v12, v13, v14, v2, v3, v4, v5, v6, v7, v8, v9
	*(*t__mpfr_struct)(unsafe.Pointer(bp)) = *(*t__mpfr_struct)(unsafe.Pointer(op + 32))
	*(*t__mpfr_struct)(unsafe.Pointer(bp + 32)) = *(*t__mpfr_struct)(unsafe.Pointer(op))
	libmpfr.Xmpfr_neg(tls, bp, bp, int32(_MPFR_RNDN))
	/* Note reversal of precisions due to later multiplication by -i */
	Xmpc_init3(tls, bp+64, (*t__mpfr_struct)(unsafe.Pointer(rop+32)).F_mpfr_prec, (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec)
	if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDU) {
		v1 = int32(_MPFR_RNDD)
	} else {
		if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDD) {
			v2 = int32(_MPFR_RNDU)
		} else {
			v2 = rnd >> libc.Int32FromInt32(4)
		}
		v1 = v2
	}
	inex = Xmpc_atan(tls, bp+64, bp, v1+rnd&libc.Int32FromInt32(0x0F)<<libc.Int32FromInt32(4))
	/* change a to -i*a, i.e., x+i*y to y-i*x */
	tmp[0] = *(*t__mpfr_struct)(unsafe.Pointer(bp + 64))
	*(*t__mpfr_struct)(unsafe.Pointer(bp + 64)) = *(*t__mpfr_struct)(unsafe.Pointer(bp + 64 + 32))
	*(*t__mpfr_struct)(unsafe.Pointer(bp + 64 + 32)) = tmp[0]
	libmpfr.Xmpfr_neg(tls, bp+64+32, bp+64+32, int32(_MPFR_RNDN))
	Xmpc_set(tls, rop, bp+64, rnd)
	Xmpc_clear(tls, bp+64)
	if inex>>int32(2) == int32(2) {
		v4 = -int32(1)
	} else {
		if inex>>int32(2) == 0 {
			v5 = 0
		} else {
			v5 = int32(1)
		}
		v4 = v5
	}
	if v4 < 0 {
		v3 = int32(2)
	} else {
		if inex>>int32(2) == int32(2) {
			v7 = -int32(1)
		} else {
			if inex>>int32(2) == 0 {
				v8 = 0
			} else {
				v8 = int32(1)
			}
			v7 = v8
		}
		if v7 == 0 {
			v6 = 0
		} else {
			v6 = int32(1)
		}
		v3 = v6
	}
	if inex&int32(3) == int32(2) {
		v10 = -int32(1)
	} else {
		if inex&int32(3) == 0 {
			v11 = 0
		} else {
			v11 = int32(1)
		}
		v10 = v11
	}
	if -v10 < 0 {
		v9 = int32(2)
	} else {
		if inex&int32(3) == int32(2) {
			v13 = -int32(1)
		} else {
			if inex&int32(3) == 0 {
				v14 = 0
			} else {
				v14 = int32(1)
			}
			v13 = v14
		}
		if -v13 == 0 {
			v12 = 0
		} else {
			v12 = int32(1)
		}
		v9 = v12
	}
	return v3 | v9<<int32(2)
}

func Xmpc_clear(tls *libc.TLS, x Tmpc_ptr) {
	libmpfr.Xmpfr_clear(tls, x)
	libmpfr.Xmpfr_clear(tls, x+32)
}

// C documentation
//
//	/* return 0 iff a = b */
func Xmpc_cmp(tls *libc.TLS, a Tmpc_srcptr, b Tmpc_srcptr) (r int32) {
	var cmp_im, cmp_re, v1, v2, v3, v4 int32
	_, _, _, _, _, _ = cmp_im, cmp_re, v1, v2, v3, v4
	cmp_re = libmpfr.Xmpfr_cmp3(tls, a, b, int32(1))
	cmp_im = libmpfr.Xmpfr_cmp3(tls, a+32, b+32, int32(1))
	if cmp_re < 0 {
		v1 = int32(2)
	} else {
		if cmp_re == 0 {
			v2 = 0
		} else {
			v2 = int32(1)
		}
		v1 = v2
	}
	if cmp_im < 0 {
		v3 = int32(2)
	} else {
		if cmp_im == 0 {
			v4 = 0
		} else {
			v4 = int32(1)
		}
		v3 = v4
	}
	return v1 | v3<<int32(2)
}

// C documentation
//
//	/* return mpfr_cmp (mpc_abs (a), mpc_abs (b)) */
func Xmpc_cmp_abs(tls *libc.TLS, a Tmpc_srcptr, b Tmpc_srcptr) (r int32) {
	bp := tls.Alloc(224)
	defer tls.Free(224)
	var inex1, inex2, ret int32
	var prec Tmpfr_prec_t
	var v1, v10, v11, v2, v3, v4, v5, v6, v7, v8, v9 int64
	var _ /* n1 at bp+128 */ Tmpfr_t
	var _ /* n2 at bp+160 */ Tmpfr_t
	var _ /* nan at bp+192 */ Tmpfr_t
	var _ /* z1 at bp+0 */ Tmpc_t
	var _ /* z2 at bp+64 */ Tmpc_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = inex1, inex2, prec, ret, v1, v10, v11, v2, v3, v4, v5, v6, v7, v8, v9
	/* Handle numbers containing one NaN as mpfr_cmp. */
	if (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(a+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(b)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(b+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		libmpfr.Xmpfr_init(tls, bp+192)
		libmpfr.Xmpfr_set_nan(tls, bp+192)
		ret = libmpfr.Xmpfr_cmp3(tls, bp+192, bp+192, int32(1))
		libmpfr.Xmpfr_clear(tls, bp+192)
		return ret
	}
	/* Handle infinities. */
	if (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(a+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		if (*t__mpfr_struct)(unsafe.Pointer(b)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(b+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			return 0
		} else {
			return int32(1)
		}
	} else {
		if (*t__mpfr_struct)(unsafe.Pointer(b)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(b+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			return -int32(1)
		}
	}
	/* Replace all parts of a and b by their absolute values, then order
	   them by size. */
	(*(*Tmpc_t)(unsafe.Pointer(bp)))[0] = *(*t__mpc_struct)(unsafe.Pointer(a))
	(*(*Tmpc_t)(unsafe.Pointer(bp + 64)))[0] = *(*t__mpc_struct)(unsafe.Pointer(b))
	if (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_sign < 0 {
		libmpfr.Xmpfr_neg(tls, bp, bp, int32(_MPFR_RNDN))
	}
	if (*t__mpfr_struct)(unsafe.Pointer(a+32)).F_mpfr_sign < 0 {
		libmpfr.Xmpfr_neg(tls, bp+32, bp+32, int32(_MPFR_RNDN))
	}
	if (*t__mpfr_struct)(unsafe.Pointer(b)).F_mpfr_sign < 0 {
		libmpfr.Xmpfr_neg(tls, bp+64, bp+64, int32(_MPFR_RNDN))
	}
	if (*t__mpfr_struct)(unsafe.Pointer(b+32)).F_mpfr_sign < 0 {
		libmpfr.Xmpfr_neg(tls, bp+64+32, bp+64+32, int32(_MPFR_RNDN))
	}
	if libmpfr.Xmpfr_cmp3(tls, bp, bp+32, int32(1)) > 0 {
		libmpfr.Xmpfr_swap(tls, bp, bp+32)
	}
	if libmpfr.Xmpfr_cmp3(tls, bp+64, bp+64+32, int32(1)) > 0 {
		libmpfr.Xmpfr_swap(tls, bp+64, bp+64+32)
	}
	/* Handle cases in which only one part differs. */
	if libmpfr.Xmpfr_cmp3(tls, bp, bp+64, int32(1)) == 0 {
		return libmpfr.Xmpfr_cmp3(tls, bp+32, bp+64+32, int32(1))
	}
	if libmpfr.Xmpfr_cmp3(tls, bp+32, bp+64+32, int32(1)) == 0 {
		return libmpfr.Xmpfr_cmp3(tls, bp, bp+64, int32(1))
	}
	/* Implement the algorithm in algorithms.tex. */
	libmpfr.Xmpfr_init(tls, bp+128)
	libmpfr.Xmpfr_init(tls, bp+160)
	if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_prec {
		v3 = (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_prec
	} else {
		v3 = (*t__mpfr_struct)(unsafe.Pointer(bp + 32)).F_mpfr_prec
	}
	if (*t__mpfr_struct)(unsafe.Pointer(bp+64)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(bp+64+32)).F_mpfr_prec {
		v4 = (*t__mpfr_struct)(unsafe.Pointer(bp + 64)).F_mpfr_prec
	} else {
		v4 = (*t__mpfr_struct)(unsafe.Pointer(bp + 64 + 32)).F_mpfr_prec
	}
	if v3 > v4 {
		if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_prec {
			v5 = (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_prec
		} else {
			v5 = (*t__mpfr_struct)(unsafe.Pointer(bp + 32)).F_mpfr_prec
		}
		v2 = v5
	} else {
		if (*t__mpfr_struct)(unsafe.Pointer(bp+64)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(bp+64+32)).F_mpfr_prec {
			v6 = (*t__mpfr_struct)(unsafe.Pointer(bp + 64)).F_mpfr_prec
		} else {
			v6 = (*t__mpfr_struct)(unsafe.Pointer(bp + 64 + 32)).F_mpfr_prec
		}
		v2 = v6
	}
	if int64(libc.Int32FromInt32(50)) > v2/int64(100) {
		v1 = int64(libc.Int32FromInt32(50))
	} else {
		if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_prec {
			v8 = (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_prec
		} else {
			v8 = (*t__mpfr_struct)(unsafe.Pointer(bp + 32)).F_mpfr_prec
		}
		if (*t__mpfr_struct)(unsafe.Pointer(bp+64)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(bp+64+32)).F_mpfr_prec {
			v9 = (*t__mpfr_struct)(unsafe.Pointer(bp + 64)).F_mpfr_prec
		} else {
			v9 = (*t__mpfr_struct)(unsafe.Pointer(bp + 64 + 32)).F_mpfr_prec
		}
		if v8 > v9 {
			if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_prec {
				v10 = (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_prec
			} else {
				v10 = (*t__mpfr_struct)(unsafe.Pointer(bp + 32)).F_mpfr_prec
			}
			v7 = v10
		} else {
			if (*t__mpfr_struct)(unsafe.Pointer(bp+64)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(bp+64+32)).F_mpfr_prec {
				v11 = (*t__mpfr_struct)(unsafe.Pointer(bp + 64)).F_mpfr_prec
			} else {
				v11 = (*t__mpfr_struct)(unsafe.Pointer(bp + 64 + 32)).F_mpfr_prec
			}
			v7 = v11
		}
		v1 = v7 / int64(100)
	}
	prec = v1
	for cond := true; cond; cond = int32(1) != 0 {
		libmpfr.Xmpfr_set_prec(tls, bp+128, prec)
		libmpfr.Xmpfr_set_prec(tls, bp+160, prec)
		inex1 = Xmpc_norm(tls, bp+128, bp, int32(_MPFR_RNDD))
		inex2 = Xmpc_norm(tls, bp+160, bp+64, int32(_MPFR_RNDD))
		ret = libmpfr.Xmpfr_cmp3(tls, bp+128, bp+160, int32(1))
		if ret != 0 {
			goto end
		} else {
			if inex1 == 0 { /* n1 = norm(z1) */
				if inex2 != 0 { /* n2 < norm(z2) */
					ret = -int32(1)
					goto end
				} else { /* n2 = norm(z2) */
					ret = 0
					goto end
				}
			} else { /* n1 < norm(z1) */
				if inex2 == 0 {
					ret = int32(1)
					goto end
				}
			}
		}
		prec *= int64(2)
	}
	goto end
end:
	;
	libmpfr.Xmpfr_clear(tls, bp+128)
	libmpfr.Xmpfr_clear(tls, bp+160)
	return ret
}

// C documentation
//
//	/* return 0 iff a = b */
func Xmpc_cmp_si_si(tls *libc.TLS, a Tmpc_srcptr, b int64, c int64) (r int32) {
	var cmp_im, cmp_re, v1, v2, v3, v4 int32
	_, _, _, _, _, _ = cmp_im, cmp_re, v1, v2, v3, v4
	cmp_re = libmpfr.Xmpfr_cmp_si_2exp(tls, a, b, 0)
	cmp_im = libmpfr.Xmpfr_cmp_si_2exp(tls, a+32, c, 0)
	if cmp_re < 0 {
		v1 = int32(2)
	} else {
		if cmp_re == 0 {
			v2 = 0
		} else {
			v2 = int32(1)
		}
		v1 = v2
	}
	if cmp_im < 0 {
		v3 = int32(2)
	} else {
		if cmp_im == 0 {
			v4 = 0
		} else {
			v4 = int32(1)
		}
		v3 = v4
	}
	return v1 | v3<<int32(2)
}

func Xmpc_conj(tls *libc.TLS, a Tmpc_ptr, b Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	var _p Tmpfr_srcptr
	var inex_im, inex_re, v1, v2, v3, v4, v5 int32
	_, _, _, _, _, _, _, _ = _p, inex_im, inex_re, v1, v2, v3, v4, v5
	{
		_p = b
		v1 = libmpfr.Xmpfr_set4(tls, a, _p, rnd&libc.Int32FromInt32(0x0F), (*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign)
	}
	inex_re = v1
	inex_im = libmpfr.Xmpfr_neg(tls, a+32, b+32, rnd>>libc.Int32FromInt32(4))
	if inex_re < 0 {
		v2 = int32(2)
	} else {
		if inex_re == 0 {
			v3 = 0
		} else {
			v3 = int32(1)
		}
		v2 = v3
	}
	if inex_im < 0 {
		v4 = int32(2)
	} else {
		if inex_im == 0 {
			v5 = 0
		} else {
			v5 = int32(1)
		}
		v4 = v5
	}
	return v2 | v4<<int32(2)
}

func Xmpc_cos(tls *libc.TLS, rop Tmpc_ptr, op Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	return Xmpc_sin_cos(tls, libc.UintptrFromInt32(0), rop, op, 0, rnd) >> int32(4)
}

func Xmpc_cosh(tls *libc.TLS, rop Tmpc_ptr, op Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var _ /* z at bp+0 */ Tmpc_t
	/* z = i*op without copying significand */
	*(*t__mpfr_struct)(unsafe.Pointer(bp)) = *(*t__mpfr_struct)(unsafe.Pointer(op + 32))
	*(*t__mpfr_struct)(unsafe.Pointer(bp + 32)) = *(*t__mpfr_struct)(unsafe.Pointer(op))
	libmpfr.Xmpfr_neg(tls, bp, bp, int32(_MPFR_RNDN))
	return Xmpc_cos(tls, rop, bp, rnd)
}

func Xmpc_div_2si(tls *libc.TLS, a Tmpc_ptr, b Tmpc_srcptr, c int64, rnd Tmpc_rnd_t) (r int32) {
	var inex_im, inex_re, v1, v2, v3, v4 int32
	_, _, _, _, _, _ = inex_im, inex_re, v1, v2, v3, v4
	inex_re = libmpfr.Xmpfr_div_2si(tls, a, b, c, rnd&libc.Int32FromInt32(0x0F))
	inex_im = libmpfr.Xmpfr_div_2si(tls, a+32, b+32, c, rnd>>libc.Int32FromInt32(4))
	if inex_re < 0 {
		v1 = int32(2)
	} else {
		if inex_re == 0 {
			v2 = 0
		} else {
			v2 = int32(1)
		}
		v1 = v2
	}
	if inex_im < 0 {
		v3 = int32(2)
	} else {
		if inex_im == 0 {
			v4 = 0
		} else {
			v4 = int32(1)
		}
		v3 = v4
	}
	return v1 | v3<<int32(2)
}

func Xmpc_div_2ui(tls *libc.TLS, a Tmpc_ptr, b Tmpc_srcptr, c uint64, rnd Tmpc_rnd_t) (r int32) {
	var inex_im, inex_re, v1, v2, v3, v4 int32
	_, _, _, _, _, _ = inex_im, inex_re, v1, v2, v3, v4
	inex_re = libmpfr.Xmpfr_div_2ui(tls, a, b, c, rnd&libc.Int32FromInt32(0x0F))
	inex_im = libmpfr.Xmpfr_div_2ui(tls, a+32, b+32, c, rnd>>libc.Int32FromInt32(4))
	if inex_re < 0 {
		v1 = int32(2)
	} else {
		if inex_re == 0 {
			v2 = 0
		} else {
			v2 = int32(1)
		}
		v1 = v2
	}
	if inex_im < 0 {
		v3 = int32(2)
	} else {
		if inex_im == 0 {
			v4 = 0
		} else {
			v4 = int32(1)
		}
		v3 = v4
	}
	return v1 | v3<<int32(2)
}

// C documentation
//
//	/* this routine deals with the case where w is zero */
func _mpc_div_zero(tls *libc.TLS, a Tmpc_ptr, z Tmpc_srcptr, w Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	/* Assumes w==0, implementation according to C99 G.5.1.8 */
	var sign, v1 int32
	var _ /* infty at bp+0 */ Tmpfr_t
	_, _ = sign, v1
	if (*t__mpfr_struct)(unsafe.Pointer(w)).F_mpfr_sign < 0 {
		v1 = -int32(1)
	} else {
		v1 = int32(1)
	}
	sign = v1
	libmpfr.Xmpfr_init2(tls, bp, int64(m_MPFR_PREC_MIN))
	libmpfr.Xmpfr_set_inf(tls, bp, sign)
	libmpfr.Xmpfr_mul(tls, a, bp, z, rnd&libc.Int32FromInt32(0x0F))
	libmpfr.Xmpfr_mul(tls, a+32, bp, z+32, rnd>>libc.Int32FromInt32(4))
	libmpfr.Xmpfr_clear(tls, bp)
	return libc.Int32FromInt32(0) | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2) /* exact */
}

// C documentation
//
//	/* this routine deals with the case where z is infinite and w finite */
func _mpc_div_inf_fin(tls *libc.TLS, rop Tmpc_ptr, z Tmpc_srcptr, w Tmpc_srcptr) (r int32) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	/* Assumes w finite and non-zero and z infinite; implementation
	   according to C99 G.5.1.8                                     */
	var a, b, x, y, v1, v10, v11, v12, v13, v14, v15, v16, v17, v18, v19, v2, v20, v21, v22, v23, v24, v25, v26, v27, v28, v3, v4, v5, v6, v7, v8, v9 int32
	var _ /* sign at bp+0 */ Tmpfr_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = a, b, x, y, v1, v10, v11, v12, v13, v14, v15, v16, v17, v18, v19, v2, v20, v21, v22, v23, v24, v25, v26, v27, v28, v3, v4, v5, v6, v7, v8, v9
	if (*t__mpfr_struct)(unsafe.Pointer(z)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		if (*t__mpfr_struct)(unsafe.Pointer(z)).F_mpfr_sign < 0 {
			v2 = -int32(1)
		} else {
			v2 = int32(1)
		}
		v1 = v2
	} else {
		v1 = 0
	}
	a = v1
	if (*t__mpfr_struct)(unsafe.Pointer(z+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		if (*t__mpfr_struct)(unsafe.Pointer(z+32)).F_mpfr_sign < 0 {
			v4 = -int32(1)
		} else {
			v4 = int32(1)
		}
		v3 = v4
	} else {
		v3 = 0
	}
	b = v3
	/* a is -1 if Re(z) = -Inf, 1 if Re(z) = +Inf, 0 if Re(z) is finite
	   b is -1 if Im(z) = -Inf, 1 if Im(z) = +Inf, 0 if Im(z) is finite */
	/* x = MPC_MPFR_SIGN (a * mpc_realref (w) + b * mpc_imagref (w)) */
	/* y = MPC_MPFR_SIGN (b * mpc_realref (w) - a * mpc_imagref (w)) */
	if a == 0 || b == 0 {
		/* only one of a or b can be zero, since z is infinite */
		if (*t__mpfr_struct)(unsafe.Pointer(w)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			v5 = 0
		} else {
			if (*t__mpfr_struct)(unsafe.Pointer(w)).F_mpfr_sign < 0 {
				v6 = -int32(1)
			} else {
				v6 = int32(1)
			}
			v5 = v6
		}
		if (*t__mpfr_struct)(unsafe.Pointer(w+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			v7 = 0
		} else {
			if (*t__mpfr_struct)(unsafe.Pointer(w+32)).F_mpfr_sign < 0 {
				v8 = -int32(1)
			} else {
				v8 = int32(1)
			}
			v7 = v8
		}
		x = a*v5 + b*v7
		if (*t__mpfr_struct)(unsafe.Pointer(w)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			v9 = 0
		} else {
			if (*t__mpfr_struct)(unsafe.Pointer(w)).F_mpfr_sign < 0 {
				v10 = -int32(1)
			} else {
				v10 = int32(1)
			}
			v9 = v10
		}
		if (*t__mpfr_struct)(unsafe.Pointer(w+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			v11 = 0
		} else {
			if (*t__mpfr_struct)(unsafe.Pointer(w+32)).F_mpfr_sign < 0 {
				v12 = -int32(1)
			} else {
				v12 = int32(1)
			}
			v11 = v12
		}
		y = b*v9 - a*v11
	} else {
		libmpfr.Xmpfr_init2(tls, bp, int64(2))
		/* This is enough to determine the sign of sums and differences. */
		if a == int32(1) {
			if b == int32(1) {
				libmpfr.Xmpfr_add(tls, bp, w, w+32, int32(_MPFR_RNDN))
				if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
					v13 = 0
				} else {
					if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_sign < 0 {
						v14 = -int32(1)
					} else {
						v14 = int32(1)
					}
					v13 = v14
				}
				x = v13
				libmpfr.Xmpfr_sub(tls, bp, w, w+32, int32(_MPFR_RNDN))
				if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
					v15 = 0
				} else {
					if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_sign < 0 {
						v16 = -int32(1)
					} else {
						v16 = int32(1)
					}
					v15 = v16
				}
				y = v15
			} else { /* b == -1 */
				libmpfr.Xmpfr_sub(tls, bp, w, w+32, int32(_MPFR_RNDN))
				if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
					v17 = 0
				} else {
					if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_sign < 0 {
						v18 = -int32(1)
					} else {
						v18 = int32(1)
					}
					v17 = v18
				}
				x = v17
				libmpfr.Xmpfr_add(tls, bp, w, w+32, int32(_MPFR_RNDN))
				if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
					v19 = 0
				} else {
					if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_sign < 0 {
						v20 = -int32(1)
					} else {
						v20 = int32(1)
					}
					v19 = v20
				}
				y = -v19
			}
		} else { /* a == -1 */
			if b == int32(1) {
				libmpfr.Xmpfr_sub(tls, bp, w+32, w, int32(_MPFR_RNDN))
				if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
					v21 = 0
				} else {
					if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_sign < 0 {
						v22 = -int32(1)
					} else {
						v22 = int32(1)
					}
					v21 = v22
				}
				x = v21
				libmpfr.Xmpfr_add(tls, bp, w, w+32, int32(_MPFR_RNDN))
				if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
					v23 = 0
				} else {
					if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_sign < 0 {
						v24 = -int32(1)
					} else {
						v24 = int32(1)
					}
					v23 = v24
				}
				y = v23
			} else { /* b == -1 */
				libmpfr.Xmpfr_add(tls, bp, w, w+32, int32(_MPFR_RNDN))
				if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
					v25 = 0
				} else {
					if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_sign < 0 {
						v26 = -int32(1)
					} else {
						v26 = int32(1)
					}
					v25 = v26
				}
				x = -v25
				libmpfr.Xmpfr_sub(tls, bp, w+32, w, int32(_MPFR_RNDN))
				if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
					v27 = 0
				} else {
					if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_sign < 0 {
						v28 = -int32(1)
					} else {
						v28 = int32(1)
					}
					v27 = v28
				}
				y = v27
			}
		}
		libmpfr.Xmpfr_clear(tls, bp)
	}
	if x == 0 {
		libmpfr.Xmpfr_set_nan(tls, rop)
	} else {
		libmpfr.Xmpfr_set_inf(tls, rop, x)
	}
	if y == 0 {
		libmpfr.Xmpfr_set_nan(tls, rop+32)
	} else {
		libmpfr.Xmpfr_set_inf(tls, rop+32, y)
	}
	return libc.Int32FromInt32(0) | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2) /* exact */
}

// C documentation
//
//	/* this routine deals with the case where z if finite and w infinite */
func _mpc_div_fin_inf(tls *libc.TLS, rop Tmpc_ptr, z Tmpc_srcptr, w Tmpc_srcptr) (r int32) {
	bp := tls.Alloc(224)
	defer tls.Free(224)
	/* Assumes z finite and w infinite; implementation according to
	   C99 G.5.1.8                                                  */
	var _p Tmpfr_ptr
	var v1, v2, v4 int32
	var _ /* a at bp+64 */ Tmpfr_t
	var _ /* b at bp+96 */ Tmpfr_t
	var _ /* c at bp+0 */ Tmpfr_t
	var _ /* d at bp+32 */ Tmpfr_t
	var _ /* x at bp+128 */ Tmpfr_t
	var _ /* y at bp+160 */ Tmpfr_t
	var _ /* zero at bp+192 */ Tmpfr_t
	_, _, _, _ = _p, v1, v2, v4
	libmpfr.Xmpfr_init2(tls, bp, int64(2)) /* needed to hold a signed zero, +1 or -1 */
	libmpfr.Xmpfr_init2(tls, bp+32, int64(2))
	libmpfr.Xmpfr_init2(tls, bp+128, int64(2))
	libmpfr.Xmpfr_init2(tls, bp+160, int64(2))
	libmpfr.Xmpfr_init2(tls, bp+192, int64(2))
	{
		_p = bp + 192
		(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign = int32(1)
		(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
		_ = int32(_MPFR_RNDN)
		v1 = 0
	}
	_ = v1
	libmpfr.Xmpfr_init2(tls, bp+64, (*t__mpfr_struct)(unsafe.Pointer(z)).F_mpfr_prec)
	libmpfr.Xmpfr_init2(tls, bp+96, (*t__mpfr_struct)(unsafe.Pointer(z+32)).F_mpfr_prec)
	if (*t__mpfr_struct)(unsafe.Pointer(w)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		v2 = int32(1)
	} else {
		v2 = 0
	}
	_ = libmpfr.Xmpfr_set_ui_2exp(tls, bp, libc.Uint64FromInt32(v2), 0, int32(_MPFR_RNDN))
	if (*t__mpfr_struct)(unsafe.Pointer(w)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		libmpfr.Xmpfr_set4(tls, bp, bp, int32(_MPFR_RNDN), int32(1))
	} else {
		libmpfr.Xmpfr_set4(tls, bp, bp, int32(_MPFR_RNDN), (*t__mpfr_struct)(unsafe.Pointer(w)).F_mpfr_sign)
	}
	if (*t__mpfr_struct)(unsafe.Pointer(w+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		v4 = int32(1)
	} else {
		v4 = 0
	}
	_ = libmpfr.Xmpfr_set_ui_2exp(tls, bp+32, libc.Uint64FromInt32(v4), 0, int32(_MPFR_RNDN))
	if (*t__mpfr_struct)(unsafe.Pointer(w+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		libmpfr.Xmpfr_set4(tls, bp+32, bp+32, int32(_MPFR_RNDN), int32(1))
	} else {
		libmpfr.Xmpfr_set4(tls, bp+32, bp+32, int32(_MPFR_RNDN), (*t__mpfr_struct)(unsafe.Pointer(w+32)).F_mpfr_sign)
	}
	libmpfr.Xmpfr_mul(tls, bp+64, z, bp, int32(_MPFR_RNDN)) /* exact */
	libmpfr.Xmpfr_mul(tls, bp+96, z+32, bp+32, int32(_MPFR_RNDN))
	libmpfr.Xmpfr_add(tls, bp+128, bp+64, bp+96, int32(_MPFR_RNDN))
	libmpfr.Xmpfr_mul(tls, bp+96, z+32, bp, int32(_MPFR_RNDN))
	libmpfr.Xmpfr_mul(tls, bp+64, z, bp+32, int32(_MPFR_RNDN))
	libmpfr.Xmpfr_sub(tls, bp+160, bp+96, bp+64, int32(_MPFR_RNDN))
	if (*t__mpfr_struct)(unsafe.Pointer(bp+128)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		libmpfr.Xmpfr_set4(tls, rop, bp+192, int32(_MPFR_RNDN), int32(1))
	} else {
		libmpfr.Xmpfr_set4(tls, rop, bp+192, int32(_MPFR_RNDN), (*t__mpfr_struct)(unsafe.Pointer(bp+128)).F_mpfr_sign)
	}
	if (*t__mpfr_struct)(unsafe.Pointer(bp+160)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		libmpfr.Xmpfr_set4(tls, rop+32, bp+192, int32(_MPFR_RNDN), int32(1))
	} else {
		libmpfr.Xmpfr_set4(tls, rop+32, bp+192, int32(_MPFR_RNDN), (*t__mpfr_struct)(unsafe.Pointer(bp+160)).F_mpfr_sign)
	}
	libmpfr.Xmpfr_clear(tls, bp)
	libmpfr.Xmpfr_clear(tls, bp+32)
	libmpfr.Xmpfr_clear(tls, bp+128)
	libmpfr.Xmpfr_clear(tls, bp+160)
	libmpfr.Xmpfr_clear(tls, bp+192)
	libmpfr.Xmpfr_clear(tls, bp+64)
	libmpfr.Xmpfr_clear(tls, bp+96)
	return libc.Int32FromInt32(0) | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2) /* exact */
}

func _mpc_div_real(tls *libc.TLS, rop Tmpc_ptr, z Tmpc_srcptr, w Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	/*
	   Assumes z finite and w finite and non-zero, with imaginary part
	   	of w a signed zero.
	*/
	var inex_im, inex_re, wis, wrs, zis, zrs, v1, v10, v2, v3, v4, v5, v6, v7, v8, v9 int32
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = inex_im, inex_re, wis, wrs, zis, zrs, v1, v10, v2, v3, v4, v5, v6, v7, v8, v9
	if (*t__mpfr_struct)(unsafe.Pointer(z)).F_mpfr_sign < 0 {
		v1 = -int32(1)
	} else {
		v1 = int32(1)
	}
	/* save signs of operands in case there are overlaps */
	zrs = v1
	if (*t__mpfr_struct)(unsafe.Pointer(z+32)).F_mpfr_sign < 0 {
		v2 = -int32(1)
	} else {
		v2 = int32(1)
	}
	zis = v2
	if (*t__mpfr_struct)(unsafe.Pointer(w)).F_mpfr_sign < 0 {
		v3 = -int32(1)
	} else {
		v3 = int32(1)
	}
	wrs = v3
	if (*t__mpfr_struct)(unsafe.Pointer(w+32)).F_mpfr_sign < 0 {
		v4 = -int32(1)
	} else {
		v4 = int32(1)
	}
	wis = v4
	/* warning: rop may overlap with z,w so treat the imaginary part first */
	inex_im = libmpfr.Xmpfr_div(tls, rop+32, z+32, w, rnd>>libc.Int32FromInt32(4))
	inex_re = libmpfr.Xmpfr_div(tls, rop, z, w, rnd&libc.Int32FromInt32(0x0F))
	/* correct signs of zeroes if necessary, which does not affect the
	   inexact flags                                                    */
	if (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		if zrs != wrs && zis != wis {
			v5 = -int32(1)
		} else {
			v5 = int32(1)
		}
		libmpfr.Xmpfr_set4(tls, rop, rop, int32(_MPFR_RNDN), v5)
	} /* exact */
	if (*t__mpfr_struct)(unsafe.Pointer(rop+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		if zis != wrs && zrs == wis {
			v6 = -int32(1)
		} else {
			v6 = int32(1)
		}
		libmpfr.Xmpfr_set4(tls, rop+32, rop+32, int32(_MPFR_RNDN), v6)
	}
	if inex_re < 0 {
		v7 = int32(2)
	} else {
		if inex_re == 0 {
			v8 = 0
		} else {
			v8 = int32(1)
		}
		v7 = v8
	}
	if inex_im < 0 {
		v9 = int32(2)
	} else {
		if inex_im == 0 {
			v10 = 0
		} else {
			v10 = int32(1)
		}
		v9 = v10
	}
	return v7 | v9<<int32(2)
}

func _mpc_div_imag(tls *libc.TLS, rop Tmpc_ptr, z Tmpc_srcptr, w Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(96)
	defer tls.Free(96)
	/* Assumes z finite and w finite and non-zero, with real part
	   of w a signed zero.                                        */
	var dest Tmpc_ptr
	var imag_z, inex_im, inex_re, overlap, wis, wrs, zis, zrs, v10, v11, v2, v3, v4, v5, v6, v7, v8, v9 int32
	var v1 uintptr
	var _ /* tmprop at bp+32 */ Tmpc_t
	var _ /* wloc at bp+0 */ Tmpfr_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = dest, imag_z, inex_im, inex_re, overlap, wis, wrs, zis, zrs, v1, v10, v11, v2, v3, v4, v5, v6, v7, v8, v9
	overlap = libc.BoolInt32(rop == z || rop == w)
	imag_z = libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(z)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)))
	if overlap != 0 {
		v1 = bp + 32
	} else {
		v1 = rop
	}
	dest = v1
	if (*t__mpfr_struct)(unsafe.Pointer(z)).F_mpfr_sign < 0 {
		v2 = -int32(1)
	} else {
		v2 = int32(1)
	}
	/* save signs of operands in case there are overlaps */
	zrs = v2
	if (*t__mpfr_struct)(unsafe.Pointer(z+32)).F_mpfr_sign < 0 {
		v3 = -int32(1)
	} else {
		v3 = int32(1)
	}
	zis = v3
	if (*t__mpfr_struct)(unsafe.Pointer(w)).F_mpfr_sign < 0 {
		v4 = -int32(1)
	} else {
		v4 = int32(1)
	}
	wrs = v4
	if (*t__mpfr_struct)(unsafe.Pointer(w+32)).F_mpfr_sign < 0 {
		v5 = -int32(1)
	} else {
		v5 = int32(1)
	}
	wis = v5
	if overlap != 0 {
		Xmpc_init3(tls, bp+32, (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec, (*t__mpfr_struct)(unsafe.Pointer(rop+32)).F_mpfr_prec)
	}
	(*(*Tmpfr_t)(unsafe.Pointer(bp)))[0] = *(*t__mpfr_struct)(unsafe.Pointer(w + 32)) /* copies mpfr struct IM(w) into wloc */
	inex_re = libmpfr.Xmpfr_div(tls, dest, z+32, bp, rnd&libc.Int32FromInt32(0x0F))
	libmpfr.Xmpfr_neg(tls, bp, bp, int32(_MPFR_RNDN))
	/* changes the sign only in wloc, not in w; no need to correct later */
	inex_im = libmpfr.Xmpfr_div(tls, dest+32, z, bp, rnd>>libc.Int32FromInt32(4))
	if overlap != 0 {
		/* Note: we could use mpc_swap here, but this might cause problems
		   if rop and tmprop have been allocated using different methods, since
		   it will swap the significands of rop and tmprop. See
		   https://sympa.inria.fr/sympa/arc/mpc-discuss/2009-08/msg00004.html */
		Xmpc_set(tls, rop, bp+32, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4)) /* exact */
		Xmpc_clear(tls, bp+32)
	}
	/* correct signs of zeroes if necessary, which does not affect the
	   inexact flags                                                    */
	if (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		if zrs != wrs && zis != wis {
			v6 = -int32(1)
		} else {
			v6 = int32(1)
		}
		libmpfr.Xmpfr_set4(tls, rop, rop, int32(_MPFR_RNDN), v6)
	} /* exact */
	if imag_z != 0 {
		if zis != wrs && zrs == wis {
			v7 = -int32(1)
		} else {
			v7 = int32(1)
		}
		libmpfr.Xmpfr_set4(tls, rop+32, rop+32, int32(_MPFR_RNDN), v7)
	}
	if inex_re < 0 {
		v8 = int32(2)
	} else {
		if inex_re == 0 {
			v9 = 0
		} else {
			v9 = int32(1)
		}
		v8 = v9
	}
	if inex_im < 0 {
		v10 = int32(2)
	} else {
		if inex_im == 0 {
			v11 = 0
		} else {
			v11 = int32(1)
		}
		v10 = v11
	}
	return v8 | v10<<int32(2)
}

func Xmpc_div(tls *libc.TLS, a Tmpc_ptr, b Tmpc_srcptr, c Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(160)
	defer tls.Free(160)
	var _p Tmpfr_ptr
	var inex, inexact_im, inexact_norm, inexact_prod, inexact_re, isinf, loops, ok_im, ok_re, overflow_im, overflow_norm, overflow_prod, overflow_re, saved_overflow, saved_underflow, tmpsgn, underflow_im, underflow_norm, underflow_prod, underflow_re, v10, v12, v13, v14, v15, v16, v18, v20, v21, v23, v25, v26, v27, v28, v29, v3, v4, v5, v6, v7, v8 int32
	var prec Tmpfr_prec_t
	var rnd_im, rnd_re Tmpfr_rnd_t
	var saved_emax, saved_emin Tmpfr_exp_t
	var v1, v2 int64
	var _ /* c_conj at bp+64 */ Tmpc_t
	var _ /* q at bp+128 */ Tmpfr_t
	var _ /* res at bp+0 */ Tmpc_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = _p, inex, inexact_im, inexact_norm, inexact_prod, inexact_re, isinf, loops, ok_im, ok_re, overflow_im, overflow_norm, overflow_prod, overflow_re, prec, rnd_im, rnd_re, saved_emax, saved_emin, saved_overflow, saved_underflow, tmpsgn, underflow_im, underflow_norm, underflow_prod, underflow_re, v1, v10, v12, v13, v14, v15, v16, v18, v2, v20, v21, v23, v25, v26, v27, v28, v29, v3, v4, v5, v6, v7, v8
	ok_re = 0
	ok_im = 0
	loops = 0
	underflow_re = 0
	overflow_re = 0
	underflow_im = 0
	overflow_im = 0
	rnd_re = rnd & libc.Int32FromInt32(0x0F)
	rnd_im = rnd >> libc.Int32FromInt32(4)
	/* According to the C standard G.3, there are three types of numbers:   */
	/* finite (both parts are usual real numbers; contains 0), infinite     */
	/* (at least one part is a real infinity) and all others; the latter    */
	/* are numbers containing a nan, but no infinity, and could reasonably  */
	/* be called nan.                                                       */
	/* By G.5.1.4, infinite/finite=infinite; finite/infinite=0;             */
	/* all other divisions that are not finite/finite return nan+i*nan.     */
	/* Division by 0 could be handled by the following case of division by  */
	/* a real; we handle it separately instead.                             */
	if (*t__mpfr_struct)(unsafe.Pointer(c)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) && (*t__mpfr_struct)(unsafe.Pointer(c+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) { /* both Re(c) and Im(c) are zero */
		return _mpc_div_zero(tls, a, b, c, rnd)
	} else {
		if ((*t__mpfr_struct)(unsafe.Pointer(b)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(b+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))) && (libmpfr.Xmpfr_number_p(tls, c) != 0 && libmpfr.Xmpfr_number_p(tls, c+32) != 0) { /* either Re(b) or Im(b) is infinite
			   and both Re(c) and Im(c) are ordinary */
			return _mpc_div_inf_fin(tls, a, b, c)
		} else {
			if libmpfr.Xmpfr_number_p(tls, b) != 0 && libmpfr.Xmpfr_number_p(tls, b+32) != 0 && ((*t__mpfr_struct)(unsafe.Pointer(c)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(c+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))) {
				return _mpc_div_fin_inf(tls, a, b, c)
			} else {
				if !(libmpfr.Xmpfr_number_p(tls, b) != 0 && libmpfr.Xmpfr_number_p(tls, b+32) != 0) || !(libmpfr.Xmpfr_number_p(tls, c) != 0 && libmpfr.Xmpfr_number_p(tls, c+32) != 0) {
					Xmpc_set_nan(tls, a)
					return libc.Int32FromInt32(0) | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2)
				} else {
					if (*t__mpfr_struct)(unsafe.Pointer(c+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
						return _mpc_div_real(tls, a, b, c, rnd)
					} else {
						if (*t__mpfr_struct)(unsafe.Pointer(c)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
							return _mpc_div_imag(tls, a, b, c, rnd)
						}
					}
				}
			}
		}
	}
	if (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(a+32)).F_mpfr_prec {
		v1 = (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_prec
	} else {
		v1 = (*t__mpfr_struct)(unsafe.Pointer(a + 32)).F_mpfr_prec
	}
	prec = v1
	Xmpc_init2(tls, bp, int64(2))
	libmpfr.Xmpfr_init(tls, bp+128)
	/* we perform the division in the largest possible exponent range,
	   to avoid underflow/overflow in intermediate computations */
	saved_emin = libmpfr.Xmpfr_get_emin(tls)
	saved_emax = libmpfr.Xmpfr_get_emax(tls)
	libmpfr.Xmpfr_set_emin(tls, libmpfr.Xmpfr_get_emin_min(tls))
	libmpfr.Xmpfr_set_emax(tls, libmpfr.Xmpfr_get_emax_max(tls))
	/* create the conjugate of c in c_conj without allocating new memory */
	*(*t__mpfr_struct)(unsafe.Pointer(bp + 64)) = *(*t__mpfr_struct)(unsafe.Pointer(c))
	*(*t__mpfr_struct)(unsafe.Pointer(bp + 64 + 32)) = *(*t__mpfr_struct)(unsafe.Pointer(c + 32))
	libmpfr.Xmpfr_neg(tls, bp+64+32, bp+64+32, int32(_MPFR_RNDN))
	/* save the underflow or overflow flags from MPFR */
	saved_underflow = libmpfr.Xmpfr_underflow_p(tls)
	saved_overflow = libmpfr.Xmpfr_overflow_p(tls)
	for cond := true; cond; cond = (!(ok_re != 0) || !(ok_im != 0)) && !(underflow_norm != 0) && !(overflow_norm != 0) && !(underflow_prod != 0) && !(overflow_prod != 0) {
		loops++
		if loops <= int32(2) {
			v2 = Xmpc_ceil_log2(tls, prec) + int64(5)
		} else {
			v2 = prec / int64(2)
		}
		prec += v2
		Xmpc_set_prec(tls, bp, prec)
		libmpfr.Xmpfr_set_prec(tls, bp+128, prec)
		/* first compute norm(c) */
		libmpfr.Xmpfr_clear_underflow(tls)
		libmpfr.Xmpfr_clear_overflow(tls)
		inexact_norm = Xmpc_norm(tls, bp+128, c, int32(_MPFR_RNDU))
		underflow_norm = libmpfr.Xmpfr_underflow_p(tls)
		overflow_norm = libmpfr.Xmpfr_overflow_p(tls)
		if underflow_norm != 0 {
			{
				_p = bp + 128
				(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign = int32(1)
				(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
				_ = int32(_MPFR_RNDN)
				v3 = 0
			}
			_ = v3
		}
		/* to obtain divisions by 0 later on */
		/* now compute b*conjugate(c) */
		libmpfr.Xmpfr_clear_underflow(tls)
		libmpfr.Xmpfr_clear_overflow(tls)
		inexact_prod = Xmpc_mul(tls, bp, b, bp+64, int32(_MPFR_RNDZ)+int32(_MPFR_RNDZ)<<libc.Int32FromInt32(4))
		if inexact_prod&int32(3) == int32(2) {
			v4 = -int32(1)
		} else {
			if inexact_prod&int32(3) == 0 {
				v5 = 0
			} else {
				v5 = int32(1)
			}
			v4 = v5
		}
		inexact_re = v4
		if inexact_prod>>int32(2) == int32(2) {
			v6 = -int32(1)
		} else {
			if inexact_prod>>int32(2) == 0 {
				v7 = 0
			} else {
				v7 = int32(1)
			}
			v6 = v7
		}
		inexact_im = v6
		underflow_prod = libmpfr.Xmpfr_underflow_p(tls)
		overflow_prod = libmpfr.Xmpfr_overflow_p(tls)
		/* unfortunately, does not distinguish between under-/overflow
		   in real or imaginary parts
		   hopefully, the side-effects of mpc_mul do indeed raise the
		   mpfr exceptions */
		if overflow_prod != 0 {
			/* FIXME: in case overflow_norm is also true, the code below is wrong,
			   since the after division by the norm, we might end up with finite
			   real and/or imaginary parts. A workaround would be to scale the
			   inputs (in case the exponents are within the same range). */
			isinf = 0
			/* determine if the real part of res is the maximum or the minimum
			   representable number */
			if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
					libmpfr.Xmpfr_set_erangeflag(tls)
				}
				v8 = libc.Int32FromInt32(0)
			} else {
				v8 = (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_sign
			}
			tmpsgn = v8
			if tmpsgn > 0 {
				libmpfr.Xmpfr_nextabove(tls, bp)
				isinf = libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)))
				libmpfr.Xmpfr_nextbelow(tls, bp)
			} else {
				if tmpsgn < 0 {
					libmpfr.Xmpfr_nextbelow(tls, bp)
					isinf = libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)))
					libmpfr.Xmpfr_nextabove(tls, bp)
				}
			}
			if isinf != 0 {
				libmpfr.Xmpfr_set_inf(tls, bp, tmpsgn)
				overflow_re = int32(1)
			}
			/* same for the imaginary part */
			if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
					libmpfr.Xmpfr_set_erangeflag(tls)
				}
				v10 = libc.Int32FromInt32(0)
			} else {
				v10 = (*t__mpfr_struct)(unsafe.Pointer(bp + 32)).F_mpfr_sign
			}
			tmpsgn = v10
			isinf = 0
			if tmpsgn > 0 {
				libmpfr.Xmpfr_nextabove(tls, bp+32)
				isinf = libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)))
				libmpfr.Xmpfr_nextbelow(tls, bp+32)
			} else {
				if tmpsgn < 0 {
					libmpfr.Xmpfr_nextbelow(tls, bp+32)
					isinf = libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)))
					libmpfr.Xmpfr_nextabove(tls, bp+32)
				}
			}
			if isinf != 0 {
				libmpfr.Xmpfr_set_inf(tls, bp+32, tmpsgn)
				overflow_im = int32(1)
			}
			Xmpc_set(tls, a, bp, rnd)
			goto end
		}
		/* divide the product by the norm */
		if inexact_norm == 0 && (inexact_re == 0 || inexact_im == 0) {
			/* The division has good chances to be exact in at least one part.  */
			/* Since this can cause problems when not rounding to the nearest,  */
			/* we use the division code of mpfr, which handles the situation.   */
			libmpfr.Xmpfr_clear_underflow(tls)
			libmpfr.Xmpfr_clear_overflow(tls)
			inexact_re |= libmpfr.Xmpfr_div(tls, bp, bp, bp+128, int32(_MPFR_RNDZ))
			underflow_re = libmpfr.Xmpfr_underflow_p(tls)
			overflow_re = libmpfr.Xmpfr_overflow_p(tls)
			ok_re = libc.BoolInt32(!(inexact_re != 0) || underflow_re != 0 || overflow_re != 0 || libmpfr.Xmpfr_can_round(tls, bp, prec-int64(4), int32(_MPFR_RNDN), int32(_MPFR_RNDZ), (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_prec+libc.BoolInt64(rnd_re == int32(_MPFR_RNDN))) != 0)
			if ok_re != 0 { /* compute imaginary part */
				libmpfr.Xmpfr_clear_underflow(tls)
				libmpfr.Xmpfr_clear_overflow(tls)
				inexact_im |= libmpfr.Xmpfr_div(tls, bp+32, bp+32, bp+128, int32(_MPFR_RNDZ))
				underflow_im = libmpfr.Xmpfr_underflow_p(tls)
				overflow_im = libmpfr.Xmpfr_overflow_p(tls)
				ok_im = libc.BoolInt32(!(inexact_im != 0) || underflow_im != 0 || overflow_im != 0 || libmpfr.Xmpfr_can_round(tls, bp+32, prec-int64(4), int32(_MPFR_RNDN), int32(_MPFR_RNDZ), (*t__mpfr_struct)(unsafe.Pointer(a+32)).F_mpfr_prec+libc.BoolInt64(rnd_im == int32(_MPFR_RNDN))) != 0)
			}
		} else {
			/* The division is inexact, so for efficiency reasons we invert q */
			/* only once and multiply by the inverse. */
			if libmpfr.Xmpfr_ui_div(tls, bp+128, uint64(1), bp+128, int32(_MPFR_RNDZ)) != 0 || inexact_norm != 0 {
				/* if 1/q is inexact, the approximations of the real and
				   imaginary part below will be inexact, unless RE(res)
				   or IM(res) is zero */
				inexact_re |= libc.BoolInt32(!((*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))))
				inexact_im |= libc.BoolInt32(!((*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))))
			}
			libmpfr.Xmpfr_clear_underflow(tls)
			libmpfr.Xmpfr_clear_overflow(tls)
			inexact_re |= libmpfr.Xmpfr_mul(tls, bp, bp, bp+128, int32(_MPFR_RNDZ))
			underflow_re = libmpfr.Xmpfr_underflow_p(tls)
			overflow_re = libmpfr.Xmpfr_overflow_p(tls)
			ok_re = libc.BoolInt32(!(inexact_re != 0) || underflow_re != 0 || overflow_re != 0 || libmpfr.Xmpfr_can_round(tls, bp, prec-int64(4), int32(_MPFR_RNDN), int32(_MPFR_RNDZ), (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_prec+libc.BoolInt64(rnd_re == int32(_MPFR_RNDN))) != 0)
			if ok_re != 0 { /* compute imaginary part */
				libmpfr.Xmpfr_clear_underflow(tls)
				libmpfr.Xmpfr_clear_overflow(tls)
				inexact_im |= libmpfr.Xmpfr_mul(tls, bp+32, bp+32, bp+128, int32(_MPFR_RNDZ))
				underflow_im = libmpfr.Xmpfr_underflow_p(tls)
				overflow_im = libmpfr.Xmpfr_overflow_p(tls)
				ok_im = libc.BoolInt32(!(inexact_im != 0) || underflow_im != 0 || overflow_im != 0 || libmpfr.Xmpfr_can_round(tls, bp+32, prec-int64(4), int32(_MPFR_RNDN), int32(_MPFR_RNDZ), (*t__mpfr_struct)(unsafe.Pointer(a+32)).F_mpfr_prec+libc.BoolInt64(rnd_im == int32(_MPFR_RNDN))) != 0)
			}
		}
	}
	inex = Xmpc_set(tls, a, bp, rnd)
	if inex&int32(3) == int32(2) {
		v12 = -int32(1)
	} else {
		if inex&int32(3) == 0 {
			v13 = 0
		} else {
			v13 = int32(1)
		}
		v12 = v13
	}
	inexact_re = v12
	if inex>>int32(2) == int32(2) {
		v14 = -int32(1)
	} else {
		if inex>>int32(2) == 0 {
			v15 = 0
		} else {
			v15 = int32(1)
		}
		v14 = v15
	}
	inexact_im = v14
	goto end
end:
	;
	/* fix values and inexact flags in case of overflow/underflow */
	/* FIXME: heuristic, certainly does not cover all cases */
	if overflow_re != 0 || underflow_norm != 0 && !(underflow_prod != 0) {
		if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				libmpfr.Xmpfr_set_erangeflag(tls)
			}
			v16 = libc.Int32FromInt32(0)
		} else {
			v16 = (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_sign
		}
		libmpfr.Xmpfr_set_inf(tls, a, v16)
		if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				libmpfr.Xmpfr_set_erangeflag(tls)
			}
			v18 = libc.Int32FromInt32(0)
		} else {
			v18 = (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_sign
		}
		inexact_re = v18
	} else {
		if underflow_re != 0 || overflow_norm != 0 && !(overflow_prod != 0) {
			if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_sign < 0 {
				v20 = int32(1)
			} else {
				v20 = -int32(1)
			}
			inexact_re = v20
			libmpfr.Xmpfr_set_zero(tls, a, -inexact_re)
		}
	}
	if overflow_im != 0 || underflow_norm != 0 && !(underflow_prod != 0) {
		if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				libmpfr.Xmpfr_set_erangeflag(tls)
			}
			v21 = libc.Int32FromInt32(0)
		} else {
			v21 = (*t__mpfr_struct)(unsafe.Pointer(bp + 32)).F_mpfr_sign
		}
		libmpfr.Xmpfr_set_inf(tls, a+32, v21)
		if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				libmpfr.Xmpfr_set_erangeflag(tls)
			}
			v23 = libc.Int32FromInt32(0)
		} else {
			v23 = (*t__mpfr_struct)(unsafe.Pointer(bp + 32)).F_mpfr_sign
		}
		inexact_im = v23
	} else {
		if underflow_im != 0 || overflow_norm != 0 && !(overflow_prod != 0) {
			if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_sign < 0 {
				v25 = int32(1)
			} else {
				v25 = -int32(1)
			}
			inexact_im = v25
			libmpfr.Xmpfr_set_zero(tls, a+32, -inexact_im)
		}
	}
	Xmpc_clear(tls, bp)
	libmpfr.Xmpfr_clear(tls, bp+128)
	/* restore underflow and overflow flags from MPFR */
	if saved_underflow != 0 {
		libmpfr.Xmpfr_set_underflow(tls)
	}
	if saved_overflow != 0 {
		libmpfr.Xmpfr_set_overflow(tls)
	}
	/* restore the exponent range, and check the range of results */
	libmpfr.Xmpfr_set_emin(tls, saved_emin)
	libmpfr.Xmpfr_set_emax(tls, saved_emax)
	inexact_re = libmpfr.Xmpfr_check_range(tls, a, inexact_re, rnd_re)
	inexact_im = libmpfr.Xmpfr_check_range(tls, a+32, inexact_im, rnd_im)
	if inexact_re < 0 {
		v26 = int32(2)
	} else {
		if inexact_re == 0 {
			v27 = 0
		} else {
			v27 = int32(1)
		}
		v26 = v27
	}
	if inexact_im < 0 {
		v28 = int32(2)
	} else {
		if inexact_im == 0 {
			v29 = 0
		} else {
			v29 = int32(1)
		}
		v28 = v29
	}
	return v26 | v28<<int32(2)
}

func Xmpc_div_fr(tls *libc.TLS, a Tmpc_ptr, b Tmpc_srcptr, c Tmpfr_srcptr, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var _p Tmpfr_srcptr
	var inex_im, inex_re, v1, v2, v3, v4, v5 int32
	var _ /* real at bp+0 */ Tmpfr_t
	_, _, _, _, _, _, _, _ = _p, inex_im, inex_re, v1, v2, v3, v4, v5
	/* We have to use temporary variable in case c=mpc_realref (a). */
	libmpfr.Xmpfr_init2(tls, bp, (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_prec)
	inex_re = libmpfr.Xmpfr_div(tls, bp, b, c, rnd&libc.Int32FromInt32(0x0F))
	inex_im = libmpfr.Xmpfr_div(tls, a+32, b+32, c, rnd>>libc.Int32FromInt32(4))
	{
		_p = bp
		v1 = libmpfr.Xmpfr_set4(tls, a, _p, int32(_MPFR_RNDN), (*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign)
	}
	_ = v1
	libmpfr.Xmpfr_clear(tls, bp)
	if inex_re < 0 {
		v2 = int32(2)
	} else {
		if inex_re == 0 {
			v3 = 0
		} else {
			v3 = int32(1)
		}
		v2 = v3
	}
	if inex_im < 0 {
		v4 = int32(2)
	} else {
		if inex_im == 0 {
			v5 = 0
		} else {
			v5 = int32(1)
		}
		v4 = v5
	}
	return v2 | v4<<int32(2)
}

func Xmpc_div_ui(tls *libc.TLS, a Tmpc_ptr, b Tmpc_srcptr, c uint64, rnd Tmpc_rnd_t) (r int32) {
	var inex_im, inex_re, v1, v2, v3, v4 int32
	_, _, _, _, _, _ = inex_im, inex_re, v1, v2, v3, v4
	inex_re = libmpfr.Xmpfr_div_ui(tls, a, b, c, rnd&libc.Int32FromInt32(0x0F))
	inex_im = libmpfr.Xmpfr_div_ui(tls, a+32, b+32, c, rnd>>libc.Int32FromInt32(4))
	if inex_re < 0 {
		v1 = int32(2)
	} else {
		if inex_re == 0 {
			v2 = 0
		} else {
			v2 = int32(1)
		}
		v1 = v2
	}
	if inex_im < 0 {
		v3 = int32(2)
	} else {
		if inex_im == 0 {
			v4 = 0
		} else {
			v4 = int32(1)
		}
		v3 = v4
	}
	return v1 | v3<<int32(2)
}

// C documentation
//
//	/* res <- x[0]*y[0] + ... + x[n-1]*y[n-1] */
func Xmpc_dot(tls *libc.TLS, res Tmpc_ptr, x uintptr, y uintptr, n uint64, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var i uint64
	var inex_im, inex_re, v6, v7, v8, v9 int32
	var prec_x_im, prec_x_im1, prec_x_re, prec_x_re1, prec_y_im, prec_y_im1, prec_y_max, prec_y_re, prec_y_re1 Tmpfr_prec_t
	var t, z uintptr
	var v3 int64
	var _ /* re_res at bp+0 */ Tmpfr_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = i, inex_im, inex_re, prec_x_im, prec_x_im1, prec_x_re, prec_x_re1, prec_y_im, prec_y_im1, prec_y_max, prec_y_re, prec_y_re1, t, z, v3, v6, v7, v8, v9
	z = libc.Xmalloc(tls, uint64(2)*n*uint64(32))
	/* warning: when n=0, malloc() might return NULL (e.g., gcc119) */
	t = libc.Xmalloc(tls, uint64(2)*n*uint64(8))
	i = uint64(0)
	for {
		if !(i < uint64(2)*n) {
			break
		}
		*(*Tmpfr_ptr)(unsafe.Pointer(t + uintptr(i)*8)) = z + uintptr(i)*32
		goto _1
	_1:
		;
		i++
	}
	/* we first store in z[i] the value of Re(x[i])*Re(y[i])
	   and in z[n+i] that of -Im(x[i])*Im(y[i]) */
	i = uint64(0)
	for {
		if !(i < n) {
			break
		}
		prec_x_re = (*t__mpfr_struct)(unsafe.Pointer(*(*Tmpc_ptr)(unsafe.Pointer(x + uintptr(i)*8)))).F_mpfr_prec
		prec_x_im = (*t__mpfr_struct)(unsafe.Pointer(*(*Tmpc_ptr)(unsafe.Pointer(x + uintptr(i)*8)) + 32)).F_mpfr_prec
		prec_y_re = (*t__mpfr_struct)(unsafe.Pointer(*(*Tmpc_ptr)(unsafe.Pointer(y + uintptr(i)*8)))).F_mpfr_prec
		prec_y_im = (*t__mpfr_struct)(unsafe.Pointer(*(*Tmpc_ptr)(unsafe.Pointer(y + uintptr(i)*8)) + 32)).F_mpfr_prec
		if prec_y_re > prec_y_im {
			v3 = prec_y_re
		} else {
			v3 = prec_y_im
		}
		prec_y_max = v3
		/* we allocate z[i] with prec_x_re + prec_y_max bits
		   so that the second loop below does not reallocate */
		libmpfr.Xmpfr_init2(tls, z+uintptr(i)*32, prec_x_re+prec_y_max)
		libmpfr.Xmpfr_set_prec(tls, z+uintptr(i)*32, prec_x_re+prec_y_re)
		libmpfr.Xmpfr_mul(tls, z+uintptr(i)*32, *(*Tmpc_ptr)(unsafe.Pointer(x + uintptr(i)*8)), *(*Tmpc_ptr)(unsafe.Pointer(y + uintptr(i)*8)), int32(_MPFR_RNDZ))
		/* idem for z[n+i]: we allocate with prec_x_im + prec_y_max bits */
		libmpfr.Xmpfr_init2(tls, z+uintptr(n+i)*32, prec_x_im+prec_y_max)
		libmpfr.Xmpfr_set_prec(tls, z+uintptr(n+i)*32, prec_x_im+prec_y_im)
		libmpfr.Xmpfr_mul(tls, z+uintptr(n+i)*32, *(*Tmpc_ptr)(unsafe.Pointer(x + uintptr(i)*8))+32, *(*Tmpc_ptr)(unsafe.Pointer(y + uintptr(i)*8))+32, int32(_MPFR_RNDZ))
		libmpfr.Xmpfr_neg(tls, z+uintptr(n+i)*32, z+uintptr(n+i)*32, int32(_MPFR_RNDZ))
		goto _2
	_2:
		;
		i++
	}
	/* copy the real part in a temporary variable, since it might be in the
	   input array */
	libmpfr.Xmpfr_init2(tls, bp, (*t__mpfr_struct)(unsafe.Pointer(res)).F_mpfr_prec)
	inex_re = libmpfr.Xmpfr_sum(tls, bp, t, uint64(2)*n, rnd&libc.Int32FromInt32(0x0F))
	/* we then store in z[i] the value of Re(x[i])*Im(y[i])
	   and in z[n+i] that of Im(x[i])*Re(y[i]) */
	i = uint64(0)
	for {
		if !(i < n) {
			break
		}
		prec_x_re1 = (*t__mpfr_struct)(unsafe.Pointer(*(*Tmpc_ptr)(unsafe.Pointer(x + uintptr(i)*8)))).F_mpfr_prec
		prec_x_im1 = (*t__mpfr_struct)(unsafe.Pointer(*(*Tmpc_ptr)(unsafe.Pointer(x + uintptr(i)*8)) + 32)).F_mpfr_prec
		prec_y_re1 = (*t__mpfr_struct)(unsafe.Pointer(*(*Tmpc_ptr)(unsafe.Pointer(y + uintptr(i)*8)))).F_mpfr_prec
		prec_y_im1 = (*t__mpfr_struct)(unsafe.Pointer(*(*Tmpc_ptr)(unsafe.Pointer(y + uintptr(i)*8)) + 32)).F_mpfr_prec
		libmpfr.Xmpfr_set_prec(tls, z+uintptr(i)*32, prec_x_re1+prec_y_im1)
		libmpfr.Xmpfr_mul(tls, z+uintptr(i)*32, *(*Tmpc_ptr)(unsafe.Pointer(x + uintptr(i)*8)), *(*Tmpc_ptr)(unsafe.Pointer(y + uintptr(i)*8))+32, int32(_MPFR_RNDZ))
		libmpfr.Xmpfr_set_prec(tls, z+uintptr(n+i)*32, prec_x_im1+prec_y_re1)
		libmpfr.Xmpfr_mul(tls, z+uintptr(n+i)*32, *(*Tmpc_ptr)(unsafe.Pointer(x + uintptr(i)*8))+32, *(*Tmpc_ptr)(unsafe.Pointer(y + uintptr(i)*8)), int32(_MPFR_RNDZ))
		goto _4
	_4:
		;
		i++
	}
	inex_im = libmpfr.Xmpfr_sum(tls, res+32, t, uint64(2)*n, rnd>>libc.Int32FromInt32(4))
	libmpfr.Xmpfr_swap(tls, res, bp)
	libmpfr.Xmpfr_clear(tls, bp)
	i = uint64(0)
	for {
		if !(i < uint64(2)*n) {
			break
		}
		libmpfr.Xmpfr_clear(tls, z+uintptr(i)*32)
		goto _5
	_5:
		;
		i++
	}
	libc.Xfree(tls, t)
	libc.Xfree(tls, z)
	if inex_re < 0 {
		v6 = int32(2)
	} else {
		if inex_re == 0 {
			v7 = 0
		} else {
			v7 = int32(1)
		}
		v6 = v7
	}
	if inex_im < 0 {
		v8 = int32(2)
	} else {
		if inex_im == 0 {
			v9 = 0
		} else {
			v9 = int32(1)
		}
		v8 = v9
	}
	return v6 | v8<<int32(2)
}

const m_FP_ILOGB0 = "FP_ILOGBNAN"
const m_FP_INFINITE = 1
const m_FP_NAN = 0
const m_FP_NORMAL = 4
const m_FP_SUBNORMAL = 3
const m_FP_ZERO = 2
const m_HUGE = 3.40282346638528859812e+38
const m_HUGE_VALF = "INFINITY"
const m_MATH_ERREXCEPT = 2
const m_MATH_ERRNO = 1
const m_M_1_PI = 0.31830988618379067154
const m_M_2_PI = 0.63661977236758134308
const m_M_2_SQRTPI = 1.12837916709551257390
const m_M_E = 2.7182818284590452354
const m_M_LN10 = 2.30258509299404568402
const m_M_LN2 = 0.69314718055994530942
const m_M_LOG10E = 0.43429448190325182765
const m_M_LOG2E = 1.4426950408889634074
const m_M_PI = 3.14159265358979323846
const m_M_PI_2 = 1.57079632679489661923
const m_M_PI_4 = 0.78539816339744830962
const m_M_SQRT1_2 = 0.70710678118654752440
const m_M_SQRT2 = 1.41421356237309504880
const m_math_errhandling = 2

type Tfloat_t = float32

type Tdouble_t = float64

func _eta_series(tls *libc.TLS, eta Tmpcb_ptr, q Tmpcb_srcptr, expq Tmpfr_exp_t, N int32) {
	bp := tls.Alloc(432)
	defer tls.Free(432)
	/* Evaluate 2N+1 terms of the Dedekind eta function without the q^(1/24)
	   factor (where internally N is taken to be at least 1).
	   expq is an upper bound on the exponent of |q|, valid everywhere
	   inside the ball; for the error analysis to hold the function assumes
	   that expq < -1, which implies |q| < 1/4. */
	var M, n, v1 int32
	var p Tmpfr_prec_t
	var _ /* q2 at bp+0 */ Tmpcb_t
	var _ /* q2n1 at bp+160 */ Tmpcb_t
	var _ /* q3nm1 at bp+240 */ Tmpcb_t
	var _ /* q3np1 at bp+320 */ Tmpcb_t
	var _ /* qn at bp+80 */ Tmpcb_t
	var _ /* r at bp+400 */ Tmpcr_t
	var _ /* r2 at bp+416 */ Tmpcr_t
	_, _, _, _ = M, n, p, v1
	p = Xmpcb_get_prec(tls, q)
	Xmpcb_init(tls, bp)
	Xmpcb_init(tls, bp+80)
	Xmpcb_init(tls, bp+160)
	Xmpcb_init(tls, bp+240)
	Xmpcb_init(tls, bp+320)
	Xmpcb_sqr(tls, bp, q)
	/* n = 0 */
	Xmpcb_set_ui_ui(tls, eta, uint64(1), uint64(0), p)
	/* n = 1 */
	Xmpcb_set(tls, bp+80, q)   /* q^n */
	Xmpcb_neg(tls, bp+160, q)  /* -q^(2n-1) */
	Xmpcb_neg(tls, bp+240, q)  /* +- q^((3n-1)*n/2) */
	Xmpcb_neg(tls, bp+320, bp) /* +- q^(3n+1)*n/2) */
	Xmpcb_add(tls, eta, eta, bp+240)
	Xmpcb_add(tls, eta, eta, bp+320)
	if int32(1) > N {
		v1 = int32(1)
	} else {
		v1 = N
	}
	N = v1
	n = int32(2)
	for {
		if !(n <= N) {
			break
		}
		Xmpcb_mul(tls, bp+80, bp+80, q)
		Xmpcb_mul(tls, bp+160, bp+160, bp)
		Xmpcb_mul(tls, bp+240, bp+320, bp+160)
		Xmpcb_mul(tls, bp+320, bp+240, bp+80)
		Xmpcb_add(tls, eta, eta, bp+240)
		Xmpcb_add(tls, eta, eta, bp+320)
		goto _2
	_2:
		;
		n++
	}
	/* Compute the relative error due to the truncation of the series
	   as explained in algorithms.tex. */
	M = (int32(3)*(N+int32(1)) - int32(1)) * (N + int32(1)) / int32(2)
	Xmpcr_set_one(tls, bp+400)
	Xmpcr_div_2ui(tls, bp+400, bp+400, libc.Uint64FromInt64(-(int64(M)*expq + libc.Int64FromInt32(1))))
	/* Compose the two relative errors. */
	Xmpcr_mul(tls, bp+416, bp+400, eta+64)
	Xmpcr_add(tls, eta+64, eta+64, bp+400)
	Xmpcr_add(tls, eta+64, eta+64, bp+416)
	Xmpcb_clear(tls, bp)
	Xmpcb_clear(tls, bp+80)
	Xmpcb_clear(tls, bp+160)
	Xmpcb_clear(tls, bp+240)
	Xmpcb_clear(tls, bp+320)
}

func _mpcb_eta_q24(tls *libc.TLS, eta Tmpcb_ptr, q24 Tmpcb_srcptr) {
	bp := tls.Alloc(80)
	defer tls.Free(80)
	/* Assuming that q24 is a ball containing
	   q^{1/24} = exp (2 * pi * i * z / 24) for z in the fundamental domain,
	   the function computes eta (z).
	   In fact it works on a larger domain and checks that |q|=|q24^24| < 1/4
	   in the ball; otherwise or if in doubt it returns infinity. */
	var N int32
	var expq Tmpfr_exp_t
	var v1 int64
	var _ /* q at bp+0 */ Tmpcb_t
	_, _, _ = N, expq, v1
	Xmpcb_init(tls, bp)
	Xmpcb_pow_ui(tls, bp, q24, uint64(24))
	/* We need an upper bound on the exponent of |q|. Writing q as having
	   the centre x+i*y and the radius r, we have
	   |q| =  sqrt (x^2+y^2) |1+\theta| with |theta| <= r
	       <= (1 + r) \sqrt 2 max (|x|, |y|)
	       <  2^{max (Exp x, Exp y) + 1}
	   assuming that r < sqrt 2 - 1, which is the case for r < 1/4
	   or Exp r < -1.
	   Then Exp (|q|) <= max (Exp x, Exp y) + 1. */
	if Xmpcr_inf_p(tls, bp+64) != 0 || Xmpcr_get_exp(tls, bp+64) >= int64(-int32(1)) {
		Xmpcb_set_inf(tls, eta)
	} else {
		if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp > (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp {
			v1 = (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp
		} else {
			v1 = (*t__mpfr_struct)(unsafe.Pointer(bp + 32)).F_mpfr_exp
		}
		expq = v1 + int64(1)
		if expq >= int64(-int32(1)) {
			Xmpcb_set_inf(tls, eta)
		} else {
			/* Compute an approximate N such that
			   (3*N+1)*N/2 * |expq| > prec. */
			N = int32(libc.Xsqrt(tls, float64(int64(2)*Xmpcb_get_prec(tls, q24))/float64(3)/float64(-expq))) + int32(1)
			_eta_series(tls, eta, bp, expq, N)
			Xmpcb_mul(tls, eta, eta, q24)
		}
	}
	Xmpcb_clear(tls, bp)
}

func _q24_from_z(tls *libc.TLS, q24 Tmpcb_ptr, z Tmpc_srcptr, err_re uint64, err_im uint64) {
	bp := tls.Alloc(256)
	defer tls.Free(256)
	/* Given z=x+i*y, compute q24 = exp (pi*i*z/12).
	   err_re and err_im are a priori errors (in 1/2 ulp) of x and y,
	   respectively; they can be 0 if a part is exact. In particular we
	   need err_re=0 when x=0.
	   The function requires and checks that |x|<=5/8 and y>=1/2.
	   Moreover if err_im != 0, it assumes (but cannot check, so this must be
	   assured by the caller) that y is a lower bound on the correct value.
	   The algorithm is taken from algorithms.tex.
	   The precision of q24 is computed from z with a little extra so that
	   the series has a good chance of being rounded to the precision of z. */
	var Y, err_a, err_b, v1 int64
	var _p Tmpfr_srcptr
	var _p1 Tmpfr_ptr
	var min, p, pz Tmpfr_prec_t
	var xzero, v2, v3 int32
	var _ /* pi at bp+0 */ Tmpfr_t
	var _ /* q24c at bp+192 */ Tmpc_t
	var _ /* r at bp+128 */ Tmpfr_t
	var _ /* s at bp+160 */ Tmpfr_t
	var _ /* t at bp+96 */ Tmpfr_t
	var _ /* u at bp+32 */ Tmpfr_t
	var _ /* v at bp+64 */ Tmpfr_t
	_, _, _, _, _, _, _, _, _, _, _, _ = Y, _p, _p1, err_a, err_b, min, p, pz, xzero, v1, v2, v3
	if (*t__mpfr_struct)(unsafe.Pointer(z)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(z+32)).F_mpfr_prec {
		v1 = (*t__mpfr_struct)(unsafe.Pointer(z)).F_mpfr_prec
	} else {
		v1 = (*t__mpfr_struct)(unsafe.Pointer(z + 32)).F_mpfr_prec
	}
	pz = v1
	xzero = libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(z)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)))
	if libmpfr.Xmpfr_cmp_d(tls, z, float64(0.625)) > 0 || libmpfr.Xmpfr_cmp_d(tls, z, -libc.Float64FromFloat64(0.625)) < 0 || libmpfr.Xmpfr_cmp_d(tls, z+32, float64(0.5)) < 0 || xzero != 0 && err_re > uint64(0) {
		Xmpcb_set_inf(tls, q24)
	} else {
		/* Experiments seem to imply that it is enough to add 20 bits to the
		   target precision; to be on the safe side, we also add 1%. */
		p = pz*int64(101)/int64(100) + int64(20)
		/* We need 2^p >= 240 + 66 k_x = 240 + 33 err_re. */
		if p < libc.Int64FromUint64(libc.Uint64FromInt32(m_CHAR_BIT)*libc.Uint64FromInt64(8)) {
			min = libc.Int64FromUint64((uint64(240) + uint64(33)*err_re) >> p)
			for min > 0 {
				p++
				min >>= int64(1)
			}
		}
		libmpfr.Xmpfr_init2(tls, bp, p)
		libmpfr.Xmpfr_init2(tls, bp+32, p)
		libmpfr.Xmpfr_init2(tls, bp+64, p)
		libmpfr.Xmpfr_init2(tls, bp+96, p)
		libmpfr.Xmpfr_init2(tls, bp+128, p)
		libmpfr.Xmpfr_init2(tls, bp+160, p)
		Xmpc_init2(tls, bp+192, p)
		libmpfr.Xmpfr_const_pi(tls, bp, int32(_MPFR_RNDD))
		_ = libmpfr.Xmpfr_div_ui(tls, bp, bp, libc.Uint64FromInt32(libc.Int32FromInt32(12)), int32(_MPFR_RNDD))
		libmpfr.Xmpfr_mul(tls, bp+32, z+32, bp, int32(_MPFR_RNDD))
		libmpfr.Xmpfr_neg(tls, bp+32, bp+32, int32(_MPFR_RNDU))
		libmpfr.Xmpfr_mul(tls, bp+64, z, bp, int32(_MPFR_RNDN))
		libmpfr.Xmpfr_exp(tls, bp+96, bp+32, int32(_MPFR_RNDU))
		if xzero != 0 {
			{
				_p = bp + 96
				v2 = libmpfr.Xmpfr_set4(tls, bp+192, _p, int32(_MPFR_RNDN), (*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign)
			}
			_ = v2
			{
				_p1 = bp + 192 + 32
				(*t__mpfr_struct)(unsafe.Pointer(_p1)).F_mpfr_sign = int32(1)
				(*t__mpfr_struct)(unsafe.Pointer(_p1)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
				_ = int32(_MPFR_RNDN)
				v3 = 0
			}
			_ = v3
		} else {
			/* Unfortunately we cannot round in two different directions with
			   mpfr_sin_cos. */
			libmpfr.Xmpfr_cos(tls, bp+128, bp+64, int32(_MPFR_RNDZ))
			libmpfr.Xmpfr_sin(tls, bp+160, bp+64, int32(_MPFR_RNDA))
			libmpfr.Xmpfr_mul(tls, bp+192, bp+96, bp+128, int32(_MPFR_RNDN))
			libmpfr.Xmpfr_mul(tls, bp+192+32, bp+96, bp+160, int32(_MPFR_RNDN))
		}
		Y = (*t__mpfr_struct)(unsafe.Pointer(z + 32)).F_mpfr_exp
		if xzero != 0 {
			Y = libc.Int64FromUint64((uint64(224) + uint64(33)*err_im + uint64(63)) / uint64(64) << Y)
			err_a = Y + int64(1)
			err_b = 0
		} else {
			if Y >= int64(2) {
				Y = libc.Int64FromUint64((uint64(32) + uint64(5)*err_im) << (Y - int64(2)))
			} else {
				if Y == int64(1) {
					Y = libc.Int64FromUint64(uint64(16) + (uint64(5)*err_im+uint64(1))/uint64(2))
				} else { /* Y == 0 */
					Y = libc.Int64FromUint64(uint64(8) + (uint64(5)*err_im+uint64(3))/uint64(4))
				}
			}
			err_a = libc.Int64FromUint64(libc.Uint64FromInt64(Y+int64(9)) + err_re)
			err_b = libc.Int64FromUint64(libc.Uint64FromInt64(Y) + (uint64(67)+uint64(9)*err_re+uint64(1))/uint64(2))
		}
		Xmpcb_set_c(tls, q24, bp+192, p, libc.Uint64FromInt64(err_a), libc.Uint64FromInt64(err_b))
		libmpfr.Xmpfr_clear(tls, bp)
		libmpfr.Xmpfr_clear(tls, bp+32)
		libmpfr.Xmpfr_clear(tls, bp+64)
		libmpfr.Xmpfr_clear(tls, bp+96)
		libmpfr.Xmpfr_clear(tls, bp+128)
		libmpfr.Xmpfr_clear(tls, bp+160)
		Xmpc_clear(tls, bp+192)
	}
}

func Xmpcb_eta_err(tls *libc.TLS, eta Tmpcb_ptr, z Tmpc_srcptr, err_re uint64, err_im uint64) {
	bp := tls.Alloc(80)
	defer tls.Free(80)
	/* Given z=x+i*y in the fundamental domain, compute eta (z).
	   err_re and err_im are a priori errors (in 1/2 ulp) of x and y,
	   respectively; they can be 0 if a part is exact. In particular we
	   need err_re=0 when x=0.
	   The function requires (and checks through the call to q24_from_z)
	   that |x|<=5/8 and y>=1/2.
	   Moreover if err_im != 0, it assumes (but cannot check, so this must
	   be assured by the caller) that y is a lower bound on the correct
	   value. */
	var _ /* q24 at bp+0 */ Tmpcb_t
	Xmpcb_init(tls, bp)
	_q24_from_z(tls, bp, z, err_re, err_im)
	_mpcb_eta_q24(tls, eta, bp)
	Xmpcb_clear(tls, bp)
}

func Xmpc_eta_fund(tls *libc.TLS, rop Tmpc_ptr, z Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(288)
	defer tls.Free(288)
	/* Given z in the fundamental domain for Sl_2 (Z), that is,
	   |Re z| <= 1/2 and |z| >= 1, compute Dedekind eta (z).
	   Outside the fundamental domain, the function may loop
	   indefinitely. */
	var _p, _p1 Tmpfr_srcptr
	var _p2, _p3 Tmpfr_ptr
	var inex, ok, xzero, v10, v11, v12, v13, v6, v7, v8, v9 int32
	var prec Tmpfr_prec_t
	var v1, v2, v3, v4, v5 int64
	var _ /* eta at bp+64 */ Tmpcb_t
	var _ /* fuzz at bp+144 */ Tmpc_t
	var _ /* fuzzb at bp+208 */ Tmpcb_t
	var _ /* zl at bp+0 */ Tmpc_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = _p, _p1, _p2, _p3, inex, ok, prec, xzero, v1, v10, v11, v12, v13, v2, v3, v4, v5, v6, v7, v8, v9
	Xmpc_init2(tls, bp, int64(2))
	Xmpcb_init(tls, bp+64)
	xzero = libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(z)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)))
	if (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(rop+32)).F_mpfr_prec {
		v2 = (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec
	} else {
		v2 = (*t__mpfr_struct)(unsafe.Pointer(rop + 32)).F_mpfr_prec
	}
	if (*t__mpfr_struct)(unsafe.Pointer(z)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(z+32)).F_mpfr_prec {
		v3 = (*t__mpfr_struct)(unsafe.Pointer(z)).F_mpfr_prec
	} else {
		v3 = (*t__mpfr_struct)(unsafe.Pointer(z + 32)).F_mpfr_prec
	}
	if v2 > v3 {
		if (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(rop+32)).F_mpfr_prec {
			v4 = (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec
		} else {
			v4 = (*t__mpfr_struct)(unsafe.Pointer(rop + 32)).F_mpfr_prec
		}
		v1 = v4
	} else {
		if (*t__mpfr_struct)(unsafe.Pointer(z)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(z+32)).F_mpfr_prec {
			v5 = (*t__mpfr_struct)(unsafe.Pointer(z)).F_mpfr_prec
		} else {
			v5 = (*t__mpfr_struct)(unsafe.Pointer(z + 32)).F_mpfr_prec
		}
		v1 = v5
	}
	prec = v1
	for cond := true; cond; cond = !(ok != 0) {
		Xmpc_set_prec(tls, bp, prec)
		Xmpc_set(tls, bp, z, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4)) /* exact */
		Xmpcb_eta_err(tls, bp+64, bp, uint64(0), uint64(0))
		if !(xzero != 0) {
			ok = Xmpcb_can_round(tls, bp+64, (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec, (*t__mpfr_struct)(unsafe.Pointer(rop+32)).F_mpfr_prec, rnd)
		} else {
			Xmpc_init2(tls, bp+144, prec)
			Xmpcb_init(tls, bp+208)
			Xmpc_set_ui_ui(tls, bp+144, uint64(0), uint64(1), int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
			Xmpc_div_ui(tls, bp+144, bp+144, uint64(10), int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
			Xmpcb_set_c(tls, bp+208, bp+144, prec, uint64(0), uint64(1))
			ok = libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(bp+64+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)))
			Xmpcb_add(tls, bp+64, bp+64, bp+208)
			ok &= Xmpcb_can_round(tls, bp+64, (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec, int64(2), rnd)
			Xmpc_clear(tls, bp+144)
			Xmpcb_clear(tls, bp+208)
		}
		prec += int64(20)
	}
	if !(xzero != 0) {
		inex = Xmpcb_round(tls, rop, bp+64, rnd)
	} else {
		{
			_p = bp + 64
			v7 = libmpfr.Xmpfr_set4(tls, rop, _p, rnd&libc.Int32FromInt32(0x0F), (*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign)
		}
		if v7 < 0 {
			v6 = int32(2)
		} else {
			{
				_p1 = bp + 64
				v9 = libmpfr.Xmpfr_set4(tls, rop, _p1, rnd&libc.Int32FromInt32(0x0F), (*t__mpfr_struct)(unsafe.Pointer(_p1)).F_mpfr_sign)
			}
			if v9 == 0 {
				v8 = 0
			} else {
				v8 = int32(1)
			}
			v6 = v8
		}
		{
			_p2 = rop + 32
			(*t__mpfr_struct)(unsafe.Pointer(_p2)).F_mpfr_sign = int32(1)
			(*t__mpfr_struct)(unsafe.Pointer(_p2)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
			_ = int32(_MPFR_RNDN)
			v11 = 0
		}
		if v11 < 0 {
			v10 = int32(2)
		} else {
			{
				_p3 = rop + 32
				(*t__mpfr_struct)(unsafe.Pointer(_p3)).F_mpfr_sign = int32(1)
				(*t__mpfr_struct)(unsafe.Pointer(_p3)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
				_ = int32(_MPFR_RNDN)
				v13 = 0
			}
			if v13 == 0 {
				v12 = 0
			} else {
				v12 = int32(1)
			}
			v10 = v12
		}
		inex = v6 | v10<<int32(2)
	}
	Xmpc_clear(tls, bp)
	Xmpcb_clear(tls, bp+64)
	return inex
}

func Xmpc_exp(tls *libc.TLS, rop Tmpc_ptr, op Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(192)
	defer tls.Free(192)
	var _p, _p2, _p3, _p4, _p5 Tmpfr_srcptr
	var _p1 Tmpfr_ptr
	var inex_im, inex_re, ok, real_sign, saved_overflow, saved_underflow, v1, v10, v11, v12, v13, v14, v15, v16, v2, v21, v22, v23, v24, v25, v26, v3, v4, v5, v6, v7, v8, v9 int32
	var prec Tmpfr_prec_t
	var saved_emax, saved_emin Tmpfr_exp_t
	var v17, v18, v19, v20 int64
	var _ /* c at bp+128 */ Tmpfr_t
	var _ /* n at bp+96 */ Tmpfr_t
	var _ /* s at bp+160 */ Tmpfr_t
	var _ /* x at bp+0 */ Tmpfr_t
	var _ /* y at bp+32 */ Tmpfr_t
	var _ /* z at bp+64 */ Tmpfr_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = _p, _p1, _p2, _p3, _p4, _p5, inex_im, inex_re, ok, prec, real_sign, saved_emax, saved_emin, saved_overflow, saved_underflow, v1, v10, v11, v12, v13, v14, v15, v16, v17, v18, v19, v2, v20, v21, v22, v23, v24, v25, v26, v3, v4, v5, v6, v7, v8, v9
	ok = 0
	/* special values */
	if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		/* NaNs
		   exp(nan +i*y) = nan -i*0   if y = -0,
		                   nan +i*0   if y = +0,
		                   nan +i*nan otherwise
		   exp(x+i*nan) =   +/-0 +/-i*0 if x=-inf,
		                  +/-inf +i*nan if x=+inf,
		                     nan +i*nan otherwise */
		if (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			return Xmpc_set(tls, rop, op, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
		}
		if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_sign < 0 {
				return Xmpc_set_ui_ui(tls, rop, uint64(0), uint64(0), int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
			} else {
				libmpfr.Xmpfr_set_inf(tls, rop, +libc.Int32FromInt32(1))
				libmpfr.Xmpfr_set_nan(tls, rop+32)
				return libc.Int32FromInt32(0) | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2) /* Inf/NaN are exact */
			}
		}
		libmpfr.Xmpfr_set_nan(tls, rop)
		libmpfr.Xmpfr_set_nan(tls, rop+32)
		return libc.Int32FromInt32(0) | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2) /* NaN is exact */
	}
	if (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		/* special case when the input is real
		   exp(x-i*0) = exp(x) -i*0, even if x is NaN
		   exp(x+i*0) = exp(x) +i*0, even if x is NaN */
		inex_re = libmpfr.Xmpfr_exp(tls, rop, op, rnd&libc.Int32FromInt32(0x0F))
		{
			_p = op + 32
			v1 = libmpfr.Xmpfr_set4(tls, rop+32, _p, rnd>>libc.Int32FromInt32(4), (*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign)
		}
		inex_im = v1
		if inex_re < 0 {
			v2 = int32(2)
		} else {
			if inex_re == 0 {
				v3 = 0
			} else {
				v3 = int32(1)
			}
			v2 = v3
		}
		if inex_im < 0 {
			v4 = int32(2)
		} else {
			if inex_im == 0 {
				v5 = 0
			} else {
				v5 = int32(1)
			}
			v4 = v5
		}
		return v2 | v4<<int32(2)
	}
	if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		/* special case when the input is imaginary  */
		inex_re = libmpfr.Xmpfr_cos(tls, rop, op+32, rnd&libc.Int32FromInt32(0x0F))
		inex_im = libmpfr.Xmpfr_sin(tls, rop+32, op+32, rnd>>libc.Int32FromInt32(4))
		if inex_re < 0 {
			v6 = int32(2)
		} else {
			if inex_re == 0 {
				v7 = 0
			} else {
				v7 = int32(1)
			}
			v6 = v7
		}
		if inex_im < 0 {
			v8 = int32(2)
		} else {
			if inex_im == 0 {
				v9 = 0
			} else {
				v9 = int32(1)
			}
			v8 = v9
		}
		return v6 | v8<<int32(2)
	}
	if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		/* real part is an infinity,
		   exp(-inf +i*y) = 0*(cos y +i*sin y)
		   exp(+inf +i*y) = +/-inf +i*nan         if y = +/-inf
		                    +inf*(cos y +i*sin y) if 0 < |y| < inf */
		libmpfr.Xmpfr_init2(tls, bp+96, int64(2))
		if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_sign < 0 {
			{
				_p1 = bp + 96
				(*t__mpfr_struct)(unsafe.Pointer(_p1)).F_mpfr_sign = int32(1)
				(*t__mpfr_struct)(unsafe.Pointer(_p1)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
				_ = int32(_MPFR_RNDN)
				v10 = 0
			}
			_ = v10
		} else {
			libmpfr.Xmpfr_set_inf(tls, bp+96, +libc.Int32FromInt32(1))
		}
		if (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			real_sign = libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_sign < 0)
			{
				_p2 = bp + 96
				v11 = libmpfr.Xmpfr_set4(tls, rop, _p2, int32(_MPFR_RNDN), (*t__mpfr_struct)(unsafe.Pointer(_p2)).F_mpfr_sign)
			}
			inex_re = v11
			if real_sign != 0 {
				{
					_p3 = bp + 96
					v12 = libmpfr.Xmpfr_set4(tls, rop+32, _p3, int32(_MPFR_RNDN), (*t__mpfr_struct)(unsafe.Pointer(_p3)).F_mpfr_sign)
				}
				inex_im = v12
			} else {
				libmpfr.Xmpfr_set_nan(tls, rop+32)
				inex_im = 0 /* NaN is exact */
			}
		} else {
			libmpfr.Xmpfr_init2(tls, bp+128, int64(2))
			libmpfr.Xmpfr_init2(tls, bp+160, int64(2))
			libmpfr.Xmpfr_sin_cos(tls, bp+160, bp+128, op+32, int32(_MPFR_RNDN))
			inex_re = libmpfr.Xmpfr_set4(tls, rop, bp+96, int32(_MPFR_RNDN), (*t__mpfr_struct)(unsafe.Pointer(bp+128)).F_mpfr_sign)
			inex_im = libmpfr.Xmpfr_set4(tls, rop+32, bp+96, int32(_MPFR_RNDN), (*t__mpfr_struct)(unsafe.Pointer(bp+160)).F_mpfr_sign)
			libmpfr.Xmpfr_clear(tls, bp+160)
			libmpfr.Xmpfr_clear(tls, bp+128)
		}
		libmpfr.Xmpfr_clear(tls, bp+96)
		if inex_re < 0 {
			v13 = int32(2)
		} else {
			if inex_re == 0 {
				v14 = 0
			} else {
				v14 = int32(1)
			}
			v13 = v14
		}
		if inex_im < 0 {
			v15 = int32(2)
		} else {
			if inex_im == 0 {
				v16 = 0
			} else {
				v16 = int32(1)
			}
			v15 = v16
		}
		return v13 | v15<<int32(2)
	}
	if (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		/* real part is finite non-zero number, imaginary part is an infinity */
		libmpfr.Xmpfr_set_nan(tls, rop)
		libmpfr.Xmpfr_set_nan(tls, rop+32)
		return libc.Int32FromInt32(0) | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2) /* NaN is exact */
	}
	saved_emin = libmpfr.Xmpfr_get_emin(tls)
	saved_emax = libmpfr.Xmpfr_get_emax(tls)
	libmpfr.Xmpfr_set_emin(tls, libmpfr.Xmpfr_get_emin_min(tls))
	libmpfr.Xmpfr_set_emax(tls, libmpfr.Xmpfr_get_emax_max(tls))
	/* from now on, both parts of op are regular numbers */
	if (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(rop+32)).F_mpfr_prec {
		v17 = (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec
	} else {
		v17 = (*t__mpfr_struct)(unsafe.Pointer(rop + 32)).F_mpfr_prec
	}
	if -(*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp > int64(libc.Int32FromInt32(0)) {
		v19 = -(*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp
	} else {
		v19 = int64(libc.Int32FromInt32(0))
	}
	if v19 > -(*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp {
		if -(*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp > int64(libc.Int32FromInt32(0)) {
			v20 = -(*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp
		} else {
			v20 = int64(libc.Int32FromInt32(0))
		}
		v18 = v20
	} else {
		v18 = -(*t__mpfr_struct)(unsafe.Pointer(op + 32)).F_mpfr_exp
	}
	prec = v17 + v18
	/* When op is close to 0, then exp is close to 1+Re(op), while
	   cos is close to 1-Im(op); to decide on the ternary value of exp*cos,
	   we need a high enough precision so that none of exp or cos is
	   computed as 1. */
	libmpfr.Xmpfr_init2(tls, bp, int64(2))
	libmpfr.Xmpfr_init2(tls, bp+32, int64(2))
	libmpfr.Xmpfr_init2(tls, bp+64, int64(2))
	/* save the underflow or overflow flags from MPFR */
	saved_underflow = libmpfr.Xmpfr_underflow_p(tls)
	saved_overflow = libmpfr.Xmpfr_overflow_p(tls)
	for cond := true; cond; cond = ok == 0 {
		prec += prec/int64(2) + Xmpc_ceil_log2(tls, prec) + int64(5)
		libmpfr.Xmpfr_set_prec(tls, bp, prec)
		libmpfr.Xmpfr_set_prec(tls, bp+32, prec)
		libmpfr.Xmpfr_set_prec(tls, bp+64, prec)
		/* FIXME: x may overflow so x.y does overflow too, while Re(exp(op))
		   could be represented in the precision of rop. */
		libmpfr.Xmpfr_clear_overflow(tls)
		libmpfr.Xmpfr_clear_underflow(tls)
		libmpfr.Xmpfr_exp(tls, bp, op, int32(_MPFR_RNDN))                  /* error <= 0.5ulp */
		libmpfr.Xmpfr_sin_cos(tls, bp+64, bp+32, op+32, int32(_MPFR_RNDN)) /* errors <= 0.5ulp */
		libmpfr.Xmpfr_mul(tls, bp+32, bp+32, bp, int32(_MPFR_RNDN))        /* error <= 2ulp */
		ok = libc.BoolInt32(libmpfr.Xmpfr_overflow_p(tls) != 0 || (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || libmpfr.Xmpfr_can_round(tls, bp+32, prec-int64(2), int32(_MPFR_RNDN), int32(_MPFR_RNDZ), (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec+libc.BoolInt64(rnd&libc.Int32FromInt32(0x0F) == int32(_MPFR_RNDN))) != 0)
		if ok != 0 { /* compute imaginary part */
			libmpfr.Xmpfr_mul(tls, bp+64, bp+64, bp, int32(_MPFR_RNDN))
			ok = libc.BoolInt32(libmpfr.Xmpfr_overflow_p(tls) != 0 || (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || libmpfr.Xmpfr_can_round(tls, bp+64, prec-int64(2), int32(_MPFR_RNDN), int32(_MPFR_RNDZ), (*t__mpfr_struct)(unsafe.Pointer(rop+32)).F_mpfr_prec+libc.BoolInt64(rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDN))) != 0)
		}
	}
	{
		_p4 = bp + 32
		v21 = libmpfr.Xmpfr_set4(tls, rop, _p4, rnd&libc.Int32FromInt32(0x0F), (*t__mpfr_struct)(unsafe.Pointer(_p4)).F_mpfr_sign)
	}
	inex_re = v21
	{
		_p5 = bp + 64
		v22 = libmpfr.Xmpfr_set4(tls, rop+32, _p5, rnd>>libc.Int32FromInt32(4), (*t__mpfr_struct)(unsafe.Pointer(_p5)).F_mpfr_sign)
	}
	inex_im = v22
	if libmpfr.Xmpfr_overflow_p(tls) != 0 {
		inex_re = Xmpc_fix_inf(tls, rop, rnd&libc.Int32FromInt32(0x0F))
		inex_im = Xmpc_fix_inf(tls, rop+32, rnd>>libc.Int32FromInt32(4))
	} else {
		if libmpfr.Xmpfr_underflow_p(tls) != 0 {
			inex_re = Xmpc_fix_zero(tls, rop, rnd&libc.Int32FromInt32(0x0F))
			inex_im = Xmpc_fix_zero(tls, rop+32, rnd>>libc.Int32FromInt32(4))
		}
	}
	libmpfr.Xmpfr_clear(tls, bp)
	libmpfr.Xmpfr_clear(tls, bp+32)
	libmpfr.Xmpfr_clear(tls, bp+64)
	/* restore underflow and overflow flags from MPFR */
	if saved_underflow != 0 {
		libmpfr.Xmpfr_set_underflow(tls)
	}
	if saved_overflow != 0 {
		libmpfr.Xmpfr_set_overflow(tls)
	}
	/* restore the exponent range, and check the range of results */
	libmpfr.Xmpfr_set_emin(tls, saved_emin)
	libmpfr.Xmpfr_set_emax(tls, saved_emax)
	inex_re = libmpfr.Xmpfr_check_range(tls, rop, inex_re, rnd&libc.Int32FromInt32(0x0F))
	inex_im = libmpfr.Xmpfr_check_range(tls, rop+32, inex_im, rnd>>libc.Int32FromInt32(4))
	if inex_re < 0 {
		v23 = int32(2)
	} else {
		if inex_re == 0 {
			v24 = 0
		} else {
			v24 = int32(1)
		}
		v23 = v24
	}
	if inex_im < 0 {
		v25 = int32(2)
	} else {
		if inex_im == 0 {
			v26 = 0
		} else {
			v26 = int32(1)
		}
		v25 = v26
	}
	return v23 | v25<<int32(2)
}

func Xmpc_fma_naive(tls *libc.TLS, r Tmpc_ptr, a Tmpc_srcptr, b Tmpc_srcptr, c Tmpc_srcptr, rnd Tmpc_rnd_t) (r1 int32) {
	bp := tls.Alloc(160)
	defer tls.Free(160)
	var inex_im, inex_re, v1, v2, v3, v4 int32
	var _ /* ima_imb at bp+96 */ Tmpfr_t
	var _ /* ima_reb at bp+64 */ Tmpfr_t
	var _ /* rea_imb at bp+32 */ Tmpfr_t
	var _ /* rea_reb at bp+0 */ Tmpfr_t
	var _ /* sum at bp+128 */ [3]Tmpfr_ptr
	_, _, _, _, _, _ = inex_im, inex_re, v1, v2, v3, v4
	libmpfr.Xmpfr_init2(tls, bp, (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_prec+(*t__mpfr_struct)(unsafe.Pointer(b)).F_mpfr_prec)
	libmpfr.Xmpfr_init2(tls, bp+32, (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_prec+(*t__mpfr_struct)(unsafe.Pointer(b+32)).F_mpfr_prec)
	libmpfr.Xmpfr_init2(tls, bp+64, (*t__mpfr_struct)(unsafe.Pointer(a+32)).F_mpfr_prec+(*t__mpfr_struct)(unsafe.Pointer(b)).F_mpfr_prec)
	libmpfr.Xmpfr_init2(tls, bp+96, (*t__mpfr_struct)(unsafe.Pointer(a+32)).F_mpfr_prec+(*t__mpfr_struct)(unsafe.Pointer(b+32)).F_mpfr_prec)
	libmpfr.Xmpfr_mul(tls, bp, a, b, int32(_MPFR_RNDZ))          /* exact */
	libmpfr.Xmpfr_mul(tls, bp+32, a, b+32, int32(_MPFR_RNDZ))    /* exact */
	libmpfr.Xmpfr_mul(tls, bp+64, a+32, b, int32(_MPFR_RNDZ))    /* exact */
	libmpfr.Xmpfr_mul(tls, bp+96, a+32, b+32, int32(_MPFR_RNDZ)) /* exact */
	libmpfr.Xmpfr_neg(tls, bp+96, bp+96, int32(_MPFR_RNDZ))
	(*(*[3]Tmpfr_ptr)(unsafe.Pointer(bp + 128)))[0] = bp
	(*(*[3]Tmpfr_ptr)(unsafe.Pointer(bp + 128)))[int32(1)] = bp + 96
	(*(*[3]Tmpfr_ptr)(unsafe.Pointer(bp + 128)))[int32(2)] = c
	inex_re = libmpfr.Xmpfr_sum(tls, r, bp+128, uint64(3), rnd&libc.Int32FromInt32(0x0F))
	(*(*[3]Tmpfr_ptr)(unsafe.Pointer(bp + 128)))[0] = bp + 32
	(*(*[3]Tmpfr_ptr)(unsafe.Pointer(bp + 128)))[int32(1)] = bp + 64
	(*(*[3]Tmpfr_ptr)(unsafe.Pointer(bp + 128)))[int32(2)] = c + 32
	inex_im = libmpfr.Xmpfr_sum(tls, r+32, bp+128, uint64(3), rnd>>libc.Int32FromInt32(4))
	libmpfr.Xmpfr_clear(tls, bp)
	libmpfr.Xmpfr_clear(tls, bp+32)
	libmpfr.Xmpfr_clear(tls, bp+64)
	libmpfr.Xmpfr_clear(tls, bp+96)
	if inex_re < 0 {
		v1 = int32(2)
	} else {
		if inex_re == 0 {
			v2 = 0
		} else {
			v2 = int32(1)
		}
		v1 = v2
	}
	if inex_im < 0 {
		v3 = int32(2)
	} else {
		if inex_im == 0 {
			v4 = 0
		} else {
			v4 = int32(1)
		}
		v3 = v4
	}
	return v1 | v3<<int32(2)
}

// C documentation
//
//	/* The algorithm is as follows:
//	   - in a first pass, we use the target precision + some extra bits
//	   - if it fails, we add the number of cancelled bits when adding
//	     Re(a*b) and Re(c) [similarly for the imaginary part]
//	   - it is fails again, we call the mpc_fma_naive function, which also
//	     deals with the special cases */
func Xmpc_fma(tls *libc.TLS, r Tmpc_ptr, a Tmpc_srcptr, b Tmpc_srcptr, c Tmpc_srcptr, rnd Tmpc_rnd_t) (r1 int32) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var diffim, diffre Tmpfr_exp_t
	var i, inex, okim, okre, v4, v5, v6 int32
	var pim, pre, wpim, wpre Tmpfr_prec_t
	var v2, v3 int64
	var _ /* ab at bp+0 */ Tmpc_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = diffim, diffre, i, inex, okim, okre, pim, pre, wpim, wpre, v2, v3, v4, v5, v6
	inex = 0
	okre = 0
	okim = 0
	if libc.BoolInt32(libmpfr.Xmpfr_number_p(tls, a) != 0 && libmpfr.Xmpfr_number_p(tls, a+32) != 0) == 0 || libc.BoolInt32(libmpfr.Xmpfr_number_p(tls, b) != 0 && libmpfr.Xmpfr_number_p(tls, b+32) != 0) == 0 || libc.BoolInt32(libmpfr.Xmpfr_number_p(tls, c) != 0 && libmpfr.Xmpfr_number_p(tls, c+32) != 0) == 0 {
		return Xmpc_fma_naive(tls, r, a, b, c, rnd)
	}
	pre = (*t__mpfr_struct)(unsafe.Pointer(r)).F_mpfr_prec
	pim = (*t__mpfr_struct)(unsafe.Pointer(r + 32)).F_mpfr_prec
	wpre = pre + Xmpc_ceil_log2(tls, pre) + int64(10)
	wpim = pim + Xmpc_ceil_log2(tls, pim) + int64(10)
	Xmpc_init3(tls, bp, wpre, wpim)
	i = 0
	for {
		if !(i < int32(2)) {
			break
		}
		Xmpc_mul(tls, bp, a, b, int32(_MPFR_RNDZ)+int32(_MPFR_RNDZ)<<libc.Int32FromInt32(4))
		if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			break
		}
		diffre = (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp
		diffim = (*t__mpfr_struct)(unsafe.Pointer(bp + 32)).F_mpfr_exp
		Xmpc_add(tls, bp, bp, c, int32(_MPFR_RNDZ)+int32(_MPFR_RNDZ)<<libc.Int32FromInt32(4))
		if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			break
		}
		diffre -= (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp
		diffim -= (*t__mpfr_struct)(unsafe.Pointer(bp + 32)).F_mpfr_exp
		if diffre > 0 {
			v2 = diffre + int64(1)
		} else {
			v2 = int64(1)
		}
		diffre = v2
		if diffim > 0 {
			v3 = diffim + int64(1)
		} else {
			v3 = int64(1)
		}
		diffim = v3
		if diffre > wpre {
			v4 = 0
		} else {
			v4 = libmpfr.Xmpfr_can_round(tls, bp, wpre-diffre, int32(_MPFR_RNDN), int32(_MPFR_RNDZ), pre+libc.BoolInt64(rnd&libc.Int32FromInt32(0x0F) == int32(_MPFR_RNDN)))
		}
		okre = v4
		if diffim > wpim {
			v5 = 0
		} else {
			v5 = libmpfr.Xmpfr_can_round(tls, bp+32, wpim-diffim, int32(_MPFR_RNDN), int32(_MPFR_RNDZ), pim+libc.BoolInt64(rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDN)))
		}
		okim = v5
		if okre != 0 && okim != 0 {
			inex = Xmpc_set(tls, r, bp, rnd)
			break
		}
		if i == int32(1) {
			break
		}
		if okre == 0 && diffre > int64(1) {
			wpre += diffre
		}
		if okim == 0 && diffim > int64(1) {
			wpim += diffim
		}
		libmpfr.Xmpfr_set_prec(tls, bp, wpre)
		libmpfr.Xmpfr_set_prec(tls, bp+32, wpim)
		goto _1
	_1:
		;
		i++
	}
	Xmpc_clear(tls, bp)
	if okre != 0 && okim != 0 {
		v6 = inex
	} else {
		v6 = Xmpc_fma_naive(tls, r, a, b, c, rnd)
	}
	return v6
}

func Xmpc_fr_div(tls *libc.TLS, a Tmpc_ptr, b Tmpfr_srcptr, c Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var _p Tmpfr_ptr
	var inexact, v1 int32
	var _ /* bc at bp+0 */ Tmpc_t
	_, _, _ = _p, inexact, v1
	*(*t__mpfr_struct)(unsafe.Pointer(bp)) = *(*t__mpfr_struct)(unsafe.Pointer(b))
	libmpfr.Xmpfr_init(tls, bp+32)
	/* we consider the operand b to have imaginary part +0 */
	{
		_p = bp + 32
		(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign = int32(1)
		(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
		_ = int32(_MPFR_RNDN)
		v1 = 0
	}
	_ = v1
	inexact = Xmpc_div(tls, a, bp, c, rnd)
	libmpfr.Xmpfr_clear(tls, bp+32)
	return inexact
}

// C documentation
//
//	/* return 0 iff both the real and imaginary parts are exact */
func Xmpc_fr_sub(tls *libc.TLS, a Tmpc_ptr, b Tmpfr_srcptr, c Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	var inex_im, inex_re, v1, v2, v3, v4 int32
	_, _, _, _, _, _ = inex_im, inex_re, v1, v2, v3, v4
	inex_re = libmpfr.Xmpfr_sub(tls, a, b, c, rnd&libc.Int32FromInt32(0x0F))
	inex_im = libmpfr.Xmpfr_neg(tls, a+32, c+32, rnd>>libc.Int32FromInt32(4))
	if inex_re < 0 {
		v1 = int32(2)
	} else {
		if inex_re == 0 {
			v2 = 0
		} else {
			v2 = int32(1)
		}
		v1 = v2
	}
	if inex_im < 0 {
		v3 = int32(2)
	} else {
		if inex_im == 0 {
			v4 = 0
		} else {
			v4 = int32(1)
		}
		v3 = v4
	}
	return v1 | v3<<int32(2)
}

func Xmpc_get_prec2(tls *libc.TLS, pr uintptr, pi uintptr, x Tmpc_srcptr) {
	*(*Tmpfr_prec_t)(unsafe.Pointer(pr)) = (*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_prec
	*(*Tmpfr_prec_t)(unsafe.Pointer(pi)) = (*t__mpfr_struct)(unsafe.Pointer(x + 32)).F_mpfr_prec
}

func Xmpc_get_prec(tls *libc.TLS, x Tmpc_srcptr) (r Tmpfr_prec_t) {
	var precre Tmpfr_prec_t
	var v1 int64
	_, _ = precre, v1
	precre = (*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_prec
	if (*t__mpfr_struct)(unsafe.Pointer(x+32)).F_mpfr_prec == precre {
		v1 = precre
	} else {
		v1 = 0
	}
	return v1
}

func Xmpc_get_version(tls *libc.TLS) (r uintptr) {
	return __ccgo_ts
}

const m_I = "_Complex_I"
const m_LC_ALL = 6
const m_LC_ALL_MASK = 0x7fffffff
const m_LC_COLLATE = 3
const m_LC_CTYPE = 0
const m_LC_MESSAGES = 5
const m_LC_MONETARY = 4
const m_LC_NUMERIC = 1
const m_LC_TIME = 2
const m_MPC_EXP_FORMAT_SPEC = "li"
const m_complex = "_Complex"

type Tlconv = struct {
	Fdecimal_point      uintptr
	Fthousands_sep      uintptr
	Fgrouping           uintptr
	Fint_curr_symbol    uintptr
	Fcurrency_symbol    uintptr
	Fmon_decimal_point  uintptr
	Fmon_thousands_sep  uintptr
	Fmon_grouping       uintptr
	Fpositive_sign      uintptr
	Fnegative_sign      uintptr
	Fint_frac_digits    int8
	Ffrac_digits        int8
	Fp_cs_precedes      int8
	Fp_sep_by_space     int8
	Fn_cs_precedes      int8
	Fn_sep_by_space     int8
	Fp_sign_posn        int8
	Fn_sign_posn        int8
	Fint_p_cs_precedes  int8
	Fint_p_sep_by_space int8
	Fint_n_cs_precedes  int8
	Fint_n_sep_by_space int8
	Fint_p_sign_posn    int8
	Fint_n_sign_posn    int8
}

type Tlocale_t = uintptr

func Xmpc_get_dc(tls *libc.TLS, op Tmpc_srcptr, rnd Tmpc_rnd_t) (r complex128) {
	return libc.Complex128FromFloat64(libc.Float64FromComplex64(libc.Complex64FromFloat32(0)+libc.Complex64FromComplex64(0+1i))*libmpfr.Xmpfr_get_d(tls, op+32, rnd>>libc.Int32FromInt32(4)) + libmpfr.Xmpfr_get_d(tls, op, rnd&libc.Int32FromInt32(0x0F)))
}

func Xmpc_get_ldc(tls *libc.TLS, op Tmpc_srcptr, rnd Tmpc_rnd_t) (r complex128) {
	return libc.Complex128FromFloat64(libc.Float64FromComplex64(libc.Complex64FromFloat32(0)+libc.Complex64FromComplex64(0+1i))*libmpfr.Xmpfr_get_ld(tls, op+32, rnd>>libc.Int32FromInt32(4)) + libmpfr.Xmpfr_get_ld(tls, op, rnd&libc.Int32FromInt32(0x0F)))
}

/* Code for mpc_get_str. The output format is "(real imag)", the decimal point
   of the locale is used. */

/* mpfr_prec_t can be either int or long int */

func _pretty_zero(tls *libc.TLS, zero Tmpfr_srcptr) (r uintptr) {
	var pretty uintptr
	var v1 int32
	_, _ = pretty, v1
	pretty = Xmpc_alloc_str(tls, uint64(3))
	if (*t__mpfr_struct)(unsafe.Pointer(zero)).F_mpfr_sign < 0 {
		v1 = int32('-')
	} else {
		v1 = int32('+')
	}
	*(*int8)(unsafe.Pointer(pretty)) = int8(v1)
	*(*int8)(unsafe.Pointer(pretty + 1)) = int8('0')
	*(*int8)(unsafe.Pointer(pretty + 2)) = int8('\000')
	return pretty
}

func _prettify(tls *libc.TLS, str uintptr, expo Tmpfr_exp_t, base int32, special int32) (r uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var p, pretty, s, v1, v2, v3, v4, v5, v6, v7, v8 uintptr
	var sign int32
	var sz Tsize_t
	var x, xx Tmpfr_exp_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = p, pretty, s, sign, sz, x, xx, v1, v2, v3, v4, v5, v6, v7, v8
	sz = libc.Xstrlen(tls, str) + uint64(1) /* + terminal '\0' */
	if special != 0 {
		/* special number: nan or inf */
		pretty = Xmpc_alloc_str(tls, sz)
		libc.Xstrcpy(tls, pretty, str)
		return pretty
	}
	/* regular number */
	sign = libc.BoolInt32(int32(*(*int8)(unsafe.Pointer(str))) == int32('-') || int32(*(*int8)(unsafe.Pointer(str))) == int32('+'))
	x = expo - int64(1) /* expo is the exponent value with decimal point BEFORE
	   the first digit, we want decimal point AFTER the first
	   digit */
	if base == int32(16) {
		x *= int64(4)
	} /* the output exponent is a binary exponent */
	sz++ /* + decimal point */
	if x != 0 {
		sz += uint64(3) /* + exponent char + sign + 1 digit */
		if x < 0 {
			/* avoid overflow when changing sign (assuming that, for the
			   mpfr_exp_t type, (max value) is greater than (- min value / 10)) */
			if x < int64(-int32(10)) {
				xx = -(x / int64(10))
				sz++
			} else {
				xx = -x
			}
		} else {
			xx = x
		}
		/* compute sz += floor(log(expo)/log(10)) without using libm
		   functions */
		for xx > int64(9) {
			sz++
			xx /= int64(10)
		}
	}
	pretty = Xmpc_alloc_str(tls, sz)
	p = pretty
	/* 1. optional sign plus first digit */
	s = str
	v1 = p
	p++
	v2 = s
	s++
	*(*int8)(unsafe.Pointer(v1)) = *(*int8)(unsafe.Pointer(v2))
	if sign != 0 {
		v3 = p
		p++
		v4 = s
		s++
		*(*int8)(unsafe.Pointer(v3)) = *(*int8)(unsafe.Pointer(v4))
	}
	/* 2. decimal point */
	v5 = p
	p++
	*(*int8)(unsafe.Pointer(v5)) = *(*int8)(unsafe.Pointer((*Tlconv)(unsafe.Pointer(libc.Xlocaleconv(tls))).Fdecimal_point))
	*(*int8)(unsafe.Pointer(p)) = int8('\000')
	/* 3. other significant digits */
	libc.Xstrcat(tls, pretty, s)
	/* 4. exponent (in base ten) */
	if x == 0 {
		return pretty
	}
	p = pretty + uintptr(libc.Xstrlen(tls, str)) + uintptr(1)
	switch base {
	case int32(10):
		v6 = p
		p++
		*(*int8)(unsafe.Pointer(v6)) = int8('e')
	case int32(2):
		fallthrough
	case int32(16):
		v7 = p
		p++
		*(*int8)(unsafe.Pointer(v7)) = int8('p')
	default:
		v8 = p
		p++
		*(*int8)(unsafe.Pointer(v8)) = int8('@')
	}
	*(*int8)(unsafe.Pointer(p)) = int8('\000')
	libc.Xsprintf(tls, p, __ccgo_ts+6, libc.VaList(bp+8, x))
	return pretty
}

func _get_pretty_str(tls *libc.TLS, base int32, n Tsize_t, x Tmpfr_srcptr, rnd Tmpfr_rnd_t) (r uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var pretty, ugly uintptr
	var _ /* expo at bp+0 */ Tmpfr_exp_t
	_, _ = pretty, ugly
	if (*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		return _pretty_zero(tls, x)
	}
	ugly = libmpfr.Xmpfr_get_str(tls, libc.UintptrFromInt32(0), bp, base, n, x, rnd)
	pretty = _prettify(tls, ugly, *(*Tmpfr_exp_t)(unsafe.Pointer(bp)), base, libc.BoolInt32(!(libmpfr.Xmpfr_number_p(tls, x) != 0)))
	libmpfr.Xmpfr_free_str(tls, ugly)
	return pretty
}

func Xmpc_get_str(tls *libc.TLS, base int32, n Tsize_t, op Tmpc_srcptr, rnd Tmpc_rnd_t) (r uintptr) {
	var complex_str, imag_str, real_str uintptr
	var needed_size Tsize_t
	_, _, _, _ = complex_str, imag_str, needed_size, real_str
	complex_str = libc.UintptrFromInt32(0)
	if base < int32(2) || base > int32(36) {
		return libc.UintptrFromInt32(0)
	}
	real_str = _get_pretty_str(tls, base, n, op, rnd&libc.Int32FromInt32(0x0F))
	imag_str = _get_pretty_str(tls, base, n, op+32, rnd>>libc.Int32FromInt32(4))
	needed_size = libc.Xstrlen(tls, real_str) + libc.Xstrlen(tls, imag_str) + uint64(4)
	complex_str = Xmpc_alloc_str(tls, needed_size)
	libc.Xstrcpy(tls, complex_str, __ccgo_ts+11)
	libc.Xstrcat(tls, complex_str, real_str)
	libc.Xstrcat(tls, complex_str, __ccgo_ts+13)
	libc.Xstrcat(tls, complex_str, imag_str)
	libc.Xstrcat(tls, complex_str, __ccgo_ts+15)
	Xmpc_free_str(tls, real_str)
	Xmpc_free_str(tls, imag_str)
	return complex_str
}

func Xmpc_imag(tls *libc.TLS, a Tmpfr_ptr, b Tmpc_srcptr, rnd Tmpfr_rnd_t) (r int32) {
	var _p Tmpfr_srcptr
	var v1 int32
	_, _ = _p, v1
	{
		_p = b + 32
		v1 = libmpfr.Xmpfr_set4(tls, a, _p, rnd, (*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign)
	}
	return v1
}

func Xmpc_init2(tls *libc.TLS, x Tmpc_ptr, prec Tmpfr_prec_t) {
	libmpfr.Xmpfr_init2(tls, x, prec)
	libmpfr.Xmpfr_init2(tls, x+32, prec)
}

func Xmpc_init3(tls *libc.TLS, x Tmpc_ptr, prec_re Tmpfr_prec_t, prec_im Tmpfr_prec_t) {
	libmpfr.Xmpfr_init2(tls, x, prec_re)
	libmpfr.Xmpfr_init2(tls, x+32, prec_im)
}

func _skip_whitespace(tls *libc.TLS, stream uintptr) (r Tsize_t) {
	var c, v1, v2 int32
	var size Tsize_t
	var v4 bool
	_, _, _, _, _ = c, size, v1, v2, v4
	c = libc.Xgetc(tls, stream)
	size = uint64(0)
	for {
		if v4 = c != -int32(1); v4 {
			v1 = libc.Int32FromUint8(libc.Uint8FromInt32(c))
			v2 = libc.BoolInt32(v1 == int32(' ') || libc.Uint32FromInt32(v1)-uint32('\t') < uint32(5))
			goto _3
		_3:
		}
		if !(v4 && v2 != 0) {
			break
		}
		c = libc.Xgetc(tls, stream)
		size++
	}
	if c != -int32(1) {
		libc.Xungetc(tls, c, stream)
	}
	return size
}

// C documentation
//
//	/* Extract from stream the longest string made up of alphanumeric char and
//	   '_' (i.e. n-char-sequence).
//	   The user must free the returned string. */
func _extract_suffix(tls *libc.TLS, stream uintptr) (r uintptr) {
	var c int32
	var nread, strsize Tsize_t
	var str uintptr
	_, _, _, _ = c, nread, str, strsize
	nread = uint64(0)
	strsize = uint64(100)
	str = Xmpc_alloc_str(tls, strsize)
	c = libc.Xgetc(tls, stream)
	for libc.Xisalnum(tls, libc.Int32FromUint8(libc.Uint8FromInt32(c))) != 0 || c == int32('_') {
		*(*int8)(unsafe.Pointer(str + uintptr(nread))) = int8(c)
		nread++
		if nread == strsize {
			str = Xmpc_realloc_str(tls, str, strsize, uint64(2)*strsize)
			strsize *= uint64(2)
		}
		c = libc.Xgetc(tls, stream)
	}
	str = Xmpc_realloc_str(tls, str, strsize, nread+uint64(1))
	strsize = nread + uint64(1)
	*(*int8)(unsafe.Pointer(str + uintptr(nread))) = int8('\000')
	if c != -int32(1) {
		libc.Xungetc(tls, c, stream)
	}
	return str
}

// C documentation
//
//	/* Extract from the stream the longest string of characters which are neither
//	   whitespace nor brackets (except for an optional bracketed n-char_sequence
//	   directly following nan or @nan@ independently of case).
//	   The user must free the returned string.                                    */
func _extract_string(tls *libc.TLS, stream uintptr) (r uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var c, ret, v1, v2 int32
	var lenstr, n, nread, strsize Tsize_t
	var str, suffix uintptr
	var v4 bool
	_, _, _, _, _, _, _, _, _, _, _ = c, lenstr, n, nread, ret, str, strsize, suffix, v1, v2, v4
	nread = uint64(0)
	strsize = uint64(100)
	str = Xmpc_alloc_str(tls, strsize)
	c = libc.Xgetc(tls, stream)
	for {
		if v4 = c != -int32(1) && c != int32('\n'); v4 {
			v1 = libc.Int32FromUint8(libc.Uint8FromInt32(c))
			v2 = libc.BoolInt32(v1 == int32(' ') || libc.Uint32FromInt32(v1)-uint32('\t') < uint32(5))
			goto _3
		_3:
		}
		if !(v4 && !(v2 != 0) && c != int32('(') && c != int32(')')) {
			break
		}
		*(*int8)(unsafe.Pointer(str + uintptr(nread))) = int8(c)
		nread++
		if nread == strsize {
			str = Xmpc_realloc_str(tls, str, strsize, uint64(2)*strsize)
			strsize *= uint64(2)
		}
		c = libc.Xgetc(tls, stream)
	}
	str = Xmpc_realloc_str(tls, str, strsize, nread+uint64(1))
	strsize = nread + uint64(1)
	*(*int8)(unsafe.Pointer(str + uintptr(nread))) = int8('\000')
	if nread == uint64(0) {
		return str
	}
	lenstr = nread
	if c == int32('(') {
		/* (n-char-sequence) only after a NaN */
		if (nread != uint64(3) || libc.Xtolower(tls, libc.Int32FromUint8(libc.Uint8FromInt8(*(*int8)(unsafe.Pointer(str))))) != int32('n') || libc.Xtolower(tls, libc.Int32FromUint8(libc.Uint8FromInt8(*(*int8)(unsafe.Pointer(str + 1))))) != int32('a') || libc.Xtolower(tls, libc.Int32FromUint8(libc.Uint8FromInt8(*(*int8)(unsafe.Pointer(str + 2))))) != int32('n')) && (nread != uint64(5) || int32(*(*int8)(unsafe.Pointer(str))) != int32('@') || libc.Xtolower(tls, libc.Int32FromUint8(libc.Uint8FromInt8(*(*int8)(unsafe.Pointer(str + 1))))) != int32('n') || libc.Xtolower(tls, libc.Int32FromUint8(libc.Uint8FromInt8(*(*int8)(unsafe.Pointer(str + 2))))) != int32('a') || libc.Xtolower(tls, libc.Int32FromUint8(libc.Uint8FromInt8(*(*int8)(unsafe.Pointer(str + 3))))) != int32('n') || int32(*(*int8)(unsafe.Pointer(str + 4))) != int32('@')) {
			libc.Xungetc(tls, c, stream)
			return str
		}
		suffix = _extract_suffix(tls, stream)
		nread += libc.Xstrlen(tls, suffix) + uint64(1)
		if nread >= strsize {
			str = Xmpc_realloc_str(tls, str, strsize, nread+uint64(1))
			strsize = nread + uint64(1)
		}
		/* Warning: the sprintf does not allow overlap between arguments. */
		ret = libc.Xsprintf(tls, str+uintptr(lenstr), __ccgo_ts+17, libc.VaList(bp+8, suffix))
		n = lenstr + libc.Uint64FromInt32(ret)
		c = libc.Xgetc(tls, stream)
		if c == int32(')') {
			str = Xmpc_realloc_str(tls, str, strsize, nread+uint64(2))
			strsize = nread + uint64(2)
			*(*int8)(unsafe.Pointer(str + uintptr(nread))) = int8(c)
			*(*int8)(unsafe.Pointer(str + uintptr(nread+uint64(1)))) = int8('\000')
			nread++
		} else {
			if c != -int32(1) {
				libc.Xungetc(tls, c, stream)
			}
		}
		Xmpc_free_str(tls, suffix)
	} else {
		if c != -int32(1) {
			libc.Xungetc(tls, c, stream)
		}
	}
	return str
}

func Xmpc_inp_str(tls *libc.TLS, rop Tmpc_ptr, stream uintptr, read uintptr, base int32, rnd_mode Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var c, inex, ret, v1, v2 int32
	var imag_str, real_str, str uintptr
	var n, nread, white Tsize_t
	_, _, _, _, _, _, _, _, _, _, _ = c, imag_str, inex, n, nread, real_str, ret, str, white, v1, v2
	nread = uint64(0)
	inex = -int32(1)
	if stream == libc.UintptrFromInt32(0) {
		stream = libc.Xstdin
	}
	white = _skip_whitespace(tls, stream)
	c = libc.Xgetc(tls, stream)
	if c != -int32(1) {
		if c == int32('(') {
			nread++ /* the opening parenthesis */
			white = _skip_whitespace(tls, stream)
			real_str = _extract_string(tls, stream)
			nread += libc.Xstrlen(tls, real_str)
			c = libc.Xgetc(tls, stream)
			v1 = libc.Int32FromUint32(libc.Uint32FromInt32(c))
			v2 = libc.BoolInt32(v1 == int32(' ') || libc.Uint32FromInt32(v1)-uint32('\t') < uint32(5))
			goto _3
		_3:
			if !(v2 != 0) {
				if c != -int32(1) {
					libc.Xungetc(tls, c, stream)
				}
				Xmpc_free_str(tls, real_str)
				goto error
			} else {
				libc.Xungetc(tls, c, stream)
			}
			white += _skip_whitespace(tls, stream)
			imag_str = _extract_string(tls, stream)
			nread += libc.Xstrlen(tls, imag_str)
			str = Xmpc_alloc_str(tls, nread+uint64(2))
			ret = libc.Xsprintf(tls, str, __ccgo_ts+21, libc.VaList(bp+8, real_str, imag_str))
			n = libc.Uint64FromInt32(ret)
			Xmpc_free_str(tls, real_str)
			Xmpc_free_str(tls, imag_str)
			white += _skip_whitespace(tls, stream)
			c = libc.Xgetc(tls, stream)
			if c == int32(')') {
				str = Xmpc_realloc_str(tls, str, nread+uint64(2), nread+uint64(3))
				*(*int8)(unsafe.Pointer(str + uintptr(nread+uint64(1)))) = int8(c)
				*(*int8)(unsafe.Pointer(str + uintptr(nread+uint64(2)))) = int8('\000')
				nread++
			} else {
				if c != -int32(1) {
					libc.Xungetc(tls, c, stream)
				}
			}
		} else {
			if c != -int32(1) {
				libc.Xungetc(tls, c, stream)
			}
			str = _extract_string(tls, stream)
			nread += libc.Xstrlen(tls, str)
		}
		inex = Xmpc_set_str(tls, rop, str, base, rnd_mode)
		Xmpc_free_str(tls, str)
	}
	goto error
error:
	;
	if inex == -int32(1) {
		libmpfr.Xmpfr_set_nan(tls, rop)
		libmpfr.Xmpfr_set_nan(tls, rop+32)
	}
	if read != libc.UintptrFromInt32(0) {
		*(*Tsize_t)(unsafe.Pointer(read)) = white + nread
	}
	return inex
}

func Xmpc_log(tls *libc.TLS, rop Tmpc_ptr, op Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var _p, _p1, x, y Tmpfr_srcptr
	var err, im_cmp, inex_im, inex_re, loops, negative_zero, ok, re_cmp, sgnw, underflow, v1, v10, v11, v12, v13, v14, v15, v19, v2, v20, v21, v22, v23, v3, v4, v5, v6, v7, v8, v9 int32
	var expw Tmpfr_exp_t
	var prec Tmpfr_prec_t
	var rnd_im Tmpfr_rnd_t
	var v17, v18 int64
	var _ /* v at bp+0 */ Tmpfr_t
	var _ /* w at bp+32 */ Tmpfr_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = _p, _p1, err, expw, im_cmp, inex_im, inex_re, loops, negative_zero, ok, prec, re_cmp, rnd_im, sgnw, underflow, x, y, v1, v10, v11, v12, v13, v14, v15, v17, v18, v19, v2, v20, v21, v22, v23, v3, v4, v5, v6, v7, v8, v9
	underflow = 0
	/* special values: NaN and infinities */
	if !(libmpfr.Xmpfr_number_p(tls, op) != 0 && libmpfr.Xmpfr_number_p(tls, op+32) != 0) {
		if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			if (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				libmpfr.Xmpfr_set_inf(tls, rop, +libc.Int32FromInt32(1))
			} else {
				libmpfr.Xmpfr_set_nan(tls, rop)
			}
			libmpfr.Xmpfr_set_nan(tls, rop+32)
			inex_im = 0 /* Inf/NaN is exact */
		} else {
			if (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
					libmpfr.Xmpfr_set_inf(tls, rop, +libc.Int32FromInt32(1))
				} else {
					libmpfr.Xmpfr_set_nan(tls, rop)
				}
				libmpfr.Xmpfr_set_nan(tls, rop+32)
				inex_im = 0 /* Inf/NaN is exact */
			} else { /* We have an infinity in at least one part. */
				inex_im = libmpfr.Xmpfr_atan2(tls, rop+32, op+32, op, rnd>>libc.Int32FromInt32(4))
				libmpfr.Xmpfr_set_inf(tls, rop, +libc.Int32FromInt32(1))
			}
		}
		if inex_im < 0 {
			v1 = int32(2)
		} else {
			if inex_im == 0 {
				v2 = 0
			} else {
				v2 = int32(1)
			}
			v1 = v2
		}
		return 0 | v1<<int32(2)
	}
	/* special cases: real and purely imaginary numbers */
	re_cmp = libmpfr.Xmpfr_sgn(tls, op)
	im_cmp = libmpfr.Xmpfr_sgn(tls, op+32)
	if im_cmp == 0 {
		if re_cmp == 0 {
			inex_im = libmpfr.Xmpfr_atan2(tls, rop+32, op+32, op, rnd>>libc.Int32FromInt32(4))
			libmpfr.Xmpfr_set_inf(tls, rop, -int32(1))
			inex_re = 0 /* -Inf is exact */
		} else {
			if re_cmp > 0 {
				inex_re = libmpfr.Xmpfr_log(tls, rop, op, rnd&libc.Int32FromInt32(0x0F))
				{
					_p = op + 32
					v3 = libmpfr.Xmpfr_set4(tls, rop+32, _p, rnd>>libc.Int32FromInt32(4), (*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign)
				}
				inex_im = v3
			} else {
				negative_zero = libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_sign < 0)
				if negative_zero != 0 {
					if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDU) {
						v4 = int32(_MPFR_RNDD)
					} else {
						if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDD) {
							v5 = int32(_MPFR_RNDU)
						} else {
							v5 = rnd >> libc.Int32FromInt32(4)
						}
						v4 = v5
					}
					rnd_im = v4
				} else {
					rnd_im = rnd >> libc.Int32FromInt32(4)
				}
				(*(*Tmpfr_t)(unsafe.Pointer(bp + 32)))[0] = *(*t__mpfr_struct)(unsafe.Pointer(op))
				libmpfr.Xmpfr_neg(tls, bp+32, bp+32, int32(_MPFR_RNDN))
				inex_re = libmpfr.Xmpfr_log(tls, rop, bp+32, rnd&libc.Int32FromInt32(0x0F))
				inex_im = libmpfr.Xmpfr_const_pi(tls, rop+32, rnd_im)
				if negative_zero != 0 {
					Xmpc_conj(tls, rop, rop, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
					inex_im = -inex_im
				}
			}
		}
		if inex_re < 0 {
			v6 = int32(2)
		} else {
			if inex_re == 0 {
				v7 = 0
			} else {
				v7 = int32(1)
			}
			v6 = v7
		}
		if inex_im < 0 {
			v8 = int32(2)
		} else {
			if inex_im == 0 {
				v9 = 0
			} else {
				v9 = int32(1)
			}
			v8 = v9
		}
		return v6 | v8<<int32(2)
	} else {
		if re_cmp == 0 {
			if im_cmp > 0 {
				inex_re = libmpfr.Xmpfr_log(tls, rop, op+32, rnd&libc.Int32FromInt32(0x0F))
				inex_im = libmpfr.Xmpfr_const_pi(tls, rop+32, rnd>>libc.Int32FromInt32(4))
				/* division by 2 does not change the ternary flag */
				libmpfr.Xmpfr_div_2ui(tls, rop+32, rop+32, uint64(1), int32(_MPFR_RNDN))
			} else {
				(*(*Tmpfr_t)(unsafe.Pointer(bp + 32)))[0] = *(*t__mpfr_struct)(unsafe.Pointer(op + 32))
				libmpfr.Xmpfr_neg(tls, bp+32, bp+32, int32(_MPFR_RNDN))
				inex_re = libmpfr.Xmpfr_log(tls, rop, bp+32, rnd&libc.Int32FromInt32(0x0F))
				if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDU) {
					v10 = int32(_MPFR_RNDD)
				} else {
					if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDD) {
						v11 = int32(_MPFR_RNDU)
					} else {
						v11 = rnd >> libc.Int32FromInt32(4)
					}
					v10 = v11
				}
				inex_im = libmpfr.Xmpfr_const_pi(tls, rop+32, v10)
				/* division by 2 does not change the ternary flag */
				libmpfr.Xmpfr_div_2ui(tls, rop+32, rop+32, uint64(1), int32(_MPFR_RNDN))
				libmpfr.Xmpfr_neg(tls, rop+32, rop+32, int32(_MPFR_RNDN))
				inex_im = -inex_im /* negate the ternary flag */
			}
			if inex_re < 0 {
				v12 = int32(2)
			} else {
				if inex_re == 0 {
					v13 = 0
				} else {
					v13 = int32(1)
				}
				v12 = v13
			}
			if inex_im < 0 {
				v14 = int32(2)
			} else {
				if inex_im == 0 {
					v15 = 0
				} else {
					v15 = int32(1)
				}
				v14 = v15
			}
			return v12 | v14<<int32(2)
		}
	}
	prec = (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec
	libmpfr.Xmpfr_init2(tls, bp+32, int64(2))
	/* let op = x + iy; log = 1/2 log (x^2 + y^2) + i atan2 (y, x)   */
	/* loop for the real part: 1/2 log (x^2 + y^2), fast, but unsafe */
	/* implementation                                                */
	ok = 0
	loops = int32(1)
	for {
		if !(!(ok != 0) && loops <= int32(2)) {
			break
		}
		prec += Xmpc_ceil_log2(tls, prec) + int64(4)
		libmpfr.Xmpfr_set_prec(tls, bp+32, prec)
		Xmpc_abs(tls, bp+32, op, int32(_MPFR_RNDN))
		/* error 0.5 ulp */
		if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			/* intermediate overflow; the logarithm may be representable.
			   Intermediate underflow is impossible.                      */
			break
		}
		libmpfr.Xmpfr_log(tls, bp+32, bp+32, int32(_MPFR_RNDN))
		/* generic error of log: (2^(- exp(w)) + 0.5) ulp */
		if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			/* impossible to round, switch to second algorithm */
			break
		}
		if -(*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp > int64(libc.Int32FromInt32(0)) {
			v17 = -(*t__mpfr_struct)(unsafe.Pointer(bp + 32)).F_mpfr_exp
		} else {
			v17 = int64(libc.Int32FromInt32(0))
		}
		err = int32(v17 + int64(1))
		/* number of lost digits */
		ok = libmpfr.Xmpfr_can_round(tls, bp+32, prec-int64(err), int32(_MPFR_RNDN), int32(_MPFR_RNDZ), (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec+libc.BoolInt64(rnd&libc.Int32FromInt32(0x0F) == int32(_MPFR_RNDN)))
		goto _16
	_16:
		;
		loops++
	}
	if !(ok != 0) {
		prec = (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec
		libmpfr.Xmpfr_init2(tls, bp, int64(2))
		/* compute 1/2 log (x^2 + y^2) = log |x| + 1/2 * log (1 + (y/x)^2)
		   if |x| >= |y|; otherwise, exchange x and y                   */
		if libmpfr.Xmpfr_cmpabs(tls, op, op+32) >= 0 {
			x = op
			y = op + 32
		} else {
			x = op + 32
			y = op
		}
		for cond := true; cond; cond = !(underflow != 0) && !(libmpfr.Xmpfr_can_round(tls, bp+32, prec-int64(err), int32(_MPFR_RNDN), int32(_MPFR_RNDZ), (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec+libc.BoolInt64(rnd&libc.Int32FromInt32(0x0F) == int32(_MPFR_RNDN))) != 0) {
			prec += Xmpc_ceil_log2(tls, prec) + int64(4)
			libmpfr.Xmpfr_set_prec(tls, bp, prec)
			libmpfr.Xmpfr_set_prec(tls, bp+32, prec)
			libmpfr.Xmpfr_div(tls, bp, y, x, int32(_MPFR_RNDD)) /* error 1 ulp */
			libmpfr.Xmpfr_sqr(tls, bp, bp, int32(_MPFR_RNDD))
			/* generic error of multiplication:
			   1 + 2*1*(2+1*2^(1-prec)) <= 5.0625 since prec >= 6 */
			libmpfr.Xmpfr_log1p(tls, bp, bp, int32(_MPFR_RNDD))
			/* error 1 + 4*5.0625 = 21.25 , see algorithms.tex */
			libmpfr.Xmpfr_div_2ui(tls, bp, bp, uint64(1), int32(_MPFR_RNDD))
			/* If the result is 0, then there has been an underflow somewhere. */
			libmpfr.Xmpfr_set4(tls, bp+32, x, int32(_MPFR_RNDN), int32(1)) /* exact */
			libmpfr.Xmpfr_log(tls, bp+32, bp+32, int32(_MPFR_RNDN))        /* error 0.5 ulp */
			expw = (*t__mpfr_struct)(unsafe.Pointer(bp + 32)).F_mpfr_exp
			sgnw = libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_sign < 0)
			libmpfr.Xmpfr_add(tls, bp+32, bp+32, bp, int32(_MPFR_RNDN))
			if !(sgnw != 0) { /* v is positive, so no cancellation;
				   error 22.25 ulp; error counts lost bits */
				err = int32(5)
			} else {
				if int64(5)+(*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp > int64(-int32(1))+expw-(*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp {
					v18 = int64(5) + (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp
				} else {
					v18 = int64(-int32(1)) + expw - (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp
				}
				err = int32(v18 + int64(2))
			}
			/* handle one special case: |x|=1, and (y/x)^2 underflows;
			   then 1/2*log(x^2+y^2) \approx 1/2*y^2 also underflows.  */
			if (libmpfr.Xmpfr_cmp_si_2exp(tls, x, int64(-libc.Int32FromInt32(1)), 0) == 0 || libmpfr.Xmpfr_cmp_ui_2exp(tls, x, libc.Uint64FromInt32(libc.Int32FromInt32(1)), 0) == 0) && (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				underflow = int32(1)
			}
		}
		libmpfr.Xmpfr_clear(tls, bp)
	}
	/* imaginary part */
	inex_im = libmpfr.Xmpfr_atan2(tls, rop+32, op+32, op, rnd>>libc.Int32FromInt32(4))
	/* set the real part; cannot be done before if rop==op */
	if underflow != 0 {
		/* create underflow in result */
		inex_re = libmpfr.Xmpfr_set_ui_2exp(tls, rop, uint64(1), libmpfr.Xmpfr_get_emin_min(tls)-int64(2), rnd&libc.Int32FromInt32(0x0F))
	} else {
		{
			_p1 = bp + 32
			v19 = libmpfr.Xmpfr_set4(tls, rop, _p1, rnd&libc.Int32FromInt32(0x0F), (*t__mpfr_struct)(unsafe.Pointer(_p1)).F_mpfr_sign)
		}
		inex_re = v19
	}
	libmpfr.Xmpfr_clear(tls, bp+32)
	if inex_re < 0 {
		v20 = int32(2)
	} else {
		if inex_re == 0 {
			v21 = 0
		} else {
			v21 = int32(1)
		}
		v20 = v21
	}
	if inex_im < 0 {
		v22 = int32(2)
	} else {
		if inex_im == 0 {
			v23 = 0
		} else {
			v23 = int32(1)
		}
		v22 = v23
	}
	return v20 | v22<<int32(2)
}

func _mpfr_const_log10(tls *libc.TLS, log10 Tmpfr_ptr) {
	_ = libmpfr.Xmpfr_set_ui_2exp(tls, log10, libc.Uint64FromInt32(libc.Int32FromInt32(10)), 0, int32(_MPFR_RNDN)) /* exact since prec >= 4 */
	libmpfr.Xmpfr_log(tls, log10, log10, int32(_MPFR_RNDN))                                                        /* error <= 1/2 ulp */
}

func Xmpc_log10(tls *libc.TLS, rop Tmpc_ptr, op Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(128)
	defer tls.Free(128)
	var _p, _p1 Tmpfr_srcptr
	var check_exact, inex, inex_im, inex_re, loops, ok, special_im, special_re, v10, v11, v12, v3, v4, v5, v6, v7, v8, v9 int32
	var prec Tmpfr_prec_t
	var s, v uint64
	var saved_emax, saved_emin Tmpfr_exp_t
	var v1, v2 int64
	var _ /* log at bp+32 */ Tmpc_t
	var _ /* log10 at bp+0 */ Tmpfr_t
	var _ /* x at bp+96 */ Tmpz_t
	var _ /* y at bp+112 */ Tmpz_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = _p, _p1, check_exact, inex, inex_im, inex_re, loops, ok, prec, s, saved_emax, saved_emin, special_im, special_re, v, v1, v10, v11, v12, v2, v3, v4, v5, v6, v7, v8, v9
	ok = 0
	loops = 0
	check_exact = 0
	saved_emin = libmpfr.Xmpfr_get_emin(tls)
	saved_emax = libmpfr.Xmpfr_get_emax(tls)
	libmpfr.Xmpfr_set_emin(tls, libmpfr.Xmpfr_get_emin_min(tls))
	libmpfr.Xmpfr_set_emax(tls, libmpfr.Xmpfr_get_emax_max(tls))
	libmpfr.Xmpfr_init2(tls, bp, int64(2))
	Xmpc_init2(tls, bp+32, int64(2))
	if (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(rop+32)).F_mpfr_prec {
		v1 = (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec
	} else {
		v1 = (*t__mpfr_struct)(unsafe.Pointer(rop + 32)).F_mpfr_prec
	}
	prec = v1
	/* compute log(op)/log(10) */
	for ok == 0 {
		loops++
		if loops <= int32(2) {
			v2 = Xmpc_ceil_log2(tls, prec) + int64(4)
		} else {
			v2 = prec / int64(2)
		}
		prec += v2
		libmpfr.Xmpfr_set_prec(tls, bp, prec)
		Xmpc_set_prec(tls, bp+32, prec)
		inex = Xmpc_log(tls, bp+32, op, rnd) /* error <= 1 ulp */
		if !(libmpfr.Xmpfr_number_p(tls, bp+32+32) != 0) || (*t__mpfr_struct)(unsafe.Pointer(bp+32+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			/* no need to divide by log(10) */
			special_im = int32(1)
			ok = int32(1)
		} else {
			special_im = 0
			_mpfr_const_log10(tls, bp)
			libmpfr.Xmpfr_div(tls, bp+32+32, bp+32+32, bp, int32(_MPFR_RNDN))
			ok = libmpfr.Xmpfr_can_round(tls, bp+32+32, prec-int64(2), int32(_MPFR_RNDN), int32(_MPFR_RNDZ), (*t__mpfr_struct)(unsafe.Pointer(rop+32)).F_mpfr_prec+libc.BoolInt64(rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDN)))
		}
		if ok != 0 {
			if !(libmpfr.Xmpfr_number_p(tls, bp+32) != 0) || (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				special_re = int32(1)
			} else {
				special_re = 0
				if special_im != 0 {
					/* log10 not yet computed */
					_mpfr_const_log10(tls, bp)
				}
				libmpfr.Xmpfr_div(tls, bp+32, bp+32, bp, int32(_MPFR_RNDN))
				/* error <= 24/7 ulp < 4 ulp for prec >= 4, see algorithms.tex */
				ok = libmpfr.Xmpfr_can_round(tls, bp+32, prec-int64(2), int32(_MPFR_RNDN), int32(_MPFR_RNDZ), (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec+libc.BoolInt64(rnd&libc.Int32FromInt32(0x0F) == int32(_MPFR_RNDN)))
			}
			/* Special code to deal with cases where the real part of log10(x+i*y)
			   is exact, like x=3 and y=1. Since Re(log10(x+i*y)) = log10(x^2+y^2)/2
			   this happens whenever x^2+y^2 is a nonnegative power of 10.
			   Indeed x^2+y^2 cannot equal 10^(a/2^b) for a, b integers, a odd, b>0,
			   since x^2+y^2 is rational, and 10^(a/2^b) is irrational.
			   Similarly, for b=0, x^2+y^2 cannot equal 10^a for a < 0 since x^2+y^2
			   is a rational with denominator a power of 2.
			   Now let x^2+y^2 = 10^s. Without loss of generality we can assume
			   x = u/2^e and y = v/2^e with u, v, e integers: u^2+v^2 = 10^s*2^(2e)
			   thus u^2+v^2 = 0 mod 2^(2e). By recurrence on e, necessarily
			   u = v = 0 mod 2^e, thus x and y are necessarily integers.
			*/
			if !(ok != 0) && !(check_exact != 0) && libmpfr.Xmpfr_integer_p(tls, op) != 0 && libmpfr.Xmpfr_integer_p(tls, op+32) != 0 {
				check_exact = int32(1)
				libgmp.X__gmpz_init(tls, bp+96)
				libgmp.X__gmpz_init(tls, bp+112)
				libmpfr.Xmpfr_get_z(tls, bp+96, op, int32(_MPFR_RNDN))     /* exact */
				libmpfr.Xmpfr_get_z(tls, bp+112, op+32, int32(_MPFR_RNDN)) /* exact */
				libgmp.X__gmpz_mul(tls, bp+96, bp+96, bp+96)
				libgmp.X__gmpz_mul(tls, bp+112, bp+112, bp+112)
				libgmp.X__gmpz_add(tls, bp+96, bp+96, bp+112) /* x^2+y^2 */
				v = libgmp.X__gmpz_scan1(tls, bp+96, uint64(0))
				/* if x = 10^s then necessarily s = v */
				s = libgmp.X__gmpz_sizeinbase(tls, bp+96, int32(10))
				/* since s is either the number of digits of x or one more,
				   then x = 10^(s-1) or 10^(s-2) */
				if s == v+uint64(1) || s == v+uint64(2) {
					libgmp.X__gmpz_fdiv_q_2exp(tls, bp+96, bp+96, v)
					libgmp.X__gmpz_ui_pow_ui(tls, bp+112, uint64(5), v)
					if libgmp.X__gmpz_cmp(tls, bp+112, bp+96) == 0 {
						/* Re(log10(x+i*y)) is exactly v/2
						   we reset the precision of Re(log) so that v can be
						   represented exactly */
						libmpfr.Xmpfr_set_prec(tls, bp+32, libc.Int64FromUint64(libc.Uint64FromInt64(8)*libc.Uint64FromInt32(m_CHAR_BIT)))
						libmpfr.Xmpfr_set_ui_2exp(tls, bp+32, v, int64(-int32(1)), int32(_MPFR_RNDN))
						/* exact */
						ok = int32(1)
					}
				}
				libgmp.X__gmpz_clear(tls, bp+96)
				libgmp.X__gmpz_clear(tls, bp+112)
			}
		}
	}
	{
		_p = bp + 32
		v3 = libmpfr.Xmpfr_set4(tls, rop, _p, rnd&libc.Int32FromInt32(0x0F), (*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign)
	}
	inex_re = v3
	if special_re != 0 {
		if inex&int32(3) == int32(2) {
			v4 = -int32(1)
		} else {
			if inex&int32(3) == 0 {
				v5 = 0
			} else {
				v5 = int32(1)
			}
			v4 = v5
		}
		inex_re = v4
	}
	/* recover flag from call to mpc_log above */
	{
		_p1 = bp + 32 + 32
		v6 = libmpfr.Xmpfr_set4(tls, rop+32, _p1, rnd>>libc.Int32FromInt32(4), (*t__mpfr_struct)(unsafe.Pointer(_p1)).F_mpfr_sign)
	}
	inex_im = v6
	if special_im != 0 {
		if inex>>int32(2) == int32(2) {
			v7 = -int32(1)
		} else {
			if inex>>int32(2) == 0 {
				v8 = 0
			} else {
				v8 = int32(1)
			}
			v7 = v8
		}
		inex_im = v7
	}
	libmpfr.Xmpfr_clear(tls, bp)
	Xmpc_clear(tls, bp+32)
	/* restore the exponent range, and check the range of results */
	libmpfr.Xmpfr_set_emin(tls, saved_emin)
	libmpfr.Xmpfr_set_emax(tls, saved_emax)
	inex_re = libmpfr.Xmpfr_check_range(tls, rop, inex_re, rnd&libc.Int32FromInt32(0x0F))
	inex_im = libmpfr.Xmpfr_check_range(tls, rop+32, inex_im, rnd>>libc.Int32FromInt32(4))
	if inex_re < 0 {
		v9 = int32(2)
	} else {
		if inex_re == 0 {
			v10 = 0
		} else {
			v10 = int32(1)
		}
		v9 = v10
	}
	if inex_im < 0 {
		v11 = int32(2)
	} else {
		if inex_im == 0 {
			v12 = 0
		} else {
			v12 = int32(1)
		}
		v11 = v12
	}
	return v9 | v11<<int32(2)
}

func Xmpc_alloc_str(tls *libc.TLS, len1 Tsize_t) (r uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var _ /* allocfunc at bp+0 */ uintptr
	libgmp.X__gmp_get_memory_functions(tls, bp, libc.UintptrFromInt32(0), libc.UintptrFromInt32(0))
	return (*(*func(*libc.TLS, Tsize_t) uintptr)(unsafe.Pointer(bp)))(tls, len1)
}

func Xmpc_realloc_str(tls *libc.TLS, str uintptr, oldlen Tsize_t, newlen Tsize_t) (r uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var _ /* reallocfunc at bp+0 */ uintptr
	libgmp.X__gmp_get_memory_functions(tls, libc.UintptrFromInt32(0), bp, libc.UintptrFromInt32(0))
	return (*(*func(*libc.TLS, uintptr, Tsize_t, Tsize_t) uintptr)(unsafe.Pointer(bp)))(tls, str, oldlen, newlen)
}

func Xmpc_free_str(tls *libc.TLS, str uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var _ /* freefunc at bp+0 */ uintptr
	libgmp.X__gmp_get_memory_functions(tls, libc.UintptrFromInt32(0), libc.UintptrFromInt32(0), bp)
	(*(*func(*libc.TLS, uintptr, Tsize_t))(unsafe.Pointer(bp)))(tls, str, libc.Xstrlen(tls, str)+uint64(1))
}

func Xmpc_mul_2si(tls *libc.TLS, a Tmpc_ptr, b Tmpc_srcptr, c int64, rnd Tmpc_rnd_t) (r int32) {
	var inex_im, inex_re, v1, v2, v3, v4 int32
	_, _, _, _, _, _ = inex_im, inex_re, v1, v2, v3, v4
	inex_re = libmpfr.Xmpfr_mul_2si(tls, a, b, c, rnd&libc.Int32FromInt32(0x0F))
	inex_im = libmpfr.Xmpfr_mul_2si(tls, a+32, b+32, c, rnd>>libc.Int32FromInt32(4))
	if inex_re < 0 {
		v1 = int32(2)
	} else {
		if inex_re == 0 {
			v2 = 0
		} else {
			v2 = int32(1)
		}
		v1 = v2
	}
	if inex_im < 0 {
		v3 = int32(2)
	} else {
		if inex_im == 0 {
			v4 = 0
		} else {
			v4 = int32(1)
		}
		v3 = v4
	}
	return v1 | v3<<int32(2)
}

func Xmpc_mul_2ui(tls *libc.TLS, a Tmpc_ptr, b Tmpc_srcptr, c uint64, rnd Tmpc_rnd_t) (r int32) {
	var inex_im, inex_re, v1, v2, v3, v4 int32
	_, _, _, _, _, _ = inex_im, inex_re, v1, v2, v3, v4
	inex_re = libmpfr.Xmpfr_mul_2ui(tls, a, b, c, rnd&libc.Int32FromInt32(0x0F))
	inex_im = libmpfr.Xmpfr_mul_2ui(tls, a+32, b+32, c, rnd>>libc.Int32FromInt32(4))
	if inex_re < 0 {
		v1 = int32(2)
	} else {
		if inex_re == 0 {
			v2 = 0
		} else {
			v2 = int32(1)
		}
		v1 = v2
	}
	if inex_im < 0 {
		v3 = int32(2)
	} else {
		if inex_im == 0 {
			v4 = 0
		} else {
			v4 = int32(1)
		}
		v3 = v4
	}
	return v1 | v3<<int32(2)
}

// C documentation
//
//	/* compute z=x*y when x has an infinite part */
func _mul_infinite(tls *libc.TLS, z Tmpc_ptr, x Tmpc_srcptr, y Tmpc_srcptr) (r int32) {
	var u, v, xi, xis, xr, xrs, yi, yis, yr, yrs, v1, v10, v11, v12, v13, v14, v15, v16, v2, v3, v4, v5, v6, v7, v8, v9 int32
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = u, v, xi, xis, xr, xrs, yi, yis, yr, yrs, v1, v10, v11, v12, v13, v14, v15, v16, v2, v3, v4, v5, v6, v7, v8, v9
	if (*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_sign < 0 {
		v1 = -int32(1)
	} else {
		v1 = int32(1)
	}
	/* Let x=xr+i*xi and y=yr+i*yi; extract the signs of the operands */
	xrs = v1
	if (*t__mpfr_struct)(unsafe.Pointer(x+32)).F_mpfr_sign < 0 {
		v2 = -int32(1)
	} else {
		v2 = int32(1)
	}
	xis = v2
	if (*t__mpfr_struct)(unsafe.Pointer(y)).F_mpfr_sign < 0 {
		v3 = -int32(1)
	} else {
		v3 = int32(1)
	}
	yrs = v3
	if (*t__mpfr_struct)(unsafe.Pointer(y+32)).F_mpfr_sign < 0 {
		v4 = -int32(1)
	} else {
		v4 = int32(1)
	}
	yis = v4
	/* compute the sign of
	   u = xrs * yrs * xr * yr - xis * yis * xi * yi
	   v = xrs * yis * xr * yi + xis * yrs * xi * yr
	   +1 if positive, -1 if negative, 0 if NaN */
	if (*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(x+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(y)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(y+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		u = 0
		v = 0
	} else {
		if (*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			/* x = (+/-inf) xr + i*xi */
			if (*t__mpfr_struct)(unsafe.Pointer(y)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(x+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) && (*t__mpfr_struct)(unsafe.Pointer(y+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(x+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) && (*t__mpfr_struct)(unsafe.Pointer(y+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || ((*t__mpfr_struct)(unsafe.Pointer(x+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(y+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))) && xrs*yrs == xis*yis {
				v5 = 0
			} else {
				v5 = xrs * yrs
			}
			u = v5
			if (*t__mpfr_struct)(unsafe.Pointer(y+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(x+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) && (*t__mpfr_struct)(unsafe.Pointer(y)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(x+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) && (*t__mpfr_struct)(unsafe.Pointer(y)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(x+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) && xrs*yis != xis*yrs {
				v6 = 0
			} else {
				v6 = xrs * yis
			}
			v = v6
		} else {
			/* x = xr + i*(+/-inf) with |xr| != inf */
			if (*t__mpfr_struct)(unsafe.Pointer(y+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) && (*t__mpfr_struct)(unsafe.Pointer(y)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(y)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) && xrs*yrs == xis*yis {
				v7 = 0
			} else {
				v7 = -xis * yis
			}
			u = v7
			if (*t__mpfr_struct)(unsafe.Pointer(y)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) && (*t__mpfr_struct)(unsafe.Pointer(y+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(y+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) && xrs*yis != xis*yrs {
				v8 = 0
			} else {
				v8 = xis * yrs
			}
			v = v8
		}
	}
	if u == 0 && v == 0 {
		if (*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			v9 = 0
		} else {
			if (*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				v10 = int32(1)
			} else {
				v10 = 0
			}
			v9 = v10
		}
		/* Naive result is NaN+i*NaN. Obtain an infinity using the algorithm
		   given in Annex G.5.1 of the ISO C99 standard */
		xr = v9
		if (*t__mpfr_struct)(unsafe.Pointer(x+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(x+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			v11 = 0
		} else {
			if (*t__mpfr_struct)(unsafe.Pointer(x+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				v12 = int32(1)
			} else {
				v12 = 0
			}
			v11 = v12
		}
		xi = v11
		if (*t__mpfr_struct)(unsafe.Pointer(y)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(y)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			v13 = 0
		} else {
			v13 = int32(1)
		}
		yr = v13
		if (*t__mpfr_struct)(unsafe.Pointer(y+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(y+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			v14 = 0
		} else {
			v14 = int32(1)
		}
		yi = v14
		if (*t__mpfr_struct)(unsafe.Pointer(y)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(y+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			if (*t__mpfr_struct)(unsafe.Pointer(y)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				v15 = int32(1)
			} else {
				v15 = 0
			}
			yr = v15
			if (*t__mpfr_struct)(unsafe.Pointer(y+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				v16 = int32(1)
			} else {
				v16 = 0
			}
			yi = v16
		}
		u = xrs*xr*yrs*yr - xis*xi*yis*yi
		v = xrs*xr*yis*yi + xis*xi*yrs*yr
	}
	if u == 0 {
		libmpfr.Xmpfr_set_nan(tls, z)
	} else {
		libmpfr.Xmpfr_set_inf(tls, z, u)
	}
	if v == 0 {
		libmpfr.Xmpfr_set_nan(tls, z+32)
	} else {
		libmpfr.Xmpfr_set_inf(tls, z+32, v)
	}
	return libc.Int32FromInt32(0) | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2) /* exact */
}

// C documentation
//
//	/* compute z = x*y for Im(y) == 0 */
func _mul_real(tls *libc.TLS, z Tmpc_ptr, x Tmpc_srcptr, y Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	var inex, xis, xrs, yis, yrs, v1, v2, v3, v4, v5, v6 int32
	_, _, _, _, _, _, _, _, _, _, _ = inex, xis, xrs, yis, yrs, v1, v2, v3, v4, v5, v6
	/* save signs of operands */
	if (*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_sign < 0 {
		v1 = -int32(1)
	} else {
		v1 = int32(1)
	}
	xrs = v1
	if (*t__mpfr_struct)(unsafe.Pointer(x+32)).F_mpfr_sign < 0 {
		v2 = -int32(1)
	} else {
		v2 = int32(1)
	}
	xis = v2
	if (*t__mpfr_struct)(unsafe.Pointer(y)).F_mpfr_sign < 0 {
		v3 = -int32(1)
	} else {
		v3 = int32(1)
	}
	yrs = v3
	if (*t__mpfr_struct)(unsafe.Pointer(y+32)).F_mpfr_sign < 0 {
		v4 = -int32(1)
	} else {
		v4 = int32(1)
	}
	yis = v4
	inex = Xmpc_mul_fr(tls, z, x, y, rnd)
	/* Signs of zeroes may be wrong. Their correction does not change the
	   inexact flag. */
	if (*t__mpfr_struct)(unsafe.Pointer(z)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		if rnd&libc.Int32FromInt32(0x0F) == int32(_MPFR_RNDD) || xrs != yrs && xis == yis {
			v5 = -int32(1)
		} else {
			v5 = int32(1)
		}
		libmpfr.Xmpfr_set4(tls, z, z, int32(_MPFR_RNDN), v5)
	}
	if (*t__mpfr_struct)(unsafe.Pointer(z+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDD) || xrs != yis && xis != yrs {
			v6 = -int32(1)
		} else {
			v6 = int32(1)
		}
		libmpfr.Xmpfr_set4(tls, z+32, z+32, int32(_MPFR_RNDN), v6)
	}
	return inex
}

// C documentation
//
//	/* compute z = x*y for Re(y) == 0, and Im(x) != 0 and Im(y) != 0 */
func _mul_imag(tls *libc.TLS, z Tmpc_ptr, x Tmpc_srcptr, y Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var inex_im, inex_re, overlap, sign, v1, v10, v11, v12, v2, v3, v4, v6, v7, v8, v9 int32
	var v5 bool
	var _ /* rop at bp+0 */ Tmpc_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = inex_im, inex_re, overlap, sign, v1, v10, v11, v12, v2, v3, v4, v5, v6, v7, v8, v9
	overlap = libc.BoolInt32(z == x || z == y)
	if overlap != 0 {
		Xmpc_init3(tls, bp, (*t__mpfr_struct)(unsafe.Pointer(z)).F_mpfr_prec, (*t__mpfr_struct)(unsafe.Pointer(z+32)).F_mpfr_prec)
	} else {
		(*(*Tmpc_t)(unsafe.Pointer(bp)))[0] = *(*t__mpc_struct)(unsafe.Pointer(z))
	}
	if (*t__mpfr_struct)(unsafe.Pointer(y)).F_mpfr_sign < 0 {
		v1 = -int32(1)
	} else {
		v1 = int32(1)
	}
	if (*t__mpfr_struct)(unsafe.Pointer(x+32)).F_mpfr_sign < 0 {
		v2 = -int32(1)
	} else {
		v2 = int32(1)
	}
	if v5 = v1 != v2; v5 {
		if (*t__mpfr_struct)(unsafe.Pointer(y+32)).F_mpfr_sign < 0 {
			v3 = -int32(1)
		} else {
			v3 = int32(1)
		}
		if (*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_sign < 0 {
			v4 = -int32(1)
		} else {
			v4 = int32(1)
		}
	}
	sign = libc.BoolInt32(v5 && v3 != v4)
	if rnd&libc.Int32FromInt32(0x0F) == int32(_MPFR_RNDU) {
		v6 = int32(_MPFR_RNDD)
	} else {
		if rnd&libc.Int32FromInt32(0x0F) == int32(_MPFR_RNDD) {
			v7 = int32(_MPFR_RNDU)
		} else {
			v7 = rnd & libc.Int32FromInt32(0x0F)
		}
		v6 = v7
	}
	inex_re = -libmpfr.Xmpfr_mul(tls, bp, x+32, y+32, v6)
	libmpfr.Xmpfr_neg(tls, bp, bp, int32(_MPFR_RNDN)) /* exact */
	inex_im = libmpfr.Xmpfr_mul(tls, bp+32, x, y+32, rnd>>libc.Int32FromInt32(4))
	Xmpc_set(tls, z, bp, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
	/* Sign of zeroes may be wrong (note that Re(z) cannot be zero) */
	if (*t__mpfr_struct)(unsafe.Pointer(z+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDD) || sign != 0 {
			v8 = -int32(1)
		} else {
			v8 = int32(1)
		}
		libmpfr.Xmpfr_set4(tls, z+32, z+32, int32(_MPFR_RNDN), v8)
	}
	if overlap != 0 {
		Xmpc_clear(tls, bp)
	}
	if inex_re < 0 {
		v9 = int32(2)
	} else {
		if inex_re == 0 {
			v10 = 0
		} else {
			v10 = int32(1)
		}
		v9 = v10
	}
	if inex_im < 0 {
		v11 = int32(2)
	} else {
		if inex_im == 0 {
			v12 = 0
		} else {
			v12 = int32(1)
		}
		v11 = v12
	}
	return v9 | v11<<int32(2)
}

func Xmpc_mul_naive(tls *libc.TLS, z Tmpc_ptr, x Tmpc_srcptr, y Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var inex_im, inex_re, overlap, v1, v2, v3, v4 int32
	var _ /* rop at bp+0 */ Tmpc_t
	_, _, _, _, _, _, _ = inex_im, inex_re, overlap, v1, v2, v3, v4
	overlap = libc.BoolInt32(z == x || z == y)
	if overlap != 0 {
		Xmpc_init3(tls, bp, (*t__mpfr_struct)(unsafe.Pointer(z)).F_mpfr_prec, (*t__mpfr_struct)(unsafe.Pointer(z+32)).F_mpfr_prec)
	} else {
		(*(*Tmpc_t)(unsafe.Pointer(bp)))[0] = *(*t__mpc_struct)(unsafe.Pointer(z))
	}
	inex_re = libmpfr.Xmpfr_fmms(tls, bp, x, y, x+32, y+32, rnd&libc.Int32FromInt32(0x0F))
	inex_im = libmpfr.Xmpfr_fmma(tls, bp+32, x, y+32, x+32, y, rnd>>libc.Int32FromInt32(4))
	Xmpc_set(tls, z, bp, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
	if overlap != 0 {
		Xmpc_clear(tls, bp)
	}
	if inex_re < 0 {
		v1 = int32(2)
	} else {
		if inex_re == 0 {
			v2 = 0
		} else {
			v2 = int32(1)
		}
		v1 = v2
	}
	if inex_im < 0 {
		v3 = int32(2)
	} else {
		if inex_im == 0 {
			v4 = 0
		} else {
			v4 = int32(1)
		}
		v3 = v4
	}
	return v1 | v3<<int32(2)
}

func Xmpc_mul_karatsuba(tls *libc.TLS, rop Tmpc_ptr, op1 Tmpc_srcptr, op2 Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(192)
	defer tls.Free(192)
	var MAX_MUL_LOOP, inex_im, inex_re, inexact, loop, mul_a, mul_c, mul_i, ok, overlap, sign_u, sign_x, tmp4, v1, v10, v12, v14, v16, v18, v2, v20, v23, v24, v28, v29, v31, v32, v34, v36, v37, v38, v39, v40, v41, v42, v43, v44, v6, v8 int32
	var _p, a, b, c, d, tmp, tmp1, tmp2, tmp3 Tmpfr_srcptr
	var prec, prec_re, prec_u, prec_v, prec_w, prec_x, v22, v25, v4, v5 Tmpfr_prec_t
	var ref Tmpfr_ptr
	var rnd_re, rnd_u Tmpfr_rnd_t
	var v26, v27, v3 int64
	var _ /* result at bp+128 */ Tmpc_t
	var _ /* u at bp+0 */ Tmpfr_t
	var _ /* v at bp+32 */ Tmpfr_t
	var _ /* w at bp+64 */ Tmpfr_t
	var _ /* x at bp+96 */ Tmpfr_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = MAX_MUL_LOOP, _p, a, b, c, d, inex_im, inex_re, inexact, loop, mul_a, mul_c, mul_i, ok, overlap, prec, prec_re, prec_u, prec_v, prec_w, prec_x, ref, rnd_re, rnd_u, sign_u, sign_x, tmp, tmp1, tmp2, tmp3, tmp4, v1, v10, v12, v14, v16, v18, v2, v20, v22, v23, v24, v25, v26, v27, v28, v29, v3, v31, v32, v34, v36, v37, v38, v39, v4, v40, v41, v42, v43, v44, v5, v6, v8
	inex_re = 0
	inex_im = 0
	MAX_MUL_LOOP = int32(1)
	overlap = libc.BoolInt32(rop == op1 || rop == op2)
	if overlap != 0 {
		Xmpc_init3(tls, bp+128, (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec, (*t__mpfr_struct)(unsafe.Pointer(rop+32)).F_mpfr_prec)
	} else {
		(*(*Tmpc_t)(unsafe.Pointer(bp + 128)))[0] = *(*t__mpc_struct)(unsafe.Pointer(rop))
	}
	a = op1
	b = op1 + 32
	c = op2
	d = op2 + 32
	/* (a + i*b) * (c + i*d) = [ac - bd] + i*[ad + bc] */
	mul_i = 0        /* number of multiplications by i */
	mul_a = int32(1) /* implicit factor for a */
	mul_c = int32(1) /* implicit factor for c */
	if libmpfr.Xmpfr_cmpabs(tls, a, b) < 0 {
		tmp = a
		a = b
		b = tmp
		mul_i++
		mul_a = -int32(1) /* consider i * (a+i*b) = -b + i*a */
	}
	if libmpfr.Xmpfr_cmpabs(tls, c, d) < 0 {
		tmp1 = c
		c = d
		d = tmp1
		mul_i++
		mul_c = -int32(1) /* consider -d + i*c instead of c + i*d */
	}
	/* find the precision and rounding mode for the new real part */
	if mul_i%int32(2) != 0 {
		prec_re = (*t__mpfr_struct)(unsafe.Pointer(rop + 32)).F_mpfr_prec
		rnd_re = rnd >> libc.Int32FromInt32(4)
	} else { /* mul_i = 0 or 2 */
		prec_re = (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec
		rnd_re = rnd & libc.Int32FromInt32(0x0F)
	}
	if mul_i != 0 {
		if rnd_re == int32(_MPFR_RNDU) {
			v1 = int32(_MPFR_RNDD)
		} else {
			if rnd_re == int32(_MPFR_RNDD) {
				v2 = int32(_MPFR_RNDU)
			} else {
				v2 = rnd_re
			}
			v1 = v2
		}
		rnd_re = v1
	}
	/* now |a| >= |b| and |c| >= |d| */
	if (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(rop+32)).F_mpfr_prec {
		v3 = (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec
	} else {
		v3 = (*t__mpfr_struct)(unsafe.Pointer(rop + 32)).F_mpfr_prec
	}
	prec = v3
	v4 = (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_prec + (*t__mpfr_struct)(unsafe.Pointer(d)).F_mpfr_prec
	prec_v = v4
	libmpfr.Xmpfr_init2(tls, bp+32, v4)
	v5 = (*t__mpfr_struct)(unsafe.Pointer(b)).F_mpfr_prec + (*t__mpfr_struct)(unsafe.Pointer(c)).F_mpfr_prec
	prec_w = v5
	libmpfr.Xmpfr_init2(tls, bp+64, v5)
	libmpfr.Xmpfr_init2(tls, bp, int64(2))
	libmpfr.Xmpfr_init2(tls, bp+96, int64(2))
	inexact = libmpfr.Xmpfr_mul(tls, bp+32, a, d, int32(_MPFR_RNDN))
	if inexact != 0 {
		/* over- or underflow */
		ok = 0
		goto clear
	}
	if mul_a == -int32(1) {
		libmpfr.Xmpfr_neg(tls, bp+32, bp+32, int32(_MPFR_RNDN))
	}
	inexact = libmpfr.Xmpfr_mul(tls, bp+64, b, c, int32(_MPFR_RNDN))
	if inexact != 0 {
		/* over- or underflow */
		ok = 0
		goto clear
	}
	if mul_c == -int32(1) {
		libmpfr.Xmpfr_neg(tls, bp+64, bp+64, int32(_MPFR_RNDN))
	}
	/* compute sign(v-w) */
	sign_x = libmpfr.Xmpfr_cmpabs(tls, bp+32, bp+64)
	if sign_x > 0 {
		if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				libmpfr.Xmpfr_set_erangeflag(tls)
			}
			v6 = libc.Int32FromInt32(0)
		} else {
			v6 = (*t__mpfr_struct)(unsafe.Pointer(bp + 32)).F_mpfr_sign
		}
		if (*t__mpfr_struct)(unsafe.Pointer(bp+64)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			if (*t__mpfr_struct)(unsafe.Pointer(bp+64)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				libmpfr.Xmpfr_set_erangeflag(tls)
			}
			v8 = libc.Int32FromInt32(0)
		} else {
			v8 = (*t__mpfr_struct)(unsafe.Pointer(bp + 64)).F_mpfr_sign
		}
		sign_x = int32(2)*v6 - v8
	} else {
		if sign_x == 0 {
			if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
					libmpfr.Xmpfr_set_erangeflag(tls)
				}
				v10 = libc.Int32FromInt32(0)
			} else {
				v10 = (*t__mpfr_struct)(unsafe.Pointer(bp + 32)).F_mpfr_sign
			}
			if (*t__mpfr_struct)(unsafe.Pointer(bp+64)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				if (*t__mpfr_struct)(unsafe.Pointer(bp+64)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
					libmpfr.Xmpfr_set_erangeflag(tls)
				}
				v12 = libc.Int32FromInt32(0)
			} else {
				v12 = (*t__mpfr_struct)(unsafe.Pointer(bp + 64)).F_mpfr_sign
			}
			sign_x = v10 - v12
		} else {
			if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
					libmpfr.Xmpfr_set_erangeflag(tls)
				}
				v14 = libc.Int32FromInt32(0)
			} else {
				v14 = (*t__mpfr_struct)(unsafe.Pointer(bp + 32)).F_mpfr_sign
			}
			if (*t__mpfr_struct)(unsafe.Pointer(bp+64)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				if (*t__mpfr_struct)(unsafe.Pointer(bp+64)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
					libmpfr.Xmpfr_set_erangeflag(tls)
				}
				v16 = libc.Int32FromInt32(0)
			} else {
				v16 = (*t__mpfr_struct)(unsafe.Pointer(bp + 64)).F_mpfr_sign
			}
			sign_x = v14 - int32(2)*v16
		}
	}
	if (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		if (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			libmpfr.Xmpfr_set_erangeflag(tls)
		}
		v18 = libc.Int32FromInt32(0)
	} else {
		v18 = (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_sign
	}
	if (*t__mpfr_struct)(unsafe.Pointer(c)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		if (*t__mpfr_struct)(unsafe.Pointer(c)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			libmpfr.Xmpfr_set_erangeflag(tls)
		}
		v20 = libc.Int32FromInt32(0)
	} else {
		v20 = (*t__mpfr_struct)(unsafe.Pointer(c)).F_mpfr_sign
	}
	sign_u = mul_a * v18 * mul_c * v20
	if sign_x*sign_u < 0 {
		/* swap inputs */
		tmp2 = a
		a = c
		c = tmp2
		tmp3 = b
		b = d
		d = tmp3
		libmpfr.Xmpfr_swap(tls, bp+32, bp+64)
		tmp4 = mul_a
		mul_a = mul_c
		mul_c = tmp4
		sign_x = -sign_x
	}
	/* now sign_x * sign_u >= 0 */
	loop = 0
	for cond := true; cond; cond = !(ok != 0) && loop <= MAX_MUL_LOOP {
		loop++
		/* the following should give failures with prob. <= 1/prec */
		prec += Xmpc_ceil_log2(tls, prec) + int64(3)
		v22 = prec
		prec_u = v22
		libmpfr.Xmpfr_set_prec(tls, bp, v22)
		libmpfr.Xmpfr_set_prec(tls, bp+96, prec)
		/* first compute away(b +/- a) and store it in u */
		if mul_a == -int32(1) {
			v23 = libmpfr.Xmpfr_sub(tls, bp, b, a, int32(_MPFR_RNDA))
		} else {
			v23 = libmpfr.Xmpfr_add(tls, bp, b, a, int32(_MPFR_RNDA))
		}
		inexact = v23
		/* then compute away(+/-c - d) and store it in x */
		if mul_c == -int32(1) {
			v24 = libmpfr.Xmpfr_add(tls, bp+96, c, d, int32(_MPFR_RNDA))
		} else {
			v24 = libmpfr.Xmpfr_sub(tls, bp+96, c, d, int32(_MPFR_RNDA))
		}
		inexact |= v24
		if mul_c == -int32(1) {
			libmpfr.Xmpfr_neg(tls, bp+96, bp+96, int32(_MPFR_RNDN))
		}
		if inexact == 0 {
			v25 = libc.Int64FromInt32(2) * prec
			prec_u = v25
			libmpfr.Xmpfr_prec_round(tls, bp, v25, int32(_MPFR_RNDN))
		}
		/* compute away(u*x) and store it in u */
		inexact |= libmpfr.Xmpfr_mul(tls, bp, bp, bp+96, int32(_MPFR_RNDA))
		/* (a+b)*(c-d) */
		/* if all computations are exact up to here, it may be that
		   the real part is exact, thus we need if possible to
		   compute v - w exactly */
		if inexact == 0 {
			/* v and w are different from 0, so mpfr_get_exp is safe to use */
			if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp-(*t__mpfr_struct)(unsafe.Pointer(bp+64)).F_mpfr_exp >= 0 {
				v26 = (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp - (*t__mpfr_struct)(unsafe.Pointer(bp+64)).F_mpfr_exp
			} else {
				v26 = -((*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp - (*t__mpfr_struct)(unsafe.Pointer(bp+64)).F_mpfr_exp)
			}
			if prec_v > prec_w {
				v27 = prec_v
			} else {
				v27 = prec_w
			}
			prec_x = v26 + v27 + int64(1)
			/* +1 is necessary for a potential carry */
			/* ensure we do not use a too large precision */
			if prec_x > prec_u {
				prec_x = prec_u
			}
			if prec_x > prec {
				libmpfr.Xmpfr_prec_round(tls, bp+96, prec_x, int32(_MPFR_RNDN))
			}
		}
		if sign_u > 0 {
			v28 = int32(_MPFR_RNDU)
		} else {
			v28 = int32(_MPFR_RNDD)
		}
		rnd_u = v28
		inexact |= libmpfr.Xmpfr_sub(tls, bp+96, bp+32, bp+64, rnd_u) /* ad - bc */
		/* in case u=0, ensure that rnd_u rounds x away from zero */
		if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				libmpfr.Xmpfr_set_erangeflag(tls)
			}
			v29 = libc.Int32FromInt32(0)
		} else {
			v29 = (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_sign
		}
		if v29 == 0 {
			if (*t__mpfr_struct)(unsafe.Pointer(bp+96)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				if (*t__mpfr_struct)(unsafe.Pointer(bp+96)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
					libmpfr.Xmpfr_set_erangeflag(tls)
				}
				v32 = libc.Int32FromInt32(0)
			} else {
				v32 = (*t__mpfr_struct)(unsafe.Pointer(bp + 96)).F_mpfr_sign
			}
			if v32 > 0 {
				v31 = int32(_MPFR_RNDU)
			} else {
				v31 = int32(_MPFR_RNDD)
			}
			rnd_u = v31
		}
		inexact |= libmpfr.Xmpfr_add(tls, bp, bp, bp+96, rnd_u) /* ac - bd */
		ok = libc.BoolInt32(inexact == 0 || libmpfr.Xmpfr_can_round(tls, bp, prec_u-int64(3), rnd_u, int32(_MPFR_RNDZ), prec_re+libc.BoolInt64(rnd_re == int32(_MPFR_RNDN))) != 0)
		/* this ensures both we can round correctly and determine the correct
		   inexact flag (for rounding to nearest) */
	}
	/* after MAX_MUL_LOOP rounds, use mpc_naive instead */
	if ok != 0 {
		/* if inexact is zero, then u is exactly ac-bd, otherwise fix the sign
		   of the inexact flag for u, which was rounded away from ac-bd */
		if inexact != 0 {
			if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
					libmpfr.Xmpfr_set_erangeflag(tls)
				}
				v34 = libc.Int32FromInt32(0)
			} else {
				v34 = (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_sign
			}
			inexact = v34
		}
		if mul_i == 0 {
			{
				_p = bp
				v36 = libmpfr.Xmpfr_set4(tls, bp+128, _p, rnd&libc.Int32FromInt32(0x0F), (*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign)
			}
			inex_re = v36
			if inex_re == 0 {
				inex_re = inexact /* u is rounded away from 0 */
				inex_im = libmpfr.Xmpfr_add(tls, bp+128+32, bp+32, bp+64, rnd>>libc.Int32FromInt32(4))
			} else {
				inex_im = libmpfr.Xmpfr_add(tls, bp+128+32, bp+32, bp+64, rnd>>libc.Int32FromInt32(4))
			}
		} else {
			if mul_i == int32(1) { /* (x+i*y)/i = y - i*x */
				inex_im = libmpfr.Xmpfr_neg(tls, bp+128+32, bp, rnd>>libc.Int32FromInt32(4))
				if inex_im == 0 {
					inex_im = -inexact /* u is rounded away from 0 */
					inex_re = libmpfr.Xmpfr_add(tls, bp+128, bp+32, bp+64, rnd&libc.Int32FromInt32(0x0F))
				} else {
					inex_re = libmpfr.Xmpfr_add(tls, bp+128, bp+32, bp+64, rnd&libc.Int32FromInt32(0x0F))
				}
			} else { /* mul_i = 2, z/i^2 = -z */
				inex_re = libmpfr.Xmpfr_neg(tls, bp+128, bp, rnd&libc.Int32FromInt32(0x0F))
				if inex_re == 0 {
					inex_re = -inexact /* u is rounded away from 0 */
					if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDU) {
						v37 = int32(_MPFR_RNDD)
					} else {
						if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDD) {
							v38 = int32(_MPFR_RNDU)
						} else {
							v38 = rnd >> libc.Int32FromInt32(4)
						}
						v37 = v38
					}
					inex_im = -libmpfr.Xmpfr_add(tls, bp+128+32, bp+32, bp+64, v37)
					libmpfr.Xmpfr_neg(tls, bp+128+32, bp+128+32, rnd>>libc.Int32FromInt32(4))
				} else {
					if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDU) {
						v39 = int32(_MPFR_RNDD)
					} else {
						if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDD) {
							v40 = int32(_MPFR_RNDU)
						} else {
							v40 = rnd >> libc.Int32FromInt32(4)
						}
						v39 = v40
					}
					inex_im = -libmpfr.Xmpfr_add(tls, bp+128+32, bp+32, bp+64, v39)
					libmpfr.Xmpfr_neg(tls, bp+128+32, bp+128+32, rnd>>libc.Int32FromInt32(4))
				}
			}
		}
		/* Clear potential signs of 0. */
		if !(inex_re != 0) {
			ref = bp + 128
			if (*t__mpfr_struct)(unsafe.Pointer(ref)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) && (*t__mpfr_struct)(unsafe.Pointer(ref)).F_mpfr_sign < 0 {
				libmpfr.Xmpfr_neg(tls, ref, ref, int32(_MPFR_RNDN))
			}
		}
		if !(inex_im != 0) {
			ref = bp + 128 + 32
			if (*t__mpfr_struct)(unsafe.Pointer(ref)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) && (*t__mpfr_struct)(unsafe.Pointer(ref)).F_mpfr_sign < 0 {
				libmpfr.Xmpfr_neg(tls, ref, ref, int32(_MPFR_RNDN))
			}
		}
		Xmpc_set(tls, rop, bp+128, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
	}
	goto clear
clear:
	;
	libmpfr.Xmpfr_clear(tls, bp)
	libmpfr.Xmpfr_clear(tls, bp+32)
	libmpfr.Xmpfr_clear(tls, bp+64)
	libmpfr.Xmpfr_clear(tls, bp+96)
	if overlap != 0 {
		Xmpc_clear(tls, bp+128)
	}
	if ok != 0 {
		if inex_re < 0 {
			v41 = int32(2)
		} else {
			if inex_re == 0 {
				v42 = 0
			} else {
				v42 = int32(1)
			}
			v41 = v42
		}
		if inex_im < 0 {
			v43 = int32(2)
		} else {
			if inex_im == 0 {
				v44 = 0
			} else {
				v44 = int32(1)
			}
			v43 = v44
		}
		return v41 | v43<<int32(2)
	} else {
		return Xmpc_mul_naive(tls, rop, op1, op2, rnd)
	}
	return r
}

func Xmpc_mul(tls *libc.TLS, a Tmpc_ptr, b Tmpc_srcptr, c Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	var v1, v2, v3, v4, v7 int64
	var v5 bool
	var v6 func(*libc.TLS, Tmpc_ptr, Tmpc_srcptr, Tmpc_srcptr, Tmpc_rnd_t) int32
	_, _, _, _, _, _, _ = v1, v2, v3, v4, v5, v6, v7
	/* Conforming to ISO C99 standard (G.5.1 multiplicative operators),
	   infinities are treated specially if both parts are NaN when computed
	   naively. */
	if (*t__mpfr_struct)(unsafe.Pointer(b)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(b+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		return _mul_infinite(tls, a, b, c)
	}
	if (*t__mpfr_struct)(unsafe.Pointer(c)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(c+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		return _mul_infinite(tls, a, c, b)
	}
	/* NaN contamination of both parts in result */
	if (*t__mpfr_struct)(unsafe.Pointer(b)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(b+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(c)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(c+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		libmpfr.Xmpfr_set_nan(tls, a)
		libmpfr.Xmpfr_set_nan(tls, a+32)
		return libc.Int32FromInt32(0) | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2)
	}
	/* check for real multiplication */
	if (*t__mpfr_struct)(unsafe.Pointer(b+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		return _mul_real(tls, a, c, b, rnd)
	}
	if (*t__mpfr_struct)(unsafe.Pointer(c+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		return _mul_real(tls, a, b, c, rnd)
	}
	/* check for purely imaginary multiplication */
	if (*t__mpfr_struct)(unsafe.Pointer(b)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		return _mul_imag(tls, a, c, b, rnd)
	}
	if (*t__mpfr_struct)(unsafe.Pointer(c)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		return _mul_imag(tls, a, b, c, rnd)
	}
	/* If the real and imaginary part of one argument have a very different */
	/* exponent, it is not reasonable to use Karatsuba multiplication.      */
	if (*t__mpfr_struct)(unsafe.Pointer(b)).F_mpfr_exp-(*t__mpfr_struct)(unsafe.Pointer(b+32)).F_mpfr_exp >= 0 {
		v1 = (*t__mpfr_struct)(unsafe.Pointer(b)).F_mpfr_exp - (*t__mpfr_struct)(unsafe.Pointer(b+32)).F_mpfr_exp
	} else {
		v1 = -((*t__mpfr_struct)(unsafe.Pointer(b)).F_mpfr_exp - (*t__mpfr_struct)(unsafe.Pointer(b+32)).F_mpfr_exp)
	}
	if (*t__mpfr_struct)(unsafe.Pointer(b)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(b+32)).F_mpfr_prec {
		v2 = (*t__mpfr_struct)(unsafe.Pointer(b)).F_mpfr_prec
	} else {
		v2 = (*t__mpfr_struct)(unsafe.Pointer(b + 32)).F_mpfr_prec
	}
	if v5 = v1 > v2/int64(2); !v5 {
		if (*t__mpfr_struct)(unsafe.Pointer(c)).F_mpfr_exp-(*t__mpfr_struct)(unsafe.Pointer(c+32)).F_mpfr_exp >= 0 {
			v3 = (*t__mpfr_struct)(unsafe.Pointer(c)).F_mpfr_exp - (*t__mpfr_struct)(unsafe.Pointer(c+32)).F_mpfr_exp
		} else {
			v3 = -((*t__mpfr_struct)(unsafe.Pointer(c)).F_mpfr_exp - (*t__mpfr_struct)(unsafe.Pointer(c+32)).F_mpfr_exp)
		}
		if (*t__mpfr_struct)(unsafe.Pointer(c)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(c+32)).F_mpfr_prec {
			v4 = (*t__mpfr_struct)(unsafe.Pointer(c)).F_mpfr_prec
		} else {
			v4 = (*t__mpfr_struct)(unsafe.Pointer(c + 32)).F_mpfr_prec
		}
	}
	if v5 || v3 > v4/int64(2) {
		return Xmpc_mul_naive(tls, a, b, c, rnd)
	} else {
		if (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(a+32)).F_mpfr_prec {
			v7 = (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_prec
		} else {
			v7 = (*t__mpfr_struct)(unsafe.Pointer(a + 32)).F_mpfr_prec
		}
		if v7 <= libc.Int64FromInt32(m_MUL_KARATSUBA_THRESHOLD)*int64(libgmp.X__gmp_bits_per_limb) {
			v6 = Xmpc_mul_naive
		} else {
			v6 = Xmpc_mul_karatsuba
		}
		return v6(tls, a, b, c, rnd)
	}
	return r
}

func Xmpc_mul_fr(tls *libc.TLS, a Tmpc_ptr, b Tmpc_srcptr, c Tmpfr_srcptr, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var _p Tmpfr_srcptr
	var inex_im, inex_re, v1, v2, v3, v4, v5 int32
	var _ /* real at bp+0 */ Tmpfr_t
	_, _, _, _, _, _, _, _ = _p, inex_im, inex_re, v1, v2, v3, v4, v5
	if c == a {
		/* We have to use a temporary variable. */
		libmpfr.Xmpfr_init2(tls, bp, (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_prec)
	} else {
		(*(*Tmpfr_t)(unsafe.Pointer(bp)))[0] = *(*t__mpfr_struct)(unsafe.Pointer(a))
	}
	inex_re = libmpfr.Xmpfr_mul(tls, bp, b, c, rnd&libc.Int32FromInt32(0x0F))
	inex_im = libmpfr.Xmpfr_mul(tls, a+32, b+32, c, rnd>>libc.Int32FromInt32(4))
	{
		_p = bp
		v1 = libmpfr.Xmpfr_set4(tls, a, _p, int32(_MPFR_RNDN), (*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign)
	}
	_ = v1 /* exact */
	if c == a {
		libmpfr.Xmpfr_clear(tls, bp)
	}
	if inex_re < 0 {
		v2 = int32(2)
	} else {
		if inex_re == 0 {
			v3 = 0
		} else {
			v3 = int32(1)
		}
		v2 = v3
	}
	if inex_im < 0 {
		v4 = int32(2)
	} else {
		if inex_im == 0 {
			v5 = 0
		} else {
			v5 = int32(1)
		}
		v4 = v5
	}
	return v2 | v4<<int32(2)
}

func Xmpc_mul_i(tls *libc.TLS, a Tmpc_ptr, b Tmpc_srcptr, sign int32, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	/* if sign is >= 0, multiply by i, otherwise by -i */
	var _p, _p1, _p2, _p3, _p4, _p5 Tmpfr_srcptr
	var inex_im, inex_re, v1, v10, v2, v3, v4, v5, v6, v7, v8, v9 int32
	var _ /* tmp at bp+0 */ Tmpfr_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = _p, _p1, _p2, _p3, _p4, _p5, inex_im, inex_re, v1, v10, v2, v3, v4, v5, v6, v7, v8, v9
	/* Treat the most probable case of compatible precisions first */
	if (*t__mpfr_struct)(unsafe.Pointer(b)).F_mpfr_prec == (*t__mpfr_struct)(unsafe.Pointer(a+32)).F_mpfr_prec && (*t__mpfr_struct)(unsafe.Pointer(b+32)).F_mpfr_prec == (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_prec {
		if a == b {
			libmpfr.Xmpfr_swap(tls, a, a+32)
		} else {
			{
				_p = b + 32
				v1 = libmpfr.Xmpfr_set4(tls, a, _p, int32(_MPFR_RNDN), (*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign)
			}
			_ = v1
			{
				_p1 = b
				v2 = libmpfr.Xmpfr_set4(tls, a+32, _p1, int32(_MPFR_RNDN), (*t__mpfr_struct)(unsafe.Pointer(_p1)).F_mpfr_sign)
			}
			_ = v2
		}
		if sign >= 0 {
			libmpfr.Xmpfr_neg(tls, a, a, int32(_MPFR_RNDN))
		} else {
			libmpfr.Xmpfr_neg(tls, a+32, a+32, int32(_MPFR_RNDN))
		}
		inex_re = 0
		inex_im = 0
	} else {
		if a == b {
			libmpfr.Xmpfr_init2(tls, bp, (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_prec)
			if sign >= 0 {
				inex_re = libmpfr.Xmpfr_neg(tls, bp, b+32, rnd&libc.Int32FromInt32(0x0F))
				{
					_p2 = b
					v3 = libmpfr.Xmpfr_set4(tls, a+32, _p2, rnd>>libc.Int32FromInt32(4), (*t__mpfr_struct)(unsafe.Pointer(_p2)).F_mpfr_sign)
				}
				inex_im = v3
			} else {
				{
					_p3 = b + 32
					v4 = libmpfr.Xmpfr_set4(tls, bp, _p3, rnd&libc.Int32FromInt32(0x0F), (*t__mpfr_struct)(unsafe.Pointer(_p3)).F_mpfr_sign)
				}
				inex_re = v4
				inex_im = libmpfr.Xmpfr_neg(tls, a+32, b, rnd>>libc.Int32FromInt32(4))
			}
			libmpfr.Xmpfr_clear(tls, a)
			*(*t__mpfr_struct)(unsafe.Pointer(a)) = (*(*Tmpfr_t)(unsafe.Pointer(bp)))[0]
		} else {
			if sign >= 0 {
				inex_re = libmpfr.Xmpfr_neg(tls, a, b+32, rnd&libc.Int32FromInt32(0x0F))
				{
					_p4 = b
					v5 = libmpfr.Xmpfr_set4(tls, a+32, _p4, rnd>>libc.Int32FromInt32(4), (*t__mpfr_struct)(unsafe.Pointer(_p4)).F_mpfr_sign)
				}
				inex_im = v5
			} else {
				{
					_p5 = b + 32
					v6 = libmpfr.Xmpfr_set4(tls, a, _p5, rnd&libc.Int32FromInt32(0x0F), (*t__mpfr_struct)(unsafe.Pointer(_p5)).F_mpfr_sign)
				}
				inex_re = v6
				inex_im = libmpfr.Xmpfr_neg(tls, a+32, b, rnd>>libc.Int32FromInt32(4))
			}
		}
	}
	if inex_re < 0 {
		v7 = int32(2)
	} else {
		if inex_re == 0 {
			v8 = 0
		} else {
			v8 = int32(1)
		}
		v7 = v8
	}
	if inex_im < 0 {
		v9 = int32(2)
	} else {
		if inex_im == 0 {
			v10 = 0
		} else {
			v10 = int32(1)
		}
		v9 = v10
	}
	return v7 | v9<<int32(2)
}

func Xmpc_mul_si(tls *libc.TLS, a Tmpc_ptr, b Tmpc_srcptr, c int64, rnd Tmpc_rnd_t) (r int32) {
	var inex_im, inex_re, v1, v2, v3, v4 int32
	_, _, _, _, _, _ = inex_im, inex_re, v1, v2, v3, v4
	inex_re = libmpfr.Xmpfr_mul_si(tls, a, b, c, rnd&libc.Int32FromInt32(0x0F))
	inex_im = libmpfr.Xmpfr_mul_si(tls, a+32, b+32, c, rnd>>libc.Int32FromInt32(4))
	if inex_re < 0 {
		v1 = int32(2)
	} else {
		if inex_re == 0 {
			v2 = 0
		} else {
			v2 = int32(1)
		}
		v1 = v2
	}
	if inex_im < 0 {
		v3 = int32(2)
	} else {
		if inex_im == 0 {
			v4 = 0
		} else {
			v4 = int32(1)
		}
		v3 = v4
	}
	return v1 | v3<<int32(2)
}

func Xmpc_mul_ui(tls *libc.TLS, a Tmpc_ptr, b Tmpc_srcptr, c uint64, rnd Tmpc_rnd_t) (r int32) {
	var inex_im, inex_re, v1, v2, v3, v4 int32
	_, _, _, _, _, _ = inex_im, inex_re, v1, v2, v3, v4
	inex_re = libmpfr.Xmpfr_mul_ui(tls, a, b, c, rnd&libc.Int32FromInt32(0x0F))
	inex_im = libmpfr.Xmpfr_mul_ui(tls, a+32, b+32, c, rnd>>libc.Int32FromInt32(4))
	if inex_re < 0 {
		v1 = int32(2)
	} else {
		if inex_re == 0 {
			v2 = 0
		} else {
			v2 = int32(1)
		}
		v1 = v2
	}
	if inex_im < 0 {
		v3 = int32(2)
	} else {
		if inex_im == 0 {
			v4 = 0
		} else {
			v4 = int32(1)
		}
		v3 = v4
	}
	return v1 | v3<<int32(2)
}

func Xmpc_neg(tls *libc.TLS, a Tmpc_ptr, b Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	var inex_im, inex_re, v1, v2, v3, v4 int32
	_, _, _, _, _, _ = inex_im, inex_re, v1, v2, v3, v4
	inex_re = libmpfr.Xmpfr_neg(tls, a, b, rnd&libc.Int32FromInt32(0x0F))
	inex_im = libmpfr.Xmpfr_neg(tls, a+32, b+32, rnd>>libc.Int32FromInt32(4))
	if inex_re < 0 {
		v1 = int32(2)
	} else {
		if inex_re == 0 {
			v2 = 0
		} else {
			v2 = int32(1)
		}
		v1 = v2
	}
	if inex_im < 0 {
		v3 = int32(2)
	} else {
		if inex_im == 0 {
			v4 = 0
		} else {
			v4 = int32(1)
		}
		v3 = v4
	}
	return v1 | v3<<int32(2)
}

// C documentation
//
//	/* a <- norm(b) = b * conj(b)
//	   (the rounding mode is mpfr_rnd_t here since we return an mpfr number) */
func Xmpc_norm(tls *libc.TLS, a Tmpfr_ptr, b Tmpc_srcptr, rnd Tmpfr_rnd_t) (r int32) {
	bp := tls.Alloc(96)
	defer tls.Free(96)
	var _p Tmpfr_ptr
	var _p1 Tmpfr_srcptr
	var emin Tmpfr_exp_t
	var exp_im, exp_re, scale uint64
	var inex_underflow, inexact, loops, max_loops, saved_overflow, saved_underflow, v1, v4 int32
	var prec, prec_u, prec_v Tmpfr_prec_t
	var v2, v3 int64
	var _ /* res at bp+64 */ Tmpfr_t
	var _ /* u at bp+0 */ Tmpfr_t
	var _ /* v at bp+32 */ Tmpfr_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = _p, _p1, emin, exp_im, exp_re, inex_underflow, inexact, loops, max_loops, prec, prec_u, prec_v, saved_overflow, saved_underflow, scale, v1, v2, v3, v4
	/* handling of special values; consistent with abs in that
	   norm = abs^2; so norm (+-inf, xxx) = norm (xxx, +-inf) = +inf */
	if !(libmpfr.Xmpfr_number_p(tls, b) != 0 && libmpfr.Xmpfr_number_p(tls, b+32) != 0) {
		return Xmpc_abs(tls, a, b, rnd)
	} else {
		if (*t__mpfr_struct)(unsafe.Pointer(b)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			if (*t__mpfr_struct)(unsafe.Pointer(b+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				{
					_p = a
					(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign = int32(1)
					(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
					_ = rnd
					v1 = 0
				}
				return v1
			} else {
				return libmpfr.Xmpfr_sqr(tls, a, b+32, rnd)
			}
		} else {
			if (*t__mpfr_struct)(unsafe.Pointer(b+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				return libmpfr.Xmpfr_sqr(tls, a, b, rnd)
			} else { /* everything finite and non-zero */
				max_loops = int32(2)
				/* switch to exact squarings when loops==max_loops */
				prec = (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_prec
				libmpfr.Xmpfr_init(tls, bp)
				libmpfr.Xmpfr_init(tls, bp+32)
				libmpfr.Xmpfr_init(tls, bp+64)
				/* save the underflow or overflow flags from MPFR */
				saved_underflow = libmpfr.Xmpfr_underflow_p(tls)
				saved_overflow = libmpfr.Xmpfr_overflow_p(tls)
				loops = 0
				libmpfr.Xmpfr_clear_underflow(tls)
				libmpfr.Xmpfr_clear_overflow(tls)
				for cond := true; cond; cond = loops < max_loops && inexact != 0 && !(libmpfr.Xmpfr_can_round(tls, bp+64, prec-int64(2), int32(_MPFR_RNDD), int32(_MPFR_RNDU), (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_prec+libc.BoolInt64(rnd == int32(_MPFR_RNDN))) != 0) {
					loops++
					prec += Xmpc_ceil_log2(tls, prec) + int64(3)
					if loops >= max_loops {
						prec_u = int64(2) * (*t__mpfr_struct)(unsafe.Pointer(b)).F_mpfr_prec
						prec_v = int64(2) * (*t__mpfr_struct)(unsafe.Pointer(b+32)).F_mpfr_prec
					} else {
						if prec < int64(2)*(*t__mpfr_struct)(unsafe.Pointer(b)).F_mpfr_prec {
							v2 = prec
						} else {
							v2 = int64(2) * (*t__mpfr_struct)(unsafe.Pointer(b)).F_mpfr_prec
						}
						prec_u = v2
						if prec < int64(2)*(*t__mpfr_struct)(unsafe.Pointer(b+32)).F_mpfr_prec {
							v3 = prec
						} else {
							v3 = int64(2) * (*t__mpfr_struct)(unsafe.Pointer(b+32)).F_mpfr_prec
						}
						prec_v = v3
					}
					libmpfr.Xmpfr_set_prec(tls, bp, prec_u)
					libmpfr.Xmpfr_set_prec(tls, bp+32, prec_v)
					inexact = libmpfr.Xmpfr_sqr(tls, bp, b, int32(_MPFR_RNDD))        /* err <= 1 ulp in prec */
					inexact |= libmpfr.Xmpfr_sqr(tls, bp+32, b+32, int32(_MPFR_RNDD)) /* err <= 1 ulp in prec */
					/* If loops = max_loops, inexact should be 0 here, except in case
					      of underflow or overflow.
					   If loops < max_loops and inexact is zero, we can exit the
					   while-loop since it only remains to add u and v into a. */
					if inexact != 0 {
						libmpfr.Xmpfr_set_prec(tls, bp+64, prec)
						libmpfr.Xmpfr_add(tls, bp+64, bp, bp+32, int32(_MPFR_RNDD)) /* err <= 3 ulp in prec */
					}
				}
				if !(inexact != 0) {
					/* squarings were exact, neither underflow nor overflow */
					inexact = libmpfr.Xmpfr_add(tls, a, bp, bp+32, rnd)
				} else {
					if libmpfr.Xmpfr_overflow_p(tls) != 0 {
						/* replace by "correctly rounded overflow" */
						_ = libmpfr.Xmpfr_set_ui_2exp(tls, a, uint64(1), 0, int32(_MPFR_RNDN))
						inexact = libmpfr.Xmpfr_mul_2ui(tls, a, a, libc.Uint64FromInt64(libmpfr.Xmpfr_get_emax(tls)), rnd)
					} else {
						if libmpfr.Xmpfr_underflow_p(tls) != 0 {
							/* necessarily one of the squarings did underflow (otherwise their
							   sum could not underflow), thus one of u, v is zero. */
							emin = libmpfr.Xmpfr_get_emin(tls)
							/* Now either both u and v are zero, or u is zero and v exact,
							   or v is zero and u exact.
							   In the latter case, Im(b)^2 < 2^(emin-1).
							   If ulp(u) >= 2^(emin+1) and norm(b) is not exactly
							   representable at the target precision, then rounding u+Im(b)^2
							   is equivalent to rounding u+2^(emin-1).
							   For instance, if exp(u)>0 and the target precision is smaller
							   than about |emin|, the norm is not representable. To make the
							   scaling in the "else" case work without underflow, we test
							   whether exp(u) is larger than a small negative number instead.
							   The second case is handled analogously.                        */
							if !((*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))) && (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp-int64(2)*prec_u > emin && (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp > int64(-int32(10)) {
								libmpfr.Xmpfr_set_prec(tls, bp+32, int64(m_MPFR_PREC_MIN))
								libmpfr.Xmpfr_set_ui_2exp(tls, bp+32, uint64(1), emin-int64(1), int32(_MPFR_RNDZ))
								inexact = libmpfr.Xmpfr_add(tls, a, bp, bp+32, rnd)
							} else {
								if !((*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))) && (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp-int64(2)*prec_v > emin && (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp > int64(-int32(10)) {
									libmpfr.Xmpfr_set_prec(tls, bp, int64(m_MPFR_PREC_MIN))
									libmpfr.Xmpfr_set_ui_2exp(tls, bp, uint64(1), emin-int64(1), int32(_MPFR_RNDZ))
									inexact = libmpfr.Xmpfr_add(tls, a, bp, bp+32, rnd)
								} else {
									/* scale the input to an average exponent close to 0 */
									exp_re = libc.Uint64FromInt64(-(*t__mpfr_struct)(unsafe.Pointer(b)).F_mpfr_exp)
									exp_im = libc.Uint64FromInt64(-(*t__mpfr_struct)(unsafe.Pointer(b + 32)).F_mpfr_exp)
									scale = exp_re/uint64(2) + exp_im/uint64(2) + (exp_re%uint64(2)+exp_im%uint64(2))/uint64(2)
									/* (exp_re + exp_im) / 2, computed in a way avoiding
									   integer overflow                                  */
									if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
										/* recompute the scaled value exactly */
										libmpfr.Xmpfr_mul_2ui(tls, bp, b, scale, int32(_MPFR_RNDN))
										libmpfr.Xmpfr_sqr(tls, bp, bp, int32(_MPFR_RNDN))
									} else { /* just scale */
										libmpfr.Xmpfr_mul_2ui(tls, bp, bp, uint64(2)*scale, int32(_MPFR_RNDN))
									}
									if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
										libmpfr.Xmpfr_mul_2ui(tls, bp+32, b+32, scale, int32(_MPFR_RNDN))
										libmpfr.Xmpfr_sqr(tls, bp+32, bp+32, int32(_MPFR_RNDN))
									} else {
										libmpfr.Xmpfr_mul_2ui(tls, bp+32, bp+32, uint64(2)*scale, int32(_MPFR_RNDN))
									}
									inexact = libmpfr.Xmpfr_add(tls, a, bp, bp+32, rnd)
									libmpfr.Xmpfr_clear_underflow(tls)
									inex_underflow = libmpfr.Xmpfr_div_2ui(tls, a, a, uint64(2)*scale, rnd)
									if libmpfr.Xmpfr_underflow_p(tls) != 0 {
										inexact = inex_underflow
									}
								}
							}
						} else { /* no problems, ternary value due to mpfr_can_round trick */
							{
								_p1 = bp + 64
								v4 = libmpfr.Xmpfr_set4(tls, a, _p1, rnd, (*t__mpfr_struct)(unsafe.Pointer(_p1)).F_mpfr_sign)
							}
							inexact = v4
						}
					}
				}
				/* restore underflow and overflow flags from MPFR */
				if saved_underflow != 0 {
					libmpfr.Xmpfr_set_underflow(tls)
				}
				if saved_overflow != 0 {
					libmpfr.Xmpfr_set_overflow(tls)
				}
				libmpfr.Xmpfr_clear(tls, bp)
				libmpfr.Xmpfr_clear(tls, bp+32)
				libmpfr.Xmpfr_clear(tls, bp+64)
			}
		}
	}
	return inexact
}

func Xmpc_out_str(tls *libc.TLS, stream uintptr, base int32, n Tsize_t, op Tmpc_srcptr, rnd Tmpc_rnd_t) (r Tsize_t) {
	var size Tsize_t
	_ = size
	size = uint64(3) /* for '(', ' ' and ')' */
	if stream == libc.UintptrFromInt32(0) {
		stream = libc.Xstdout
	} /* fprintf does not allow NULL as first argument */
	libc.Xfprintf(tls, stream, __ccgo_ts+11, 0)
	size += libmpfr.X__gmpfr_out_str(tls, stream, base, n, op, rnd&libc.Int32FromInt32(0x0F))
	libc.Xfprintf(tls, stream, __ccgo_ts+13, 0)
	size += libmpfr.X__gmpfr_out_str(tls, stream, base, n, op+32, rnd&libc.Int32FromInt32(0x0F))
	libc.Xfprintf(tls, stream, __ccgo_ts+15, 0)
	return size
}

// C documentation
//
//	/* Return non-zero iff c+i*d is an exact square (a+i*b)^2,
//	   with a, b both of the form m*2^e with m, e integers.
//	   If so, returns in a+i*b the corresponding square root, with a >= 0.
//	   The variables a, b must not overlap with c, d.
//
//	   We have c = a^2 - b^2 and d = 2*a*b.
//
//	   If one of a, b is exact, then both are (see algorithms.tex).
//
//	   Case 1: a <> 0 and b <> 0.
//	   Let a = m*2^e and b = n*2^f with m, e, n, f integers, m and n odd
//	   (we will treat apart the case a = 0 or b = 0).
//	   Then 2*a*b = m*n*2^(e+f+1), thus necessarily e+f >= -1.
//	   Assume e < 0, then f >= 0, then a^2 - b^2 = m^2*2^(2e) - n^2*2^(2f) cannot
//	   be an integer, since n^2*2^(2f) is an integer, and m^2*2^(2e) is not.
//	   Similarly when f < 0 (and thus e >= 0).
//	   Thus we have e, f >= 0, and a, b are both integers.
//	   Let A = 2a^2, then eliminating b between c = a^2 - b^2 and d = 2*a*b
//	   gives A^2 - 2c*A - d^2 = 0, which has solutions c +/- sqrt(c^2+d^2).
//	   We thus need c^2+d^2 to be a square, and c + sqrt(c^2+d^2) --- the solution
//	   we are interested in --- to be two times a square. Then b = d/(2a) is
//	   necessarily an integer.
//
//	   Case 2: a = 0. Then d is necessarily zero, thus it suffices to check
//	   whether c = -b^2, i.e., if -c is a square.
//
//	   Case 3: b = 0. Then d is necessarily zero, thus it suffices to check
//	   whether c = a^2, i.e., if c is a square.
//	*/
func _mpc_perfect_square_p(tls *libc.TLS, a uintptr, b uintptr, c uintptr, d uintptr) (r int32) {
	var __gmp_asize Tmp_size_t
	var __gmp_result, v1, v11, v5, v8 int32
	var v10, v3, v4, v7 Tmpz_srcptr
	var v2 Tmpz_ptr
	_, _, _, _, _, _, _, _, _, _, _ = __gmp_asize, __gmp_result, v1, v10, v11, v2, v3, v4, v5, v7, v8
	if (*t__mpz_struct)(unsafe.Pointer(d)).F_mp_size < 0 {
		v1 = -int32(1)
	} else {
		v1 = libc.BoolInt32((*t__mpz_struct)(unsafe.Pointer(d)).F_mp_size > 0)
	}
	if v1 == 0 { /* case a = 0 or b = 0 */
		/* necessarily c < 0 here, since we have already considered the case
		   where x is real non-negative and y is real */
		v2 = b
		v3 = c
		if v2 != v3 {
			libgmp.X__gmpz_set(tls, v2, v3)
		}
		(*t__mpz_struct)(unsafe.Pointer(v2)).F_mp_size = -(*t__mpz_struct)(unsafe.Pointer(v2)).F_mp_size
		v4 = b
		__gmp_asize = int64((*t__mpz_struct)(unsafe.Pointer(v4)).F_mp_size)
		__gmp_result = libc.BoolInt32(__gmp_asize >= 0)
		if libc.X__builtin_expect(tls, libc.BoolInt64(libc.BoolInt32(__gmp_asize > 0) != 0), int64(1)) != 0 {
			__gmp_result = libgmp.X__gmpn_perfect_square_p(tls, (*t__mpz_struct)(unsafe.Pointer(v4)).F_mp_d, __gmp_asize)
		}
		v5 = __gmp_result
		goto _6
	_6:
		if v5 != 0 { /* case 2 above */
			libgmp.X__gmpz_sqrt(tls, b, b)
			libgmp.X__gmpz_set_ui(tls, a, uint64(0))
			return int32(1) /* c + i*d = (0 + i*b)^2 */
		}
	} else { /* both a and b are non-zero */
		if libgmp.X__gmpz_divisible_2exp_p(tls, d, uint64(1)) == 0 {
			return 0
		} /* d must be even */
		libgmp.X__gmpz_mul(tls, a, c, c)
		libgmp.X__gmpz_addmul(tls, a, d, d) /* c^2 + d^2 */
		v7 = a
		__gmp_asize = int64((*t__mpz_struct)(unsafe.Pointer(v7)).F_mp_size)
		__gmp_result = libc.BoolInt32(__gmp_asize >= 0)
		if libc.X__builtin_expect(tls, libc.BoolInt64(libc.BoolInt32(__gmp_asize > 0) != 0), int64(1)) != 0 {
			__gmp_result = libgmp.X__gmpn_perfect_square_p(tls, (*t__mpz_struct)(unsafe.Pointer(v7)).F_mp_d, __gmp_asize)
		}
		v8 = __gmp_result
		goto _9
	_9:
		if v8 != 0 {
			libgmp.X__gmpz_sqrt(tls, a, a)
			libgmp.X__gmpz_add(tls, a, c, a) /* c + sqrt(c^2+d^2) */
			if libgmp.X__gmpz_divisible_2exp_p(tls, a, uint64(1)) != 0 {
				libgmp.X__gmpz_tdiv_q_2exp(tls, a, a, uint64(1))
				v10 = a
				__gmp_asize = int64((*t__mpz_struct)(unsafe.Pointer(v10)).F_mp_size)
				__gmp_result = libc.BoolInt32(__gmp_asize >= 0)
				if libc.X__builtin_expect(tls, libc.BoolInt64(libc.BoolInt32(__gmp_asize > 0) != 0), int64(1)) != 0 {
					__gmp_result = libgmp.X__gmpn_perfect_square_p(tls, (*t__mpz_struct)(unsafe.Pointer(v10)).F_mp_d, __gmp_asize)
				}
				v11 = __gmp_result
				goto _12
			_12:
				if v11 != 0 {
					libgmp.X__gmpz_sqrt(tls, a, a)
					libgmp.X__gmpz_tdiv_q_2exp(tls, b, d, uint64(1)) /* d/2 */
					libgmp.X__gmpz_divexact(tls, b, b, a)            /* d/(2a) */
					return int32(1)
				}
			}
		}
	}
	return 0 /* not a square */
}

// C documentation
//
//	/* fix the sign of Re(z) or Im(z) in case it is zero,
//	   and Re(x) is zero.
//	   sign_eps is 0 if Re(x) = +0, 1 if Re(x) = -0
//	   sign_a is the sign bit of Im(x).
//	   Assume y is an integer (does nothing otherwise).
//	*/
func _fix_sign(tls *libc.TLS, z Tmpc_ptr, sign_eps int32, sign_a int32, y Tmpfr_srcptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var ey Tmpfr_exp_t
	var t uint64
	var ymod4, v1 int32
	var _ /* my at bp+0 */ Tmpz_t
	_, _, _, _ = ey, t, ymod4, v1
	ymod4 = -int32(1)
	libgmp.X__gmpz_init(tls, bp)
	ey = libmpfr.Xmpfr_get_z_2exp(tls, bp, y)
	/* normalize so that my is odd */
	t = libgmp.X__gmpz_scan1(tls, bp, uint64(0))
	ey += libc.Int64FromUint64(t)
	libgmp.X__gmpz_tdiv_q_2exp(tls, bp, bp, t)
	/* y = my*2^ey */
	/* compute y mod 4 (in case y is an integer) */
	if ey >= int64(2) {
		ymod4 = 0
	} else {
		if ey == int64(1) {
			ymod4 = libgmp.X__gmpz_tstbit(tls, bp, uint64(0)) * int32(2)
		} else {
			if ey == 0 {
				ymod4 = libgmp.X__gmpz_tstbit(tls, bp, uint64(1))*int32(2) + libgmp.X__gmpz_tstbit(tls, bp, uint64(0))
				if (*t__mpz_struct)(unsafe.Pointer(bp)).F_mp_size < 0 {
					v1 = -int32(1)
				} else {
					v1 = libc.BoolInt32((*t__mpz_struct)(unsafe.Pointer(bp)).F_mp_size > 0)
				}
				if v1 < 0 {
					ymod4 = int32(4) - ymod4
				}
			} else { /* y is not an integer */
				goto end
			}
		}
	}
	if (*t__mpfr_struct)(unsafe.Pointer(z)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		/* we assume y is always integer in that case (FIXME: prove it):
		   (eps+I*a)^y = +0 + I*a^y for y = 1 mod 4 and sign_eps = 0
		   (eps+I*a)^y = -0 - I*a^y for y = 3 mod 4 and sign_eps = 0 */
		if ymod4 == int32(3) && sign_eps == 0 || ymod4 == int32(1) && sign_eps == int32(1) {
			libmpfr.Xmpfr_neg(tls, z, z, int32(_MPFR_RNDZ))
		}
	} else {
		if (*t__mpfr_struct)(unsafe.Pointer(z+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			/* we assume y is always integer in that case (FIXME: prove it):
			   (eps+I*a)^y =  a^y - 0*I for y = 0 mod 4 and sign_a = sign_eps
			   (eps+I*a)^y =  -a^y +0*I for y = 2 mod 4 and sign_a = sign_eps */
			if ymod4 == 0 && sign_a == sign_eps || ymod4 == int32(2) && sign_a != sign_eps {
				libmpfr.Xmpfr_neg(tls, z+32, z+32, int32(_MPFR_RNDZ))
			}
		}
	}
	goto end
end:
	;
	libgmp.X__gmpz_clear(tls, bp)
}

// C documentation
//
//	/* If x^y is exactly representable (with maybe a larger precision than z),
//	   round it in z and return the (mpc) inexact flag in [0, 10].
//
//	   If x^y is not exactly representable, return -1.
//
//	   If intermediate computations lead to numbers of more than maxprec bits,
//	   then abort and return -2 (in that case, to avoid loops, mpc_pow_exact
//	   should be called again with a larger value of maxprec).
//
//	   Assume one of Re(x) or Im(x) is non-zero, and y is non-zero (y is real).
//
//	   Warning: z and x might be the same variable, same for Re(z) or Im(z) and y.
//
//	   In case -1 or -2 is returned, z is not modified.
//	*/
func _mpc_pow_exact(tls *libc.TLS, z Tmpc_ptr, x Tmpc_srcptr, y Tmpfr_srcptr, rnd Tmpc_rnd_t, maxprec Tmpfr_prec_t) (r int32) {
	bp := tls.Alloc(128)
	defer tls.Free(128)
	var _p Tmpfr_srcptr
	var ec, ed, ey, v12 Tmpfr_exp_t
	var inex_im, ret, sign_imx, sign_rex, Ximag, z_is_y, v11, v10, v111, v14, v15, v16, v17, v2, v3, v4 int32
	var sa, sb, t, v, v1, w, v13, v9 uint64
	var v18 uintptr
	var v5, v7 Tmpz_ptr
	var v6, v8 Tmpz_srcptr
	var _ /* a at bp+16 */ Tmpz_t
	var _ /* b at bp+32 */ Tmpz_t
	var _ /* c at bp+48 */ Tmpz_t
	var _ /* copy_of_y at bp+96 */ Tmpfr_t
	var _ /* d at bp+64 */ Tmpz_t
	var _ /* my at bp+0 */ Tmpz_t
	var _ /* u at bp+80 */ Tmpz_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = _p, ec, ed, ey, inex_im, ret, sa, sb, sign_imx, sign_rex, t, v, v1, w, Ximag, z_is_y, v11, v10, v111, v12, v13, v14, v15, v16, v17, v18, v2, v3, v4, v5, v6, v7, v8, v9
	ret = -int32(2)
	sign_rex = libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_sign < 0)
	sign_imx = libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(x+32)).F_mpfr_sign < 0)
	Ximag = libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)))
	z_is_y = 0
	if z == y || z+32 == y {
		z_is_y = int32(1)
		libmpfr.Xmpfr_init2(tls, bp+96, (*t__mpfr_struct)(unsafe.Pointer(y)).F_mpfr_prec)
		{
			_p = y
			v11 = libmpfr.Xmpfr_set4(tls, bp+96, _p, int32(_MPFR_RNDN), (*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign)
		}
		_ = v11
	}
	libgmp.X__gmpz_init(tls, bp)
	libgmp.X__gmpz_init(tls, bp+16)
	libgmp.X__gmpz_init(tls, bp+32)
	libgmp.X__gmpz_init(tls, bp+48)
	libgmp.X__gmpz_init(tls, bp+64)
	libgmp.X__gmpz_init(tls, bp+80)
	ey = libmpfr.Xmpfr_get_z_2exp(tls, bp, y)
	/* normalize so that my is odd */
	t = libgmp.X__gmpz_scan1(tls, bp, uint64(0))
	ey += libc.Int64FromUint64(t)
	libgmp.X__gmpz_tdiv_q_2exp(tls, bp, bp, t)
	/* y = my*2^ey with my odd */
	if Ximag != 0 {
		libgmp.X__gmpz_set_ui(tls, bp+48, uint64(0))
		ec = 0
	} else {
		ec = libmpfr.Xmpfr_get_z_2exp(tls, bp+48, x)
	}
	if (*t__mpfr_struct)(unsafe.Pointer(x+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		libgmp.X__gmpz_set_ui(tls, bp+64, uint64(0))
		ed = ec
	} else {
		ed = libmpfr.Xmpfr_get_z_2exp(tls, bp+64, x+32)
		if Ximag != 0 {
			ec = ed
		}
	}
	/* x = c*2^ec + I * d*2^ed */
	/* equalize the exponents of x */
	if ec < ed {
		libgmp.X__gmpz_mul_2exp(tls, bp+64, bp+64, libc.Uint64FromInt64(ed-ec))
		if libc.Int64FromUint64(libgmp.X__gmpz_sizeinbase(tls, bp+64, int32(2))) > maxprec {
			goto end
		}
	} else {
		if ed < ec {
			libgmp.X__gmpz_mul_2exp(tls, bp+48, bp+48, libc.Uint64FromInt64(ec-ed))
			if libc.Int64FromUint64(libgmp.X__gmpz_sizeinbase(tls, bp+48, int32(2))) > maxprec {
				goto end
			}
			ec = ed
		}
	}
	/* now ec=ed and x = (c + I * d) * 2^ec */
	/* divide by two if possible */
	if (*t__mpz_struct)(unsafe.Pointer(bp+48)).F_mp_size < 0 {
		v2 = -int32(1)
	} else {
		v2 = libc.BoolInt32((*t__mpz_struct)(unsafe.Pointer(bp+48)).F_mp_size > 0)
	}
	if v2 == 0 {
		t = libgmp.X__gmpz_scan1(tls, bp+64, uint64(0))
		libgmp.X__gmpz_tdiv_q_2exp(tls, bp+64, bp+64, t)
		ec += libc.Int64FromUint64(t)
	} else {
		if (*t__mpz_struct)(unsafe.Pointer(bp+64)).F_mp_size < 0 {
			v3 = -int32(1)
		} else {
			v3 = libc.BoolInt32((*t__mpz_struct)(unsafe.Pointer(bp+64)).F_mp_size > 0)
		}
		if v3 == 0 {
			t = libgmp.X__gmpz_scan1(tls, bp+48, uint64(0))
			libgmp.X__gmpz_tdiv_q_2exp(tls, bp+48, bp+48, t)
			ec += libc.Int64FromUint64(t)
		} else { /* neither c nor d is zero */
			t = libgmp.X__gmpz_scan1(tls, bp+48, uint64(0))
			v = libgmp.X__gmpz_scan1(tls, bp+64, uint64(0))
			if v < t {
				t = v
			}
			libgmp.X__gmpz_tdiv_q_2exp(tls, bp+48, bp+48, t)
			libgmp.X__gmpz_tdiv_q_2exp(tls, bp+64, bp+64, t)
			ec += libc.Int64FromUint64(t)
		}
	}
	/* now either one of c, d is odd */
	for ey < 0 {
		/* check if x is a square */
		if ec&int64(1) != 0 {
			libgmp.X__gmpz_mul_2exp(tls, bp+48, bp+48, uint64(1))
			libgmp.X__gmpz_mul_2exp(tls, bp+64, bp+64, uint64(1))
			ec--
		}
		/* now ec is even */
		if _mpc_perfect_square_p(tls, bp+16, bp+32, bp+48, bp+64) == 0 {
			break
		}
		libgmp.X__gmpz_swap(tls, bp+16, bp+48)
		libgmp.X__gmpz_swap(tls, bp+32, bp+64)
		ec /= int64(2)
		ey++
	}
	if ey < 0 {
		ret = -int32(1) /* not representable */
		goto end
	}
	/* Now ey >= 0, it thus suffices to check that x^my is representable.
	   If my > 0, this is always true. If my < 0, we first try to invert
	   (c+I*d)*2^ec.
	*/
	if (*t__mpz_struct)(unsafe.Pointer(bp)).F_mp_size < 0 {
		v4 = -int32(1)
	} else {
		v4 = libc.BoolInt32((*t__mpz_struct)(unsafe.Pointer(bp)).F_mp_size > 0)
	}
	if v4 < 0 {
		/* If my < 0, 1 / (c + I*d) = (c - I*d)/(c^2 + d^2), thus a sufficient
		   condition is that c^2 + d^2 is a power of two, assuming |c| <> |d|.
		   Assume a prime p <> 2 divides c^2 + d^2,
		   then if p does not divide c or d, 1 / (c + I*d) cannot be exact.
		   If p divides both c and d, then we can write c = p*c', d = p*d',
		   and 1 / (c + I*d) = 1/p * 1/(c' + I*d'). This shows that if 1/(c+I*d)
		   is exact, then 1/(c' + I*d') is exact too, and we are back to the
		   previous case. In conclusion, a necessary and sufficient condition
		   is that c^2 + d^2 is a power of two.
		*/
		/* FIXME: we could first compute c^2+d^2 mod a limb for example */
		libgmp.X__gmpz_mul(tls, bp+16, bp+48, bp+48)
		libgmp.X__gmpz_addmul(tls, bp+16, bp+64, bp+64)
		t = libgmp.X__gmpz_scan1(tls, bp+16, uint64(0))
		if libgmp.X__gmpz_sizeinbase(tls, bp+16, int32(2)) != uint64(1)+t { /* a is not a power of two */
			ret = -int32(1) /* not representable */
			goto end
		}
		/* replace (c,d) by (c/(c^2+d^2), -d/(c^2+d^2)) */
		v5 = bp + 64
		v6 = bp + 64
		if v5 != v6 {
			libgmp.X__gmpz_set(tls, v5, v6)
		}
		(*t__mpz_struct)(unsafe.Pointer(v5)).F_mp_size = -(*t__mpz_struct)(unsafe.Pointer(v5)).F_mp_size
		ec = -ec - libc.Int64FromUint64(t)
		v7 = bp
		v8 = bp
		if v7 != v8 {
			libgmp.X__gmpz_set(tls, v7, v8)
		}
		(*t__mpz_struct)(unsafe.Pointer(v7)).F_mp_size = -(*t__mpz_struct)(unsafe.Pointer(v7)).F_mp_size
	}
	/* now ey >= 0 and my >= 0, and we want to compute
	   [(c + I * d) * 2^ec] ^ (my * 2^ey).
	   We first compute [(c + I * d) * 2^ec]^my, then square ey times. */
	t = libgmp.X__gmpz_sizeinbase(tls, bp, int32(2)) - uint64(1)
	libgmp.X__gmpz_set(tls, bp+16, bp+48)
	libgmp.X__gmpz_set(tls, bp+32, bp+64)
	ed = ec
	/* invariant: (a + I*b) * 2^ed = ((c + I*d) * 2^ec)^trunc(my/2^t) */
	for {
		v9 = t
		t--
		if !(v9 > uint64(0)) {
			break
		}
		/* square a + I*b */
		libgmp.X__gmpz_mul(tls, bp+80, bp+16, bp+32)
		libgmp.X__gmpz_mul(tls, bp+16, bp+16, bp+16)
		libgmp.X__gmpz_submul(tls, bp+16, bp+32, bp+32)
		libgmp.X__gmpz_mul_2exp(tls, bp+32, bp+80, uint64(1))
		ed *= int64(2)
		if libgmp.X__gmpz_tstbit(tls, bp, t) != 0 { /* multiply by c + I*d */
			libgmp.X__gmpz_mul(tls, bp+80, bp+16, bp+48)
			libgmp.X__gmpz_submul(tls, bp+80, bp+32, bp+64) /* ac-bd */
			libgmp.X__gmpz_mul(tls, bp+32, bp+32, bp+48)
			libgmp.X__gmpz_addmul(tls, bp+32, bp+16, bp+64) /* bc+ad */
			libgmp.X__gmpz_swap(tls, bp+16, bp+80)
			ed += ec
		}
		/* remove powers of two in (a,b) */
		if (*t__mpz_struct)(unsafe.Pointer(bp+16)).F_mp_size < 0 {
			v10 = -int32(1)
		} else {
			v10 = libc.BoolInt32((*t__mpz_struct)(unsafe.Pointer(bp+16)).F_mp_size > 0)
		}
		if v10 == 0 {
			w = libgmp.X__gmpz_scan1(tls, bp+32, uint64(0))
			libgmp.X__gmpz_tdiv_q_2exp(tls, bp+32, bp+32, w)
			ed += libc.Int64FromUint64(w)
		} else {
			if (*t__mpz_struct)(unsafe.Pointer(bp+32)).F_mp_size < 0 {
				v111 = -int32(1)
			} else {
				v111 = libc.BoolInt32((*t__mpz_struct)(unsafe.Pointer(bp+32)).F_mp_size > 0)
			}
			if v111 == 0 {
				w = libgmp.X__gmpz_scan1(tls, bp+16, uint64(0))
				libgmp.X__gmpz_tdiv_q_2exp(tls, bp+16, bp+16, w)
				ed += libc.Int64FromUint64(w)
			} else {
				w = libgmp.X__gmpz_scan1(tls, bp+16, uint64(0))
				v1 = libgmp.X__gmpz_scan1(tls, bp+32, uint64(0))
				if v1 < w {
					w = v1
				}
				libgmp.X__gmpz_tdiv_q_2exp(tls, bp+16, bp+16, w)
				libgmp.X__gmpz_tdiv_q_2exp(tls, bp+32, bp+32, w)
				ed += libc.Int64FromUint64(w)
			}
		}
		if libc.Int64FromUint64(libgmp.X__gmpz_sizeinbase(tls, bp+16, int32(2))) > maxprec || libc.Int64FromUint64(libgmp.X__gmpz_sizeinbase(tls, bp+32, int32(2))) > maxprec {
			goto end
		}
	}
	/* now a+I*b = (c+I*d)^my */
	for {
		v12 = ey
		ey--
		if !(v12 > 0) {
			break
		}
		/* square a + I*b */
		libgmp.X__gmpz_mul(tls, bp+80, bp+16, bp+32)
		libgmp.X__gmpz_mul(tls, bp+16, bp+16, bp+16)
		libgmp.X__gmpz_submul(tls, bp+16, bp+32, bp+32)
		libgmp.X__gmpz_mul_2exp(tls, bp+32, bp+80, uint64(1))
		ed *= int64(2)
		/* divide by largest 2^n possible, to avoid many loops for e.g.,
		   (2+2*I)^16777216 */
		sa = libgmp.X__gmpz_scan1(tls, bp+16, uint64(0))
		sb = libgmp.X__gmpz_scan1(tls, bp+32, uint64(0))
		if sa <= sb {
			v13 = sa
		} else {
			v13 = sb
		}
		sa = v13
		libgmp.X__gmpz_tdiv_q_2exp(tls, bp+16, bp+16, sa)
		libgmp.X__gmpz_tdiv_q_2exp(tls, bp+32, bp+32, sa)
		ed += libc.Int64FromUint64(sa)
		if libc.Int64FromUint64(libgmp.X__gmpz_sizeinbase(tls, bp+16, int32(2))) > maxprec || libc.Int64FromUint64(libgmp.X__gmpz_sizeinbase(tls, bp+32, int32(2))) > maxprec {
			goto end
		}
	}
	ret = libmpfr.Xmpfr_set_z(tls, z, bp+16, rnd&libc.Int32FromInt32(0x0F))
	inex_im = libmpfr.Xmpfr_set_z(tls, z+32, bp+32, rnd>>libc.Int32FromInt32(4))
	if ret < 0 {
		v14 = int32(2)
	} else {
		if ret == 0 {
			v15 = 0
		} else {
			v15 = int32(1)
		}
		v14 = v15
	}
	if inex_im < 0 {
		v16 = int32(2)
	} else {
		if inex_im == 0 {
			v17 = 0
		} else {
			v17 = int32(1)
		}
		v16 = v17
	}
	ret = v14 | v16<<int32(2)
	libmpfr.Xmpfr_mul_2si(tls, z, z, ed, rnd&libc.Int32FromInt32(0x0F))
	libmpfr.Xmpfr_mul_2si(tls, z+32, z+32, ed, rnd>>libc.Int32FromInt32(4))
	goto end
end:
	;
	libgmp.X__gmpz_clear(tls, bp)
	libgmp.X__gmpz_clear(tls, bp+16)
	libgmp.X__gmpz_clear(tls, bp+32)
	libgmp.X__gmpz_clear(tls, bp+48)
	libgmp.X__gmpz_clear(tls, bp+64)
	libgmp.X__gmpz_clear(tls, bp+80)
	if ret >= 0 && Ximag != 0 {
		if z_is_y != 0 {
			v18 = bp + 96
		} else {
			v18 = y
		}
		_fix_sign(tls, z, sign_rex, sign_imx, v18)
	}
	if z_is_y != 0 {
		libmpfr.Xmpfr_clear(tls, bp+96)
	}
	return ret
}

// C documentation
//
//	/* Return 1 if y*2^k is an odd integer, 0 otherwise.
//	   Adapted from MPFR, file pow.c.
//
//	   Examples: with k=0, check if y is an odd integer,
//	             with k=1, check if y is half-an-integer,
//	             with k=-1, check if y/2 is an odd integer.
//	*/
func _is_odd(tls *libc.TLS, y Tmpfr_srcptr, k Tmpfr_exp_t) (r int32) {
	var expo Tmpfr_exp_t
	var prec Tmpfr_prec_t
	var yn, v2 Tmp_size_t
	var yp uintptr
	var v1 int32
	_, _, _, _, _, _ = expo, prec, yn, yp, v1, v2
	expo = (*t__mpfr_struct)(unsafe.Pointer(y)).F_mpfr_exp + k
	if expo <= 0 {
		return 0
	} /* |y| < 1 and not 0 */
	prec = (*t__mpfr_struct)(unsafe.Pointer(y)).F_mpfr_prec
	if expo > prec {
		return 0
	} /* y is a multiple of 2^(expo-prec), thus not odd */
	/* 0 < expo <= prec:
	   y = 1xxxxxxxxxt.zzzzzzzzzzzzzzzzzz[000]
	        expo bits   (prec-expo) bits
	   We have to check that:
	   (a) the bit 't' is set
	   (b) all the 'z' bits are zero
	*/
	prec = ((prec-int64(1))/int64(libgmp.X__gmp_bits_per_limb)+int64(1))*int64(libgmp.X__gmp_bits_per_limb) - expo
	/* number of z+0 bits */
	yn = prec / int64(libgmp.X__gmp_bits_per_limb)
	/* yn is the index of limb containing the 't' bit */
	yp = (*t__mpfr_struct)(unsafe.Pointer(y)).F_mpfr_d
	/* if expo is a multiple of BITS_PER_MP_LIMB, t is bit 0 */
	if expo%int64(libgmp.X__gmp_bits_per_limb) == 0 {
		v1 = libc.BoolInt32(*(*Tmp_limb_t)(unsafe.Pointer(yp + uintptr(yn)*8))&uint64(1) == uint64(0))
	} else {
		v1 = libc.BoolInt32(*(*Tmp_limb_t)(unsafe.Pointer(yp + uintptr(yn)*8))<<(expo%int64(libgmp.X__gmp_bits_per_limb)-int64(1)) != libc.Uint64FromInt32(1)<<(libgmp.X__gmp_bits_per_limb-libc.Int32FromInt32(1)))
	}
	if v1 != 0 {
		return 0
	}
	for {
		yn--
		v2 = yn
		if !(v2 >= 0) {
			break
		}
		if *(*Tmp_limb_t)(unsafe.Pointer(yp + uintptr(yn)*8)) != uint64(0) {
			return 0
		}
	}
	return int32(1)
}

// C documentation
//
//	/* Put in z the value of x^y, rounded according to 'rnd'.
//	   Return the inexact flag in [0, 10]. */
func Xmpc_pow(tls *libc.TLS, z Tmpc_ptr, x Tmpc_srcptr, y Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(192)
	defer tls.Free(192)
	var _p, _p1, _p10, _p11, _p15, _p16, _p4, _p5, _p8, _p9 Tmpfr_ptr
	var _p12, _p13, _p14, _p2, _p3, _p6, _p7 Tmpfr_srcptr
	var cx1, cx11, inex, inex1, inex_im, inex_re, loop, ramified, ret, ret_exp, s1, s11, s2, s21, saved_overflow, saved_underflow, sign_imx, sign_rex, sign_rex1, sign_zi, sign_zi1, Ximag, Xreal, y_real, z_imag, z_real, v1, v10, v11, v12, v13, v14, v15, v16, v17, v18, v2, v21, v22, v23, v24, v25, v26, v27, v28, v29, v3, v30, v31, v32, v33, v34, v35, v36, v37, v38, v39, v4, v40, v41, v42, v43, v44, v45, v46, v47, v48, v49, v5, v50, v51, v52, v53, v54, v55, v56, v57, v58, v59, v60, v61, v62, v63 int32
	var di, dr, saved_emax, saved_emin Tmpfr_exp_t
	var maxprec, p, pi, pr, q Tmpfr_prec_t
	var v19, v20, v6, v7, v9 int64
	var _ /* n at bp+128 */ Tmpfr_t
	var _ /* n at bp+160 */ Tmpfr_t
	var _ /* t at bp+0 */ Tmpc_t
	var _ /* u at bp+64 */ Tmpc_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = _p, _p1, _p10, _p11, _p12, _p13, _p14, _p15, _p16, _p2, _p3, _p4, _p5, _p6, _p7, _p8, _p9, cx1, cx11, di, dr, inex, inex1, inex_im, inex_re, loop, maxprec, p, pi, pr, q, ramified, ret, ret_exp, s1, s11, s2, s21, saved_emax, saved_emin, saved_overflow, saved_underflow, sign_imx, sign_rex, sign_rex1, sign_zi, sign_zi1, Ximag, Xreal, y_real, z_imag, z_real, v1, v10, v11, v12, v13, v14, v15, v16, v17, v18, v19, v2, v20, v21, v22, v23, v24, v25, v26, v27, v28, v29, v3, v30, v31, v32, v33, v34, v35, v36, v37, v38, v39, v4, v40, v41, v42, v43, v44, v45, v46, v47, v48, v49, v5, v50, v51, v52, v53, v54, v55, v56, v57, v58, v59, v6, v60, v61, v62, v63, v7, v9
	ret = -int32(2)
	z_real = 0
	z_imag = 0
	ramified = 0
	/* save the underflow or overflow flags from MPFR */
	saved_underflow = libmpfr.Xmpfr_underflow_p(tls)
	saved_overflow = libmpfr.Xmpfr_overflow_p(tls)
	Xreal = libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(x+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)))
	y_real = libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(y+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)))
	if y_real != 0 && (*t__mpfr_struct)(unsafe.Pointer(y)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) { /* case y zero */
		if Xreal != 0 && (*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			/* we define 0^0 to be (1, +0) since the real part is
			   coherent with MPFR where 0^0 gives 1, and the sign of the
			   imaginary part cannot be determined                       */
			Xmpc_set_ui_ui(tls, z, uint64(1), uint64(0), int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
			return 0
		} else { /* x^0 = 1 +/- i*0 even for x=NaN see algorithms.tex for the
			   sign of zero */
			/* cx1 < 0 if |x| < 1
			   cx1 = 0 if |x| = 1
			   cx1 > 0 if |x| > 1
			*/
			libmpfr.Xmpfr_init(tls, bp+128)
			inex = Xmpc_norm(tls, bp+128, x, int32(_MPFR_RNDN))
			cx1 = libmpfr.Xmpfr_cmp_ui_2exp(tls, bp+128, libc.Uint64FromInt32(libc.Int32FromInt32(1)), 0)
			if cx1 == 0 && inex != 0 {
				cx1 = -inex
			}
			sign_zi = libc.BoolInt32(cx1 < 0 && libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(y+32)).F_mpfr_sign < 0) == 0 || cx1 == 0 && libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(x+32)).F_mpfr_sign < 0) != libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(y)).F_mpfr_sign < 0) || cx1 > 0 && (*t__mpfr_struct)(unsafe.Pointer(y+32)).F_mpfr_sign < 0)
			/* warning: mpc_set_ui_ui does not set Im(z) to -0 if Im(rnd)=RNDD */
			ret = Xmpc_set_ui_ui(tls, z, uint64(1), uint64(0), rnd)
			if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDD) || sign_zi != 0 {
				Xmpc_conj(tls, z, z, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
			}
			libmpfr.Xmpfr_clear(tls, bp+128)
			return ret
		}
	}
	if !(libmpfr.Xmpfr_number_p(tls, x) != 0 && libmpfr.Xmpfr_number_p(tls, x+32) != 0) || !(libmpfr.Xmpfr_number_p(tls, y) != 0 && libmpfr.Xmpfr_number_p(tls, y+32) != 0) {
		/* special values: exp(y*log(x)) */
		Xmpc_init2(tls, bp+64, int64(2))
		Xmpc_log(tls, bp+64, x, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
		Xmpc_mul(tls, bp+64, bp+64, y, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
		ret = Xmpc_exp(tls, z, bp+64, rnd)
		Xmpc_clear(tls, bp+64)
		goto end
	}
	if Xreal != 0 { /* case x real */
		if (*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) { /* x is zero */
			/* special values: exp(y*log(x)) */
			Xmpc_init2(tls, bp+64, int64(2))
			Xmpc_log(tls, bp+64, x, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
			Xmpc_mul(tls, bp+64, bp+64, y, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
			ret = Xmpc_exp(tls, z, bp+64, rnd)
			Xmpc_clear(tls, bp+64)
			goto end
		}
		/* Special case 1^y = 1 */
		if libmpfr.Xmpfr_cmp_ui_2exp(tls, x, libc.Uint64FromInt32(libc.Int32FromInt32(1)), 0) == 0 {
			s1 = libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(y)).F_mpfr_sign < 0)
			s2 = libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(x+32)).F_mpfr_sign < 0)
			ret = Xmpc_set_ui(tls, z, libc.Uint64FromInt32(+libc.Int32FromInt32(1)), rnd)
			/* the sign of the zero imaginary part is known in some cases (see
			   algorithm.tex). In such cases we have
			   (x +s*0i)^(y+/-0i) = x^y + s*sign(y)*0i
			   where s = +/-1.  We extend here this rule to fix the sign of the
			   zero part.
			   Note that the sign must also be set explicitly when rnd=RNDD
			   because mpfr_set_ui(z_i, 0, rnd) always sets z_i to +0.
			*/
			if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDD) || s1 != s2 {
				Xmpc_conj(tls, z, z, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
			}
			goto end
		}
		/* x^y is real when:
		   (a) x is real and y is integer
		   (b) x is real non-negative and y is real */
		if y_real != 0 && (libmpfr.Xmpfr_integer_p(tls, y) != 0 || libmpfr.Xmpfr_sgn(tls, x) >= 0) {
			s11 = libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(y)).F_mpfr_sign < 0)
			s21 = libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(x+32)).F_mpfr_sign < 0)
			ret = libmpfr.Xmpfr_pow(tls, z, x, y, rnd&libc.Int32FromInt32(0x0F))
			{
				_p = z + 32
				(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign = int32(1)
				(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
				_ = rnd >> libc.Int32FromInt32(4)
				v1 = 0
			}
			inex_im = v1
			if ret < 0 {
				v2 = int32(2)
			} else {
				if ret == 0 {
					v3 = 0
				} else {
					v3 = int32(1)
				}
				v2 = v3
			}
			if inex_im < 0 {
				v4 = int32(2)
			} else {
				if inex_im == 0 {
					v5 = 0
				} else {
					v5 = int32(1)
				}
				v4 = v5
			}
			ret = v2 | v4<<int32(2)
			/* the sign of the zero imaginary part is known in some cases
			   (see algorithm.tex). In such cases we have (x +s*0i)^(y+/-0i)
			   = x^y + s*sign(y)*0i where s = +/-1.
			   We extend here this rule to fix the sign of the zero part.
			   Note that the sign must also be set explicitly when rnd=RNDD
			   because mpfr_set_ui(z_i, 0, rnd) always sets z_i to +0.
			*/
			if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDD) || s11 != s21 {
				libmpfr.Xmpfr_neg(tls, z+32, z+32, rnd>>libc.Int32FromInt32(4))
			}
			goto end
		}
		/* (-1)^(n+I*t) is real for n integer and t real */
		if libmpfr.Xmpfr_cmp_si_2exp(tls, x, int64(-libc.Int32FromInt32(1)), 0) == 0 && libmpfr.Xmpfr_integer_p(tls, y) != 0 {
			z_real = int32(1)
		}
		/* for x real, x^y is imaginary when:
		   (a) x is negative and y is half-an-integer
		   (b) x = -1 and Re(y) is half-an-integer
		*/
		if libmpfr.Xmpfr_sgn(tls, x) < 0 && _is_odd(tls, y, int64(1)) != 0 && (y_real != 0 || libmpfr.Xmpfr_cmp_si_2exp(tls, x, int64(-libc.Int32FromInt32(1)), 0) == 0) {
			z_imag = int32(1)
		}
	} else { /* x non real */
		/* I^(t*I) and (-I)^(t*I) are real for t real,
		   I^(n+t*I) and (-I)^(n+t*I) are real for n even and t real, and
		   I^(n+t*I) and (-I)^(n+t*I) are imaginary for n odd and t real
		   (s*I)^n is real for n even and imaginary for n odd */
		if (Xmpc_cmp_si_si(tls, x, 0, int64(1)) == 0 || Xmpc_cmp_si_si(tls, x, 0, int64(-int32(1))) == 0 || libmpfr.Xmpfr_sgn(tls, x) == 0 && y_real != 0) && libmpfr.Xmpfr_integer_p(tls, y) != 0 {
			/* x is I or -I, and Re(y) is an integer */
			if _is_odd(tls, y, 0) != 0 {
				z_imag = int32(1)
			} else {
				z_real = int32(1)
			} /* Re(y) even: z is real */
		} else { /* (t+/-t*I)^(2n) is imaginary for n odd and real for n even */
			if libmpfr.Xmpfr_cmpabs(tls, x, x+32) == 0 && y_real != 0 && libmpfr.Xmpfr_integer_p(tls, y) != 0 && _is_odd(tls, y, 0) == 0 {
				ramified = int32(1)
				if _is_odd(tls, y, int64(-int32(1))) != 0 { /* y/2 is odd */
					z_imag = int32(1)
				} else {
					z_real = int32(1)
				}
			}
		}
	}
	saved_emin = libmpfr.Xmpfr_get_emin(tls)
	saved_emax = libmpfr.Xmpfr_get_emax(tls)
	libmpfr.Xmpfr_set_emin(tls, libmpfr.Xmpfr_get_emin_min(tls))
	libmpfr.Xmpfr_set_emax(tls, libmpfr.Xmpfr_get_emax_max(tls))
	pr = (*t__mpfr_struct)(unsafe.Pointer(z)).F_mpfr_prec
	pi = (*t__mpfr_struct)(unsafe.Pointer(z + 32)).F_mpfr_prec
	if pr > pi {
		v6 = pr
	} else {
		v6 = pi
	}
	p = v6
	p += int64(12) /* experimentally, seems to give less than 10% of failures in
	   Ziv's strategy; probably wrong now since q is not computed */
	if p < int64(64) {
		p = int64(64)
	}
	Xmpc_init2(tls, bp+64, p)
	Xmpc_init2(tls, bp, p)
	pr += libc.BoolInt64(rnd&libc.Int32FromInt32(0x0F) == int32(_MPFR_RNDN))
	pi += libc.BoolInt64(rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDN))
	if (*t__mpfr_struct)(unsafe.Pointer(z)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(z+32)).F_mpfr_prec {
		v7 = (*t__mpfr_struct)(unsafe.Pointer(z)).F_mpfr_prec
	} else {
		v7 = (*t__mpfr_struct)(unsafe.Pointer(z + 32)).F_mpfr_prec
	}
	maxprec = v7
	Ximag = libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)))
	loop = 0
	for {
		Xmpc_log(tls, bp, x, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
		Xmpc_mul(tls, bp, bp, y, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
		/* Compute q such that |Re (y log x)|, |Im (y log x)| < 2^q.
		   We recompute it at each loop since we might get different
		   bounds if the precision is not enough. */
		if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp > 0 {
			v9 = (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp
		} else {
			v9 = 0
		}
		q = v9
		if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp > q {
			q = (*t__mpfr_struct)(unsafe.Pointer(bp + 32)).F_mpfr_exp
		}
		/* the signs of the real/imaginary parts of exp(t) are determined by the
		   quadrant of exp(i*imag(t)), which depends on imag(t) mod (2pi).
		   We ensure that p >= q + 64 to get enough precision, but this might
		   be not enough in corner cases (FIXME). */
		if p < q+int64(64) {
			p = q + int64(64)
			goto try_again
		}
		libmpfr.Xmpfr_clear_overflow(tls)
		libmpfr.Xmpfr_clear_underflow(tls)
		ret_exp = Xmpc_exp(tls, bp+64, bp, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
		if libmpfr.Xmpfr_underflow_p(tls) != 0 || libmpfr.Xmpfr_overflow_p(tls) != 0 {
			/* under- and overflow flags are set by mpc_exp */
			Xmpc_set(tls, z, bp+64, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
			if ret_exp&int32(3) == int32(2) {
				v10 = -int32(1)
			} else {
				if ret_exp&int32(3) == 0 {
					v11 = 0
				} else {
					v11 = int32(1)
				}
				v10 = v11
			}
			inex_re = v10
			if ret_exp>>int32(2) == int32(2) {
				v12 = -int32(1)
			} else {
				if ret_exp>>int32(2) == 0 {
					v13 = 0
				} else {
					v13 = int32(1)
				}
				v12 = v13
			}
			inex_im = v12
			if (*t__mpfr_struct)(unsafe.Pointer(z)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				inex_re = Xmpc_fix_inf(tls, z, rnd&libc.Int32FromInt32(0x0F))
			}
			if (*t__mpfr_struct)(unsafe.Pointer(z+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				if z_real != 0 {
					{
						_p1 = z + 32
						(*t__mpfr_struct)(unsafe.Pointer(_p1)).F_mpfr_sign = int32(1)
						(*t__mpfr_struct)(unsafe.Pointer(_p1)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
						_ = rnd >> libc.Int32FromInt32(4)
						v14 = 0
					}
					inex_im = v14
				} else {
					inex_im = Xmpc_fix_inf(tls, z+32, rnd>>libc.Int32FromInt32(4))
				}
			}
			if inex_re < 0 {
				v15 = int32(2)
			} else {
				if inex_re == 0 {
					v16 = 0
				} else {
					v16 = int32(1)
				}
				v15 = v16
			}
			if inex_im < 0 {
				v17 = int32(2)
			} else {
				if inex_im == 0 {
					v18 = 0
				} else {
					v18 = int32(1)
				}
				v17 = v18
			}
			ret = v15 | v17<<int32(2)
			goto exact
		}
		/* Since the error bound is global, we have to take into account the
		   exponent difference between the real and imaginary parts. We assume
		   either the real or the imaginary part of u is not zero.
		*/
		if (*t__mpfr_struct)(unsafe.Pointer(bp+64)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			v19 = (*t__mpfr_struct)(unsafe.Pointer(bp + 64 + 32)).F_mpfr_exp
		} else {
			v19 = (*t__mpfr_struct)(unsafe.Pointer(bp + 64)).F_mpfr_exp
		}
		dr = v19
		if (*t__mpfr_struct)(unsafe.Pointer(bp+64+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			v20 = dr
		} else {
			v20 = (*t__mpfr_struct)(unsafe.Pointer(bp + 64 + 32)).F_mpfr_exp
		}
		di = v20
		if dr > di {
			di = dr - di
			dr = 0
		} else {
			dr = di - dr
			di = 0
		}
		/* the term -3 takes into account the factor 4 in the complex error
		   (see algorithms.tex) plus one due to the exponent difference: if
		   z = a + I*b, where the relative error on z is at most 2^(-p), and
		   EXP(a) = EXP(b) + k, the relative error on b is at most 2^(k-p) */
		if (z_imag != 0 || p > q+int64(3)+dr && libmpfr.Xmpfr_can_round(tls, bp+64, p-q-int64(3)-dr, int32(_MPFR_RNDN), int32(_MPFR_RNDZ), pr) != 0) && (z_real != 0 || p > q+int64(3)+di && libmpfr.Xmpfr_can_round(tls, bp+64+32, p-q-int64(3)-di, int32(_MPFR_RNDN), int32(_MPFR_RNDZ), pi) != 0) {
			break
		}
		/* if Re(u) is not known to be zero, assume it is a normal number, i.e.,
		   neither zero, Inf or NaN, otherwise we might enter an infinite loop */
		/* idem for Im(u) */
		if ret == -int32(2) { /* we did not yet call mpc_pow_exact, or it aborted
			   because intermediate computations had > maxprec bits */
			/* check exact cases (see algorithms.tex) */
			if y_real != 0 {
				maxprec *= int64(2)
				ret = _mpc_pow_exact(tls, z, x, y, rnd, maxprec)
				if ret != -int32(1) && ret != -int32(2) {
					goto exact
				}
			}
			p += dr + di + int64(64)
		} else {
			p += p / int64(2)
		}
		goto try_again
	try_again:
		;
		Xmpc_set_prec(tls, bp, p)
		Xmpc_set_prec(tls, bp+64, p)
		goto _8
	_8:
		;
		loop++
	}
	if z_real != 0 {
		/* When the result is real (see algorithm.tex for details) and
		   x=x1 * (1 \pm i), y a positive integer divisible by 4, then
		   Im(x^y) = 0i with a sign that cannot be determined (and is thus
		   chosen as _1). Otherwise,
		   Im(x^y) =
		   + sign(imag(y))*0i,               if |x| > 1
		   + sign(imag(x))*sign(real(y))*0i, if |x| = 1
		   - sign(imag(y))*0i,               if |x| < 1
		*/
		if ramified != 0 {
			{
				_p2 = bp + 64
				v22 = libmpfr.Xmpfr_set4(tls, z, _p2, rnd&libc.Int32FromInt32(0x0F), (*t__mpfr_struct)(unsafe.Pointer(_p2)).F_mpfr_sign)
			}
			if v22 < 0 {
				v21 = int32(2)
			} else {
				{
					_p3 = bp + 64
					v24 = libmpfr.Xmpfr_set4(tls, z, _p3, rnd&libc.Int32FromInt32(0x0F), (*t__mpfr_struct)(unsafe.Pointer(_p3)).F_mpfr_sign)
				}
				if v24 == 0 {
					v23 = 0
				} else {
					v23 = int32(1)
				}
				v21 = v23
			}
			{
				_p4 = z + 32
				(*t__mpfr_struct)(unsafe.Pointer(_p4)).F_mpfr_sign = int32(1)
				(*t__mpfr_struct)(unsafe.Pointer(_p4)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
				_ = int32(_MPFR_RNDN)
				v26 = 0
			}
			if v26 < 0 {
				v25 = int32(2)
			} else {
				{
					_p5 = z + 32
					(*t__mpfr_struct)(unsafe.Pointer(_p5)).F_mpfr_sign = int32(1)
					(*t__mpfr_struct)(unsafe.Pointer(_p5)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
					_ = int32(_MPFR_RNDN)
					v28 = 0
				}
				if v28 == 0 {
					v27 = 0
				} else {
					v27 = int32(1)
				}
				v25 = v27
			}
			ret = v21 | v25<<int32(2)
		} else {
			/* cx1 < 0 if |x| < 1
			   cx1 = 0 if |x| = 1
			   cx1 > 0 if |x| > 1
			*/
			sign_rex = libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_sign < 0)
			sign_imx = libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(x+32)).F_mpfr_sign < 0)
			libmpfr.Xmpfr_init(tls, bp+160)
			inex1 = Xmpc_norm(tls, bp+160, x, int32(_MPFR_RNDN))
			cx11 = libmpfr.Xmpfr_cmp_ui_2exp(tls, bp+160, libc.Uint64FromInt32(libc.Int32FromInt32(1)), 0)
			if cx11 == 0 && inex1 != 0 {
				cx11 = -inex1
			}
			sign_zi1 = libc.BoolInt32(cx11 < 0 && libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(y+32)).F_mpfr_sign < 0) == 0 || cx11 == 0 && sign_imx != libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(y)).F_mpfr_sign < 0) || cx11 > 0 && (*t__mpfr_struct)(unsafe.Pointer(y+32)).F_mpfr_sign < 0)
			/* copy RE(y) to n since if z==y we will destroy Re(y) below */
			libmpfr.Xmpfr_set_prec(tls, bp+160, (*t__mpfr_struct)(unsafe.Pointer(y)).F_mpfr_prec)
			{
				_p6 = y
				v29 = libmpfr.Xmpfr_set4(tls, bp+160, _p6, int32(_MPFR_RNDN), (*t__mpfr_struct)(unsafe.Pointer(_p6)).F_mpfr_sign)
			}
			_ = v29
			{
				_p7 = bp + 64
				v30 = libmpfr.Xmpfr_set4(tls, z, _p7, rnd&libc.Int32FromInt32(0x0F), (*t__mpfr_struct)(unsafe.Pointer(_p7)).F_mpfr_sign)
			}
			ret = v30
			if y_real != 0 && (Xreal != 0 || Ximag != 0) {
				/* FIXME: with y_real we assume Im(y) is really 0, which is the case
				   for example when y comes from pow_fr, but in case Im(y) is +0 or
				   -0, we might get different results */
				{
					_p8 = z + 32
					(*t__mpfr_struct)(unsafe.Pointer(_p8)).F_mpfr_sign = int32(1)
					(*t__mpfr_struct)(unsafe.Pointer(_p8)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
					_ = rnd >> libc.Int32FromInt32(4)
					v31 = 0
				}
				_ = v31
				_fix_sign(tls, z, sign_rex, sign_imx, bp+160)
				if ret < 0 {
					v32 = int32(2)
				} else {
					if ret == 0 {
						v33 = 0
					} else {
						v33 = int32(1)
					}
					v32 = v33
				}
				ret = v32 | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2) /* imaginary part is exact */
			} else {
				{
					_p9 = z + 32
					(*t__mpfr_struct)(unsafe.Pointer(_p9)).F_mpfr_sign = int32(1)
					(*t__mpfr_struct)(unsafe.Pointer(_p9)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
					_ = rnd >> libc.Int32FromInt32(4)
					v34 = 0
				}
				inex_im = v34
				if ret < 0 {
					v35 = int32(2)
				} else {
					if ret == 0 {
						v36 = 0
					} else {
						v36 = int32(1)
					}
					v35 = v36
				}
				if inex_im < 0 {
					v37 = int32(2)
				} else {
					if inex_im == 0 {
						v38 = 0
					} else {
						v38 = int32(1)
					}
					v37 = v38
				}
				ret = v35 | v37<<int32(2)
				/* warning: mpfr_set_ui does not set Im(z) to -0 if Im(rnd) = RNDD */
				if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDD) || sign_zi1 != 0 {
					Xmpc_conj(tls, z, z, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
				}
			}
			libmpfr.Xmpfr_clear(tls, bp+160)
		}
	} else {
		if z_imag != 0 {
			if ramified != 0 {
				{
					_p10 = z
					(*t__mpfr_struct)(unsafe.Pointer(_p10)).F_mpfr_sign = int32(1)
					(*t__mpfr_struct)(unsafe.Pointer(_p10)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
					_ = int32(_MPFR_RNDN)
					v40 = 0
				}
				if v40 < 0 {
					v39 = int32(2)
				} else {
					{
						_p11 = z
						(*t__mpfr_struct)(unsafe.Pointer(_p11)).F_mpfr_sign = int32(1)
						(*t__mpfr_struct)(unsafe.Pointer(_p11)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
						_ = int32(_MPFR_RNDN)
						v42 = 0
					}
					if v42 == 0 {
						v41 = 0
					} else {
						v41 = int32(1)
					}
					v39 = v41
				}
				{
					_p12 = bp + 64 + 32
					v44 = libmpfr.Xmpfr_set4(tls, z+32, _p12, rnd>>libc.Int32FromInt32(4), (*t__mpfr_struct)(unsafe.Pointer(_p12)).F_mpfr_sign)
				}
				if v44 < 0 {
					v43 = int32(2)
				} else {
					{
						_p13 = bp + 64 + 32
						v46 = libmpfr.Xmpfr_set4(tls, z+32, _p13, rnd>>libc.Int32FromInt32(4), (*t__mpfr_struct)(unsafe.Pointer(_p13)).F_mpfr_sign)
					}
					if v46 == 0 {
						v45 = 0
					} else {
						v45 = int32(1)
					}
					v43 = v45
				}
				ret = v39 | v43<<int32(2)
			} else {
				{
					_p14 = bp + 64 + 32
					v47 = libmpfr.Xmpfr_set4(tls, z+32, _p14, rnd>>libc.Int32FromInt32(4), (*t__mpfr_struct)(unsafe.Pointer(_p14)).F_mpfr_sign)
				}
				ret = v47
				/* if z is imaginary and y real, then x cannot be real */
				if y_real != 0 && Ximag != 0 {
					sign_rex1 = libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_sign < 0)
					/* If z overlaps with y we set Re(z) before checking Re(y) below,
					   but in that case y=0, which was dealt with above. */
					{
						_p15 = z
						(*t__mpfr_struct)(unsafe.Pointer(_p15)).F_mpfr_sign = int32(1)
						(*t__mpfr_struct)(unsafe.Pointer(_p15)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
						_ = rnd & libc.Int32FromInt32(0x0F)
						v48 = 0
					}
					_ = v48
					/* Note: fix_sign only does something when y is an integer,
					   then necessarily y = 1 or 3 (mod 4), and in that case the
					   sign of Im(x) is irrelevant. */
					_fix_sign(tls, z, sign_rex1, 0, y)
					if ret < 0 {
						v49 = int32(2)
					} else {
						if ret == 0 {
							v50 = 0
						} else {
							v50 = int32(1)
						}
						v49 = v50
					}
					ret = 0 | v49<<int32(2)
				} else {
					{
						_p16 = z
						(*t__mpfr_struct)(unsafe.Pointer(_p16)).F_mpfr_sign = int32(1)
						(*t__mpfr_struct)(unsafe.Pointer(_p16)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
						_ = rnd & libc.Int32FromInt32(0x0F)
						v51 = 0
					}
					inex_re = v51
					if inex_re < 0 {
						v52 = int32(2)
					} else {
						if inex_re == 0 {
							v53 = 0
						} else {
							v53 = int32(1)
						}
						v52 = v53
					}
					if ret < 0 {
						v54 = int32(2)
					} else {
						if ret == 0 {
							v55 = 0
						} else {
							v55 = int32(1)
						}
						v54 = v55
					}
					ret = v52 | v54<<int32(2)
				}
			}
		} else {
			ret = Xmpc_set(tls, z, bp+64, rnd)
		}
	}
	goto exact
exact:
	;
	Xmpc_clear(tls, bp)
	Xmpc_clear(tls, bp+64)
	/* restore underflow and overflow flags from MPFR */
	if saved_underflow != 0 {
		libmpfr.Xmpfr_set_underflow(tls)
	}
	if saved_overflow != 0 {
		libmpfr.Xmpfr_set_overflow(tls)
	}
	/* restore the exponent range, and check the range of results */
	libmpfr.Xmpfr_set_emin(tls, saved_emin)
	libmpfr.Xmpfr_set_emax(tls, saved_emax)
	if ret&int32(3) == int32(2) {
		v56 = -int32(1)
	} else {
		if ret&int32(3) == 0 {
			v57 = 0
		} else {
			v57 = int32(1)
		}
		v56 = v57
	}
	inex_re = libmpfr.Xmpfr_check_range(tls, z, v56, rnd&libc.Int32FromInt32(0x0F))
	if ret>>int32(2) == int32(2) {
		v58 = -int32(1)
	} else {
		if ret>>int32(2) == 0 {
			v59 = 0
		} else {
			v59 = int32(1)
		}
		v58 = v59
	}
	inex_im = libmpfr.Xmpfr_check_range(tls, z+32, v58, rnd>>libc.Int32FromInt32(4))
	if inex_re < 0 {
		v60 = int32(2)
	} else {
		if inex_re == 0 {
			v61 = 0
		} else {
			v61 = int32(1)
		}
		v60 = v61
	}
	if inex_im < 0 {
		v62 = int32(2)
	} else {
		if inex_im == 0 {
			v63 = 0
		} else {
			v63 = int32(1)
		}
		v62 = v63
	}
	ret = v60 | v62<<int32(2)
	goto end
end:
	;
	return ret
	return r
}

func Xmpc_pow_fr(tls *libc.TLS, z Tmpc_ptr, x Tmpc_srcptr, y Tmpfr_srcptr, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var _p Tmpfr_ptr
	var inex, v1 int32
	var _ /* yy at bp+0 */ Tmpc_t
	_, _, _ = _p, inex, v1
	/* avoid copying the significand of y by copying only the struct */
	*(*t__mpfr_struct)(unsafe.Pointer(bp)) = *(*t__mpfr_struct)(unsafe.Pointer(y))
	libmpfr.Xmpfr_init2(tls, bp+32, int64(m_MPFR_PREC_MIN))
	{
		_p = bp + 32
		(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign = int32(1)
		(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
		_ = int32(_MPFR_RNDN)
		v1 = 0
	}
	_ = v1
	inex = Xmpc_pow(tls, z, x, bp, rnd)
	libmpfr.Xmpfr_clear(tls, bp+32)
	return inex
}

const m_DBL_DECIMAL_DIG = 17
const m_DBL_DIG = 15
const m_DBL_EPSILON = 2.22044604925031308085e-16
const m_DBL_HAS_SUBNORM = 1
const m_DBL_MANT_DIG = 53
const m_DBL_MAX = 1.79769313486231570815e+308
const m_DBL_MAX_10_EXP = 308
const m_DBL_MAX_EXP = 1024
const m_DBL_MIN = 2.22507385850720138309e-308
const m_DBL_TRUE_MIN = 4.94065645841246544177e-324
const m_DECIMAL_DIG = 17
const m_FLT_DECIMAL_DIG = 9
const m_FLT_DIG = 6
const m_FLT_EPSILON = 1.1920928955078125e-07
const m_FLT_EVAL_METHOD = 0
const m_FLT_HAS_SUBNORM = 1
const m_FLT_MANT_DIG = 24
const m_FLT_MAX = 3.40282346638528859812e+38
const m_FLT_MAX_10_EXP = 38
const m_FLT_MAX_EXP = 128
const m_FLT_MIN = 1.17549435082228750797e-38
const m_FLT_RADIX = 2
const m_FLT_TRUE_MIN = 1.40129846432481707092e-45
const m_LDBL_DECIMAL_DIG = "DECIMAL_DIG"
const m_LDBL_DIG = 15
const m_LDBL_EPSILON = 2.22044604925031308085e-16
const m_LDBL_HAS_SUBNORM = 1
const m_LDBL_MANT_DIG = 53
const m_LDBL_MAX = 1.79769313486231570815e+308
const m_LDBL_MAX_10_EXP = 308
const m_LDBL_MAX_EXP = 1024
const m_LDBL_MIN = 2.22507385850720138309e-308
const m_LDBL_TRUE_MIN = 4.94065645841246544177e-324

func Xmpc_pow_ld(tls *libc.TLS, z Tmpc_ptr, x Tmpc_srcptr, y float64, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var inex int32
	var _ /* yy at bp+0 */ Tmpc_t
	_ = inex
	Xmpc_init3(tls, bp, int64(m_LDBL_MANT_DIG), int64(m_MPFR_PREC_MIN))
	Xmpc_set_ld(tls, bp, y, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4)) /* exact */
	inex = Xmpc_pow(tls, z, x, bp, rnd)
	Xmpc_clear(tls, bp)
	return inex
}

func Xmpc_pow_d(tls *libc.TLS, z Tmpc_ptr, x Tmpc_srcptr, y float64, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var inex int32
	var _ /* yy at bp+0 */ Tmpc_t
	_ = inex
	Xmpc_init3(tls, bp, int64(m_DBL_MANT_DIG), int64(m_MPFR_PREC_MIN))
	Xmpc_set_d(tls, bp, y, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4)) /* exact */
	inex = Xmpc_pow(tls, z, x, bp, rnd)
	Xmpc_clear(tls, bp)
	return inex
}

func Xmpc_pow_si(tls *libc.TLS, z Tmpc_ptr, x Tmpc_srcptr, y int64, rnd Tmpc_rnd_t) (r int32) {
	if y >= 0 {
		return Xmpc_pow_usi(tls, z, x, libc.Uint64FromInt64(y), int32(1), rnd)
	} else {
		return Xmpc_pow_usi(tls, z, x, libc.Uint64FromInt64(-y), -int32(1), rnd)
	}
	return r
}

func _mpc_pow_usi_naive(tls *libc.TLS, z Tmpc_ptr, x Tmpc_srcptr, y uint64, sign int32, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var inex int32
	var _ /* t at bp+0 */ Tmpc_t
	_ = inex
	Xmpc_init3(tls, bp, libc.Int64FromUint64(libc.Uint64FromInt64(8)*libc.Uint64FromInt32(m_CHAR_BIT)), int64(m_MPFR_PREC_MIN))
	if sign > 0 {
		Xmpc_set_ui(tls, bp, y, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
	} else {
		Xmpc_set_si(tls, bp, -libc.Int64FromUint64(y), int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
	}
	inex = Xmpc_pow(tls, z, x, bp, rnd)
	Xmpc_clear(tls, bp)
	return inex
}

func Xmpc_pow_usi(tls *libc.TLS, z Tmpc_ptr, x Tmpc_srcptr, y uint64, sign int32, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(128)
	defer tls.Free(128)
	/* computes z = x^(sign*y) */
	var diff, exp_i, exp_r Tmpfr_exp_t
	var done, has3, inex, loop int32
	var ei, er, l, l0, p, v6 Tmpfr_prec_t
	var u uint64
	var v1, v10, v12, v2, v5, v7, v8, v9 int64
	var v11, v3 bool
	var _ /* t at bp+0 */ Tmpc_t
	var _ /* x3 at bp+64 */ Tmpc_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = diff, done, ei, er, exp_i, exp_r, has3, inex, l, l0, loop, p, u, v1, v10, v11, v12, v2, v3, v5, v6, v7, v8, v9
	/* let mpc_pow deal with special values */
	if !(libmpfr.Xmpfr_number_p(tls, x) != 0 && libmpfr.Xmpfr_number_p(tls, x+32) != 0) || (*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(x+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || y == uint64(0) {
		return _mpc_pow_usi_naive(tls, z, x, y, sign, rnd)
	} else {
		if y == uint64(1) {
			if sign > 0 {
				return Xmpc_set(tls, z, x, rnd)
			} else {
				return Xmpc_ui_div(tls, z, uint64(1), x, rnd)
			}
		} else {
			if y == uint64(2) && sign > 0 {
				return Xmpc_sqr(tls, z, x, rnd)
			} else {
				exp_r = (*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_exp
				exp_i = (*t__mpfr_struct)(unsafe.Pointer(x + 32)).F_mpfr_exp
				if exp_r > exp_i {
					v1 = exp_r
				} else {
					v1 = exp_i
				}
				if v3 = v1 > libmpfr.Xmpfr_get_emax(tls)/libc.Int64FromUint64(y); !v3 {
					if -exp_r > -exp_i {
						v2 = -exp_r
					} else {
						v2 = -exp_i
					}
				}
				if v3 || v2 > -libmpfr.Xmpfr_get_emin(tls)/libc.Int64FromUint64(y) {
					return _mpc_pow_usi_naive(tls, z, x, y, sign, rnd)
				}
			}
		}
	}
	has3 = libc.BoolInt32(y&(y>>int32(1)) != uint64(0))
	l = 0
	u = y
	for {
		if !(u > uint64(3)) {
			break
		}
		goto _4
	_4:
		;
		l++
		u >>= uint64(1)
	}
	/* l>0 is the number of bits of y, minus 2, thus y has bits:
	   y_{l+1} y_l y_{l-1} ... y_1 y_0 */
	l0 = l + int64(2)
	if (*t__mpfr_struct)(unsafe.Pointer(z)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(z+32)).F_mpfr_prec {
		v5 = (*t__mpfr_struct)(unsafe.Pointer(z)).F_mpfr_prec
	} else {
		v5 = (*t__mpfr_struct)(unsafe.Pointer(z + 32)).F_mpfr_prec
	}
	p = v5 + l0 + int64(32) /* l0 ensures that y*2^{-p} <= 1 below */
	Xmpc_init2(tls, bp, p)
	if has3 != 0 {
		Xmpc_init2(tls, bp+64, p)
	}
	loop = 0
	done = 0
	for !(done != 0) {
		loop++
		Xmpc_sqr(tls, bp, x, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
		if has3 != 0 {
			Xmpc_mul(tls, bp+64, bp, x, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
			if y>>l&uint64(1) != 0 { /* y starts with 11... */
				Xmpc_set(tls, bp, bp+64, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
			}
		}
		for {
			v6 = l
			l--
			if !(v6 > 0) {
				break
			}
			Xmpc_sqr(tls, bp, bp, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
			if y>>l&uint64(1) != 0 {
				if l > 0 && y>>(l-int64(1))&uint64(1) != 0 { /* implies has3 <> 0 */
					l--
					Xmpc_sqr(tls, bp, bp, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
					Xmpc_mul(tls, bp, bp, bp+64, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
				} else {
					Xmpc_mul(tls, bp, bp, x, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
				}
			}
		}
		if sign < 0 {
			Xmpc_ui_div(tls, bp, uint64(1), bp, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
		}
		if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			inex = _mpc_pow_usi_naive(tls, z, x, y, sign, rnd)
			/* since mpfr_get_exp() is not defined for zero */
			done = int32(1)
		} else {
			diff = (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp - (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp
			/* the factor on the real part is 2+2^(-diff+2) <= 4 for diff >= 1
			   and < 2^(-diff+3) for diff <= 0 */
			if diff >= int64(1) {
				v7 = l0 + int64(3)
			} else {
				v7 = l0 + -diff + int64(3)
			}
			er = v7
			/* the factor on the imaginary part is 2+2^(diff+2) <= 4 for diff <= -1
			   and < 2^(diff+3) for diff >= 0 */
			if diff <= int64(-int32(1)) {
				v8 = l0 + int64(3)
			} else {
				v8 = l0 + diff + int64(3)
			}
			ei = v8
			if libmpfr.Xmpfr_can_round(tls, bp, p-er, int32(_MPFR_RNDN), int32(_MPFR_RNDZ), (*t__mpfr_struct)(unsafe.Pointer(z)).F_mpfr_prec+libc.BoolInt64(rnd&libc.Int32FromInt32(0x0F) == int32(_MPFR_RNDN))) != 0 && libmpfr.Xmpfr_can_round(tls, bp+32, p-ei, int32(_MPFR_RNDN), int32(_MPFR_RNDZ), (*t__mpfr_struct)(unsafe.Pointer(z+32)).F_mpfr_prec+libc.BoolInt64(rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDN))) != 0 {
				inex = Xmpc_set(tls, z, bp, rnd)
				done = int32(1)
			} else {
				if v11 = loop == int32(1); v11 {
					if diff >= 0 {
						v9 = diff
					} else {
						v9 = -diff
					}
					if (*t__mpfr_struct)(unsafe.Pointer(z)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(z+32)).F_mpfr_prec {
						v10 = (*t__mpfr_struct)(unsafe.Pointer(z)).F_mpfr_prec
					} else {
						v10 = (*t__mpfr_struct)(unsafe.Pointer(z + 32)).F_mpfr_prec
					}
				}
				if v11 && v9 < v10 {
					/* common case, make a second trial at higher precision */
					if (*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(x+32)).F_mpfr_prec {
						v12 = (*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_prec
					} else {
						v12 = (*t__mpfr_struct)(unsafe.Pointer(x + 32)).F_mpfr_prec
					}
					p += v12
					Xmpc_set_prec(tls, bp, p)
					if has3 != 0 {
						Xmpc_set_prec(tls, bp+64, p)
					}
					l = l0 - int64(2)
				} else {
					/* stop the loop and use mpc_pow */
					inex = _mpc_pow_usi_naive(tls, z, x, y, sign, rnd)
					done = int32(1)
				}
			}
		}
	}
	Xmpc_clear(tls, bp)
	if has3 != 0 {
		Xmpc_clear(tls, bp+64)
	}
	return inex
}

func Xmpc_pow_ui(tls *libc.TLS, z Tmpc_ptr, x Tmpc_srcptr, y uint64, rnd Tmpc_rnd_t) (r int32) {
	return Xmpc_pow_usi(tls, z, x, y, int32(1), rnd)
}

func Xmpc_pow_z(tls *libc.TLS, z Tmpc_ptr, x Tmpc_srcptr, y Tmpz_srcptr, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var __gmp_l Tmp_limb_t
	var __gmp_n, __gmp_n1 Tmp_size_t
	var __gmp_p, __gmp_p1 Tmp_ptr
	var inex, v1, v3 int32
	var n Tmpfr_prec_t
	var v2, v5 Tmpz_srcptr
	var v6, v8 uint64
	var v9 int64
	var _ /* yy at bp+0 */ Tmpc_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _ = __gmp_l, __gmp_n, __gmp_n1, __gmp_p, __gmp_p1, inex, n, v1, v2, v3, v5, v6, v8, v9
	n = libc.Int64FromUint64(libgmp.X__gmpz_sizeinbase(tls, y, int32(2)))
	/* if y fits in an unsigned long or long, call the corresponding functions,
	   which are supposed to be more efficient */
	if (*t__mpz_struct)(unsafe.Pointer(y)).F_mp_size < 0 {
		v1 = -int32(1)
	} else {
		v1 = libc.BoolInt32((*t__mpz_struct)(unsafe.Pointer(y)).F_mp_size > 0)
	}
	if v1 >= 0 {
		v2 = y
		__gmp_n = int64((*t__mpz_struct)(unsafe.Pointer(v2)).F_mp_size)
		__gmp_p = (*t__mpz_struct)(unsafe.Pointer(v2)).F_mp_d
		v3 = libc.BoolInt32(__gmp_n == 0 || __gmp_n == int64(1) && *(*Tmp_limb_t)(unsafe.Pointer(__gmp_p)) <= libc.Uint64FromUint64(2)*libc.Uint64FromInt64(0x7fffffffffffffff)+libc.Uint64FromInt32(1))
		goto _4
	_4:
		if v3 != 0 {
			v5 = y
			__gmp_p1 = (*t__mpz_struct)(unsafe.Pointer(v5)).F_mp_d
			__gmp_n1 = int64((*t__mpz_struct)(unsafe.Pointer(v5)).F_mp_size)
			__gmp_l = *(*Tmp_limb_t)(unsafe.Pointer(__gmp_p1))
			if __gmp_n1 != 0 {
				v8 = __gmp_l
			} else {
				v8 = uint64(0)
			}
			v6 = v8
			goto _7
		_7:
			return Xmpc_pow_usi(tls, z, x, v6, int32(1), rnd)
		}
	} else {
		if libgmp.X__gmpz_fits_slong_p(tls, y) != 0 {
			return Xmpc_pow_usi(tls, z, x, libc.Uint64FromInt64(-libgmp.X__gmpz_get_si(tls, y)), -int32(1), rnd)
		}
	}
	if n < int64(m_MPFR_PREC_MIN) {
		v9 = int64(m_MPFR_PREC_MIN)
	} else {
		v9 = n
	}
	Xmpc_init3(tls, bp, v9, int64(m_MPFR_PREC_MIN))
	Xmpc_set_z(tls, bp, y, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4)) /* exact */
	inex = Xmpc_pow(tls, z, x, bp, rnd)
	Xmpc_clear(tls, bp)
	return inex
}

func Xmpc_proj(tls *libc.TLS, a Tmpc_ptr, b Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	var v1 int32
	_ = v1
	if (*t__mpfr_struct)(unsafe.Pointer(b)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(b+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		/* infinities project to +Inf +i* copysign(0.0, cimag(z)) */
		libmpfr.Xmpfr_set_inf(tls, a, +libc.Int32FromInt32(1))
		if (*t__mpfr_struct)(unsafe.Pointer(b+32)).F_mpfr_sign < 0 {
			v1 = -int32(1)
		} else {
			v1 = int32(1)
		}
		libmpfr.Xmpfr_set_zero(tls, a+32, v1)
		return libc.Int32FromInt32(0) | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2)
	} else {
		return Xmpc_set(tls, a, b, rnd)
	}
	return r
}

func Xmpc_real(tls *libc.TLS, a Tmpfr_ptr, b Tmpc_srcptr, rnd Tmpfr_rnd_t) (r int32) {
	var _p Tmpfr_srcptr
	var v1 int32
	_, _ = _p, v1
	{
		_p = b
		v1 = libmpfr.Xmpfr_set4(tls, a, _p, rnd, (*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign)
	}
	return v1
}

func _gcd(tls *libc.TLS, a uint64, b uint64) (r uint64) {
	if b == uint64(0) {
		return a
	} else {
		return _gcd(tls, b, a%b)
	}
	return r
}

// C documentation
//
//	/* put in rop the value of exp(2*i*pi*k/n) rounded according to rnd */
func Xmpc_rootofunity(tls *libc.TLS, rop Tmpc_ptr, n uint64, k uint64, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(128)
	defer tls.Free(128)
	var _p, _p1 Tmpfr_srcptr
	var g uint64
	var inex_im, inex_re, v1, v10, v11, v12, v13, v14, v15, v16, v17, v18, v19, v2, v20, v21, v22, v24, v25, v26, v27, v28, v29, v3, v4, v5, v6, v7, v8, v9 int32
	var prec Tmpfr_prec_t
	var rnd_im, rnd_re Tmpfr_rnd_t
	var v23 int64
	var _ /* c at bp+96 */ Tmpfr_t
	var _ /* kn at bp+0 */ Tmpq_t
	var _ /* s at bp+64 */ Tmpfr_t
	var _ /* t at bp+32 */ Tmpfr_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = _p, _p1, g, inex_im, inex_re, prec, rnd_im, rnd_re, v1, v10, v11, v12, v13, v14, v15, v16, v17, v18, v19, v2, v20, v21, v22, v23, v24, v25, v26, v27, v28, v29, v3, v4, v5, v6, v7, v8, v9
	if n == uint64(0) {
		/* Compute exp (0 + i*inf). */
		libmpfr.Xmpfr_set_nan(tls, rop)
		libmpfr.Xmpfr_set_nan(tls, rop+32)
		return libc.Int32FromInt32(0) | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2)
	}
	/* Eliminate common denominator. */
	k %= n
	g = _gcd(tls, k, n)
	k /= g
	n /= g
	/* Now 0 <= k < n and gcd(k,n)=1. */
	/* We assume that only n=1, 2, 3, 4, 6 and 12 may yield exact results
	   and treat them separately; n=8 is also treated here for efficiency
	   reasons. */
	if n == uint64(1) {
		/* necessarily k=0 thus we want exp(0)=1 */
		return Xmpc_set_ui_ui(tls, rop, uint64(1), uint64(0), rnd)
	} else {
		if n == uint64(2) {
			/* since gcd(k,n)=1, necessarily k=1, thus we want exp(i*pi)=-1 */
			return Xmpc_set_si_si(tls, rop, int64(-int32(1)), 0, rnd)
		} else {
			if n == uint64(4) {
				/* since gcd(k,n)=1, necessarily k=1 or k=3, thus we want
				   exp(2*i*pi/4)=i or exp(2*i*pi*3/4)=-i */
				if k == uint64(1) {
					return Xmpc_set_ui_ui(tls, rop, uint64(0), uint64(1), rnd)
				} else {
					return Xmpc_set_si_si(tls, rop, 0, int64(-int32(1)), rnd)
				}
			} else {
				if n == uint64(3) || n == uint64(6) {
					/* for n=3, necessarily k=1 or k=2: -1/2+/-1/2*sqrt(3)*i;
					   for n=6, necessarily k=1 or k=5: 1/2+/-1/2*sqrt(3)*i */
					if n == uint64(3) {
						v1 = -int32(1)
					} else {
						v1 = int32(1)
					}
					inex_re = libmpfr.Xmpfr_set_si_2exp(tls, rop, int64(v1), 0, rnd&libc.Int32FromInt32(0x0F))
					/* inverse the rounding mode for -sqrt(3)/2 for zeta_3^2 and zeta_6^5 */
					rnd_im = rnd >> libc.Int32FromInt32(4)
					if k != uint64(1) {
						if rnd_im == int32(_MPFR_RNDU) {
							v2 = int32(_MPFR_RNDD)
						} else {
							if rnd_im == int32(_MPFR_RNDD) {
								v3 = int32(_MPFR_RNDU)
							} else {
								v3 = rnd_im
							}
							v2 = v3
						}
						rnd_im = v2
					}
					inex_im = libmpfr.Xmpfr_sqrt_ui(tls, rop+32, uint64(3), rnd_im)
					Xmpc_div_2ui(tls, rop, rop, uint64(1), int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
					if k != uint64(1) {
						libmpfr.Xmpfr_neg(tls, rop+32, rop+32, int32(_MPFR_RNDN))
						inex_im = -inex_im
					}
					if inex_re < 0 {
						v4 = int32(2)
					} else {
						if inex_re == 0 {
							v5 = 0
						} else {
							v5 = int32(1)
						}
						v4 = v5
					}
					if inex_im < 0 {
						v6 = int32(2)
					} else {
						if inex_im == 0 {
							v7 = 0
						} else {
							v7 = int32(1)
						}
						v6 = v7
					}
					return v4 | v6<<int32(2)
				} else {
					if n == uint64(12) {
						/* necessarily k=1, 5, 7, 11:
						   k=1: 1/2*sqrt(3) + 1/2*I
						   k=5: -1/2*sqrt(3) + 1/2*I
						   k=7: -1/2*sqrt(3) - 1/2*I
						   k=11: 1/2*sqrt(3) - 1/2*I */
						/* inverse the rounding mode for -sqrt(3)/2 for zeta_12^5 and zeta_12^7 */
						rnd_re = rnd & libc.Int32FromInt32(0x0F)
						if k == uint64(5) || k == uint64(7) {
							if rnd_re == int32(_MPFR_RNDU) {
								v8 = int32(_MPFR_RNDD)
							} else {
								if rnd_re == int32(_MPFR_RNDD) {
									v9 = int32(_MPFR_RNDU)
								} else {
									v9 = rnd_re
								}
								v8 = v9
							}
							rnd_re = v8
						}
						inex_re = libmpfr.Xmpfr_sqrt_ui(tls, rop, uint64(3), rnd_re)
						if k < uint64(6) {
							v10 = int32(1)
						} else {
							v10 = -int32(1)
						}
						inex_im = libmpfr.Xmpfr_set_si_2exp(tls, rop+32, int64(v10), 0, rnd>>libc.Int32FromInt32(4))
						Xmpc_div_2ui(tls, rop, rop, uint64(1), int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
						if k == uint64(5) || k == uint64(7) {
							libmpfr.Xmpfr_neg(tls, rop, rop, int32(_MPFR_RNDN))
							inex_re = -inex_re
						}
						if inex_re < 0 {
							v11 = int32(2)
						} else {
							if inex_re == 0 {
								v12 = 0
							} else {
								v12 = int32(1)
							}
							v11 = v12
						}
						if inex_im < 0 {
							v13 = int32(2)
						} else {
							if inex_im == 0 {
								v14 = 0
							} else {
								v14 = int32(1)
							}
							v13 = v14
						}
						return v11 | v13<<int32(2)
					} else {
						if n == uint64(8) {
							/* k=1, 3, 5 or 7:
							   k=1: (1/2*I + 1/2)*sqrt(2)
							   k=3: (1/2*I - 1/2)*sqrt(2)
							   k=5: -(1/2*I + 1/2)*sqrt(2)
							   k=7: -(1/2*I - 1/2)*sqrt(2) */
							rnd_re = rnd & libc.Int32FromInt32(0x0F)
							if k == uint64(3) || k == uint64(5) {
								if rnd_re == int32(_MPFR_RNDU) {
									v15 = int32(_MPFR_RNDD)
								} else {
									if rnd_re == int32(_MPFR_RNDD) {
										v16 = int32(_MPFR_RNDU)
									} else {
										v16 = rnd_re
									}
									v15 = v16
								}
								rnd_re = v15
							}
							rnd_im = rnd >> libc.Int32FromInt32(4)
							if k > uint64(4) {
								if rnd_im == int32(_MPFR_RNDU) {
									v17 = int32(_MPFR_RNDD)
								} else {
									if rnd_im == int32(_MPFR_RNDD) {
										v18 = int32(_MPFR_RNDU)
									} else {
										v18 = rnd_im
									}
									v17 = v18
								}
								rnd_im = v17
							}
							inex_re = libmpfr.Xmpfr_sqrt_ui(tls, rop, uint64(2), rnd_re)
							inex_im = libmpfr.Xmpfr_sqrt_ui(tls, rop+32, uint64(2), rnd_im)
							Xmpc_div_2ui(tls, rop, rop, uint64(1), int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
							if k == uint64(3) || k == uint64(5) {
								libmpfr.Xmpfr_neg(tls, rop, rop, int32(_MPFR_RNDN))
								inex_re = -inex_re
							}
							if k > uint64(4) {
								libmpfr.Xmpfr_neg(tls, rop+32, rop+32, int32(_MPFR_RNDN))
								inex_im = -inex_im
							}
							if inex_re < 0 {
								v19 = int32(2)
							} else {
								if inex_re == 0 {
									v20 = 0
								} else {
									v20 = int32(1)
								}
								v19 = v20
							}
							if inex_im < 0 {
								v21 = int32(2)
							} else {
								if inex_im == 0 {
									v22 = 0
								} else {
									v22 = int32(1)
								}
								v21 = v22
							}
							return v19 | v21<<int32(2)
						}
					}
				}
			}
		}
	}
	if (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(rop+32)).F_mpfr_prec {
		v23 = (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec
	} else {
		v23 = (*t__mpfr_struct)(unsafe.Pointer(rop + 32)).F_mpfr_prec
	}
	prec = v23
	/* For the error analysis justifying the following algorithm,
	   see algorithms.tex. */
	libmpfr.Xmpfr_init2(tls, bp+32, int64(67))
	libmpfr.Xmpfr_init2(tls, bp+64, int64(67))
	libmpfr.Xmpfr_init2(tls, bp+96, int64(67))
	libgmp.X__gmpq_init(tls, bp)
	libgmp.X__gmpq_set_ui(tls, bp, k, n)
	libgmp.X__gmpq_mul_2exp(tls, bp, bp, uint64(1)) /* kn=2*k/n < 2 */
	for cond := true; cond; cond = !(libmpfr.Xmpfr_can_round(tls, bp+96, prec-(int64(4)-(*t__mpfr_struct)(unsafe.Pointer(bp+96)).F_mpfr_exp), int32(_MPFR_RNDN), int32(_MPFR_RNDZ), (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec+libc.BoolInt64(rnd&libc.Int32FromInt32(0x0F) == int32(_MPFR_RNDN))) != 0) || !(libmpfr.Xmpfr_can_round(tls, bp+64, prec-(int64(4)-(*t__mpfr_struct)(unsafe.Pointer(bp+64)).F_mpfr_exp), int32(_MPFR_RNDN), int32(_MPFR_RNDZ), (*t__mpfr_struct)(unsafe.Pointer(rop+32)).F_mpfr_prec+libc.BoolInt64(rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDN))) != 0) {
		prec += Xmpc_ceil_log2(tls, prec) + int64(5) /* prec >= 6 */
		libmpfr.Xmpfr_set_prec(tls, bp+32, prec)
		libmpfr.Xmpfr_set_prec(tls, bp+64, prec)
		libmpfr.Xmpfr_set_prec(tls, bp+96, prec)
		libmpfr.Xmpfr_const_pi(tls, bp+32, int32(_MPFR_RNDN))
		libmpfr.Xmpfr_mul_q(tls, bp+32, bp+32, bp, int32(_MPFR_RNDN))
		libmpfr.Xmpfr_sin_cos(tls, bp+64, bp+96, bp+32, int32(_MPFR_RNDN))
	}
	{
		_p = bp + 96
		v24 = libmpfr.Xmpfr_set4(tls, rop, _p, rnd&libc.Int32FromInt32(0x0F), (*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign)
	}
	inex_re = v24
	{
		_p1 = bp + 64
		v25 = libmpfr.Xmpfr_set4(tls, rop+32, _p1, rnd>>libc.Int32FromInt32(4), (*t__mpfr_struct)(unsafe.Pointer(_p1)).F_mpfr_sign)
	}
	inex_im = v25
	libmpfr.Xmpfr_clear(tls, bp+32)
	libmpfr.Xmpfr_clear(tls, bp+64)
	libmpfr.Xmpfr_clear(tls, bp+96)
	libgmp.X__gmpq_clear(tls, bp)
	if inex_re < 0 {
		v26 = int32(2)
	} else {
		if inex_re == 0 {
			v27 = 0
		} else {
			v27 = int32(1)
		}
		v26 = v27
	}
	if inex_im < 0 {
		v28 = int32(2)
	} else {
		if inex_im == 0 {
			v29 = 0
		} else {
			v29 = int32(1)
		}
		v28 = v29
	}
	return v26 | v28<<int32(2)
}

func Xmpc_urandom(tls *libc.TLS, a Tmpc_ptr, state uintptr) (r1 int32) {
	var i, r int32
	_, _ = i, r
	r = libmpfr.Xmpfr_urandomb(tls, a, state)
	i = libmpfr.Xmpfr_urandomb(tls, a+32, state)
	return libc.BoolInt32(r != 0 && i != 0)
}

func Xmpc_set(tls *libc.TLS, a Tmpc_ptr, b Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	var _p, _p1 Tmpfr_srcptr
	var inex_im, inex_re, v1, v2, v3, v4, v5, v6 int32
	_, _, _, _, _, _, _, _, _, _ = _p, _p1, inex_im, inex_re, v1, v2, v3, v4, v5, v6
	{
		_p = b
		v1 = libmpfr.Xmpfr_set4(tls, a, _p, rnd&libc.Int32FromInt32(0x0F), (*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign)
	}
	inex_re = v1
	{
		_p1 = b + 32
		v2 = libmpfr.Xmpfr_set4(tls, a+32, _p1, rnd>>libc.Int32FromInt32(4), (*t__mpfr_struct)(unsafe.Pointer(_p1)).F_mpfr_sign)
	}
	inex_im = v2
	if inex_re < 0 {
		v3 = int32(2)
	} else {
		if inex_re == 0 {
			v4 = 0
		} else {
			v4 = int32(1)
		}
		v3 = v4
	}
	if inex_im < 0 {
		v5 = int32(2)
	} else {
		if inex_im == 0 {
			v6 = 0
		} else {
			v6 = int32(1)
		}
		v5 = v6
	}
	return v3 | v5<<int32(2)
}

func Xmpc_set_prec(tls *libc.TLS, x Tmpc_ptr, prec Tmpfr_prec_t) {
	libmpfr.Xmpfr_set_prec(tls, x, prec)
	libmpfr.Xmpfr_set_prec(tls, x+32, prec)
}

func Xmpc_set_str(tls *libc.TLS, z Tmpc_ptr, str uintptr, base int32, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var inex, v1, v2 int32
	var _ /* p at bp+0 */ uintptr
	_, _, _ = inex, v1, v2
	inex = Xmpc_strtoc(tls, z, str, bp, base, rnd)
	if inex != -int32(1) {
		for {
			v1 = libc.Int32FromUint8(libc.Uint8FromInt8(*(*int8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp))))))
			v2 = libc.BoolInt32(v1 == int32(' ') || libc.Uint32FromInt32(v1)-uint32('\t') < uint32(5))
			goto _3
		_3:
			if !(v2 != 0) {
				break
			}
			*(*uintptr)(unsafe.Pointer(bp))++
		}
		if int32(*(*int8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp))))) == int32('\000') {
			return inex
		}
	}
	libmpfr.Xmpfr_set_nan(tls, z)
	libmpfr.Xmpfr_set_nan(tls, z+32)
	return -int32(1)
}

func Xmpc_set_fr(tls *libc.TLS, a Tmpc_ptr, b Tmpfr_srcptr, rnd Tmpc_rnd_t) (r int32) {
	var _inex_im, _inex_re, v1, v2, v3, v4, v5 int32
	var _p Tmpfr_ptr
	_, _, _, _, _, _, _, _ = _inex_im, _inex_re, _p, v1, v2, v3, v4, v5
	_inex_re = libmpfr.Xmpfr_set(tls, a, b, rnd&libc.Int32FromInt32(0x0F))
	{
		_p = a + 32
		(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign = int32(1)
		(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
		_ = rnd >> libc.Int32FromInt32(4)
		v1 = 0
	}
	_inex_im = v1
	if _inex_re < 0 {
		v2 = int32(2)
	} else {
		if _inex_re == 0 {
			v3 = 0
		} else {
			v3 = int32(1)
		}
		v2 = v3
	}
	if _inex_im < 0 {
		v4 = int32(2)
	} else {
		if _inex_im == 0 {
			v5 = 0
		} else {
			v5 = int32(1)
		}
		v4 = v5
	}
	return v2 | v4<<int32(2)
}

func Xmpc_set_d(tls *libc.TLS, a Tmpc_ptr, b float64, rnd Tmpc_rnd_t) (r int32) {
	var _inex_im, _inex_re, v1, v2, v3, v4, v5 int32
	var _p Tmpfr_ptr
	_, _, _, _, _, _, _, _ = _inex_im, _inex_re, _p, v1, v2, v3, v4, v5
	_inex_re = libmpfr.Xmpfr_set_d(tls, a, b, rnd&libc.Int32FromInt32(0x0F))
	{
		_p = a + 32
		(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign = int32(1)
		(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
		_ = rnd >> libc.Int32FromInt32(4)
		v1 = 0
	}
	_inex_im = v1
	if _inex_re < 0 {
		v2 = int32(2)
	} else {
		if _inex_re == 0 {
			v3 = 0
		} else {
			v3 = int32(1)
		}
		v2 = v3
	}
	if _inex_im < 0 {
		v4 = int32(2)
	} else {
		if _inex_im == 0 {
			v5 = 0
		} else {
			v5 = int32(1)
		}
		v4 = v5
	}
	return v2 | v4<<int32(2)
}

func Xmpc_set_ld(tls *libc.TLS, a Tmpc_ptr, b float64, rnd Tmpc_rnd_t) (r int32) {
	var _inex_im, _inex_re, v1, v2, v3, v4, v5 int32
	var _p Tmpfr_ptr
	_, _, _, _, _, _, _, _ = _inex_im, _inex_re, _p, v1, v2, v3, v4, v5
	_inex_re = libmpfr.Xmpfr_set_ld(tls, a, b, rnd&libc.Int32FromInt32(0x0F))
	{
		_p = a + 32
		(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign = int32(1)
		(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
		_ = rnd >> libc.Int32FromInt32(4)
		v1 = 0
	}
	_inex_im = v1
	if _inex_re < 0 {
		v2 = int32(2)
	} else {
		if _inex_re == 0 {
			v3 = 0
		} else {
			v3 = int32(1)
		}
		v2 = v3
	}
	if _inex_im < 0 {
		v4 = int32(2)
	} else {
		if _inex_im == 0 {
			v5 = 0
		} else {
			v5 = int32(1)
		}
		v4 = v5
	}
	return v2 | v4<<int32(2)
}

func Xmpc_set_ui(tls *libc.TLS, a Tmpc_ptr, b uint64, rnd Tmpc_rnd_t) (r int32) {
	var _inex_im, _inex_re, v1, v2, v3, v4, v5 int32
	var _p Tmpfr_ptr
	_, _, _, _, _, _, _, _ = _inex_im, _inex_re, _p, v1, v2, v3, v4, v5
	_inex_re = libmpfr.Xmpfr_set_ui(tls, a, b, rnd&libc.Int32FromInt32(0x0F))
	{
		_p = a + 32
		(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign = int32(1)
		(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
		_ = rnd >> libc.Int32FromInt32(4)
		v1 = 0
	}
	_inex_im = v1
	if _inex_re < 0 {
		v2 = int32(2)
	} else {
		if _inex_re == 0 {
			v3 = 0
		} else {
			v3 = int32(1)
		}
		v2 = v3
	}
	if _inex_im < 0 {
		v4 = int32(2)
	} else {
		if _inex_im == 0 {
			v5 = 0
		} else {
			v5 = int32(1)
		}
		v4 = v5
	}
	return v2 | v4<<int32(2)
}

func Xmpc_set_si(tls *libc.TLS, a Tmpc_ptr, b int64, rnd Tmpc_rnd_t) (r int32) {
	var _inex_im, _inex_re, v1, v2, v3, v4, v5 int32
	var _p Tmpfr_ptr
	_, _, _, _, _, _, _, _ = _inex_im, _inex_re, _p, v1, v2, v3, v4, v5
	_inex_re = libmpfr.Xmpfr_set_si(tls, a, b, rnd&libc.Int32FromInt32(0x0F))
	{
		_p = a + 32
		(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign = int32(1)
		(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
		_ = rnd >> libc.Int32FromInt32(4)
		v1 = 0
	}
	_inex_im = v1
	if _inex_re < 0 {
		v2 = int32(2)
	} else {
		if _inex_re == 0 {
			v3 = 0
		} else {
			v3 = int32(1)
		}
		v2 = v3
	}
	if _inex_im < 0 {
		v4 = int32(2)
	} else {
		if _inex_im == 0 {
			v5 = 0
		} else {
			v5 = int32(1)
		}
		v4 = v5
	}
	return v2 | v4<<int32(2)
}

func Xmpc_set_z(tls *libc.TLS, a Tmpc_ptr, b Tmpz_srcptr, rnd Tmpc_rnd_t) (r int32) {
	var _inex_im, _inex_re, v1, v2, v3, v4, v5 int32
	var _p Tmpfr_ptr
	_, _, _, _, _, _, _, _ = _inex_im, _inex_re, _p, v1, v2, v3, v4, v5
	_inex_re = libmpfr.Xmpfr_set_z(tls, a, b, rnd&libc.Int32FromInt32(0x0F))
	{
		_p = a + 32
		(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign = int32(1)
		(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
		_ = rnd >> libc.Int32FromInt32(4)
		v1 = 0
	}
	_inex_im = v1
	if _inex_re < 0 {
		v2 = int32(2)
	} else {
		if _inex_re == 0 {
			v3 = 0
		} else {
			v3 = int32(1)
		}
		v2 = v3
	}
	if _inex_im < 0 {
		v4 = int32(2)
	} else {
		if _inex_im == 0 {
			v5 = 0
		} else {
			v5 = int32(1)
		}
		v4 = v5
	}
	return v2 | v4<<int32(2)
}

func Xmpc_set_q(tls *libc.TLS, a Tmpc_ptr, b Tmpq_srcptr, rnd Tmpc_rnd_t) (r int32) {
	var _inex_im, _inex_re, v1, v2, v3, v4, v5 int32
	var _p Tmpfr_ptr
	_, _, _, _, _, _, _, _ = _inex_im, _inex_re, _p, v1, v2, v3, v4, v5
	_inex_re = libmpfr.Xmpfr_set_q(tls, a, b, rnd&libc.Int32FromInt32(0x0F))
	{
		_p = a + 32
		(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign = int32(1)
		(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
		_ = rnd >> libc.Int32FromInt32(4)
		v1 = 0
	}
	_inex_im = v1
	if _inex_re < 0 {
		v2 = int32(2)
	} else {
		if _inex_re == 0 {
			v3 = 0
		} else {
			v3 = int32(1)
		}
		v2 = v3
	}
	if _inex_im < 0 {
		v4 = int32(2)
	} else {
		if _inex_im == 0 {
			v5 = 0
		} else {
			v5 = int32(1)
		}
		v4 = v5
	}
	return v2 | v4<<int32(2)
}

func Xmpc_set_f(tls *libc.TLS, a Tmpc_ptr, b Tmpf_srcptr, rnd Tmpc_rnd_t) (r int32) {
	var _inex_im, _inex_re, v1, v2, v3, v4, v5 int32
	var _p Tmpfr_ptr
	_, _, _, _, _, _, _, _ = _inex_im, _inex_re, _p, v1, v2, v3, v4, v5
	_inex_re = libmpfr.Xmpfr_set_f(tls, a, b, rnd&libc.Int32FromInt32(0x0F))
	{
		_p = a + 32
		(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign = int32(1)
		(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
		_ = rnd >> libc.Int32FromInt32(4)
		v1 = 0
	}
	_inex_im = v1
	if _inex_re < 0 {
		v2 = int32(2)
	} else {
		if _inex_re == 0 {
			v3 = 0
		} else {
			v3 = int32(1)
		}
		v2 = v3
	}
	if _inex_im < 0 {
		v4 = int32(2)
	} else {
		if _inex_im == 0 {
			v5 = 0
		} else {
			v5 = int32(1)
		}
		v4 = v5
	}
	return v2 | v4<<int32(2)
}

func Xmpc_set_uj(tls *libc.TLS, a Tmpc_ptr, b Tuintmax_t, rnd Tmpc_rnd_t) (r int32) {
	var _inex_im, _inex_re, v1, v2, v3, v4, v5 int32
	var _p Tmpfr_ptr
	_, _, _, _, _, _, _, _ = _inex_im, _inex_re, _p, v1, v2, v3, v4, v5
	_inex_re = libmpfr.X__gmpfr_set_uj(tls, a, b, rnd&libc.Int32FromInt32(0x0F))
	{
		_p = a + 32
		(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign = int32(1)
		(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
		_ = rnd >> libc.Int32FromInt32(4)
		v1 = 0
	}
	_inex_im = v1
	if _inex_re < 0 {
		v2 = int32(2)
	} else {
		if _inex_re == 0 {
			v3 = 0
		} else {
			v3 = int32(1)
		}
		v2 = v3
	}
	if _inex_im < 0 {
		v4 = int32(2)
	} else {
		if _inex_im == 0 {
			v5 = 0
		} else {
			v5 = int32(1)
		}
		v4 = v5
	}
	return v2 | v4<<int32(2)
}

func Xmpc_set_sj(tls *libc.TLS, a Tmpc_ptr, b Tintmax_t, rnd Tmpc_rnd_t) (r int32) {
	var _inex_im, _inex_re, v1, v2, v3, v4, v5 int32
	var _p Tmpfr_ptr
	_, _, _, _, _, _, _, _ = _inex_im, _inex_re, _p, v1, v2, v3, v4, v5
	_inex_re = libmpfr.X__gmpfr_set_sj(tls, a, b, rnd&libc.Int32FromInt32(0x0F))
	{
		_p = a + 32
		(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign = int32(1)
		(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
		_ = rnd >> libc.Int32FromInt32(4)
		v1 = 0
	}
	_inex_im = v1
	if _inex_re < 0 {
		v2 = int32(2)
	} else {
		if _inex_re == 0 {
			v3 = 0
		} else {
			v3 = int32(1)
		}
		v2 = v3
	}
	if _inex_im < 0 {
		v4 = int32(2)
	} else {
		if _inex_im == 0 {
			v5 = 0
		} else {
			v5 = int32(1)
		}
		v4 = v5
	}
	return v2 | v4<<int32(2)
}

func Xmpc_set_dc(tls *libc.TLS, a Tmpc_ptr, b complex128, rnd Tmpc_rnd_t) (r int32) {
	return Xmpc_set_d_d(tls, a, libc.Float64FromComplex128(b), +(*(*[2]float64)(unsafe.Pointer(&b)))[int32(1)], rnd)
}

func Xmpc_set_ldc(tls *libc.TLS, a Tmpc_ptr, b complex128, rnd Tmpc_rnd_t) (r int32) {
	return Xmpc_set_ld_ld(tls, a, libc.Float64FromComplex128(b), +(*(*[2]float64)(unsafe.Pointer(&b)))[int32(1)], rnd)
}

func Xmpc_set_nan(tls *libc.TLS, a Tmpc_ptr) {
	libmpfr.Xmpfr_set_nan(tls, a)
	libmpfr.Xmpfr_set_nan(tls, a+32)
}

func Xmpc_set_d_d(tls *libc.TLS, z Tmpc_ptr, a float64, b float64, rnd Tmpc_rnd_t) (r int32) {
	var _inex_im, _inex_re, v1, v2, v3, v4 int32
	_, _, _, _, _, _ = _inex_im, _inex_re, v1, v2, v3, v4
	_inex_re = libmpfr.Xmpfr_set_d(tls, z, a, rnd&libc.Int32FromInt32(0x0F))
	_inex_im = libmpfr.Xmpfr_set_d(tls, z+32, b, rnd>>libc.Int32FromInt32(4))
	if _inex_re < 0 {
		v1 = int32(2)
	} else {
		if _inex_re == 0 {
			v2 = 0
		} else {
			v2 = int32(1)
		}
		v1 = v2
	}
	if _inex_im < 0 {
		v3 = int32(2)
	} else {
		if _inex_im == 0 {
			v4 = 0
		} else {
			v4 = int32(1)
		}
		v3 = v4
	}
	return v1 | v3<<int32(2)
}

func Xmpc_set_f_f(tls *libc.TLS, z Tmpc_ptr, a Tmpf_srcptr, b Tmpf_srcptr, rnd Tmpc_rnd_t) (r int32) {
	var _inex_im, _inex_re, v1, v2, v3, v4 int32
	_, _, _, _, _, _ = _inex_im, _inex_re, v1, v2, v3, v4
	_inex_re = libmpfr.Xmpfr_set_f(tls, z, a, rnd&libc.Int32FromInt32(0x0F))
	_inex_im = libmpfr.Xmpfr_set_f(tls, z+32, b, rnd>>libc.Int32FromInt32(4))
	if _inex_re < 0 {
		v1 = int32(2)
	} else {
		if _inex_re == 0 {
			v2 = 0
		} else {
			v2 = int32(1)
		}
		v1 = v2
	}
	if _inex_im < 0 {
		v3 = int32(2)
	} else {
		if _inex_im == 0 {
			v4 = 0
		} else {
			v4 = int32(1)
		}
		v3 = v4
	}
	return v1 | v3<<int32(2)
}

func Xmpc_set_fr_fr(tls *libc.TLS, z Tmpc_ptr, a Tmpfr_srcptr, b Tmpfr_srcptr, rnd Tmpc_rnd_t) (r int32) {
	var _inex_im, _inex_re, v1, v2, v3, v4 int32
	_, _, _, _, _, _ = _inex_im, _inex_re, v1, v2, v3, v4
	_inex_re = libmpfr.Xmpfr_set(tls, z, a, rnd&libc.Int32FromInt32(0x0F))
	_inex_im = libmpfr.Xmpfr_set(tls, z+32, b, rnd>>libc.Int32FromInt32(4))
	if _inex_re < 0 {
		v1 = int32(2)
	} else {
		if _inex_re == 0 {
			v2 = 0
		} else {
			v2 = int32(1)
		}
		v1 = v2
	}
	if _inex_im < 0 {
		v3 = int32(2)
	} else {
		if _inex_im == 0 {
			v4 = 0
		} else {
			v4 = int32(1)
		}
		v3 = v4
	}
	return v1 | v3<<int32(2)
}

func Xmpc_set_ld_ld(tls *libc.TLS, z Tmpc_ptr, a float64, b float64, rnd Tmpc_rnd_t) (r int32) {
	var _inex_im, _inex_re, v1, v2, v3, v4 int32
	_, _, _, _, _, _ = _inex_im, _inex_re, v1, v2, v3, v4
	_inex_re = libmpfr.Xmpfr_set_ld(tls, z, a, rnd&libc.Int32FromInt32(0x0F))
	_inex_im = libmpfr.Xmpfr_set_ld(tls, z+32, b, rnd>>libc.Int32FromInt32(4))
	if _inex_re < 0 {
		v1 = int32(2)
	} else {
		if _inex_re == 0 {
			v2 = 0
		} else {
			v2 = int32(1)
		}
		v1 = v2
	}
	if _inex_im < 0 {
		v3 = int32(2)
	} else {
		if _inex_im == 0 {
			v4 = 0
		} else {
			v4 = int32(1)
		}
		v3 = v4
	}
	return v1 | v3<<int32(2)
}

func Xmpc_set_q_q(tls *libc.TLS, z Tmpc_ptr, a Tmpq_srcptr, b Tmpq_srcptr, rnd Tmpc_rnd_t) (r int32) {
	var _inex_im, _inex_re, v1, v2, v3, v4 int32
	_, _, _, _, _, _ = _inex_im, _inex_re, v1, v2, v3, v4
	_inex_re = libmpfr.Xmpfr_set_q(tls, z, a, rnd&libc.Int32FromInt32(0x0F))
	_inex_im = libmpfr.Xmpfr_set_q(tls, z+32, b, rnd>>libc.Int32FromInt32(4))
	if _inex_re < 0 {
		v1 = int32(2)
	} else {
		if _inex_re == 0 {
			v2 = 0
		} else {
			v2 = int32(1)
		}
		v1 = v2
	}
	if _inex_im < 0 {
		v3 = int32(2)
	} else {
		if _inex_im == 0 {
			v4 = 0
		} else {
			v4 = int32(1)
		}
		v3 = v4
	}
	return v1 | v3<<int32(2)
}

func Xmpc_set_si_si(tls *libc.TLS, z Tmpc_ptr, a int64, b int64, rnd Tmpc_rnd_t) (r int32) {
	var _inex_im, _inex_re, v1, v2, v3, v4 int32
	_, _, _, _, _, _ = _inex_im, _inex_re, v1, v2, v3, v4
	_inex_re = libmpfr.Xmpfr_set_si(tls, z, a, rnd&libc.Int32FromInt32(0x0F))
	_inex_im = libmpfr.Xmpfr_set_si(tls, z+32, b, rnd>>libc.Int32FromInt32(4))
	if _inex_re < 0 {
		v1 = int32(2)
	} else {
		if _inex_re == 0 {
			v2 = 0
		} else {
			v2 = int32(1)
		}
		v1 = v2
	}
	if _inex_im < 0 {
		v3 = int32(2)
	} else {
		if _inex_im == 0 {
			v4 = 0
		} else {
			v4 = int32(1)
		}
		v3 = v4
	}
	return v1 | v3<<int32(2)
}

func Xmpc_set_ui_ui(tls *libc.TLS, z Tmpc_ptr, a uint64, b uint64, rnd Tmpc_rnd_t) (r int32) {
	var _inex_im, _inex_re, v1, v2, v3, v4 int32
	_, _, _, _, _, _ = _inex_im, _inex_re, v1, v2, v3, v4
	_inex_re = libmpfr.Xmpfr_set_ui(tls, z, a, rnd&libc.Int32FromInt32(0x0F))
	_inex_im = libmpfr.Xmpfr_set_ui(tls, z+32, b, rnd>>libc.Int32FromInt32(4))
	if _inex_re < 0 {
		v1 = int32(2)
	} else {
		if _inex_re == 0 {
			v2 = 0
		} else {
			v2 = int32(1)
		}
		v1 = v2
	}
	if _inex_im < 0 {
		v3 = int32(2)
	} else {
		if _inex_im == 0 {
			v4 = 0
		} else {
			v4 = int32(1)
		}
		v3 = v4
	}
	return v1 | v3<<int32(2)
}

func Xmpc_set_z_z(tls *libc.TLS, z Tmpc_ptr, a Tmpz_srcptr, b Tmpz_srcptr, rnd Tmpc_rnd_t) (r int32) {
	var _inex_im, _inex_re, v1, v2, v3, v4 int32
	_, _, _, _, _, _ = _inex_im, _inex_re, v1, v2, v3, v4
	_inex_re = libmpfr.Xmpfr_set_z(tls, z, a, rnd&libc.Int32FromInt32(0x0F))
	_inex_im = libmpfr.Xmpfr_set_z(tls, z+32, b, rnd>>libc.Int32FromInt32(4))
	if _inex_re < 0 {
		v1 = int32(2)
	} else {
		if _inex_re == 0 {
			v2 = 0
		} else {
			v2 = int32(1)
		}
		v1 = v2
	}
	if _inex_im < 0 {
		v3 = int32(2)
	} else {
		if _inex_im == 0 {
			v4 = 0
		} else {
			v4 = int32(1)
		}
		v3 = v4
	}
	return v1 | v3<<int32(2)
}

func Xmpc_set_uj_uj(tls *libc.TLS, z Tmpc_ptr, a Tuintmax_t, b Tuintmax_t, rnd Tmpc_rnd_t) (r int32) {
	var _inex_im, _inex_re, v1, v2, v3, v4 int32
	_, _, _, _, _, _ = _inex_im, _inex_re, v1, v2, v3, v4
	_inex_re = libmpfr.X__gmpfr_set_uj(tls, z, a, rnd&libc.Int32FromInt32(0x0F))
	_inex_im = libmpfr.X__gmpfr_set_uj(tls, z+32, b, rnd>>libc.Int32FromInt32(4))
	if _inex_re < 0 {
		v1 = int32(2)
	} else {
		if _inex_re == 0 {
			v2 = 0
		} else {
			v2 = int32(1)
		}
		v1 = v2
	}
	if _inex_im < 0 {
		v3 = int32(2)
	} else {
		if _inex_im == 0 {
			v4 = 0
		} else {
			v4 = int32(1)
		}
		v3 = v4
	}
	return v1 | v3<<int32(2)
}

func Xmpc_set_sj_sj(tls *libc.TLS, z Tmpc_ptr, a Tintmax_t, b Tintmax_t, rnd Tmpc_rnd_t) (r int32) {
	var _inex_im, _inex_re, v1, v2, v3, v4 int32
	_, _, _, _, _, _ = _inex_im, _inex_re, v1, v2, v3, v4
	_inex_re = libmpfr.X__gmpfr_set_sj(tls, z, a, rnd&libc.Int32FromInt32(0x0F))
	_inex_im = libmpfr.X__gmpfr_set_sj(tls, z+32, b, rnd>>libc.Int32FromInt32(4))
	if _inex_re < 0 {
		v1 = int32(2)
	} else {
		if _inex_re == 0 {
			v2 = 0
		} else {
			v2 = int32(1)
		}
		v1 = v2
	}
	if _inex_im < 0 {
		v3 = int32(2)
	} else {
		if _inex_im == 0 {
			v4 = 0
		} else {
			v4 = int32(1)
		}
		v3 = v4
	}
	return v1 | v3<<int32(2)
}

func Xmpc_sin(tls *libc.TLS, rop Tmpc_ptr, op Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	return Xmpc_sin_cos(tls, rop, libc.UintptrFromInt32(0), op, rnd, 0) & int32(15)
}

func _mpc_sin_cos_nonfinite(tls *libc.TLS, rop_sin Tmpc_ptr, rop_cos Tmpc_ptr, op Tmpc_srcptr, rnd_sin Tmpc_rnd_t, rnd_cos Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(192)
	defer tls.Free(192)
	/* assumes that op (that is, its real or imaginary part) is not finite */
	var _p, _p1, _p2 Tmpfr_srcptr
	var overlap, same_sign, same_sign1, v1, v10, v12, v2, v3, v4, v5, v6, v7, v9 int32
	var _ /* c at bp+160 */ Tmpfr_t
	var _ /* c at bp+96 */ Tmpfr_t
	var _ /* op_loc at bp+0 */ Tmpc_t
	var _ /* s at bp+128 */ Tmpfr_t
	var _ /* s at bp+64 */ Tmpfr_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = _p, _p1, _p2, overlap, same_sign, same_sign1, v1, v10, v12, v2, v3, v4, v5, v6, v7, v9
	overlap = libc.BoolInt32(rop_sin == op || rop_cos == op)
	if overlap != 0 {
		Xmpc_init3(tls, bp, (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_prec, (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_prec)
		Xmpc_set(tls, bp, op, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
	} else {
		(*(*Tmpc_t)(unsafe.Pointer(bp)))[0] = *(*t__mpc_struct)(unsafe.Pointer(op))
	}
	if rop_sin != libc.UintptrFromInt32(0) {
		if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			Xmpc_set(tls, rop_sin, bp, rnd_sin)
			if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				/* sin(x +i*NaN) = NaN +i*NaN, except for x=0 */
				/* sin(-0 +i*NaN) = -0 +i*NaN */
				/* sin(+0 +i*NaN) = +0 +i*NaN */
				if !((*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))) {
					libmpfr.Xmpfr_set_nan(tls, rop_sin)
				}
			} else { /* op = NaN + i*y */
				if !((*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))) && !((*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))) {
					/* sin(NaN -i*Inf) = NaN -i*Inf */
					/* sin(NaN -i*0) = NaN -i*0 */
					/* sin(NaN +i*0) = NaN +i*0 */
					/* sin(NaN +i*Inf) = NaN +i*Inf */
					/* sin(NaN +i*y) = NaN +i*NaN, when 0<|y|<Inf */
					libmpfr.Xmpfr_set_nan(tls, rop_sin+32)
				}
			}
		} else {
			if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				libmpfr.Xmpfr_set_nan(tls, rop_sin)
				if !((*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))) && !((*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))) {
					/* sin(+/-Inf +i*y) = NaN +i*NaN, when 0<|y|<Inf */
					libmpfr.Xmpfr_set_nan(tls, rop_sin+32)
				} else {
					/* sin(+/-Inf -i*Inf) = NaN -i*Inf */
					/* sin(+/-Inf +i*Inf) = NaN +i*Inf */
					/* sin(+/-Inf -i*0) = NaN -i*0 */
					/* sin(+/-Inf +i*0) = NaN +i*0 */
					{
						_p = bp + 32
						v1 = libmpfr.Xmpfr_set4(tls, rop_sin+32, _p, rnd_sin>>libc.Int32FromInt32(4), (*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign)
					}
					_ = v1
				}
			} else {
				if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
					/* sin(-0 -i*Inf) = -0 -i*Inf */
					/* sin(+0 -i*Inf) = +0 -i*Inf */
					/* sin(-0 +i*Inf) = -0 +i*Inf */
					/* sin(+0 +i*Inf) = +0 +i*Inf */
					Xmpc_set(tls, rop_sin, bp, rnd_sin)
				} else {
					libmpfr.Xmpfr_init2(tls, bp+64, int64(2))
					libmpfr.Xmpfr_init2(tls, bp+96, int64(2))
					libmpfr.Xmpfr_sin_cos(tls, bp+64, bp+96, bp, int32(_MPFR_RNDZ))
					libmpfr.Xmpfr_set_inf(tls, rop_sin, (*t__mpfr_struct)(unsafe.Pointer(bp+64)).F_mpfr_sign)
					libmpfr.Xmpfr_set_inf(tls, rop_sin+32, (*t__mpfr_struct)(unsafe.Pointer(bp+96)).F_mpfr_sign*(*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_sign)
					libmpfr.Xmpfr_clear(tls, bp+64)
					libmpfr.Xmpfr_clear(tls, bp+96)
				}
			}
		}
	}
	if rop_cos != libc.UintptrFromInt32(0) {
		if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			/* cos(NaN + i * NaN) = NaN + i * NaN */
			/* cos(NaN - i * Inf) = +Inf + i * NaN */
			/* cos(NaN + i * Inf) = +Inf + i * NaN */
			/* cos(NaN - i * 0) = NaN - i * 0 */
			/* cos(NaN + i * 0) = NaN + i * 0 */
			/* cos(NaN + i * y) = NaN + i * NaN, when y != 0 */
			if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				libmpfr.Xmpfr_set_inf(tls, rop_cos, +libc.Int32FromInt32(1))
			} else {
				libmpfr.Xmpfr_set_nan(tls, rop_cos)
			}
			if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				{
					_p1 = bp + 32
					v2 = libmpfr.Xmpfr_set4(tls, rop_cos+32, _p1, rnd_cos>>libc.Int32FromInt32(4), (*t__mpfr_struct)(unsafe.Pointer(_p1)).F_mpfr_sign)
				}
				_ = v2
			} else {
				libmpfr.Xmpfr_set_nan(tls, rop_cos+32)
			}
		} else {
			if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				/* cos(-Inf + i * NaN) = NaN + i * NaN */
				/* cos(+Inf + i * NaN) = NaN + i * NaN */
				/* cos(-0 + i * NaN) = NaN - i * 0 */
				/* cos(+0 + i * NaN) = NaN + i * 0 */
				/* cos(x + i * NaN) = NaN + i * NaN, when x != 0 */
				if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
					{
						_p2 = bp
						v3 = libmpfr.Xmpfr_set4(tls, rop_cos+32, _p2, rnd_cos>>libc.Int32FromInt32(4), (*t__mpfr_struct)(unsafe.Pointer(_p2)).F_mpfr_sign)
					}
					_ = v3
				} else {
					libmpfr.Xmpfr_set_nan(tls, rop_cos+32)
				}
				libmpfr.Xmpfr_set_nan(tls, rop_cos)
			} else {
				if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
					/* cos(-Inf -i*Inf) = cos(+Inf +i*Inf) = -Inf +i*NaN */
					/* cos(-Inf +i*Inf) = cos(+Inf -i*Inf) = +Inf +i*NaN */
					/* cos(-Inf -i*0) = cos(+Inf +i*0) = NaN -i*0 */
					/* cos(-Inf +i*0) = cos(+Inf -i*0) = NaN +i*0 */
					/* cos(-Inf +i*y) = cos(+Inf +i*y) = NaN +i*NaN, when y != 0 */
					same_sign = libc.BoolInt32(libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_sign < 0) == libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_sign < 0))
					if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
						if same_sign != 0 {
							v4 = -int32(1)
						} else {
							v4 = +libc.Int32FromInt32(1)
						}
						libmpfr.Xmpfr_set_inf(tls, rop_cos, v4)
					} else {
						libmpfr.Xmpfr_set_nan(tls, rop_cos)
					}
					if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
						if same_sign != 0 {
							v5 = -int32(1)
						} else {
							v5 = int32(1)
						}
						libmpfr.Xmpfr_set4(tls, rop_cos+32, bp+32, rnd_cos>>libc.Int32FromInt32(4), v5)
					} else {
						libmpfr.Xmpfr_set_nan(tls, rop_cos+32)
					}
				} else {
					if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
						/* cos(-0 -i*Inf) = cos(+0 +i*Inf) = +Inf -i*0 */
						/* cos(-0 +i*Inf) = cos(+0 -i*Inf) = +Inf +i*0 */
						same_sign1 = libc.BoolInt32(libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_sign < 0) == libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_sign < 0))
						if same_sign1 != 0 {
							v6 = -int32(1)
						} else {
							v6 = int32(1)
						}
						libmpfr.Xmpfr_set4(tls, rop_cos+32, bp, rnd_cos>>libc.Int32FromInt32(4), v6)
						libmpfr.Xmpfr_set_inf(tls, rop_cos, +libc.Int32FromInt32(1))
					} else {
						libmpfr.Xmpfr_init2(tls, bp+160, int64(2))
						libmpfr.Xmpfr_init2(tls, bp+128, int64(2))
						libmpfr.Xmpfr_sin_cos(tls, bp+128, bp+160, bp, int32(_MPFR_RNDN))
						if (*t__mpfr_struct)(unsafe.Pointer(bp+160)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
							if (*t__mpfr_struct)(unsafe.Pointer(bp+160)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
								libmpfr.Xmpfr_set_erangeflag(tls)
							}
							v7 = libc.Int32FromInt32(0)
						} else {
							v7 = (*t__mpfr_struct)(unsafe.Pointer(bp + 160)).F_mpfr_sign
						}
						libmpfr.Xmpfr_set_inf(tls, rop_cos, v7)
						if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
							if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
								libmpfr.Xmpfr_set_erangeflag(tls)
							}
							v10 = libc.Int32FromInt32(0)
						} else {
							v10 = (*t__mpfr_struct)(unsafe.Pointer(bp + 32)).F_mpfr_sign
						}
						if (*t__mpfr_struct)(unsafe.Pointer(bp+128)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
							if (*t__mpfr_struct)(unsafe.Pointer(bp+128)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
								libmpfr.Xmpfr_set_erangeflag(tls)
							}
							v12 = libc.Int32FromInt32(0)
						} else {
							v12 = (*t__mpfr_struct)(unsafe.Pointer(bp + 128)).F_mpfr_sign
						}
						if v10 == v12 {
							v9 = -int32(1)
						} else {
							v9 = +libc.Int32FromInt32(1)
						}
						libmpfr.Xmpfr_set_inf(tls, rop_cos+32, v9)
						libmpfr.Xmpfr_clear(tls, bp+128)
						libmpfr.Xmpfr_clear(tls, bp+160)
					}
				}
			}
		}
	}
	if overlap != 0 {
		Xmpc_clear(tls, bp)
	}
	return libc.Int32FromInt32(0) | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2) | (libc.Int32FromInt32(0)|libc.Int32FromInt32(0)<<libc.Int32FromInt32(2))<<libc.Int32FromInt32(4)
	/* everything is exact */
}

func _mpc_sin_cos_real(tls *libc.TLS, rop_sin Tmpc_ptr, rop_cos Tmpc_ptr, op Tmpc_srcptr, rnd_sin Tmpc_rnd_t, rnd_cos Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	/* assumes that op is real */
	var _p, _p1 Tmpfr_srcptr
	var inex_c, inex_cos_re, inex_s, inex_sin_re, sign_im, v1, v2, v3, v4, v5, v6, v7, v8 int32
	var _ /* c at bp+32 */ Tmpfr_t
	var _ /* s at bp+0 */ Tmpfr_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = _p, _p1, inex_c, inex_cos_re, inex_s, inex_sin_re, sign_im, v1, v2, v3, v4, v5, v6, v7, v8
	inex_sin_re = 0
	inex_cos_re = 0
	sign_im = libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_sign < 0)
	/* sin(x +-0*i) = sin(x) +-0*i*sign(cos(x)) */
	/* cos(x +-i*0) = cos(x) -+i*0*sign(sin(x)) */
	if rop_sin != uintptr(0) {
		libmpfr.Xmpfr_init2(tls, bp, (*t__mpfr_struct)(unsafe.Pointer(rop_sin)).F_mpfr_prec)
	} else {
		libmpfr.Xmpfr_init2(tls, bp, int64(2))
	} /* We need only the sign. */
	if rop_cos != libc.UintptrFromInt32(0) {
		libmpfr.Xmpfr_init2(tls, bp+32, (*t__mpfr_struct)(unsafe.Pointer(rop_cos)).F_mpfr_prec)
	} else {
		libmpfr.Xmpfr_init2(tls, bp+32, int64(2))
	}
	inex_s = libmpfr.Xmpfr_sin(tls, bp, op, rnd_sin&libc.Int32FromInt32(0x0F))
	inex_c = libmpfr.Xmpfr_cos(tls, bp+32, op, rnd_cos&libc.Int32FromInt32(0x0F))
	/* We cannot use mpfr_sin_cos since we may need two distinct rounding
	   modes and the exact return values. If we need only the sign, an
	   arbitrary rounding mode will work.                                 */
	if rop_sin != libc.UintptrFromInt32(0) {
		{
			_p = bp
			v1 = libmpfr.Xmpfr_set4(tls, rop_sin, _p, int32(_MPFR_RNDN), (*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign)
		}
		_ = v1 /* exact */
		inex_sin_re = inex_s
		if sign_im != 0 && !((*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_sign < libc.Int32FromInt32(0)) || !(sign_im != 0) && (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_sign < 0 {
			v2 = -int32(1)
		} else {
			v2 = int32(1)
		}
		libmpfr.Xmpfr_set_zero(tls, rop_sin+32, v2)
	}
	if rop_cos != libc.UintptrFromInt32(0) {
		{
			_p1 = bp + 32
			v3 = libmpfr.Xmpfr_set4(tls, rop_cos, _p1, int32(_MPFR_RNDN), (*t__mpfr_struct)(unsafe.Pointer(_p1)).F_mpfr_sign)
		}
		_ = v3 /* exact */
		inex_cos_re = inex_c
		if sign_im != 0 && (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_sign < 0 || !(sign_im != 0) && !((*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_sign < libc.Int32FromInt32(0)) {
			v4 = -int32(1)
		} else {
			v4 = int32(1)
		}
		libmpfr.Xmpfr_set_zero(tls, rop_cos+32, v4)
	}
	libmpfr.Xmpfr_clear(tls, bp)
	libmpfr.Xmpfr_clear(tls, bp+32)
	if inex_sin_re < 0 {
		v5 = int32(2)
	} else {
		if inex_sin_re == 0 {
			v6 = 0
		} else {
			v6 = int32(1)
		}
		v5 = v6
	}
	if inex_cos_re < 0 {
		v7 = int32(2)
	} else {
		if inex_cos_re == 0 {
			v8 = 0
		} else {
			v8 = int32(1)
		}
		v7 = v8
	}
	return v5 | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2) | (v7|libc.Int32FromInt32(0)<<libc.Int32FromInt32(2))<<int32(4)
}

func _mpc_sin_cos_imag(tls *libc.TLS, rop_sin Tmpc_ptr, rop_cos Tmpc_ptr, op Tmpc_srcptr, rnd_sin Tmpc_rnd_t, rnd_cos Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	/* assumes that op is purely imaginary, but not zero */
	var _p Tmpfr_srcptr
	var _p1 Tmpfr_ptr
	var inex_cos_re, inex_sin_im, overlap, v1, v2, v3, v4, v5, v6 int32
	var _ /* op_loc at bp+0 */ Tmpc_t
	_, _, _, _, _, _, _, _, _, _, _ = _p, _p1, inex_cos_re, inex_sin_im, overlap, v1, v2, v3, v4, v5, v6
	inex_sin_im = 0
	inex_cos_re = 0
	overlap = libc.BoolInt32(rop_sin == op || rop_cos == op)
	if overlap != 0 {
		Xmpc_init3(tls, bp, (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_prec, (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_prec)
		Xmpc_set(tls, bp, op, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
	} else {
		(*(*Tmpc_t)(unsafe.Pointer(bp)))[0] = *(*t__mpc_struct)(unsafe.Pointer(op))
	}
	if rop_sin != libc.UintptrFromInt32(0) {
		/* sin(+-O +i*y) = +-0 +i*sinh(y) */
		{
			_p = bp
			v1 = libmpfr.Xmpfr_set4(tls, rop_sin, _p, int32(_MPFR_RNDN), (*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign)
		}
		_ = v1
		inex_sin_im = libmpfr.Xmpfr_sinh(tls, rop_sin+32, bp+32, rnd_sin>>libc.Int32FromInt32(4))
	}
	if rop_cos != libc.UintptrFromInt32(0) {
		/* cos(-0 - i * y) = cos(+0 + i * y) = cosh(y) - i * 0,
		   cos(-0 + i * y) = cos(+0 - i * y) = cosh(y) + i * 0,
		   where y > 0 */
		inex_cos_re = libmpfr.Xmpfr_cosh(tls, rop_cos, bp+32, rnd_cos&libc.Int32FromInt32(0x0F))
		{
			_p1 = rop_cos + 32
			(*t__mpfr_struct)(unsafe.Pointer(_p1)).F_mpfr_sign = int32(1)
			(*t__mpfr_struct)(unsafe.Pointer(_p1)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
			_ = rnd_cos >> libc.Int32FromInt32(4)
			v2 = 0
		}
		_ = v2
		if libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_sign < 0) == libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_sign < 0) {
			libmpfr.Xmpfr_neg(tls, rop_cos+32, rop_cos+32, int32(_MPFR_RNDN))
		}
	}
	if overlap != 0 {
		Xmpc_clear(tls, bp)
	}
	if inex_sin_im < 0 {
		v3 = int32(2)
	} else {
		if inex_sin_im == 0 {
			v4 = 0
		} else {
			v4 = int32(1)
		}
		v3 = v4
	}
	if inex_cos_re < 0 {
		v5 = int32(2)
	} else {
		if inex_cos_re == 0 {
			v6 = 0
		} else {
			v6 = int32(1)
		}
		v5 = v6
	}
	return 0 | v3<<int32(2) | (v5|libc.Int32FromInt32(0)<<libc.Int32FromInt32(2))<<int32(4)
}

// C documentation
//
//	/* Fix an inexact overflow, when x is +Inf or -Inf:
//	   When rnd is towards zero, change x into the largest (in absolute value)
//	   floating-point number.
//	   Return the inexact flag. */
func Xmpc_fix_inf(tls *libc.TLS, x uintptr, rnd Tmpfr_rnd_t) (r int32) {
	var v1, v3, v5, v7, v9 int32
	var v2, v4 bool
	_, _, _, _, _, _, _ = v1, v2, v3, v4, v5, v7, v9
	if v2 = rnd == int32(_MPFR_RNDZ); !v2 {
		if (*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_sign < 0 {
			v1 = -int32(1)
		} else {
			v1 = int32(1)
		}
	}
	if v4 = v2 || v1 < 0 && rnd == int32(_MPFR_RNDU); !v4 {
		if (*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_sign < 0 {
			v3 = -int32(1)
		} else {
			v3 = int32(1)
		}
	}
	if !(v4 || v3 > 0 && rnd == int32(_MPFR_RNDD)) {
		if (*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			if (*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				libmpfr.Xmpfr_set_erangeflag(tls)
			}
			v5 = libc.Int32FromInt32(0)
		} else {
			v5 = (*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_sign
		}
		return v5
	} else {
		if (*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			if (*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				libmpfr.Xmpfr_set_erangeflag(tls)
			}
			v7 = libc.Int32FromInt32(0)
		} else {
			v7 = (*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_sign
		}
		if v7 < 0 {
			libmpfr.Xmpfr_nextabove(tls, x)
		} else {
			libmpfr.Xmpfr_nextbelow(tls, x)
		}
		if (*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			if (*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				libmpfr.Xmpfr_set_erangeflag(tls)
			}
			v9 = libc.Int32FromInt32(0)
		} else {
			v9 = (*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_sign
		}
		return -v9
	}
	return r
}

// C documentation
//
//	/* Fix an inexact underflow, when x is +0 or -0:
//	   When rnd is away from zero, change x into the closest floating-point number.
//	   Return the inexact flag. */
func Xmpc_fix_zero(tls *libc.TLS, x uintptr, rnd Tmpfr_rnd_t) (r int32) {
	var v1, v2, v4 int32
	var v3 bool
	_, _, _, _ = v1, v2, v3, v4
	if (*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_sign < 0 {
		v1 = -int32(1)
	} else {
		v1 = int32(1)
	}
	if v3 = v1 < 0 && rnd == int32(_MPFR_RNDD); !v3 {
		if (*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_sign < 0 {
			v2 = -int32(1)
		} else {
			v2 = int32(1)
		}
	}
	if !(v3 || v2 > 0 && rnd == int32(_MPFR_RNDU)) {
		if libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_sign < 0) == 0 {
			v4 = -int32(1)
		} else {
			v4 = int32(1)
		}
		return v4
	} else {
		if libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(x)).F_mpfr_sign < 0) == 0 {
			libmpfr.Xmpfr_nextabove(tls, x)
			return int32(1)
		} else {
			libmpfr.Xmpfr_nextbelow(tls, x)
			return -int32(1)
		}
	}
	return r
}

func Xmpc_sin_cos(tls *libc.TLS, rop_sin Tmpc_ptr, rop_cos Tmpc_ptr, op Tmpc_srcptr, rnd_sin Tmpc_rnd_t, rnd_cos Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(192)
	defer tls.Free(192)
	/* Feature not documented in the texinfo file: One of rop_sin or
	   rop_cos may be NULL, in which case it is not computed, and the
	   corresponding ternary inexact value is set to 0 (exact).       */
	var _p, _p1, _p2, _p3 Tmpfr_srcptr
	var ei, er, prec Tmpfr_prec_t
	var inex_cos, inex_im, inex_re, inex_sin, loop, ok, v10, v11, v12, v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, v37 int32
	var saved_emax, saved_emin Tmpfr_exp_t
	var v1, v2, v3, v4, v5, v6, v7, v8, v9 int64
	var _ /* c at bp+32 */ Tmpfr_t
	var _ /* ch at bp+96 */ Tmpfr_t
	var _ /* csh at bp+160 */ Tmpfr_t
	var _ /* s at bp+0 */ Tmpfr_t
	var _ /* sch at bp+128 */ Tmpfr_t
	var _ /* sh at bp+64 */ Tmpfr_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = _p, _p1, _p2, _p3, ei, er, inex_cos, inex_im, inex_re, inex_sin, loop, ok, prec, saved_emax, saved_emin, v1, v10, v11, v12, v13, v14, v15, v16, v17, v18, v19, v2, v20, v21, v22, v23, v24, v25, v26, v27, v28, v29, v3, v30, v31, v32, v33, v34, v35, v36, v37, v4, v5, v6, v7, v8, v9
	if !(libmpfr.Xmpfr_number_p(tls, op) != 0 && libmpfr.Xmpfr_number_p(tls, op+32) != 0) {
		return _mpc_sin_cos_nonfinite(tls, rop_sin, rop_cos, op, rnd_sin, rnd_cos)
	} else {
		if (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			return _mpc_sin_cos_real(tls, rop_sin, rop_cos, op, rnd_sin, rnd_cos)
		} else {
			if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				return _mpc_sin_cos_imag(tls, rop_sin, rop_cos, op, rnd_sin, rnd_cos)
			} else {
				loop = 0
				saved_emin = libmpfr.Xmpfr_get_emin(tls)
				saved_emax = libmpfr.Xmpfr_get_emax(tls)
				libmpfr.Xmpfr_set_emin(tls, libmpfr.Xmpfr_get_emin_min(tls))
				libmpfr.Xmpfr_set_emax(tls, libmpfr.Xmpfr_get_emax_max(tls))
				prec = int64(2)
				if rop_sin != libc.UintptrFromInt32(0) {
					if (*t__mpfr_struct)(unsafe.Pointer(rop_sin)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(rop_sin+32)).F_mpfr_prec {
						v2 = (*t__mpfr_struct)(unsafe.Pointer(rop_sin)).F_mpfr_prec
					} else {
						v2 = (*t__mpfr_struct)(unsafe.Pointer(rop_sin + 32)).F_mpfr_prec
					}
					if prec > v2 {
						v1 = prec
					} else {
						if (*t__mpfr_struct)(unsafe.Pointer(rop_sin)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(rop_sin+32)).F_mpfr_prec {
							v3 = (*t__mpfr_struct)(unsafe.Pointer(rop_sin)).F_mpfr_prec
						} else {
							v3 = (*t__mpfr_struct)(unsafe.Pointer(rop_sin + 32)).F_mpfr_prec
						}
						v1 = v3
					}
					prec = v1
					/* since the Taylor expansion of sin(x) at x=0 is x - x^3/6 + O(x^5),
					   if x <= 2^(-p), then the second term/x is about 2^(-2p)/6, thus we
					   need at least 2p+3 bits of precision. This is true only when x is
					   exactly representable in the target precision. */
					if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_prec {
						v4 = (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_prec
					} else {
						v4 = (*t__mpfr_struct)(unsafe.Pointer(op + 32)).F_mpfr_prec
					}
					if v4 <= prec {
						er = (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp
						ei = (*t__mpfr_struct)(unsafe.Pointer(op + 32)).F_mpfr_exp
						/* consider the maximal exponent only */
						if er < ei {
							v5 = ei
						} else {
							v5 = er
						}
						er = v5
						if er < 0 {
							if prec < int64(2)*-er+int64(3) {
								prec = int64(2)*-er + int64(3)
							}
						}
					}
				}
				if rop_cos != libc.UintptrFromInt32(0) {
					if (*t__mpfr_struct)(unsafe.Pointer(rop_cos)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(rop_cos+32)).F_mpfr_prec {
						v7 = (*t__mpfr_struct)(unsafe.Pointer(rop_cos)).F_mpfr_prec
					} else {
						v7 = (*t__mpfr_struct)(unsafe.Pointer(rop_cos + 32)).F_mpfr_prec
					}
					if prec > v7 {
						v6 = prec
					} else {
						if (*t__mpfr_struct)(unsafe.Pointer(rop_cos)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(rop_cos+32)).F_mpfr_prec {
							v8 = (*t__mpfr_struct)(unsafe.Pointer(rop_cos)).F_mpfr_prec
						} else {
							v8 = (*t__mpfr_struct)(unsafe.Pointer(rop_cos + 32)).F_mpfr_prec
						}
						v6 = v8
					}
					prec = v6
				}
				libmpfr.Xmpfr_init2(tls, bp, int64(2))
				libmpfr.Xmpfr_init2(tls, bp+32, int64(2))
				libmpfr.Xmpfr_init2(tls, bp+64, int64(2))
				libmpfr.Xmpfr_init2(tls, bp+96, int64(2))
				libmpfr.Xmpfr_init2(tls, bp+128, int64(2))
				libmpfr.Xmpfr_init2(tls, bp+160, int64(2))
				for cond := true; cond; cond = ok == 0 {
					loop++
					if loop <= int32(2) {
						v9 = Xmpc_ceil_log2(tls, prec) + int64(5)
					} else {
						v9 = prec / int64(2)
					}
					prec += v9
					libmpfr.Xmpfr_set_prec(tls, bp, prec)
					libmpfr.Xmpfr_set_prec(tls, bp+32, prec)
					libmpfr.Xmpfr_set_prec(tls, bp+64, prec)
					libmpfr.Xmpfr_set_prec(tls, bp+96, prec)
					libmpfr.Xmpfr_set_prec(tls, bp+128, prec)
					libmpfr.Xmpfr_set_prec(tls, bp+160, prec)
					libmpfr.Xmpfr_sin_cos(tls, bp, bp+32, op, int32(_MPFR_RNDN))
					libmpfr.Xmpfr_sinh_cosh(tls, bp+64, bp+96, op+32, int32(_MPFR_RNDN))
					ok = int32(1)
					if rop_sin != libc.UintptrFromInt32(0) {
						/* real part of sine */
						libmpfr.Xmpfr_mul(tls, bp+128, bp, bp+96, int32(_MPFR_RNDN))
						ok = libc.BoolInt32(!(libmpfr.Xmpfr_number_p(tls, bp+128) != 0) || libmpfr.Xmpfr_can_round(tls, bp+128, prec-int64(2), int32(_MPFR_RNDN), int32(_MPFR_RNDZ), (*t__mpfr_struct)(unsafe.Pointer(rop_sin)).F_mpfr_prec+libc.BoolInt64(rnd_sin&libc.Int32FromInt32(0x0F) == int32(_MPFR_RNDN))) != 0)
						if ok != 0 {
							/* imaginary part of sine */
							libmpfr.Xmpfr_mul(tls, bp+160, bp+32, bp+64, int32(_MPFR_RNDN))
							ok = libc.BoolInt32(!(libmpfr.Xmpfr_number_p(tls, bp+160) != 0) || libmpfr.Xmpfr_can_round(tls, bp+160, prec-int64(2), int32(_MPFR_RNDN), int32(_MPFR_RNDZ), (*t__mpfr_struct)(unsafe.Pointer(rop_sin+32)).F_mpfr_prec+libc.BoolInt64(rnd_sin>>libc.Int32FromInt32(4) == int32(_MPFR_RNDN))) != 0)
						}
					}
					if rop_cos != libc.UintptrFromInt32(0) && ok != 0 {
						/* real part of cosine */
						libmpfr.Xmpfr_mul(tls, bp+32, bp+32, bp+96, int32(_MPFR_RNDN))
						ok = libc.BoolInt32(!(libmpfr.Xmpfr_number_p(tls, bp+32) != 0) || libmpfr.Xmpfr_can_round(tls, bp+32, prec-int64(2), int32(_MPFR_RNDN), int32(_MPFR_RNDZ), (*t__mpfr_struct)(unsafe.Pointer(rop_cos)).F_mpfr_prec+libc.BoolInt64(rnd_cos&libc.Int32FromInt32(0x0F) == int32(_MPFR_RNDN))) != 0)
						if ok != 0 {
							/* imaginary part of cosine */
							libmpfr.Xmpfr_mul(tls, bp, bp, bp+64, int32(_MPFR_RNDN))
							libmpfr.Xmpfr_neg(tls, bp, bp, int32(_MPFR_RNDN))
							ok = libc.BoolInt32(!(libmpfr.Xmpfr_number_p(tls, bp) != 0) || libmpfr.Xmpfr_can_round(tls, bp, prec-int64(2), int32(_MPFR_RNDN), int32(_MPFR_RNDZ), (*t__mpfr_struct)(unsafe.Pointer(rop_cos+32)).F_mpfr_prec+libc.BoolInt64(rnd_cos>>libc.Int32FromInt32(4) == int32(_MPFR_RNDN))) != 0)
						}
					}
				}
				if rop_sin != libc.UintptrFromInt32(0) {
					{
						_p = bp + 128
						v10 = libmpfr.Xmpfr_set4(tls, rop_sin, _p, rnd_sin&libc.Int32FromInt32(0x0F), (*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign)
					}
					inex_re = v10
					if (*t__mpfr_struct)(unsafe.Pointer(bp+128)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
						inex_re = Xmpc_fix_inf(tls, rop_sin, rnd_sin&libc.Int32FromInt32(0x0F))
					}
					{
						_p1 = bp + 160
						v11 = libmpfr.Xmpfr_set4(tls, rop_sin+32, _p1, rnd_sin>>libc.Int32FromInt32(4), (*t__mpfr_struct)(unsafe.Pointer(_p1)).F_mpfr_sign)
					}
					inex_im = v11
					if (*t__mpfr_struct)(unsafe.Pointer(bp+160)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
						inex_im = Xmpc_fix_inf(tls, rop_sin+32, rnd_sin>>libc.Int32FromInt32(4))
					}
					if inex_re < 0 {
						v12 = int32(2)
					} else {
						if inex_re == 0 {
							v13 = 0
						} else {
							v13 = int32(1)
						}
						v12 = v13
					}
					if inex_im < 0 {
						v14 = int32(2)
					} else {
						if inex_im == 0 {
							v15 = 0
						} else {
							v15 = int32(1)
						}
						v14 = v15
					}
					inex_sin = v12 | v14<<int32(2)
				} else {
					inex_sin = libc.Int32FromInt32(0) | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2)
				} /* return exact if not computed */
				if rop_cos != libc.UintptrFromInt32(0) {
					{
						_p2 = bp + 32
						v16 = libmpfr.Xmpfr_set4(tls, rop_cos, _p2, rnd_cos&libc.Int32FromInt32(0x0F), (*t__mpfr_struct)(unsafe.Pointer(_p2)).F_mpfr_sign)
					}
					inex_re = v16
					if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
						inex_re = Xmpc_fix_inf(tls, rop_cos, rnd_cos&libc.Int32FromInt32(0x0F))
					}
					{
						_p3 = bp
						v17 = libmpfr.Xmpfr_set4(tls, rop_cos+32, _p3, rnd_cos>>libc.Int32FromInt32(4), (*t__mpfr_struct)(unsafe.Pointer(_p3)).F_mpfr_sign)
					}
					inex_im = v17
					if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
						inex_im = Xmpc_fix_inf(tls, rop_cos+32, rnd_cos>>libc.Int32FromInt32(4))
					}
					if inex_re < 0 {
						v18 = int32(2)
					} else {
						if inex_re == 0 {
							v19 = 0
						} else {
							v19 = int32(1)
						}
						v18 = v19
					}
					if inex_im < 0 {
						v20 = int32(2)
					} else {
						if inex_im == 0 {
							v21 = 0
						} else {
							v21 = int32(1)
						}
						v20 = v21
					}
					inex_cos = v18 | v20<<int32(2)
				} else {
					inex_cos = libc.Int32FromInt32(0) | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2)
				} /* return exact if not computed */
				libmpfr.Xmpfr_clear(tls, bp)
				libmpfr.Xmpfr_clear(tls, bp+32)
				libmpfr.Xmpfr_clear(tls, bp+64)
				libmpfr.Xmpfr_clear(tls, bp+96)
				libmpfr.Xmpfr_clear(tls, bp+128)
				libmpfr.Xmpfr_clear(tls, bp+160)
				/* restore the exponent range, and check the range of results */
				libmpfr.Xmpfr_set_emin(tls, saved_emin)
				libmpfr.Xmpfr_set_emax(tls, saved_emax)
				if rop_sin != libc.UintptrFromInt32(0) {
					if inex_sin&int32(3) == int32(2) {
						v22 = -int32(1)
					} else {
						if inex_sin&int32(3) == 0 {
							v23 = 0
						} else {
							v23 = int32(1)
						}
						v22 = v23
					}
					inex_re = libmpfr.Xmpfr_check_range(tls, rop_sin, v22, rnd_sin&libc.Int32FromInt32(0x0F))
					if inex_sin>>int32(2) == int32(2) {
						v24 = -int32(1)
					} else {
						if inex_sin>>int32(2) == 0 {
							v25 = 0
						} else {
							v25 = int32(1)
						}
						v24 = v25
					}
					inex_im = libmpfr.Xmpfr_check_range(tls, rop_sin+32, v24, rnd_sin>>libc.Int32FromInt32(4))
					if inex_re < 0 {
						v26 = int32(2)
					} else {
						if inex_re == 0 {
							v27 = 0
						} else {
							v27 = int32(1)
						}
						v26 = v27
					}
					if inex_im < 0 {
						v28 = int32(2)
					} else {
						if inex_im == 0 {
							v29 = 0
						} else {
							v29 = int32(1)
						}
						v28 = v29
					}
					inex_sin = v26 | v28<<int32(2)
				}
				if rop_cos != libc.UintptrFromInt32(0) {
					if inex_cos&int32(3) == int32(2) {
						v30 = -int32(1)
					} else {
						if inex_cos&int32(3) == 0 {
							v31 = 0
						} else {
							v31 = int32(1)
						}
						v30 = v31
					}
					inex_re = libmpfr.Xmpfr_check_range(tls, rop_cos, v30, rnd_cos&libc.Int32FromInt32(0x0F))
					if inex_cos>>int32(2) == int32(2) {
						v32 = -int32(1)
					} else {
						if inex_cos>>int32(2) == 0 {
							v33 = 0
						} else {
							v33 = int32(1)
						}
						v32 = v33
					}
					inex_im = libmpfr.Xmpfr_check_range(tls, rop_cos+32, v32, rnd_cos>>libc.Int32FromInt32(4))
					if inex_re < 0 {
						v34 = int32(2)
					} else {
						if inex_re == 0 {
							v35 = 0
						} else {
							v35 = int32(1)
						}
						v34 = v35
					}
					if inex_im < 0 {
						v36 = int32(2)
					} else {
						if inex_im == 0 {
							v37 = 0
						} else {
							v37 = int32(1)
						}
						v36 = v37
					}
					inex_cos = v34 | v36<<int32(2)
				}
				return inex_sin | inex_cos<<int32(4)
			}
		}
	}
	return r
}

func Xmpc_sinh(tls *libc.TLS, rop Tmpc_ptr, op Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(128)
	defer tls.Free(128)
	var inex, v1, v10, v11, v12, v2, v3, v4, v5, v6, v7, v8, v9 int32
	var _ /* sin_z at bp+64 */ Tmpc_t
	var _ /* z at bp+0 */ Tmpc_t
	_, _, _, _, _, _, _, _, _, _, _, _, _ = inex, v1, v10, v11, v12, v2, v3, v4, v5, v6, v7, v8, v9
	/* z := conj(-i * op) and rop = conj(-i * sin(z)), in other words, we have
	   to switch real and imaginary parts. Let us set them without copying
	   significands. */
	*(*t__mpfr_struct)(unsafe.Pointer(bp)) = *(*t__mpfr_struct)(unsafe.Pointer(op + 32))
	*(*t__mpfr_struct)(unsafe.Pointer(bp + 32)) = *(*t__mpfr_struct)(unsafe.Pointer(op))
	*(*t__mpfr_struct)(unsafe.Pointer(bp + 64)) = *(*t__mpfr_struct)(unsafe.Pointer(rop + 32))
	*(*t__mpfr_struct)(unsafe.Pointer(bp + 64 + 32)) = *(*t__mpfr_struct)(unsafe.Pointer(rop))
	inex = Xmpc_sin(tls, bp+64, bp, rnd>>libc.Int32FromInt32(4)+rnd&libc.Int32FromInt32(0x0F)<<libc.Int32FromInt32(4))
	/* sin_z and rop parts share the same significands, copy the rest now. */
	*(*t__mpfr_struct)(unsafe.Pointer(rop)) = *(*t__mpfr_struct)(unsafe.Pointer(bp + 64 + 32))
	*(*t__mpfr_struct)(unsafe.Pointer(rop + 32)) = *(*t__mpfr_struct)(unsafe.Pointer(bp + 64))
	/* swap inexact flags for real and imaginary parts */
	if inex>>int32(2) == int32(2) {
		v2 = -int32(1)
	} else {
		if inex>>int32(2) == 0 {
			v3 = 0
		} else {
			v3 = int32(1)
		}
		v2 = v3
	}
	if v2 < 0 {
		v1 = int32(2)
	} else {
		if inex>>int32(2) == int32(2) {
			v5 = -int32(1)
		} else {
			if inex>>int32(2) == 0 {
				v6 = 0
			} else {
				v6 = int32(1)
			}
			v5 = v6
		}
		if v5 == 0 {
			v4 = 0
		} else {
			v4 = int32(1)
		}
		v1 = v4
	}
	if inex&int32(3) == int32(2) {
		v8 = -int32(1)
	} else {
		if inex&int32(3) == 0 {
			v9 = 0
		} else {
			v9 = int32(1)
		}
		v8 = v9
	}
	if v8 < 0 {
		v7 = int32(2)
	} else {
		if inex&int32(3) == int32(2) {
			v11 = -int32(1)
		} else {
			if inex&int32(3) == 0 {
				v12 = 0
			} else {
				v12 = int32(1)
			}
			v11 = v12
		}
		if v11 == 0 {
			v10 = 0
		} else {
			v10 = int32(1)
		}
		v7 = v10
	}
	return v1 | v7<<int32(2)
}

func _mpfr_fsss(tls *libc.TLS, z Tmpfr_ptr, a Tmpfr_srcptr, c Tmpfr_srcptr, rnd Tmpfr_rnd_t) (r int32) {
	bp := tls.Alloc(96)
	defer tls.Free(96)
	var __gmp_l Tmp_limb_t
	var __gmp_n Tmp_size_t
	var __gmp_p Tmp_ptr
	var ea, ec, emax, emin Tmpfr_exp_t
	var inex, overflow, underflow, v1, v2, v3 int32
	var v11, v13, v15, v19, v21, v5, v7, v9 uint64
	var v12, v17, v18, v4, v8 Tmpz_srcptr
	var v16 Tmpz_ptr
	var _ /* eu at bp+64 */ Tmpz_t
	var _ /* ev at bp+80 */ Tmpz_t
	var _ /* u at bp+0 */ Tmpfr_t
	var _ /* v at bp+32 */ Tmpfr_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = __gmp_l, __gmp_n, __gmp_p, ea, ec, emax, emin, inex, overflow, underflow, v1, v11, v12, v13, v15, v16, v17, v18, v19, v2, v21, v3, v4, v5, v7, v8, v9
	/* u=a^2, v=c^2 exactly */
	libmpfr.Xmpfr_init2(tls, bp, int64(2)*(*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_prec)
	libmpfr.Xmpfr_init2(tls, bp+32, int64(2)*(*t__mpfr_struct)(unsafe.Pointer(c)).F_mpfr_prec)
	libmpfr.Xmpfr_sqr(tls, bp, a, int32(_MPFR_RNDN))
	libmpfr.Xmpfr_sqr(tls, bp+32, c, int32(_MPFR_RNDN))
	/* tentatively compute z as u-v; here we need z to be distinct
	   from a and c to not lose the latter */
	inex = libmpfr.Xmpfr_sub(tls, z, bp, bp+32, rnd)
	if (*t__mpfr_struct)(unsafe.Pointer(z)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		/* replace by "correctly rounded overflow" */
		if (*t__mpfr_struct)(unsafe.Pointer(z)).F_mpfr_sign < 0 {
			v1 = -int32(1)
		} else {
			v1 = int32(1)
		}
		_ = libmpfr.Xmpfr_set_si_2exp(tls, z, int64(v1), 0, int32(_MPFR_RNDN))
		inex = libmpfr.Xmpfr_mul_2ui(tls, z, z, libc.Uint64FromInt64(libmpfr.Xmpfr_get_emax(tls)), rnd)
	} else {
		if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) && !((*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))) {
			/* exactly u underflowed, determine inexact flag */
			if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_sign < 0 {
				v2 = int32(1)
			} else {
				v2 = -int32(1)
			}
			inex = v2
		} else {
			if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) && !((*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))) {
				/* exactly v underflowed, determine inexact flag */
				if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_sign < 0 {
					v3 = -int32(1)
				} else {
					v3 = int32(1)
				}
				inex = v3
			} else {
				if (*t__mpfr_struct)(unsafe.Pointer(z)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) && (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
					/* cheat to work around the const qualifiers */
					/* Normalise the input by shifting and keep track of the shifts in
					   the exponents of u and v */
					ea = (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_exp
					ec = (*t__mpfr_struct)(unsafe.Pointer(c)).F_mpfr_exp
					libmpfr.Xmpfr_set_exp(tls, a, libc.Int64FromInt32(0))
					libmpfr.Xmpfr_set_exp(tls, c, libc.Int64FromInt32(0))
					libgmp.X__gmpz_init(tls, bp+64)
					libgmp.X__gmpz_init(tls, bp+80)
					libgmp.X__gmpz_set_si(tls, bp+64, ea)
					libgmp.X__gmpz_mul_2exp(tls, bp+64, bp+64, uint64(1))
					libgmp.X__gmpz_set_si(tls, bp+80, ec)
					libgmp.X__gmpz_mul_2exp(tls, bp+80, bp+80, uint64(1))
					/* recompute u and v and move exponents to eu and ev */
					libmpfr.Xmpfr_sqr(tls, bp, a, int32(_MPFR_RNDN))
					/* exponent of u is non-positive */
					libgmp.X__gmpz_sub_ui(tls, bp+64, bp+64, libc.Uint64FromInt64(-(*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp))
					libmpfr.Xmpfr_set_exp(tls, bp, libc.Int64FromInt32(0))
					libmpfr.Xmpfr_sqr(tls, bp+32, c, int32(_MPFR_RNDN))
					libgmp.X__gmpz_sub_ui(tls, bp+80, bp+80, libc.Uint64FromInt64(-(*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp))
					libmpfr.Xmpfr_set_exp(tls, bp+32, libc.Int64FromInt32(0))
					if (*t__mpfr_struct)(unsafe.Pointer(z)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
						emax = libmpfr.Xmpfr_get_emax(tls)
						/* We have a = ma * 2^ea with 1/2 <= |ma| < 1 and ea <= emax.
						   So eu <= 2*emax, and eu > emax since we have
						   an overflow. The same holds for ev. Shift u and v by as much as
						   possible so that one of them has exponent emax and the
						   remaining exponents in eu and ev are the same. Then carry out
						   the addition. Shifting u and v prevents an underflow. */
						if libgmp.X__gmpz_cmp(tls, bp+64, bp+80) >= 0 {
							libmpfr.Xmpfr_set_exp(tls, bp, emax)
							libgmp.X__gmpz_sub_ui(tls, bp+64, bp+64, libc.Uint64FromInt64(emax))
							libgmp.X__gmpz_sub(tls, bp+80, bp+80, bp+64)
							v4 = bp + 80
							__gmp_p = (*t__mpz_struct)(unsafe.Pointer(v4)).F_mp_d
							__gmp_n = int64((*t__mpz_struct)(unsafe.Pointer(v4)).F_mp_size)
							__gmp_l = *(*Tmp_limb_t)(unsafe.Pointer(__gmp_p))
							if __gmp_n != 0 {
								v7 = __gmp_l
							} else {
								v7 = uint64(0)
							}
							v5 = v7
							goto _6
						_6:
							libmpfr.Xmpfr_set_exp(tls, bp+32, libc.Int64FromUint64(v5))
							/* remaining common exponent is now in eu */
						} else {
							libmpfr.Xmpfr_set_exp(tls, bp+32, emax)
							libgmp.X__gmpz_sub_ui(tls, bp+80, bp+80, libc.Uint64FromInt64(emax))
							libgmp.X__gmpz_sub(tls, bp+64, bp+64, bp+80)
							v8 = bp + 64
							__gmp_p = (*t__mpz_struct)(unsafe.Pointer(v8)).F_mp_d
							__gmp_n = int64((*t__mpz_struct)(unsafe.Pointer(v8)).F_mp_size)
							__gmp_l = *(*Tmp_limb_t)(unsafe.Pointer(__gmp_p))
							if __gmp_n != 0 {
								v11 = __gmp_l
							} else {
								v11 = uint64(0)
							}
							v9 = v11
							goto _10
						_10:
							libmpfr.Xmpfr_set_exp(tls, bp, libc.Int64FromUint64(v9))
							libgmp.X__gmpz_set(tls, bp+64, bp+80)
							/* remaining common exponent is now also in eu */
						}
						inex = libmpfr.Xmpfr_sub(tls, z, bp, bp+32, rnd)
						/* Result is finite since u and v have the same sign. */
						v12 = bp + 64
						__gmp_p = (*t__mpz_struct)(unsafe.Pointer(v12)).F_mp_d
						__gmp_n = int64((*t__mpz_struct)(unsafe.Pointer(v12)).F_mp_size)
						__gmp_l = *(*Tmp_limb_t)(unsafe.Pointer(__gmp_p))
						if __gmp_n != 0 {
							v15 = __gmp_l
						} else {
							v15 = uint64(0)
						}
						v13 = v15
						goto _14
					_14:
						overflow = libmpfr.Xmpfr_mul_2ui(tls, z, z, v13, rnd)
						if overflow != 0 {
							inex = overflow
						}
					} else {
						/* Subtraction of two zeroes. We have a = ma * 2^ea
						   with 1/2 <= |ma| < 1 and ea >= emin and similarly for b.
						   So 2*emin < 2*emin+1 <= eu < emin < 0, and analogously for v. */
						emin = libmpfr.Xmpfr_get_emin(tls)
						if libgmp.X__gmpz_cmp(tls, bp+64, bp+80) <= 0 {
							libmpfr.Xmpfr_set_exp(tls, bp, emin)
							libgmp.X__gmpz_add_ui(tls, bp+64, bp+64, libc.Uint64FromInt64(-emin))
							libgmp.X__gmpz_sub(tls, bp+80, bp+80, bp+64)
							libmpfr.Xmpfr_set_exp(tls, bp+32, libgmp.X__gmpz_get_si(tls, bp+80))
						} else {
							libmpfr.Xmpfr_set_exp(tls, bp+32, emin)
							libgmp.X__gmpz_add_ui(tls, bp+80, bp+80, libc.Uint64FromInt64(-emin))
							libgmp.X__gmpz_sub(tls, bp+64, bp+64, bp+80)
							libmpfr.Xmpfr_set_exp(tls, bp, libgmp.X__gmpz_get_si(tls, bp+64))
							libgmp.X__gmpz_set(tls, bp+64, bp+80)
						}
						inex = libmpfr.Xmpfr_sub(tls, z, bp, bp+32, rnd)
						v16 = bp + 64
						v17 = bp + 64
						if v16 != v17 {
							libgmp.X__gmpz_set(tls, v16, v17)
						}
						(*t__mpz_struct)(unsafe.Pointer(v16)).F_mp_size = -(*t__mpz_struct)(unsafe.Pointer(v16)).F_mp_size
						v18 = bp + 64
						__gmp_p = (*t__mpz_struct)(unsafe.Pointer(v18)).F_mp_d
						__gmp_n = int64((*t__mpz_struct)(unsafe.Pointer(v18)).F_mp_size)
						__gmp_l = *(*Tmp_limb_t)(unsafe.Pointer(__gmp_p))
						if __gmp_n != 0 {
							v21 = __gmp_l
						} else {
							v21 = uint64(0)
						}
						v19 = v21
						goto _20
					_20:
						underflow = libmpfr.Xmpfr_div_2ui(tls, z, z, v19, rnd)
						if underflow != 0 {
							inex = underflow
						}
					}
					libgmp.X__gmpz_clear(tls, bp+64)
					libgmp.X__gmpz_clear(tls, bp+80)
					libmpfr.Xmpfr_set_exp(tls, a, ea)
					libmpfr.Xmpfr_set_exp(tls, c, ec)
					/* works also when a == c */
				}
			}
		}
	}
	libmpfr.Xmpfr_clear(tls, bp)
	libmpfr.Xmpfr_clear(tls, bp+32)
	return inex
}

func Xmpc_sqr(tls *libc.TLS, rop Tmpc_ptr, op Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(96)
	defer tls.Free(96)
	var _p, _p1, _p3 Tmpfr_ptr
	var _p2, _p4 Tmpfr_srcptr
	var emin Tmpfr_exp_t
	var inex_im, inex_re, inexact, ok, same_sign, same_sign1, saved_underflow, v10, v11, v12, v13, v14, v17, v19, v2, v22, v23, v24, v25, v26, v27, v3, v4, v5, v6, v7, v8, v9 int32
	var prec Tmpfr_prec_t
	var v1, v15, v16 int64
	var v21 bool
	var _ /* u at bp+0 */ Tmpfr_t
	var _ /* v at bp+32 */ Tmpfr_t
	var _ /* x at bp+64 */ Tmpfr_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = _p, _p1, _p2, _p3, _p4, emin, inex_im, inex_re, inexact, ok, prec, same_sign, same_sign1, saved_underflow, v1, v10, v11, v12, v13, v14, v15, v16, v17, v19, v2, v21, v22, v23, v24, v25, v26, v27, v3, v4, v5, v6, v7, v8, v9
	/* special values: NaN and infinities */
	if !(libmpfr.Xmpfr_number_p(tls, op) != 0 && libmpfr.Xmpfr_number_p(tls, op+32) != 0) {
		if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			libmpfr.Xmpfr_set_nan(tls, rop)
			libmpfr.Xmpfr_set_nan(tls, rop+32)
		} else {
			if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				if (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
					libmpfr.Xmpfr_set_inf(tls, rop+32, (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_sign*(*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_sign)
					libmpfr.Xmpfr_set_nan(tls, rop)
				} else {
					if (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
						libmpfr.Xmpfr_set_nan(tls, rop+32)
					} else {
						libmpfr.Xmpfr_set_inf(tls, rop+32, (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_sign*(*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_sign)
					}
					libmpfr.Xmpfr_set_inf(tls, rop, +libc.Int32FromInt32(1))
				}
			} else { /* IM(op) is infinity, RE(op) is not */
				if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
					libmpfr.Xmpfr_set_nan(tls, rop+32)
				} else {
					libmpfr.Xmpfr_set_inf(tls, rop+32, (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_sign*(*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_sign)
				}
				libmpfr.Xmpfr_set_inf(tls, rop, -int32(1))
			}
		}
		return libc.Int32FromInt32(0) | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2) /* exact */
	}
	if (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(rop+32)).F_mpfr_prec {
		v1 = (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec
	} else {
		v1 = (*t__mpfr_struct)(unsafe.Pointer(rop + 32)).F_mpfr_prec
	}
	prec = v1
	/* Check for real resp. purely imaginary number */
	if (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		same_sign = libc.BoolInt32(libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_sign < 0) == libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_sign < 0))
		inex_re = libmpfr.Xmpfr_sqr(tls, rop, op, rnd&libc.Int32FromInt32(0x0F))
		{
			_p = rop + 32
			(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign = int32(1)
			(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
			_ = int32(_MPFR_RNDN)
			v2 = 0
		}
		inex_im = v2
		if !(same_sign != 0) {
			Xmpc_conj(tls, rop, rop, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
		}
		if inex_re < 0 {
			v3 = int32(2)
		} else {
			if inex_re == 0 {
				v4 = 0
			} else {
				v4 = int32(1)
			}
			v3 = v4
		}
		if inex_im < 0 {
			v5 = int32(2)
		} else {
			if inex_im == 0 {
				v6 = 0
			} else {
				v6 = int32(1)
			}
			v5 = v6
		}
		return v3 | v5<<int32(2)
	}
	if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		same_sign1 = libc.BoolInt32(libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_sign < 0) == libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_sign < 0))
		if rnd&libc.Int32FromInt32(0x0F) == int32(_MPFR_RNDU) {
			v7 = int32(_MPFR_RNDD)
		} else {
			if rnd&libc.Int32FromInt32(0x0F) == int32(_MPFR_RNDD) {
				v8 = int32(_MPFR_RNDU)
			} else {
				v8 = rnd & libc.Int32FromInt32(0x0F)
			}
			v7 = v8
		}
		inex_re = -libmpfr.Xmpfr_sqr(tls, rop, op+32, v7)
		libmpfr.Xmpfr_neg(tls, rop, rop, int32(_MPFR_RNDN))
		{
			_p1 = rop + 32
			(*t__mpfr_struct)(unsafe.Pointer(_p1)).F_mpfr_sign = int32(1)
			(*t__mpfr_struct)(unsafe.Pointer(_p1)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
			_ = int32(_MPFR_RNDN)
			v9 = 0
		}
		inex_im = v9
		if !(same_sign1 != 0) {
			Xmpc_conj(tls, rop, rop, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
		}
		if inex_re < 0 {
			v10 = int32(2)
		} else {
			if inex_re == 0 {
				v11 = 0
			} else {
				v11 = int32(1)
			}
			v10 = v11
		}
		if inex_im < 0 {
			v12 = int32(2)
		} else {
			if inex_im == 0 {
				v13 = 0
			} else {
				v13 = int32(1)
			}
			v12 = v13
		}
		return v10 | v12<<int32(2)
	}
	if rop == op {
		libmpfr.Xmpfr_init2(tls, bp+64, (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_prec)
		{
			_p2 = op
			v14 = libmpfr.Xmpfr_set4(tls, bp+64, _p2, int32(_MPFR_RNDN), (*t__mpfr_struct)(unsafe.Pointer(_p2)).F_mpfr_sign)
		}
		_ = v14
	} else {
		(*(*Tmpfr_t)(unsafe.Pointer(bp + 64)))[0] = *(*t__mpfr_struct)(unsafe.Pointer(op))
	}
	/* From here on, use x instead of op->re and safely overwrite rop->re. */
	/* Compute real part of result. */
	if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp-(*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp >= 0 {
		v15 = (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp - (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp
	} else {
		v15 = -((*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp - (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp)
	}
	if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_prec {
		v16 = (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_prec
	} else {
		v16 = (*t__mpfr_struct)(unsafe.Pointer(op + 32)).F_mpfr_prec
	}
	if v15 > v16/int64(2) {
		/* If the real and imaginary parts of the argument have very different
		   exponents, it is not reasonable to use Karatsuba squaring; compute
		   exactly with the standard formulae instead, even if this means an
		   additional multiplication. Using the approach copied from mul, over-
		   and underflows are also handled correctly. */
		inex_re = _mpfr_fsss(tls, rop, bp+64, op+32, rnd&libc.Int32FromInt32(0x0F))
	} else {
		/* Karatsuba squaring: we compute the real part as (x+y)*(x-y) and the
		   imaginary part as 2*x*y, with a total of 2M instead of 2S+1M for the
		   naive algorithm, which computes x^2-y^2 and 2*y*y */
		libmpfr.Xmpfr_init(tls, bp)
		libmpfr.Xmpfr_init(tls, bp+32)
		emin = libmpfr.Xmpfr_get_emin(tls)
		for cond := true; cond; cond = !(ok != 0) {
			prec += Xmpc_ceil_log2(tls, prec) + int64(5)
			libmpfr.Xmpfr_set_prec(tls, bp, prec)
			libmpfr.Xmpfr_set_prec(tls, bp+32, prec)
			/* Let op = x + iy. We need u = x+y and v = x-y, rounded away.      */
			/* The error is bounded above by 1 ulp.                             */
			/* We first let inexact be 1 if the real part is not computed       */
			/* exactly and determine the sign later.                            */
			inexact = libmpfr.Xmpfr_add(tls, bp, bp+64, op+32, int32(_MPFR_RNDA)) | libmpfr.Xmpfr_sub(tls, bp+32, bp+64, op+32, int32(_MPFR_RNDA))
			/* compute the real part as u*v, rounded away                    */
			/* determine also the sign of inex_re                            */
			if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
					libmpfr.Xmpfr_set_erangeflag(tls)
				}
				v17 = libc.Int32FromInt32(0)
			} else {
				v17 = (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_sign
			}
			if v21 = v17 == 0; !v21 {
				if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
					if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
						libmpfr.Xmpfr_set_erangeflag(tls)
					}
					v19 = libc.Int32FromInt32(0)
				} else {
					v19 = (*t__mpfr_struct)(unsafe.Pointer(bp + 32)).F_mpfr_sign
				}
			}
			if v21 || v19 == 0 {
				/* as we have rounded away, the result is exact */
				{
					_p3 = rop
					(*t__mpfr_struct)(unsafe.Pointer(_p3)).F_mpfr_sign = int32(1)
					(*t__mpfr_struct)(unsafe.Pointer(_p3)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
					_ = int32(_MPFR_RNDN)
					v22 = 0
				}
				_ = v22
				inex_re = 0
				ok = int32(1)
			} else {
				inexact |= libmpfr.Xmpfr_mul(tls, bp, bp, bp+32, int32(_MPFR_RNDA)) /* error 5 */
				if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == emin || (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
					/* under- or overflow */
					inex_re = _mpfr_fsss(tls, rop, bp+64, op+32, rnd&libc.Int32FromInt32(0x0F))
					ok = int32(1)
				} else {
					ok = libc.BoolInt32(!(inexact != 0)) | libmpfr.Xmpfr_can_round(tls, bp, prec-int64(3), int32(_MPFR_RNDA), int32(_MPFR_RNDZ), (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec+libc.BoolInt64(rnd&libc.Int32FromInt32(0x0F) == int32(_MPFR_RNDN)))
					if ok != 0 {
						{
							_p4 = bp
							v23 = libmpfr.Xmpfr_set4(tls, rop, _p4, rnd&libc.Int32FromInt32(0x0F), (*t__mpfr_struct)(unsafe.Pointer(_p4)).F_mpfr_sign)
						}
						inex_re = v23
						if inex_re == 0 {
							/* remember that u was already rounded */
							inex_re = inexact
						}
					}
				}
			}
		}
		libmpfr.Xmpfr_clear(tls, bp)
		libmpfr.Xmpfr_clear(tls, bp+32)
	}
	saved_underflow = libmpfr.Xmpfr_underflow_p(tls)
	libmpfr.Xmpfr_clear_underflow(tls)
	inex_im = libmpfr.Xmpfr_mul(tls, rop+32, bp+64, op+32, rnd>>libc.Int32FromInt32(4))
	if !(libmpfr.Xmpfr_underflow_p(tls) != 0) {
		inex_im |= libmpfr.Xmpfr_mul_2ui(tls, rop+32, rop+32, uint64(1), rnd>>libc.Int32FromInt32(4))
	}
	/* We must not multiply by 2 if rop->im has been set to the smallest
	   representable number. */
	if saved_underflow != 0 {
		libmpfr.Xmpfr_set_underflow(tls)
	}
	if rop == op {
		libmpfr.Xmpfr_clear(tls, bp+64)
	}
	if inex_re < 0 {
		v24 = int32(2)
	} else {
		if inex_re == 0 {
			v25 = 0
		} else {
			v25 = int32(1)
		}
		v24 = v25
	}
	if inex_im < 0 {
		v26 = int32(2)
	} else {
		if inex_im == 0 {
			v27 = 0
		} else {
			v27 = int32(1)
		}
		v26 = v27
	}
	return v24 | v26<<int32(2)
}

func Xmpc_sqrt(tls *libc.TLS, a Tmpc_ptr, b Tmpc_srcptr, rnd Tmpc_rnd_t) (r1 int32) {
	bp := tls.Alloc(96)
	defer tls.Free(96)
	var _p, _p1, _p2, _p3 Tmpfr_ptr
	var _p4, _p5, _p6, _p7 Tmpfr_srcptr
	var im_cmp, im_sgn, inex_im, inex_re, inex_t, inex_w, loops, ok_t, ok_w, re_cmp, repr_t, repr_w, v1, v10, v11, v12, v13, v14, v15, v16, v17, v18, v2, v20, v21, v22, v23, v24, v25, v27, v28, v29, v3, v30, v31, v38, v39, v4, v40, v41, v5, v6, v7, v8, v9 int32
	var prec, prec_t, prec_w Tmpfr_prec_t
	var r, rnd_t, rnd_w Tmpfr_rnd_t
	var saved_emax, saved_emin Tmpfr_exp_t
	var v19, v26 int64
	var _ /* t at bp+32 */ Tmpfr_t
	var _ /* w at bp+0 */ Tmpfr_t
	var _ /* y at bp+64 */ Tmpfr_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = _p, _p1, _p2, _p3, _p4, _p5, _p6, _p7, im_cmp, im_sgn, inex_im, inex_re, inex_t, inex_w, loops, ok_t, ok_w, prec, prec_t, prec_w, r, re_cmp, repr_t, repr_w, rnd_t, rnd_w, saved_emax, saved_emin, v1, v10, v11, v12, v13, v14, v15, v16, v17, v18, v19, v2, v20, v21, v22, v23, v24, v25, v26, v27, v28, v29, v3, v30, v31, v38, v39, v4, v40, v41, v5, v6, v7, v8, v9
	ok_t = 0
	inex_t = int32(1)
	loops = 0
	re_cmp = libmpfr.Xmpfr_sgn(tls, b)
	im_cmp = libmpfr.Xmpfr_sgn(tls, b+32)
	repr_t = 0
	if libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(b+32)).F_mpfr_sign < 0) == 0 {
		v1 = 0
	} else {
		v1 = -int32(1)
	}
	/* flag indicating whether the computed value is already representable
	   at the target precision */
	im_sgn = v1
	if im_sgn != 0 {
		v2 = int32(_MPFR_RNDD)
	} else {
		v2 = int32(_MPFR_RNDU)
	}
	/* we need to know the sign of Im(b) when it is +/-0 */
	r = v2
	/* special values */
	if !(libmpfr.Xmpfr_number_p(tls, b) != 0 && libmpfr.Xmpfr_number_p(tls, b+32) != 0) {
		/* sqrt(x +i*Inf) = +Inf +I*Inf, even if x = NaN */
		/* sqrt(x -i*Inf) = +Inf -I*Inf, even if x = NaN */
		if (*t__mpfr_struct)(unsafe.Pointer(b+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			libmpfr.Xmpfr_set_inf(tls, a, +libc.Int32FromInt32(1))
			libmpfr.Xmpfr_set_inf(tls, a+32, im_sgn)
			return libc.Int32FromInt32(0) | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2)
		}
		if (*t__mpfr_struct)(unsafe.Pointer(b)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			if (*t__mpfr_struct)(unsafe.Pointer(b)).F_mpfr_sign < 0 {
				if libmpfr.Xmpfr_number_p(tls, b+32) != 0 {
					/* sqrt(-Inf +i*y) = +0 +i*Inf, when y positive */
					/* sqrt(-Inf +i*y) = +0 -i*Inf, when y positive */
					{
						_p = a
						(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign = int32(1)
						(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
						_ = int32(_MPFR_RNDN)
						v3 = 0
					}
					_ = v3
					libmpfr.Xmpfr_set_inf(tls, a+32, im_sgn)
					return libc.Int32FromInt32(0) | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2)
				} else {
					/* sqrt(-Inf +i*NaN) = NaN +/-i*Inf */
					libmpfr.Xmpfr_set_nan(tls, a)
					libmpfr.Xmpfr_set_inf(tls, a+32, im_sgn)
					return libc.Int32FromInt32(0) | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2)
				}
			} else {
				if libmpfr.Xmpfr_number_p(tls, b+32) != 0 {
					/* sqrt(+Inf +i*y) = +Inf +i*0, when y positive */
					/* sqrt(+Inf +i*y) = +Inf -i*0, when y positive */
					libmpfr.Xmpfr_set_inf(tls, a, +libc.Int32FromInt32(1))
					{
						_p1 = a + 32
						(*t__mpfr_struct)(unsafe.Pointer(_p1)).F_mpfr_sign = int32(1)
						(*t__mpfr_struct)(unsafe.Pointer(_p1)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
						_ = int32(_MPFR_RNDN)
						v4 = 0
					}
					_ = v4
					if im_sgn != 0 {
						Xmpc_conj(tls, a, a, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
					}
					return libc.Int32FromInt32(0) | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2)
				} else {
					/* sqrt(+Inf -i*Inf) = +Inf -i*Inf */
					/* sqrt(+Inf +i*Inf) = +Inf +i*Inf */
					/* sqrt(+Inf +i*NaN) = +Inf +i*NaN */
					return Xmpc_set(tls, a, b, rnd)
				}
			}
		}
		/* sqrt(x +i*NaN) = NaN +i*NaN, if x is not infinite */
		/* sqrt(NaN +i*y) = NaN +i*NaN, if y is not infinite */
		if (*t__mpfr_struct)(unsafe.Pointer(b)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(b+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			libmpfr.Xmpfr_set_nan(tls, a)
			libmpfr.Xmpfr_set_nan(tls, a+32)
			return libc.Int32FromInt32(0) | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2)
		}
	}
	/* purely real */
	if im_cmp == 0 {
		if re_cmp == 0 {
			Xmpc_set_ui_ui(tls, a, uint64(0), uint64(0), int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
			if im_sgn != 0 {
				Xmpc_conj(tls, a, a, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
			}
			return libc.Int32FromInt32(0) | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2)
		} else {
			if re_cmp > 0 {
				inex_w = libmpfr.Xmpfr_sqrt(tls, a, b, rnd&libc.Int32FromInt32(0x0F))
				{
					_p2 = a + 32
					(*t__mpfr_struct)(unsafe.Pointer(_p2)).F_mpfr_sign = int32(1)
					(*t__mpfr_struct)(unsafe.Pointer(_p2)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
					_ = int32(_MPFR_RNDN)
					v5 = 0
				}
				_ = v5
				if im_sgn != 0 {
					Xmpc_conj(tls, a, a, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
				}
				if inex_w < 0 {
					v6 = int32(2)
				} else {
					if inex_w == 0 {
						v7 = 0
					} else {
						v7 = int32(1)
					}
					v6 = v7
				}
				return v6 | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2)
			} else {
				libmpfr.Xmpfr_init2(tls, bp, (*t__mpfr_struct)(unsafe.Pointer(b)).F_mpfr_prec)
				libmpfr.Xmpfr_neg(tls, bp, b, int32(_MPFR_RNDN))
				if im_sgn != 0 {
					if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDU) {
						v8 = int32(_MPFR_RNDD)
					} else {
						if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDD) {
							v9 = int32(_MPFR_RNDU)
						} else {
							v9 = rnd >> libc.Int32FromInt32(4)
						}
						v8 = v9
					}
					inex_w = -libmpfr.Xmpfr_sqrt(tls, a+32, bp, v8)
					libmpfr.Xmpfr_neg(tls, a+32, a+32, int32(_MPFR_RNDN))
				} else {
					inex_w = libmpfr.Xmpfr_sqrt(tls, a+32, bp, rnd>>libc.Int32FromInt32(4))
				}
				{
					_p3 = a
					(*t__mpfr_struct)(unsafe.Pointer(_p3)).F_mpfr_sign = int32(1)
					(*t__mpfr_struct)(unsafe.Pointer(_p3)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
					_ = int32(_MPFR_RNDN)
					v10 = 0
				}
				_ = v10
				libmpfr.Xmpfr_clear(tls, bp)
				if inex_w < 0 {
					v11 = int32(2)
				} else {
					if inex_w == 0 {
						v12 = 0
					} else {
						v12 = int32(1)
					}
					v11 = v12
				}
				return 0 | v11<<int32(2)
			}
		}
	}
	/* purely imaginary */
	if re_cmp == 0 {
		(*(*Tmpfr_t)(unsafe.Pointer(bp + 64)))[0] = *(*t__mpfr_struct)(unsafe.Pointer(b + 32))
		/* If y/2 underflows, so does sqrt(y/2) */
		libmpfr.Xmpfr_div_2ui(tls, bp+64, bp+64, uint64(1), int32(_MPFR_RNDN))
		if im_cmp > 0 {
			inex_w = libmpfr.Xmpfr_sqrt(tls, a, bp+64, rnd&libc.Int32FromInt32(0x0F))
			inex_t = libmpfr.Xmpfr_sqrt(tls, a+32, bp+64, rnd>>libc.Int32FromInt32(4))
		} else {
			libmpfr.Xmpfr_neg(tls, bp+64, bp+64, int32(_MPFR_RNDN))
			inex_w = libmpfr.Xmpfr_sqrt(tls, a, bp+64, rnd&libc.Int32FromInt32(0x0F))
			if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDU) {
				v13 = int32(_MPFR_RNDD)
			} else {
				if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDD) {
					v14 = int32(_MPFR_RNDU)
				} else {
					v14 = rnd >> libc.Int32FromInt32(4)
				}
				v13 = v14
			}
			inex_t = -libmpfr.Xmpfr_sqrt(tls, a+32, bp+64, v13)
			libmpfr.Xmpfr_neg(tls, a+32, a+32, int32(_MPFR_RNDN))
		}
		if inex_w < 0 {
			v15 = int32(2)
		} else {
			if inex_w == 0 {
				v16 = 0
			} else {
				v16 = int32(1)
			}
			v15 = v16
		}
		if inex_t < 0 {
			v17 = int32(2)
		} else {
			if inex_t == 0 {
				v18 = 0
			} else {
				v18 = int32(1)
			}
			v17 = v18
		}
		return v15 | v17<<int32(2)
	}
	if (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(a+32)).F_mpfr_prec {
		v19 = (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_prec
	} else {
		v19 = (*t__mpfr_struct)(unsafe.Pointer(a + 32)).F_mpfr_prec
	}
	prec = v19
	libmpfr.Xmpfr_init(tls, bp)
	libmpfr.Xmpfr_init(tls, bp+32)
	if re_cmp > 0 {
		rnd_w = rnd & libc.Int32FromInt32(0x0F)
		prec_w = (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_prec
		rnd_t = rnd >> libc.Int32FromInt32(4)
		if rnd_t == int32(_MPFR_RNDZ) {
			/* force MPFR_RNDD or MPFR_RNDUP, using sign(t) = sign(y) */
			if im_cmp > 0 {
				v20 = int32(_MPFR_RNDD)
			} else {
				v20 = int32(_MPFR_RNDU)
			}
			rnd_t = v20
		} else {
			if rnd_t == int32(_MPFR_RNDA) {
				if im_cmp > 0 {
					v21 = int32(_MPFR_RNDU)
				} else {
					v21 = int32(_MPFR_RNDD)
				}
				rnd_t = v21
			}
		}
		prec_t = (*t__mpfr_struct)(unsafe.Pointer(a + 32)).F_mpfr_prec
	} else {
		prec_w = (*t__mpfr_struct)(unsafe.Pointer(a + 32)).F_mpfr_prec
		prec_t = (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_prec
		if im_cmp > 0 {
			rnd_w = rnd >> libc.Int32FromInt32(4)
			rnd_t = rnd & libc.Int32FromInt32(0x0F)
			if rnd_t == int32(_MPFR_RNDZ) {
				rnd_t = int32(_MPFR_RNDD)
			} else {
				if rnd_t == int32(_MPFR_RNDA) {
					rnd_t = int32(_MPFR_RNDU)
				}
			}
		} else {
			if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDU) {
				v22 = int32(_MPFR_RNDD)
			} else {
				if rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDD) {
					v23 = int32(_MPFR_RNDU)
				} else {
					v23 = rnd >> libc.Int32FromInt32(4)
				}
				v22 = v23
			}
			rnd_w = v22
			if rnd&libc.Int32FromInt32(0x0F) == int32(_MPFR_RNDU) {
				v24 = int32(_MPFR_RNDD)
			} else {
				if rnd&libc.Int32FromInt32(0x0F) == int32(_MPFR_RNDD) {
					v25 = int32(_MPFR_RNDU)
				} else {
					v25 = rnd & libc.Int32FromInt32(0x0F)
				}
				v24 = v25
			}
			rnd_t = v24
			if rnd_t == int32(_MPFR_RNDZ) {
				rnd_t = int32(_MPFR_RNDU)
			} else {
				if rnd_t == int32(_MPFR_RNDA) {
					rnd_t = int32(_MPFR_RNDD)
				}
			}
		}
	}
	saved_emin = libmpfr.Xmpfr_get_emin(tls)
	saved_emax = libmpfr.Xmpfr_get_emax(tls)
	libmpfr.Xmpfr_set_emin(tls, libmpfr.Xmpfr_get_emin_min(tls))
	libmpfr.Xmpfr_set_emax(tls, libmpfr.Xmpfr_get_emax_max(tls))
	for cond := true; cond; cond = inex_w != 0 && !(ok_w != 0) || inex_t != 0 && !(ok_t != 0) {
		loops++
		if loops <= int32(2) {
			v26 = Xmpc_ceil_log2(tls, prec) + int64(4)
		} else {
			v26 = prec / int64(2)
		}
		prec += v26
		libmpfr.Xmpfr_set_prec(tls, bp, prec)
		libmpfr.Xmpfr_set_prec(tls, bp+32, prec)
		/* let b = x + iy */
		/* w = sqrt ((|x| + sqrt (x^2 + y^2)) / 2), rounded down */
		/* final error on w bounded by 10 ulps, see algorithms.tex */
		inex_w = libmpfr.Xmpfr_sqr(tls, bp, b, int32(_MPFR_RNDD))
		inex_w |= libmpfr.Xmpfr_sqr(tls, bp+32, b+32, int32(_MPFR_RNDD))
		inex_w |= libmpfr.Xmpfr_add(tls, bp, bp, bp+32, int32(_MPFR_RNDD))
		inex_w |= libmpfr.Xmpfr_sqrt(tls, bp, bp, int32(_MPFR_RNDD))
		if re_cmp < 0 {
			inex_w |= libmpfr.Xmpfr_sub(tls, bp, bp, b, int32(_MPFR_RNDD))
		} else {
			inex_w |= libmpfr.Xmpfr_add(tls, bp, bp, b, int32(_MPFR_RNDD))
		}
		inex_w |= libmpfr.Xmpfr_div_2ui(tls, bp, bp, uint64(1), int32(_MPFR_RNDD))
		inex_w |= libmpfr.Xmpfr_sqrt(tls, bp, bp, int32(_MPFR_RNDD))
		repr_w = libc.BoolInt32(libmpfr.Xmpfr_min_prec(tls, bp) <= prec_w)
		if !(repr_w != 0) {
			/* use the usual trick for obtaining the ternary value */
			ok_w = libmpfr.Xmpfr_can_round(tls, bp, prec-int64(4), int32(_MPFR_RNDD), int32(_MPFR_RNDU), prec_w+libc.BoolInt64(rnd_w == int32(_MPFR_RNDN)))
		} else {
			/* w is representable in the target precision and thus cannot be
			   rounded up */
			if rnd_w == int32(_MPFR_RNDN) {
				/* If w can be rounded to nearest, then actually no rounding
				   occurs, and the ternary value is known from inex_w. */
				ok_w = libmpfr.Xmpfr_can_round(tls, bp, prec-int64(4), int32(_MPFR_RNDD), int32(_MPFR_RNDN), prec_w)
			} else {
				/* If w can be rounded down, then any direct rounding and the
				   ternary flag can be determined from inex_w. */
				ok_w = libmpfr.Xmpfr_can_round(tls, bp, prec-int64(4), int32(_MPFR_RNDD), int32(_MPFR_RNDD), prec_w)
			}
		}
		if !(inex_w != 0) || ok_w != 0 {
			/* t = y / 2w, rounded away */
			/* total error bounded by 16 ulps, see algorithms.tex */
			inex_t = libmpfr.Xmpfr_div(tls, bp+32, b+32, bp, r)
			if !(inex_t != 0) && inex_w != 0 {
				/* The division was exact, but w was not. */
				if im_sgn != 0 {
					v27 = -int32(1)
				} else {
					v27 = int32(1)
				}
				inex_t = v27
			}
			inex_t |= libmpfr.Xmpfr_div_2ui(tls, bp+32, bp+32, uint64(1), r)
			repr_t = libc.BoolInt32(libmpfr.Xmpfr_min_prec(tls, bp+32) <= prec_t)
			if !(repr_t != 0) {
				/* As for w; since t was rounded away, we check whether rounding to 0
				   is possible. */
				ok_t = libmpfr.Xmpfr_can_round(tls, bp+32, prec-int64(4), r, int32(_MPFR_RNDZ), prec_t+libc.BoolInt64(rnd_t == int32(_MPFR_RNDN)))
			} else {
				if rnd_t == int32(_MPFR_RNDN) {
					ok_t = libmpfr.Xmpfr_can_round(tls, bp+32, prec-int64(4), r, int32(_MPFR_RNDN), prec_t)
				} else {
					ok_t = libmpfr.Xmpfr_can_round(tls, bp+32, prec-int64(4), r, r, prec_t)
				}
			}
		}
	}
	if re_cmp > 0 {
		{
			_p4 = bp
			v28 = libmpfr.Xmpfr_set4(tls, a, _p4, rnd&libc.Int32FromInt32(0x0F), (*t__mpfr_struct)(unsafe.Pointer(_p4)).F_mpfr_sign)
		}
		inex_re = v28
		{
			_p5 = bp + 32
			v29 = libmpfr.Xmpfr_set4(tls, a+32, _p5, rnd>>libc.Int32FromInt32(4), (*t__mpfr_struct)(unsafe.Pointer(_p5)).F_mpfr_sign)
		}
		inex_im = v29
	} else {
		if im_cmp > 0 {
			{
				_p6 = bp + 32
				v30 = libmpfr.Xmpfr_set4(tls, a, _p6, rnd&libc.Int32FromInt32(0x0F), (*t__mpfr_struct)(unsafe.Pointer(_p6)).F_mpfr_sign)
			}
			inex_re = v30
			{
				_p7 = bp
				v31 = libmpfr.Xmpfr_set4(tls, a+32, _p7, rnd>>libc.Int32FromInt32(4), (*t__mpfr_struct)(unsafe.Pointer(_p7)).F_mpfr_sign)
			}
			inex_im = v31
		} else {
			inex_re = libmpfr.Xmpfr_neg(tls, a, bp+32, rnd&libc.Int32FromInt32(0x0F))
			inex_im = libmpfr.Xmpfr_neg(tls, a+32, bp, rnd>>libc.Int32FromInt32(4))
		}
	}
	if repr_w != 0 && inex_w != 0 {
		if rnd_w == int32(_MPFR_RNDN) {
			/* w has not been rounded with mpfr_set/mpfr_neg, determine ternary
			   value from inex_w instead */
			if re_cmp > 0 {
				inex_re = inex_w
			} else {
				if im_cmp > 0 {
					inex_im = inex_w
				} else {
					inex_im = -inex_w
				}
			}
		} else {
			/* determine ternary value, but also potentially add 1 ulp; can only
			   be done now when we are in the target precision */
			if re_cmp > 0 {
				if rnd_w == int32(_MPFR_RNDU) || rnd_w == int32(_MPFR_RNDA) {
					if (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_sign > 0 {
						libmpfr.Xmpfr_nextabove(tls, a)
					} else {
						libmpfr.Xmpfr_nextbelow(tls, a)
					}
					inex_re = +libc.Int32FromInt32(1)
				} else {
					inex_re = -int32(1)
				}
			} else {
				if im_cmp > 0 {
					if rnd_w == int32(_MPFR_RNDU) || rnd_w == int32(_MPFR_RNDA) {
						if (*t__mpfr_struct)(unsafe.Pointer(a+32)).F_mpfr_sign > 0 {
							libmpfr.Xmpfr_nextabove(tls, a+32)
						} else {
							libmpfr.Xmpfr_nextbelow(tls, a+32)
						}
						inex_im = +libc.Int32FromInt32(1)
					} else {
						inex_im = -int32(1)
					}
				} else {
					if rnd_w == int32(_MPFR_RNDU) || rnd_w == int32(_MPFR_RNDA) {
						if (*t__mpfr_struct)(unsafe.Pointer(a+32)).F_mpfr_sign > 0 {
							libmpfr.Xmpfr_nextabove(tls, a+32)
						} else {
							libmpfr.Xmpfr_nextbelow(tls, a+32)
						}
						inex_im = -int32(1)
					} else {
						inex_im = +libc.Int32FromInt32(1)
					}
				}
			}
		}
	}
	if repr_t != 0 && inex_t != 0 {
		if rnd_t == int32(_MPFR_RNDN) {
			if re_cmp > 0 {
				inex_im = inex_t
			} else {
				if im_cmp > 0 {
					inex_re = inex_t
				} else {
					inex_re = -inex_t
				}
			}
		} else {
			if re_cmp > 0 {
				if rnd_t == r {
					inex_im = inex_t
				} else {
					inex_im = -inex_t
					/* im_cmp > 0 implies that Im(b) > 0, thus im_sgn = 0
					   and r = MPFR_RNDU.
					   im_cmp < 0 implies that Im(b) < 0, thus im_sgn = -1
					   and r = MPFR_RNDD. */
					if (*t__mpfr_struct)(unsafe.Pointer(a+32)).F_mpfr_sign > 0 {
						libmpfr.Xmpfr_nextbelow(tls, a+32)
					} else {
						libmpfr.Xmpfr_nextabove(tls, a+32)
					}
				}
			} else {
				if im_cmp > 0 {
					if rnd_t == r {
						inex_re = inex_t
					} else {
						inex_re = -inex_t
						/* im_cmp > 0 implies r = MPFR_RNDU (see above) */
						if (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_sign > 0 {
							libmpfr.Xmpfr_nextbelow(tls, a)
						} else {
							libmpfr.Xmpfr_nextabove(tls, a)
						}
					}
				} else { /* im_cmp < 0 */
					if rnd_t == r {
						inex_re = -inex_t
					} else {
						inex_re = inex_t
						/* im_cmp < 0 implies r = MPFR_RNDD (see above) */
						if (*t__mpfr_struct)(unsafe.Pointer(a)).F_mpfr_sign > 0 {
							libmpfr.Xmpfr_nextbelow(tls, a)
						} else {
							libmpfr.Xmpfr_nextabove(tls, a)
						}
					}
				}
			}
		}
	}
	libmpfr.Xmpfr_clear(tls, bp)
	libmpfr.Xmpfr_clear(tls, bp+32)
	/* restore the exponent range, and check the range of results */
	libmpfr.Xmpfr_set_emin(tls, saved_emin)
	libmpfr.Xmpfr_set_emax(tls, saved_emax)
	inex_re = libmpfr.Xmpfr_check_range(tls, a, inex_re, rnd&libc.Int32FromInt32(0x0F))
	inex_im = libmpfr.Xmpfr_check_range(tls, a+32, inex_im, rnd>>libc.Int32FromInt32(4))
	if inex_re < 0 {
		v38 = int32(2)
	} else {
		if inex_re == 0 {
			v39 = 0
		} else {
			v39 = int32(1)
		}
		v38 = v39
	}
	if inex_im < 0 {
		v40 = int32(2)
	} else {
		if inex_im == 0 {
			v41 = 0
		} else {
			v41 = int32(1)
		}
		v40 = v41
	}
	return v38 | v40<<int32(2)
}

func _skip_whitespace1(tls *libc.TLS, p uintptr) {
	var v1, v2 int32
	_, _ = v1, v2
	/* TODO: This function had better be inlined, but it is unclear whether
	   the hassle to get this implemented across all platforms is worth it. */
	for {
		v1 = libc.Int32FromUint8(libc.Uint8FromInt8(*(*int8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(p))))))
		v2 = libc.BoolInt32(v1 == int32(' ') || libc.Uint32FromInt32(v1)-uint32('\t') < uint32(5))
		goto _3
	_3:
		if !(v2 != 0) {
			break
		}
		*(*uintptr)(unsafe.Pointer(p))++
	}
}

func Xmpc_strtoc(tls *libc.TLS, rop Tmpc_ptr, nptr uintptr, endptr uintptr, base int32, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var _p Tmpfr_ptr
	var bracketed, inex_im, inex_re, v1, v2, v3, v5, v6, v7, v8 int32
	var _ /* end at bp+8 */ uintptr
	var _ /* p at bp+0 */ uintptr
	_, _, _, _, _, _, _, _, _, _, _ = _p, bracketed, inex_im, inex_re, v1, v2, v3, v5, v6, v7, v8
	bracketed = 0
	inex_re = 0
	inex_im = 0
	if nptr == libc.UintptrFromInt32(0) || base > int32(36) || base == int32(1) {
		goto error
	}
	*(*uintptr)(unsafe.Pointer(bp)) = nptr
	_skip_whitespace1(tls, bp)
	if int32(*(*int8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp))))) == int32('(') {
		bracketed = int32(1)
		*(*uintptr)(unsafe.Pointer(bp))++
	}
	inex_re = libmpfr.Xmpfr_strtofr(tls, rop, *(*uintptr)(unsafe.Pointer(bp)), bp+8, base, rnd&libc.Int32FromInt32(0x0F))
	if *(*uintptr)(unsafe.Pointer(bp + 8)) == *(*uintptr)(unsafe.Pointer(bp)) {
		goto error
	}
	*(*uintptr)(unsafe.Pointer(bp)) = *(*uintptr)(unsafe.Pointer(bp + 8))
	if !(bracketed != 0) {
		{
			_p = rop + 32
			(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign = int32(1)
			(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
			_ = int32(_MPFR_RNDN)
			v1 = 0
		}
		inex_im = v1
	} else {
		v2 = libc.Int32FromUint8(libc.Uint8FromInt8(*(*int8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp))))))
		v3 = libc.BoolInt32(v2 == int32(' ') || libc.Uint32FromInt32(v2)-uint32('\t') < uint32(5))
		goto _4
	_4:
		if !(v3 != 0) {
			goto error
		}
		_skip_whitespace1(tls, bp)
		inex_im = libmpfr.Xmpfr_strtofr(tls, rop+32, *(*uintptr)(unsafe.Pointer(bp)), bp+8, base, rnd>>libc.Int32FromInt32(4))
		if *(*uintptr)(unsafe.Pointer(bp + 8)) == *(*uintptr)(unsafe.Pointer(bp)) {
			goto error
		}
		*(*uintptr)(unsafe.Pointer(bp)) = *(*uintptr)(unsafe.Pointer(bp + 8))
		_skip_whitespace1(tls, bp)
		if int32(*(*int8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp))))) != int32(')') {
			goto error
		}
		*(*uintptr)(unsafe.Pointer(bp))++
	}
	if endptr != libc.UintptrFromInt32(0) {
		*(*uintptr)(unsafe.Pointer(endptr)) = *(*uintptr)(unsafe.Pointer(bp))
	}
	if inex_re < 0 {
		v5 = int32(2)
	} else {
		if inex_re == 0 {
			v6 = 0
		} else {
			v6 = int32(1)
		}
		v5 = v6
	}
	if inex_im < 0 {
		v7 = int32(2)
	} else {
		if inex_im == 0 {
			v8 = 0
		} else {
			v8 = int32(1)
		}
		v7 = v8
	}
	return v5 | v7<<int32(2)
	goto error
error:
	;
	if endptr != libc.UintptrFromInt32(0) {
		*(*uintptr)(unsafe.Pointer(endptr)) = nptr
	}
	libmpfr.Xmpfr_set_nan(tls, rop)
	libmpfr.Xmpfr_set_nan(tls, rop+32)
	return -int32(1)
}

func Xmpc_sub(tls *libc.TLS, a Tmpc_ptr, b Tmpc_srcptr, c Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	var inex_im, inex_re, v1, v2, v3, v4 int32
	_, _, _, _, _, _ = inex_im, inex_re, v1, v2, v3, v4
	inex_re = libmpfr.Xmpfr_sub(tls, a, b, c, rnd&libc.Int32FromInt32(0x0F))
	inex_im = libmpfr.Xmpfr_sub(tls, a+32, b+32, c+32, rnd>>libc.Int32FromInt32(4))
	if inex_re < 0 {
		v1 = int32(2)
	} else {
		if inex_re == 0 {
			v2 = 0
		} else {
			v2 = int32(1)
		}
		v1 = v2
	}
	if inex_im < 0 {
		v3 = int32(2)
	} else {
		if inex_im == 0 {
			v4 = 0
		} else {
			v4 = int32(1)
		}
		v3 = v4
	}
	return v1 | v3<<int32(2)
}

// C documentation
//
//	/* return 0 iff both the real and imaginary parts are exact */
func Xmpc_sub_fr(tls *libc.TLS, a Tmpc_ptr, b Tmpc_srcptr, c Tmpfr_srcptr, rnd Tmpc_rnd_t) (r int32) {
	var _p Tmpfr_srcptr
	var inex_im, inex_re, v1, v2, v3, v4, v5 int32
	_, _, _, _, _, _, _, _ = _p, inex_im, inex_re, v1, v2, v3, v4, v5
	inex_re = libmpfr.Xmpfr_sub(tls, a, b, c, rnd&libc.Int32FromInt32(0x0F))
	{
		_p = b + 32
		v1 = libmpfr.Xmpfr_set4(tls, a+32, _p, rnd>>libc.Int32FromInt32(4), (*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign)
	}
	inex_im = v1
	if inex_re < 0 {
		v2 = int32(2)
	} else {
		if inex_re == 0 {
			v3 = 0
		} else {
			v3 = int32(1)
		}
		v2 = v3
	}
	if inex_im < 0 {
		v4 = int32(2)
	} else {
		if inex_im == 0 {
			v5 = 0
		} else {
			v5 = int32(1)
		}
		v4 = v5
	}
	return v2 | v4<<int32(2)
}

// C documentation
//
//	/* return 0 iff both the real and imaginary parts are exact */
func Xmpc_sub_ui(tls *libc.TLS, a Tmpc_ptr, b Tmpc_srcptr, c uint64, rnd Tmpc_rnd_t) (r int32) {
	var _p Tmpfr_srcptr
	var inex_im, inex_re, v1, v2, v3, v4, v5 int32
	_, _, _, _, _, _, _, _ = _p, inex_im, inex_re, v1, v2, v3, v4, v5
	inex_re = libmpfr.Xmpfr_sub_ui(tls, a, b, c, rnd&libc.Int32FromInt32(0x0F))
	{
		_p = b + 32
		v1 = libmpfr.Xmpfr_set4(tls, a+32, _p, rnd>>libc.Int32FromInt32(4), (*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign)
	}
	inex_im = v1
	if inex_re < 0 {
		v2 = int32(2)
	} else {
		if inex_re == 0 {
			v3 = 0
		} else {
			v3 = int32(1)
		}
		v2 = v3
	}
	if inex_im < 0 {
		v4 = int32(2)
	} else {
		if inex_im == 0 {
			v5 = 0
		} else {
			v5 = int32(1)
		}
		v4 = v5
	}
	return v2 | v4<<int32(2)
}

func Xmpc_sum(tls *libc.TLS, sum Tmpc_ptr, z uintptr, n uint64, rnd Tmpc_rnd_t) (r int32) {
	var i uint64
	var inex_im, inex_re, v3, v4, v5, v6 int32
	var t uintptr
	_, _, _, _, _, _, _, _ = i, inex_im, inex_re, t, v3, v4, v5, v6
	t = libc.Xmalloc(tls, n*uint64(32))
	/* warning: when n=0, malloc() might return NULL (e.g., gcc119) */
	i = uint64(0)
	for {
		if !(i < n) {
			break
		}
		*(*Tmpfr_ptr)(unsafe.Pointer(t + uintptr(i)*8)) = *(*Tmpc_ptr)(unsafe.Pointer(z + uintptr(i)*8))
		goto _1
	_1:
		;
		i++
	}
	inex_re = libmpfr.Xmpfr_sum(tls, sum, t, n, rnd&libc.Int32FromInt32(0x0F))
	i = uint64(0)
	for {
		if !(i < n) {
			break
		}
		*(*Tmpfr_ptr)(unsafe.Pointer(t + uintptr(i)*8)) = *(*Tmpc_ptr)(unsafe.Pointer(z + uintptr(i)*8)) + 32
		goto _2
	_2:
		;
		i++
	}
	inex_im = libmpfr.Xmpfr_sum(tls, sum+32, t, n, rnd>>libc.Int32FromInt32(4))
	libc.Xfree(tls, t)
	if inex_re < 0 {
		v3 = int32(2)
	} else {
		if inex_re == 0 {
			v4 = 0
		} else {
			v4 = int32(1)
		}
		v3 = v4
	}
	if inex_im < 0 {
		v5 = int32(2)
	} else {
		if inex_im == 0 {
			v6 = 0
		} else {
			v6 = int32(1)
		}
		v5 = v6
	}
	return v3 | v5<<int32(2)
}

func Xmpc_swap(tls *libc.TLS, a Tmpc_ptr, b Tmpc_ptr) {
	/* assumes real and imaginary parts do not overlap */
	libmpfr.Xmpfr_swap(tls, a, b)
	libmpfr.Xmpfr_swap(tls, a+32, b+32)
}

// C documentation
//
//	/* special case where the imaginary part of tan(op) rounds to -1 or 1:
//	   return 1 if |Im(tan(op))| > 1, and -1 if |Im(tan(op))| < 1, return 0
//	   if we can't decide.
//	   The imaginary part is sinh(2*y)/(cos(2*x) + cosh(2*y)) where op = (x,y).
//	*/
func _tan_im_cmp_one(tls *libc.TLS, op Tmpc_srcptr) (r int32) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var expc Tmpfr_exp_t
	var ret, v1 int32
	var _ /* c at bp+32 */ Tmpfr_t
	var _ /* x at bp+0 */ Tmpfr_t
	_, _, _ = expc, ret, v1
	ret = 0
	libmpfr.Xmpfr_init2(tls, bp, (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_prec)
	libmpfr.Xmpfr_mul_2ui(tls, bp, op, libc.Uint64FromInt32(libc.Int32FromInt32(1)), int32(_MPFR_RNDN))
	libmpfr.Xmpfr_init2(tls, bp+32, int64(32))
	libmpfr.Xmpfr_cos(tls, bp+32, bp, int32(_MPFR_RNDN))
	/* if cos(2x) >= 0, then |sinh(2y)/(cos(2x)+cosh(2y))| < 1 */
	if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			libmpfr.Xmpfr_set_erangeflag(tls)
		}
		v1 = libc.Int32FromInt32(0)
	} else {
		v1 = (*t__mpfr_struct)(unsafe.Pointer(bp + 32)).F_mpfr_sign
	}
	if v1 >= 0 {
		ret = -int32(1)
	} else {
		/* now cos(2x) < 0: |cosh(2y) - sinh(2y)| = exp(-2|y|) */
		expc = (*t__mpfr_struct)(unsafe.Pointer(bp + 32)).F_mpfr_exp
		libmpfr.Xmpfr_set4(tls, bp+32, op+32, int32(_MPFR_RNDN), int32(1))
		_ = libmpfr.Xmpfr_mul_si(tls, bp+32, bp+32, int64(-libc.Int32FromInt32(2)), int32(_MPFR_RNDN))
		libmpfr.Xmpfr_exp(tls, bp+32, bp+32, int32(_MPFR_RNDN))
		if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp < expc {
			ret = int32(1)
		} /* |Im(tan(op))| > 1 */
	}
	libmpfr.Xmpfr_clear(tls, bp+32)
	libmpfr.Xmpfr_clear(tls, bp)
	return ret
}

// C documentation
//
//	/* special case where the real part of tan(op) underflows to 0:
//	   return 1 if 0 < Re(tan(op)) < 2^(emin-2),
//	   -1 if -2^(emin-2) < Re(tan(op))| < 0, and 0 if we can't decide.
//	   The real part is sin(2*x)/(cos(2*x) + cosh(2*y)) where op = (x,y),
//	   thus has the sign of sin(2*x).
//	*/
func _tan_re_cmp_zero(tls *libc.TLS, op Tmpc_srcptr, emin Tmpfr_exp_t) (r int32) {
	bp := tls.Alloc(96)
	defer tls.Free(96)
	var ret, v1 int32
	var _ /* c at bp+64 */ Tmpfr_t
	var _ /* s at bp+32 */ Tmpfr_t
	var _ /* x at bp+0 */ Tmpfr_t
	_, _ = ret, v1
	ret = 0
	libmpfr.Xmpfr_init2(tls, bp, (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_prec)
	libmpfr.Xmpfr_mul_2ui(tls, bp, op, libc.Uint64FromInt32(libc.Int32FromInt32(1)), int32(_MPFR_RNDN))
	libmpfr.Xmpfr_init2(tls, bp+32, int64(32))
	libmpfr.Xmpfr_init2(tls, bp+64, int64(32))
	libmpfr.Xmpfr_sin(tls, bp+32, bp, int32(_MPFR_RNDA))
	libmpfr.Xmpfr_mul_2ui(tls, bp, op+32, libc.Uint64FromInt32(libc.Int32FromInt32(1)), int32(_MPFR_RNDN))
	libmpfr.Xmpfr_cosh(tls, bp+64, bp, int32(_MPFR_RNDZ))
	libmpfr.Xmpfr_sub_ui(tls, bp+64, bp+64, uint64(1), int32(_MPFR_RNDZ))
	libmpfr.Xmpfr_div(tls, bp+32, bp+32, bp+64, int32(_MPFR_RNDA))
	if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp <= emin-int64(2) {
		if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				libmpfr.Xmpfr_set_erangeflag(tls)
			}
			v1 = libc.Int32FromInt32(0)
		} else {
			v1 = (*t__mpfr_struct)(unsafe.Pointer(bp + 32)).F_mpfr_sign
		}
		ret = v1
	}
	libmpfr.Xmpfr_clear(tls, bp+32)
	libmpfr.Xmpfr_clear(tls, bp+64)
	libmpfr.Xmpfr_clear(tls, bp)
	return ret
}

func Xmpc_tan(tls *libc.TLS, rop Tmpc_ptr, op Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(192)
	defer tls.Free(192)
	var _p, _p1, _p4 Tmpfr_ptr
	var _p2, _p3 Tmpfr_srcptr
	var err, exr, eyi, eyr, ezr, k, saved_emax, saved_emin Tmpfr_exp_t
	var inex, inex_im, inex_re, ok, sign_re, v1, v10, v11, v12, v13, v14, v15, v16, v17, v19, v2, v20, v22, v24, v26, v28, v3, v30, v31, v32, v33, v36, v37, v4, v42, v43, v47, v48, v49, v5, v50, v51, v52, v53, v54, v6, v7, v8, v9 int32
	var prec Tmpfr_prec_t
	var v18, v39, v40, v41 int64
	var v27, v29 bool
	var _ /* c at bp+128 */ Tmpfr_t
	var _ /* s at bp+160 */ Tmpfr_t
	var _ /* x at bp+0 */ Tmpc_t
	var _ /* y at bp+64 */ Tmpc_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = _p, _p1, _p2, _p3, _p4, err, exr, eyi, eyr, ezr, inex, inex_im, inex_re, k, ok, prec, saved_emax, saved_emin, sign_re, v1, v10, v11, v12, v13, v14, v15, v16, v17, v18, v19, v2, v20, v22, v24, v26, v27, v28, v29, v3, v30, v31, v32, v33, v36, v37, v39, v4, v40, v41, v42, v43, v47, v48, v49, v5, v50, v51, v52, v53, v54, v6, v7, v8, v9
	/* special values */
	if !(libmpfr.Xmpfr_number_p(tls, op) != 0 && libmpfr.Xmpfr_number_p(tls, op+32) != 0) {
		if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			if (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				/* tan(NaN -i*Inf) = +/-0 -i */
				/* tan(NaN +i*Inf) = +/-0 +i */
				/* exact unless 1 is not in exponent range */
				if (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_sign < 0 {
					v1 = -int32(1)
				} else {
					v1 = +libc.Int32FromInt32(1)
				}
				inex = Xmpc_set_si_si(tls, rop, 0, int64(v1), rnd)
			} else {
				/* tan(NaN +i*y) = NaN +i*NaN, when y is finite */
				/* tan(NaN +i*NaN) = NaN +i*NaN */
				libmpfr.Xmpfr_set_nan(tls, rop)
				libmpfr.Xmpfr_set_nan(tls, rop+32)
				inex = libc.Int32FromInt32(0) | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2) /* always exact */
			}
		} else {
			if (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				if libmpfr.Xmpfr_sgn(tls, op) == 0 {
					/* tan(-0 +i*NaN) = -0 +i*NaN */
					/* tan(+0 +i*NaN) = +0 +i*NaN */
					Xmpc_set(tls, rop, op, rnd)
					inex = libc.Int32FromInt32(0) | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2) /* always exact */
				} else {
					/* tan(x +i*NaN) = NaN +i*NaN, when x != 0 */
					libmpfr.Xmpfr_set_nan(tls, rop)
					libmpfr.Xmpfr_set_nan(tls, rop+32)
					inex = libc.Int32FromInt32(0) | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2) /* always exact */
				}
			} else {
				if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
					if (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
						/* tan(-Inf -i*Inf) = -/+0 -i */
						/* tan(-Inf +i*Inf) = -/+0 +i */
						/* tan(+Inf -i*Inf) = +/-0 -i */
						/* tan(+Inf +i*Inf) = +/-0 +i */
						sign_re = libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_sign < libc.Int32FromInt32(0))
						{
							_p = rop
							(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_sign = int32(1)
							(*t__mpfr_struct)(unsafe.Pointer(_p)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
							_ = rnd & libc.Int32FromInt32(0x0F)
							v2 = 0
						}
						_ = v2
						if sign_re != 0 {
							v3 = -int32(1)
						} else {
							v3 = int32(1)
						}
						libmpfr.Xmpfr_set4(tls, rop, rop, int32(_MPFR_RNDN), v3)
						/* exact, unless 1 is not in exponent range */
						if (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_sign < 0 {
							v4 = -int32(1)
						} else {
							v4 = +libc.Int32FromInt32(1)
						}
						inex_im = libmpfr.Xmpfr_set_si_2exp(tls, rop+32, int64(v4), 0, rnd>>libc.Int32FromInt32(4))
						if inex_im < 0 {
							v5 = int32(2)
						} else {
							if inex_im == 0 {
								v6 = 0
							} else {
								v6 = int32(1)
							}
							v5 = v6
						}
						inex = 0 | v5<<int32(2)
					} else {
						/* tan(-Inf +i*y) = tan(+Inf +i*y) = NaN +i*NaN, when y is
						   finite */
						libmpfr.Xmpfr_set_nan(tls, rop)
						libmpfr.Xmpfr_set_nan(tls, rop+32)
						inex = libc.Int32FromInt32(0) | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2) /* always exact */
					}
				} else {
					/* tan(x -i*Inf) = +0*sin(x)*cos(x) -i, when x is finite */
					/* tan(x +i*Inf) = +0*sin(x)*cos(x) +i, when x is finite */
					libmpfr.Xmpfr_init(tls, bp+128)
					libmpfr.Xmpfr_init(tls, bp+160)
					libmpfr.Xmpfr_sin_cos(tls, bp+160, bp+128, op, int32(_MPFR_RNDN))
					{
						_p1 = rop
						(*t__mpfr_struct)(unsafe.Pointer(_p1)).F_mpfr_sign = int32(1)
						(*t__mpfr_struct)(unsafe.Pointer(_p1)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
						_ = rnd & libc.Int32FromInt32(0x0F)
						v7 = 0
					}
					_ = v7
					if libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(bp+128)).F_mpfr_sign < 0) != libc.BoolInt32((*t__mpfr_struct)(unsafe.Pointer(bp+160)).F_mpfr_sign < 0) {
						v8 = -int32(1)
					} else {
						v8 = int32(1)
					}
					libmpfr.Xmpfr_set4(tls, rop, rop, int32(_MPFR_RNDN), v8)
					/* exact, unless 1 is not in exponent range */
					if (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_sign < 0 {
						v9 = -int32(1)
					} else {
						v9 = +libc.Int32FromInt32(1)
					}
					inex_im = libmpfr.Xmpfr_set_si_2exp(tls, rop+32, int64(v9), 0, rnd>>libc.Int32FromInt32(4))
					if inex_im < 0 {
						v10 = int32(2)
					} else {
						if inex_im == 0 {
							v11 = 0
						} else {
							v11 = int32(1)
						}
						v10 = v11
					}
					inex = 0 | v10<<int32(2)
					libmpfr.Xmpfr_clear(tls, bp+160)
					libmpfr.Xmpfr_clear(tls, bp+128)
				}
			}
		}
		return inex
	}
	if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		/* tan(-0 -i*y) = -0 +i*tanh(y), when y is finite. */
		/* tan(+0 +i*y) = +0 +i*tanh(y), when y is finite. */
		{
			_p2 = op
			v12 = libmpfr.Xmpfr_set4(tls, rop, _p2, rnd&libc.Int32FromInt32(0x0F), (*t__mpfr_struct)(unsafe.Pointer(_p2)).F_mpfr_sign)
		}
		_ = v12
		inex_im = libmpfr.Xmpfr_tanh(tls, rop+32, op+32, rnd>>libc.Int32FromInt32(4))
		if inex_im < 0 {
			v13 = int32(2)
		} else {
			if inex_im == 0 {
				v14 = 0
			} else {
				v14 = int32(1)
			}
			v13 = v14
		}
		return 0 | v13<<int32(2)
	}
	if (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		/* tan(x -i*0) = tan(x) -i*0, when x is finite. */
		/* tan(x +i*0) = tan(x) +i*0, when x is finite. */
		inex_re = libmpfr.Xmpfr_tan(tls, rop, op, rnd&libc.Int32FromInt32(0x0F))
		{
			_p3 = op + 32
			v15 = libmpfr.Xmpfr_set4(tls, rop+32, _p3, rnd>>libc.Int32FromInt32(4), (*t__mpfr_struct)(unsafe.Pointer(_p3)).F_mpfr_sign)
		}
		_ = v15
		if inex_re < 0 {
			v16 = int32(2)
		} else {
			if inex_re == 0 {
				v17 = 0
			} else {
				v17 = int32(1)
			}
			v16 = v17
		}
		return v16 | libc.Int32FromInt32(0)<<libc.Int32FromInt32(2)
	}
	saved_emin = libmpfr.Xmpfr_get_emin(tls)
	saved_emax = libmpfr.Xmpfr_get_emax(tls)
	libmpfr.Xmpfr_set_emin(tls, libmpfr.Xmpfr_get_emin_min(tls))
	libmpfr.Xmpfr_set_emax(tls, libmpfr.Xmpfr_get_emax_max(tls))
	/* ordinary (non-zero) numbers */
	/* tan(op) = sin(op) / cos(op).
	   We use the following algorithm with rounding away from 0 for all
	   operations, and working precision w:
	   (1) x = A(sin(op))
	   (2) y = A(cos(op))
	   (3) z = A(x/y)
	   the error on Im(z) is at most 81 ulp,
	   the error on Re(z) is at most
	   7 ulp if k < 2,
	   8 ulp if k = 2,
	   else 5+k ulp, where
	   k = Exp(Re(x))+Exp(Re(y))-2min{Exp(Re(y)), Exp(Im(y))}-Exp(Re(x/y))
	   see proof in algorithms.tex.
	*/
	if (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec > (*t__mpfr_struct)(unsafe.Pointer(rop+32)).F_mpfr_prec {
		v18 = (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec
	} else {
		v18 = (*t__mpfr_struct)(unsafe.Pointer(rop + 32)).F_mpfr_prec
	}
	prec = v18
	Xmpc_init2(tls, bp, int64(2))
	Xmpc_init2(tls, bp+64, int64(2))
	err = int64(7)
	for cond := true; cond; cond = ok == 0 {
		ok = 0
		/* FIXME: prevent addition overflow */
		prec += Xmpc_ceil_log2(tls, prec) + err
		Xmpc_set_prec(tls, bp, prec)
		Xmpc_set_prec(tls, bp+64, prec)
		Xmpc_sin_cos(tls, bp, bp+64, op, int32(_MPFR_RNDA)+int32(_MPFR_RNDA)<<libc.Int32FromInt32(4), int32(_MPFR_RNDA)+int32(_MPFR_RNDA)<<libc.Int32FromInt32(4))
		if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(bp+64)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(bp+64+32)).F_mpfr_exp == libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			/* If the real or imaginary part of x is infinite, it means that
			   Im(op) was large, in which case the result is
			   sign(tan(Re(op)))*0 + sign(Im(op))*I,
			   where sign(tan(Re(op))) = sign(Re(x))*sign(Re(y)). */
			{
				_p4 = rop
				(*t__mpfr_struct)(unsafe.Pointer(_p4)).F_mpfr_sign = int32(1)
				(*t__mpfr_struct)(unsafe.Pointer(_p4)).F_mpfr_exp = libc.Int64FromInt32(0) - libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1))
				_ = int32(_MPFR_RNDN)
				v19 = 0
			}
			_ = v19
			if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
					libmpfr.Xmpfr_set_erangeflag(tls)
				}
				v20 = libc.Int32FromInt32(0)
			} else {
				v20 = (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_sign
			}
			if (*t__mpfr_struct)(unsafe.Pointer(bp+64)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				if (*t__mpfr_struct)(unsafe.Pointer(bp+64)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
					libmpfr.Xmpfr_set_erangeflag(tls)
				}
				v22 = libc.Int32FromInt32(0)
			} else {
				v22 = (*t__mpfr_struct)(unsafe.Pointer(bp + 64)).F_mpfr_sign
			}
			if v20*v22 < 0 {
				libmpfr.Xmpfr_neg(tls, rop, rop, int32(_MPFR_RNDN))
				inex_re = int32(1)
			} else {
				inex_re = -int32(1)
			} /* +0 is rounded down */
			if (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp < libc.Int64FromInt32(2)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				if (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp == libc.Int64FromInt32(1)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
					libmpfr.Xmpfr_set_erangeflag(tls)
				}
				v24 = libc.Int32FromInt32(0)
			} else {
				v24 = (*t__mpfr_struct)(unsafe.Pointer(op + 32)).F_mpfr_sign
			}
			if v24 > 0 {
				_ = libmpfr.Xmpfr_set_ui_2exp(tls, rop+32, libc.Uint64FromInt32(libc.Int32FromInt32(1)), 0, int32(_MPFR_RNDN))
				inex_im = int32(1)
			} else {
				_ = libmpfr.Xmpfr_set_si_2exp(tls, rop+32, int64(-libc.Int32FromInt32(1)), 0, int32(_MPFR_RNDN))
				inex_im = -int32(1)
			}
			/* if rounding is toward zero, fix the imaginary part */
			if v27 = rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDZ); !v27 {
				if (*t__mpfr_struct)(unsafe.Pointer(rop+32)).F_mpfr_sign < 0 {
					v26 = -int32(1)
				} else {
					v26 = int32(1)
				}
			}
			if v29 = v27 || v26 < 0 && rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDU); !v29 {
				if (*t__mpfr_struct)(unsafe.Pointer(rop+32)).F_mpfr_sign < 0 {
					v28 = -int32(1)
				} else {
					v28 = int32(1)
				}
			}
			if v29 || v28 > 0 && rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDD) {
				libmpfr.Xmpfr_nexttoward(tls, rop+32, rop)
				inex_im = -inex_im
			}
			if (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				inex_re = Xmpc_fix_zero(tls, rop, rnd&libc.Int32FromInt32(0x0F))
			}
			if (*t__mpfr_struct)(unsafe.Pointer(rop+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
				inex_im = Xmpc_fix_zero(tls, rop+32, rnd>>libc.Int32FromInt32(4))
			}
			if inex_re < 0 {
				v30 = int32(2)
			} else {
				if inex_re == 0 {
					v31 = 0
				} else {
					v31 = int32(1)
				}
				v30 = v31
			}
			if inex_im < 0 {
				v32 = int32(2)
			} else {
				if inex_im == 0 {
					v33 = 0
				} else {
					v33 = int32(1)
				}
				v32 = v33
			}
			inex = v30 | v32<<int32(2)
			goto end
		}
		exr = (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp
		eyr = (*t__mpfr_struct)(unsafe.Pointer(bp + 64)).F_mpfr_exp
		eyi = (*t__mpfr_struct)(unsafe.Pointer(bp + 64 + 32)).F_mpfr_exp
		/* some parts of the quotient may be exact */
		inex = Xmpc_div(tls, bp, bp, bp+64, int32(_MPFR_RNDZ)+int32(_MPFR_RNDZ)<<libc.Int32FromInt32(4))
		/* OP is no pure real nor pure imaginary, so in theory the real and
		   imaginary parts of its tangent cannot be null. However due to
		   rounding errors this might happen. Consider for example
		   tan(1+14*I) = 1.26e-10 + 1.00*I. For small precision sin(op) and
		   cos(op) differ only by a factor I, thus after mpc_div x = I and
		   its real part is zero. */
		if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
			/* since we use an extended exponent range, if real(x) is zero,
			   this means the real part underflows, and we assume we can round */
			ok = _tan_re_cmp_zero(tls, op, saved_emin)
			if ok > 0 {
				if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_sign > 0 {
					libmpfr.Xmpfr_nextabove(tls, bp)
				} else {
					libmpfr.Xmpfr_nextbelow(tls, bp)
				}
			} else {
				if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_sign > 0 {
					libmpfr.Xmpfr_nextbelow(tls, bp)
				} else {
					libmpfr.Xmpfr_nextabove(tls, bp)
				}
			}
		} else {
			if inex&int32(3) == int32(2) {
				v36 = -int32(1)
			} else {
				if inex&int32(3) == 0 {
					v37 = 0
				} else {
					v37 = int32(1)
				}
				v36 = v37
			}
			if v36 != 0 {
				if (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_sign > 0 {
					libmpfr.Xmpfr_nextabove(tls, bp)
				} else {
					libmpfr.Xmpfr_nextbelow(tls, bp)
				}
			}
			ezr = (*t__mpfr_struct)(unsafe.Pointer(bp)).F_mpfr_exp
			/* FIXME: compute
			   k = Exp(Re(x))+Exp(Re(y))-2min{Exp(Re(y)), Exp(Im(y))}-Exp(Re(x/y))
			   avoiding overflow */
			if -eyr > eyr-int64(2)*eyi {
				v39 = -eyr
			} else {
				v39 = eyr - int64(2)*eyi
			}
			k = exr - ezr + v39
			if k < int64(2) {
				v40 = int64(7)
			} else {
				if k == int64(2) {
					v41 = int64(8)
				} else {
					v41 = int64(5) + k
				}
				v40 = v41
			}
			err = v40
			/* Can the real part be rounded? */
			ok = libc.BoolInt32(!(libmpfr.Xmpfr_number_p(tls, bp) != 0) || libmpfr.Xmpfr_can_round(tls, bp, prec-err, int32(_MPFR_RNDN), int32(_MPFR_RNDZ), (*t__mpfr_struct)(unsafe.Pointer(rop)).F_mpfr_prec+libc.BoolInt64(rnd&libc.Int32FromInt32(0x0F) == int32(_MPFR_RNDN))) != 0)
		}
		if ok != 0 {
			if inex>>int32(2) == int32(2) {
				v42 = -int32(1)
			} else {
				if inex>>int32(2) == 0 {
					v43 = 0
				} else {
					v43 = int32(1)
				}
				v42 = v43
			}
			if v42 != 0 {
				if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_sign > 0 {
					libmpfr.Xmpfr_nextabove(tls, bp+32)
				} else {
					libmpfr.Xmpfr_nextbelow(tls, bp+32)
				}
			}
			/* Can the imaginary part be rounded? */
			ok = libc.BoolInt32(!(libmpfr.Xmpfr_number_p(tls, bp+32) != 0) || libmpfr.Xmpfr_can_round(tls, bp+32, prec-int64(6), int32(_MPFR_RNDN), int32(_MPFR_RNDZ), (*t__mpfr_struct)(unsafe.Pointer(rop+32)).F_mpfr_prec+libc.BoolInt64(rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDN))) != 0)
			/* Special case when Im(x) = +/- 1:
			   tan z = [sin(2x)+i*sinh(2y)] / [cos(2x) + cosh(2y)]
			   (formula 4.3.57 of Abramowitz and Stegun) thus for y large
			   in absolute value the imaginary part is near -1 or +1.
			   More precisely cos(2x) + cosh(2y) = cosh(2y) + t with |t| <= 1,
			   thus since cosh(2y) >= exp|2y|/2, then the imaginary part is:
			   tanh(2y) * 1/(1+u) where u = |cos(2x)/cosh(2y)| <= 2/exp|2y|
			   thus |im(z) - tanh(2y)| <= 2/exp|2y| * tanh(2y).
			   Since |tanh(2y)| = (1-exp(-4|y|))/(1+exp(-4|y|)),
			   we have 1-|tanh(2y)| < 2*exp(-4|y|).
			   Thus |im(z)-1| < 2/exp|2y| + 2/exp|4y| < 4/exp|2y| < 4/2^|2y|.
			   If 2^EXP(y) >= p+2, then im(z) rounds to -1 or 1. */
			if ok == 0 && (libmpfr.Xmpfr_cmp_ui_2exp(tls, bp+32, libc.Uint64FromInt32(libc.Int32FromInt32(1)), 0) == 0 || libmpfr.Xmpfr_cmp_si_2exp(tls, bp+32, int64(-libc.Int32FromInt32(1)), 0) == 0) && (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp >= 0 && (libc.Uint64FromInt64((*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp) >= libc.Uint64FromInt32(8)*libc.Uint64FromInt64(8) || libc.Int64FromInt32(1)<<(*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp >= (*t__mpfr_struct)(unsafe.Pointer(rop+32)).F_mpfr_prec+int64(2)) {
				/* subtract one ulp, so that we get the correct inexact flag */
				ok = _tan_im_cmp_one(tls, op)
				if ok < 0 {
					if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_sign > 0 {
						libmpfr.Xmpfr_nextbelow(tls, bp+32)
					} else {
						libmpfr.Xmpfr_nextabove(tls, bp+32)
					}
				} else {
					if ok > 0 {
						if (*t__mpfr_struct)(unsafe.Pointer(bp+32)).F_mpfr_sign > 0 {
							libmpfr.Xmpfr_nextabove(tls, bp+32)
						} else {
							libmpfr.Xmpfr_nextbelow(tls, bp+32)
						}
					}
				}
			}
		}
		if ok == 0 {
			prec += prec / int64(2)
		}
	}
	inex = Xmpc_set(tls, rop, bp, rnd)
	goto end
end:
	;
	Xmpc_clear(tls, bp)
	Xmpc_clear(tls, bp+64)
	/* restore the exponent range, and check the range of results */
	libmpfr.Xmpfr_set_emin(tls, saved_emin)
	libmpfr.Xmpfr_set_emax(tls, saved_emax)
	if inex&int32(3) == int32(2) {
		v47 = -int32(1)
	} else {
		if inex&int32(3) == 0 {
			v48 = 0
		} else {
			v48 = int32(1)
		}
		v47 = v48
	}
	inex_re = libmpfr.Xmpfr_check_range(tls, rop, v47, rnd&libc.Int32FromInt32(0x0F))
	if inex>>int32(2) == int32(2) {
		v49 = -int32(1)
	} else {
		if inex>>int32(2) == 0 {
			v50 = 0
		} else {
			v50 = int32(1)
		}
		v49 = v50
	}
	inex_im = libmpfr.Xmpfr_check_range(tls, rop+32, v49, rnd>>libc.Int32FromInt32(4))
	if inex_re < 0 {
		v51 = int32(2)
	} else {
		if inex_re == 0 {
			v52 = 0
		} else {
			v52 = int32(1)
		}
		v51 = v52
	}
	if inex_im < 0 {
		v53 = int32(2)
	} else {
		if inex_im == 0 {
			v54 = 0
		} else {
			v54 = int32(1)
		}
		v53 = v54
	}
	return v51 | v53<<int32(2)
}

func Xmpc_tanh(tls *libc.TLS, rop Tmpc_ptr, op Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(128)
	defer tls.Free(128)
	var inex, v1, v10, v11, v12, v2, v3, v4, v5, v6, v7, v8, v9 int32
	var _ /* tan_z at bp+64 */ Tmpc_t
	var _ /* z at bp+0 */ Tmpc_t
	_, _, _, _, _, _, _, _, _, _, _, _, _ = inex, v1, v10, v11, v12, v2, v3, v4, v5, v6, v7, v8, v9
	/* z := conj(-i * op) and rop = conj(-i * tan(z)), in other words, we have
	   to switch real and imaginary parts. Let us set them without copying
	   significands. */
	*(*t__mpfr_struct)(unsafe.Pointer(bp)) = *(*t__mpfr_struct)(unsafe.Pointer(op + 32))
	*(*t__mpfr_struct)(unsafe.Pointer(bp + 32)) = *(*t__mpfr_struct)(unsafe.Pointer(op))
	*(*t__mpfr_struct)(unsafe.Pointer(bp + 64)) = *(*t__mpfr_struct)(unsafe.Pointer(rop + 32))
	*(*t__mpfr_struct)(unsafe.Pointer(bp + 64 + 32)) = *(*t__mpfr_struct)(unsafe.Pointer(rop))
	inex = Xmpc_tan(tls, bp+64, bp, rnd>>libc.Int32FromInt32(4)+rnd&libc.Int32FromInt32(0x0F)<<libc.Int32FromInt32(4))
	/* tan_z and rop parts share the same significands, copy the rest now. */
	*(*t__mpfr_struct)(unsafe.Pointer(rop)) = *(*t__mpfr_struct)(unsafe.Pointer(bp + 64 + 32))
	*(*t__mpfr_struct)(unsafe.Pointer(rop + 32)) = *(*t__mpfr_struct)(unsafe.Pointer(bp + 64))
	/* swap inexact flags for real and imaginary parts */
	if inex>>int32(2) == int32(2) {
		v2 = -int32(1)
	} else {
		if inex>>int32(2) == 0 {
			v3 = 0
		} else {
			v3 = int32(1)
		}
		v2 = v3
	}
	if v2 < 0 {
		v1 = int32(2)
	} else {
		if inex>>int32(2) == int32(2) {
			v5 = -int32(1)
		} else {
			if inex>>int32(2) == 0 {
				v6 = 0
			} else {
				v6 = int32(1)
			}
			v5 = v6
		}
		if v5 == 0 {
			v4 = 0
		} else {
			v4 = int32(1)
		}
		v1 = v4
	}
	if inex&int32(3) == int32(2) {
		v8 = -int32(1)
	} else {
		if inex&int32(3) == 0 {
			v9 = 0
		} else {
			v9 = int32(1)
		}
		v8 = v9
	}
	if v8 < 0 {
		v7 = int32(2)
	} else {
		if inex&int32(3) == int32(2) {
			v11 = -int32(1)
		} else {
			if inex&int32(3) == 0 {
				v12 = 0
			} else {
				v12 = int32(1)
			}
			v11 = v12
		}
		if v11 == 0 {
			v10 = 0
		} else {
			v10 = int32(1)
		}
		v7 = v10
	}
	return v1 | v7<<int32(2)
}

// C documentation
//
//	/* returns ceil(log(d)/log(2)) if d > 0 */
//	/* Don't use count_leading_zeros since it is in longlong.h */
func Xmpc_ceil_log2(tls *libc.TLS, d Tmpfr_prec_t) (r Tmpfr_prec_t) {
	var exp Tmpfr_prec_t
	_ = exp
	exp = 0
	for {
		if !(d > int64(1)) {
			break
		}
		exp++
		goto _1
	_1:
		;
		d = (d + int64(1)) / int64(2)
	}
	return exp
}

func Xmpc_ui_div(tls *libc.TLS, a Tmpc_ptr, b uint64, c Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var inex int32
	var _ /* bb at bp+0 */ Tmpc_t
	_ = inex
	Xmpc_init2(tls, bp, libc.Int64FromUint64(libc.Uint64FromInt64(8)*libc.Uint64FromInt32(m_CHAR_BIT)))
	Xmpc_set_ui(tls, bp, b, rnd) /* exact */
	inex = Xmpc_div(tls, a, bp, c, rnd)
	Xmpc_clear(tls, bp)
	return inex
}

func Xmpc_ui_ui_sub(tls *libc.TLS, rop Tmpc_ptr, re uint64, im uint64, op Tmpc_srcptr, rnd Tmpc_rnd_t) (r int32) {
	var inex_im, inex_re, v1, v2, v3, v4 int32
	_, _, _, _, _, _ = inex_im, inex_re, v1, v2, v3, v4
	inex_re = libmpfr.Xmpfr_ui_sub(tls, rop, re, op, rnd&libc.Int32FromInt32(0x0F))
	inex_im = libmpfr.Xmpfr_ui_sub(tls, rop+32, im, op+32, rnd>>libc.Int32FromInt32(4))
	if inex_re < 0 {
		v1 = int32(2)
	} else {
		if inex_re == 0 {
			v2 = 0
		} else {
			v2 = int32(1)
		}
		v1 = v2
	}
	if inex_im < 0 {
		v3 = int32(2)
	} else {
		if inex_im == 0 {
			v4 = 0
		} else {
			v4 = int32(1)
		}
		v3 = v4
	}
	return v1 | v3<<int32(2)
}

const m_PRIX16 = "X"
const m_PRIX32 = "X"
const m_PRIX8 = "X"
const m_PRIXFAST16 = "X"
const m_PRIXFAST32 = "X"
const m_PRIXFAST8 = "X"
const m_PRIXLEAST16 = "X"
const m_PRIXLEAST32 = "X"
const m_PRIXLEAST8 = "X"
const m_PRId16 = "d"
const m_PRId32 = "d"
const m_PRId8 = "d"
const m_PRIdFAST16 = "d"
const m_PRIdFAST32 = "d"
const m_PRIdFAST8 = "d"
const m_PRIdLEAST16 = "d"
const m_PRIdLEAST32 = "d"
const m_PRIdLEAST8 = "d"
const m_PRIi16 = "i"
const m_PRIi32 = "i"
const m_PRIi8 = "i"
const m_PRIiFAST16 = "i"
const m_PRIiFAST32 = "i"
const m_PRIiFAST8 = "i"
const m_PRIiLEAST16 = "i"
const m_PRIiLEAST32 = "i"
const m_PRIiLEAST8 = "i"
const m_PRIo16 = "o"
const m_PRIo32 = "o"
const m_PRIo8 = "o"
const m_PRIoFAST16 = "o"
const m_PRIoFAST32 = "o"
const m_PRIoFAST8 = "o"
const m_PRIoLEAST16 = "o"
const m_PRIoLEAST32 = "o"
const m_PRIoLEAST8 = "o"
const m_PRIu16 = "u"
const m_PRIu32 = "u"
const m_PRIu8 = "u"
const m_PRIuFAST16 = "u"
const m_PRIuFAST32 = "u"
const m_PRIuFAST8 = "u"
const m_PRIuLEAST16 = "u"
const m_PRIuLEAST32 = "u"
const m_PRIuLEAST8 = "u"
const m_PRIx16 = "x"
const m_PRIx32 = "x"
const m_PRIx8 = "x"
const m_PRIxFAST16 = "x"
const m_PRIxFAST32 = "x"
const m_PRIxFAST8 = "x"
const m_PRIxLEAST16 = "x"
const m_PRIxLEAST32 = "x"
const m_PRIxLEAST8 = "x"
const m_SCNd16 = "hd"
const m_SCNd32 = "d"
const m_SCNd8 = "hhd"
const m_SCNdFAST16 = "d"
const m_SCNdFAST32 = "d"
const m_SCNdFAST8 = "hhd"
const m_SCNdLEAST16 = "hd"
const m_SCNdLEAST32 = "d"
const m_SCNdLEAST8 = "hhd"
const m_SCNi16 = "hi"
const m_SCNi32 = "i"
const m_SCNi8 = "hhi"
const m_SCNiFAST16 = "i"
const m_SCNiFAST32 = "i"
const m_SCNiFAST8 = "hhi"
const m_SCNiLEAST16 = "hi"
const m_SCNiLEAST32 = "i"
const m_SCNiLEAST8 = "hhi"
const m_SCNo16 = "ho"
const m_SCNo32 = "o"
const m_SCNo8 = "hho"
const m_SCNoFAST16 = "o"
const m_SCNoFAST32 = "o"
const m_SCNoFAST8 = "hho"
const m_SCNoLEAST16 = "ho"
const m_SCNoLEAST32 = "o"
const m_SCNoLEAST8 = "hho"
const m_SCNu16 = "hu"
const m_SCNu32 = "u"
const m_SCNu8 = "hhu"
const m_SCNuFAST16 = "u"
const m_SCNuFAST32 = "u"
const m_SCNuFAST8 = "hhu"
const m_SCNuLEAST16 = "hu"
const m_SCNuLEAST32 = "u"
const m_SCNuLEAST8 = "hhu"
const m_SCNx16 = "hx"
const m_SCNx32 = "x"
const m_SCNx8 = "hhx"
const m_SCNxFAST16 = "x"
const m_SCNxFAST32 = "x"
const m_SCNxFAST8 = "hhx"
const m_SCNxLEAST16 = "hx"
const m_SCNxLEAST32 = "x"
const m_SCNxLEAST8 = "hhx"
const m___PRI64 = "l"
const m___PRIPTR = "l"

type Timaxdiv_t = struct {
	Fquot Tintmax_t
	Frem  Tintmax_t
}

/* The radius can take three types of values, represented as follows:
   infinite: the mantissa is -1 and the exponent is undefined;
   0: the mantissa and the exponent are 0;
   positive: the mantissa is a positive integer, and the radius is
   mantissa*2^exponent. A normalised positive radius "has 31 bits",
   in the sense that the bits 0 to 29 are arbitrary, bit 30 is 1,
   and bits 31 to 63 are 0; otherwise said, the mantissa lies between
   2^30 and 2^31-1.
   Unless stated otherwise, all functions take normalised inputs and
   produce normalised output; they compute only upper bounds on the radii,
   without guaranteeing that these are tight. */

func _mpcr_add_one_ulp(tls *libc.TLS, r Tmpcr_ptr) {
	/* Add 1 to the mantissa of the normalised r and renormalise. */
	(*t__mpcr_struct)(unsafe.Pointer(r)).Fmant++
	if (*t__mpcr_struct)(unsafe.Pointer(r)).Fmant == libc.Int64FromInt32(1)<<libc.Int32FromInt32(30)<<libc.Int32FromInt32(1) {
		*(*Tint64_t)(unsafe.Pointer(r)) >>= int64(1)
		(*t__mpcr_struct)(unsafe.Pointer(r)).Fexp++
	}
}

func _leading_bit(tls *libc.TLS, n Tint64_t) (r uint32) {
	/*
	   Assuming that n is a positive integer, return the position
	   	(from 0 to 62) of the leading bit, that is, the k such that
	   	n >= 2^k, but n < 2^(k+1).
	*/
	var k uint32
	_ = k
	if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x7fffffff)<<libc.Int32FromInt32(32)) != 0 {
		if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x7fff)<<libc.Int32FromInt32(48)) != 0 {
			if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x7f)<<libc.Int32FromInt32(56)) != 0 {
				if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x7)<<libc.Int32FromInt32(60)) != 0 {
					if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x1)<<libc.Int32FromInt32(62)) != 0 {
						k = uint32(62)
					} else {
						if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x1)<<libc.Int32FromInt32(61)) != 0 {
							k = uint32(61)
						} else {
							k = uint32(60)
						}
					}
				} else {
					if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x3)<<libc.Int32FromInt32(58)) != 0 {
						if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x1)<<libc.Int32FromInt32(59)) != 0 {
							k = uint32(59)
						} else {
							k = uint32(58)
						}
					} else {
						if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x1)<<libc.Int32FromInt32(57)) != 0 {
							k = uint32(57)
						} else {
							k = uint32(56)
						}
					}
				}
			} else {
				if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0xf)<<libc.Int32FromInt32(52)) != 0 {
					if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x3)<<libc.Int32FromInt32(54)) != 0 {
						if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x1)<<libc.Int32FromInt32(55)) != 0 {
							k = uint32(55)
						} else {
							k = uint32(54)
						}
					} else {
						if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x1)<<libc.Int32FromInt32(53)) != 0 {
							k = uint32(53)
						} else {
							k = uint32(52)
						}
					}
				} else {
					if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x3)<<libc.Int32FromInt32(50)) != 0 {
						if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x1)<<libc.Int32FromInt32(51)) != 0 {
							k = uint32(51)
						} else {
							k = uint32(50)
						}
					} else {
						if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x1)<<libc.Int32FromInt32(49)) != 0 {
							k = uint32(49)
						} else {
							k = uint32(48)
						}
					}
				}
			}
		} else {
			if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0xff)<<libc.Int32FromInt32(40)) != 0 {
				if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0xf)<<libc.Int32FromInt32(44)) != 0 {
					if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x3)<<libc.Int32FromInt32(46)) != 0 {
						if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x1)<<libc.Int32FromInt32(47)) != 0 {
							k = uint32(47)
						} else {
							k = uint32(46)
						}
					} else {
						if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x1)<<libc.Int32FromInt32(45)) != 0 {
							k = uint32(45)
						} else {
							k = uint32(44)
						}
					}
				} else {
					if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x3)<<libc.Int32FromInt32(42)) != 0 {
						if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x1)<<libc.Int32FromInt32(43)) != 0 {
							k = uint32(43)
						} else {
							k = uint32(42)
						}
					} else {
						if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x1)<<libc.Int32FromInt32(41)) != 0 {
							k = uint32(41)
						} else {
							k = uint32(40)
						}
					}
				}
			} else {
				if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0xf)<<libc.Int32FromInt32(36)) != 0 {
					if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x3)<<libc.Int32FromInt32(38)) != 0 {
						if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x1)<<libc.Int32FromInt32(39)) != 0 {
							k = uint32(39)
						} else {
							k = uint32(38)
						}
					} else {
						if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x1)<<libc.Int32FromInt32(37)) != 0 {
							k = uint32(37)
						} else {
							k = uint32(36)
						}
					}
				} else {
					if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x3)<<libc.Int32FromInt32(34)) != 0 {
						if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x1)<<libc.Int32FromInt32(35)) != 0 {
							k = uint32(35)
						} else {
							k = uint32(34)
						}
					} else {
						if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x1)<<libc.Int32FromInt32(33)) != 0 {
							k = uint32(33)
						} else {
							k = uint32(32)
						}
					}
				}
			}
		}
	} else {
		if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0xffff)<<libc.Int32FromInt32(16)) != 0 {
			if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0xff)<<libc.Int32FromInt32(24)) != 0 {
				if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0xf)<<libc.Int32FromInt32(28)) != 0 {
					if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x3)<<libc.Int32FromInt32(30)) != 0 {
						if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x1)<<libc.Int32FromInt32(31)) != 0 {
							k = uint32(31)
						} else {
							k = uint32(30)
						}
					} else {
						if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x1)<<libc.Int32FromInt32(29)) != 0 {
							k = uint32(29)
						} else {
							k = uint32(28)
						}
					}
				} else {
					if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x3)<<libc.Int32FromInt32(26)) != 0 {
						if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x1)<<libc.Int32FromInt32(27)) != 0 {
							k = uint32(27)
						} else {
							k = uint32(26)
						}
					} else {
						if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x1)<<libc.Int32FromInt32(25)) != 0 {
							k = uint32(25)
						} else {
							k = uint32(24)
						}
					}
				}
			} else {
				if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0xf)<<libc.Int32FromInt32(20)) != 0 {
					if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x3)<<libc.Int32FromInt32(22)) != 0 {
						if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x1)<<libc.Int32FromInt32(23)) != 0 {
							k = uint32(23)
						} else {
							k = uint32(22)
						}
					} else {
						if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x1)<<libc.Int32FromInt32(21)) != 0 {
							k = uint32(21)
						} else {
							k = uint32(20)
						}
					}
				} else {
					if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x3)<<libc.Int32FromInt32(18)) != 0 {
						if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x1)<<libc.Int32FromInt32(19)) != 0 {
							k = uint32(19)
						} else {
							k = uint32(18)
						}
					} else {
						if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x1)<<libc.Int32FromInt32(17)) != 0 {
							k = uint32(17)
						} else {
							k = uint32(16)
						}
					}
				}
			}
		} else {
			if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0xff)<<libc.Int32FromInt32(8)) != 0 {
				if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0xf)<<libc.Int32FromInt32(12)) != 0 {
					if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x3)<<libc.Int32FromInt32(14)) != 0 {
						if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x1)<<libc.Int32FromInt32(15)) != 0 {
							k = uint32(15)
						} else {
							k = uint32(14)
						}
					} else {
						if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x1)<<libc.Int32FromInt32(13)) != 0 {
							k = uint32(13)
						} else {
							k = uint32(12)
						}
					}
				} else {
					if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x3)<<libc.Int32FromInt32(10)) != 0 {
						if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x1)<<libc.Int32FromInt32(11)) != 0 {
							k = uint32(11)
						} else {
							k = uint32(10)
						}
					} else {
						if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x1)<<libc.Int32FromInt32(9)) != 0 {
							k = uint32(9)
						} else {
							k = uint32(8)
						}
					}
				}
			} else {
				if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0xf)<<libc.Int32FromInt32(4)) != 0 {
					if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x3)<<libc.Int32FromInt32(6)) != 0 {
						if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x1)<<libc.Int32FromInt32(7)) != 0 {
							k = uint32(7)
						} else {
							k = uint32(6)
						}
					} else {
						if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x1)<<libc.Int32FromInt32(5)) != 0 {
							k = uint32(5)
						} else {
							k = uint32(4)
						}
					}
				} else {
					if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x3)<<libc.Int32FromInt32(2)) != 0 {
						if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x1)<<libc.Int32FromInt32(3)) != 0 {
							k = uint32(3)
						} else {
							k = uint32(2)
						}
					} else {
						if libc.Uint64FromInt64(n)&(libc.Uint64FromInt32(0x1)<<libc.Int32FromInt32(1)) != 0 {
							k = uint32(1)
						} else {
							k = uint32(0)
						}
					}
				}
			}
		}
	}
	return k
}

func _mpcr_normalise_rnd(tls *libc.TLS, r Tmpcr_ptr, rnd Tmpfr_rnd_t) {
	/*
	   The function computes a normalised value for the potentially
	   	unnormalised r; depending on whether rnd is MPFR_RNDU or MPFR_RNDD,
	   	the result is rounded up or down. For efficiency reasons, rounding
	   	up does not take exact cases into account and adds one ulp anyway.
	*/
	var k uint32
	_ = k
	if Xmpcr_zero_p(tls, r) != 0 {
		(*t__mpcr_struct)(unsafe.Pointer(r)).Fexp = 0
	} else {
		if !(Xmpcr_inf_p(tls, r) != 0) {
			k = _leading_bit(tls, (*t__mpcr_struct)(unsafe.Pointer(r)).Fmant)
			if k <= uint32(30) {
				*(*Tint64_t)(unsafe.Pointer(r)) <<= libc.Int64FromUint32(uint32(30) - k)
				*(*Tint64_t)(unsafe.Pointer(r + 8)) -= libc.Int64FromUint32(uint32(30) - k)
			} else {
				*(*Tint64_t)(unsafe.Pointer(r)) >>= libc.Int64FromUint32(k - uint32(30))
				*(*Tint64_t)(unsafe.Pointer(r + 8)) += libc.Int64FromUint32(k - uint32(30))
				if rnd == int32(_MPFR_RNDU) {
					_mpcr_add_one_ulp(tls, r)
				}
			}
		}
	}
}

func _mpcr_normalise(tls *libc.TLS, r Tmpcr_ptr) {
	/* The potentially unnormalised r is normalised with rounding up. */
	_mpcr_normalise_rnd(tls, r, int32(_MPFR_RNDU))
}

func Xmpcr_inf_p(tls *libc.TLS, r Tmpcr_srcptr) (r1 int32) {
	return libc.BoolInt32((*t__mpcr_struct)(unsafe.Pointer(r)).Fmant == int64(-int32(1)))
}

func Xmpcr_zero_p(tls *libc.TLS, r Tmpcr_srcptr) (r1 int32) {
	return libc.BoolInt32((*t__mpcr_struct)(unsafe.Pointer(r)).Fmant == 0)
}

func Xmpcr_lt_half_p(tls *libc.TLS, r Tmpcr_srcptr) (r1 int32) {
	/* Return true if r < 1/2, false otherwise. */
	return libc.BoolInt32(Xmpcr_zero_p(tls, r) != 0 || (*t__mpcr_struct)(unsafe.Pointer(r)).Fexp < int64(-int32(31)))
}

func Xmpcr_cmp(tls *libc.TLS, r Tmpcr_srcptr, s Tmpcr_srcptr) (r1 int32) {
	if Xmpcr_inf_p(tls, r) != 0 {
		if Xmpcr_inf_p(tls, s) != 0 {
			return 0
		} else {
			return +libc.Int32FromInt32(1)
		}
	} else {
		if Xmpcr_inf_p(tls, s) != 0 {
			return -int32(1)
		} else {
			if Xmpcr_zero_p(tls, r) != 0 {
				if Xmpcr_zero_p(tls, s) != 0 {
					return 0
				} else {
					return -int32(1)
				}
			} else {
				if Xmpcr_zero_p(tls, s) != 0 {
					return +libc.Int32FromInt32(1)
				} else {
					if (*t__mpcr_struct)(unsafe.Pointer(r)).Fexp > (*t__mpcr_struct)(unsafe.Pointer(s)).Fexp {
						return +libc.Int32FromInt32(1)
					} else {
						if (*t__mpcr_struct)(unsafe.Pointer(r)).Fexp < (*t__mpcr_struct)(unsafe.Pointer(s)).Fexp {
							return -int32(1)
						} else {
							if (*t__mpcr_struct)(unsafe.Pointer(r)).Fmant > (*t__mpcr_struct)(unsafe.Pointer(s)).Fmant {
								return +libc.Int32FromInt32(1)
							} else {
								if (*t__mpcr_struct)(unsafe.Pointer(r)).Fmant < (*t__mpcr_struct)(unsafe.Pointer(s)).Fmant {
									return -int32(1)
								} else {
									return 0
								}
							}
						}
					}
				}
			}
		}
	}
	return r1
}

func Xmpcr_set_inf(tls *libc.TLS, r Tmpcr_ptr) {
	(*t__mpcr_struct)(unsafe.Pointer(r)).Fmant = int64(-int32(1))
}

func Xmpcr_set_zero(tls *libc.TLS, r Tmpcr_ptr) {
	(*t__mpcr_struct)(unsafe.Pointer(r)).Fmant = 0
	(*t__mpcr_struct)(unsafe.Pointer(r)).Fexp = 0
}

func Xmpcr_set_one(tls *libc.TLS, r Tmpcr_ptr) {
	(*t__mpcr_struct)(unsafe.Pointer(r)).Fmant = libc.Int64FromInt32(1) << libc.Int32FromInt32(30)
	(*t__mpcr_struct)(unsafe.Pointer(r)).Fexp = int64(-int32(30))
}

func Xmpcr_set(tls *libc.TLS, r Tmpcr_ptr, s Tmpcr_srcptr) {
	*(*t__mpcr_struct)(unsafe.Pointer(r)) = *(*t__mpcr_struct)(unsafe.Pointer(s))
}

func Xmpcr_set_ui64_2si64(tls *libc.TLS, r Tmpcr_ptr, mant Tuint64_t, exp Tint64_t) {
	/* Set r to mant*2^exp, rounded up. */
	if mant == uint64(0) {
		Xmpcr_set_zero(tls, r)
	} else {
		if mant >= libc.Uint64FromInt32(1)<<libc.Int32FromInt32(63) {
			if mant%uint64(2) == uint64(0) {
				mant = mant / uint64(2)
			} else {
				mant = mant/uint64(2) + uint64(1)
			}
			exp++
		}
		(*t__mpcr_struct)(unsafe.Pointer(r)).Fmant = libc.Int64FromUint64(mant)
		(*t__mpcr_struct)(unsafe.Pointer(r)).Fexp = exp
		_mpcr_normalise(tls, r)
	}
}

func Xmpcr_max(tls *libc.TLS, r Tmpcr_ptr, s Tmpcr_srcptr, t Tmpcr_srcptr) {
	/* Set r to the maximum of s and t. */
	if Xmpcr_inf_p(tls, s) != 0 || Xmpcr_inf_p(tls, t) != 0 {
		Xmpcr_set_inf(tls, r)
	} else {
		if Xmpcr_zero_p(tls, s) != 0 {
			Xmpcr_set(tls, r, t)
		} else {
			if Xmpcr_zero_p(tls, t) != 0 {
				Xmpcr_set(tls, r, s)
			} else {
				if (*t__mpcr_struct)(unsafe.Pointer(s)).Fexp < (*t__mpcr_struct)(unsafe.Pointer(t)).Fexp {
					Xmpcr_set(tls, r, t)
				} else {
					if (*t__mpcr_struct)(unsafe.Pointer(s)).Fexp > (*t__mpcr_struct)(unsafe.Pointer(t)).Fexp {
						Xmpcr_set(tls, r, s)
					} else {
						if (*t__mpcr_struct)(unsafe.Pointer(s)).Fmant < (*t__mpcr_struct)(unsafe.Pointer(t)).Fmant {
							Xmpcr_set(tls, r, t)
						} else {
							Xmpcr_set(tls, r, s)
						}
					}
				}
			}
		}
	}
}

func Xmpcr_get_exp(tls *libc.TLS, r Tmpcr_srcptr) (r1 Tint64_t) {
	/* Return the exponent e such that r = m * 2^e with m such that
	   0.5 <= m < 1. */
	return (*t__mpcr_struct)(unsafe.Pointer(r)).Fexp + int64(31)
}

func Xmpcr_out_str(tls *libc.TLS, f uintptr, r Tmpcr_srcptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	if Xmpcr_inf_p(tls, r) != 0 {
		libc.Xfprintf(tls, f, __ccgo_ts+28, 0)
	} else {
		if Xmpcr_zero_p(tls, r) != 0 {
			libc.Xfprintf(tls, f, __ccgo_ts+32, 0)
		} else {
			libc.Xfprintf(tls, f, __ccgo_ts+34, libc.VaList(bp+8, (*t__mpcr_struct)(unsafe.Pointer(r)).Fmant, (*t__mpcr_struct)(unsafe.Pointer(r)).Fexp))
		}
	}
}

func _mpcr_mul_rnd(tls *libc.TLS, r Tmpcr_ptr, s Tmpcr_srcptr, t Tmpcr_srcptr, rnd Tmpfr_rnd_t) {
	/* Set r to the product of s and t, rounded according to whether rnd
	   is MPFR_RNDU or MPFR_RNDD. */
	if Xmpcr_inf_p(tls, s) != 0 || Xmpcr_inf_p(tls, t) != 0 {
		Xmpcr_set_inf(tls, r)
	} else {
		if Xmpcr_zero_p(tls, s) != 0 || Xmpcr_zero_p(tls, t) != 0 {
			Xmpcr_set_zero(tls, r)
		} else {
			(*t__mpcr_struct)(unsafe.Pointer(r)).Fmant = (*t__mpcr_struct)(unsafe.Pointer(s)).Fmant * (*t__mpcr_struct)(unsafe.Pointer(t)).Fmant
			(*t__mpcr_struct)(unsafe.Pointer(r)).Fexp = (*t__mpcr_struct)(unsafe.Pointer(s)).Fexp + (*t__mpcr_struct)(unsafe.Pointer(t)).Fexp
			_mpcr_normalise_rnd(tls, r, rnd)
		}
	}
}

func Xmpcr_mul(tls *libc.TLS, r Tmpcr_ptr, s Tmpcr_srcptr, t Tmpcr_srcptr) {
	_mpcr_mul_rnd(tls, r, s, t, int32(_MPFR_RNDU))
}

func Xmpcr_mul_2ui(tls *libc.TLS, r Tmpcr_ptr, s Tmpcr_srcptr, e uint64) {
	if Xmpcr_inf_p(tls, s) != 0 {
		Xmpcr_set_inf(tls, r)
	} else {
		if Xmpcr_zero_p(tls, s) != 0 {
			Xmpcr_set_zero(tls, r)
		} else {
			(*t__mpcr_struct)(unsafe.Pointer(r)).Fmant = (*t__mpcr_struct)(unsafe.Pointer(s)).Fmant
			(*t__mpcr_struct)(unsafe.Pointer(r)).Fexp = (*t__mpcr_struct)(unsafe.Pointer(s)).Fexp + libc.Int64FromUint64(e)
		}
	}
}

func Xmpcr_sqr(tls *libc.TLS, r Tmpcr_ptr, s Tmpcr_srcptr) {
	_mpcr_mul_rnd(tls, r, s, s, int32(_MPFR_RNDU))
}

func _mpcr_add_rnd(tls *libc.TLS, r Tmpcr_ptr, s Tmpcr_srcptr, t Tmpcr_srcptr, rnd Tmpfr_rnd_t) {
	/*
	   Set r to the sum of s and t, rounded according to whether rnd
	   	is MPFR_RNDU or MPFR_RNDD.
	   	The function also works correctly for certain non-normalised
	   	arguments s and t as long as the sum of their (potentially shifted
	   	if the exponents are not the same) mantissae does not flow over into
	   	the sign bit of the resulting mantissa. This is in particular the
	   	case when the mantissae of s and t start with the bits 00, that is,
	   	are less than 2^62, for instance because they are the results of
	   	multiplying two normalised mantissae together, so that an fmma
	   	function can be implemented without intermediate normalisation of
	   	the products.
	*/
	var d Tint64_t
	_ = d
	if Xmpcr_inf_p(tls, s) != 0 || Xmpcr_inf_p(tls, t) != 0 {
		Xmpcr_set_inf(tls, r)
	} else {
		if Xmpcr_zero_p(tls, s) != 0 {
			Xmpcr_set(tls, r, t)
		} else {
			if Xmpcr_zero_p(tls, t) != 0 {
				Xmpcr_set(tls, r, s)
			} else {
				/* Now all numbers are finite and non-zero. */
				d = (*t__mpcr_struct)(unsafe.Pointer(s)).Fexp - (*t__mpcr_struct)(unsafe.Pointer(t)).Fexp
				if d >= 0 {
					if d >= int64(64) {
						/* Shifting by more than the bitlength of the type may cause
						   compiler warnings and run time errors. */
						(*t__mpcr_struct)(unsafe.Pointer(r)).Fmant = (*t__mpcr_struct)(unsafe.Pointer(s)).Fmant
					} else {
						(*t__mpcr_struct)(unsafe.Pointer(r)).Fmant = (*t__mpcr_struct)(unsafe.Pointer(s)).Fmant + (*t__mpcr_struct)(unsafe.Pointer(t)).Fmant>>d
					}
					(*t__mpcr_struct)(unsafe.Pointer(r)).Fexp = (*t__mpcr_struct)(unsafe.Pointer(s)).Fexp
				} else {
					if d <= int64(-int32(64)) {
						(*t__mpcr_struct)(unsafe.Pointer(r)).Fmant = (*t__mpcr_struct)(unsafe.Pointer(t)).Fmant
					} else {
						(*t__mpcr_struct)(unsafe.Pointer(r)).Fmant = (*t__mpcr_struct)(unsafe.Pointer(t)).Fmant + (*t__mpcr_struct)(unsafe.Pointer(s)).Fmant>>-d
					}
					(*t__mpcr_struct)(unsafe.Pointer(r)).Fexp = (*t__mpcr_struct)(unsafe.Pointer(t)).Fexp
				}
				if rnd == int32(_MPFR_RNDU) {
					(*t__mpcr_struct)(unsafe.Pointer(r)).Fmant++
				}
				_mpcr_normalise_rnd(tls, r, rnd)
			}
		}
	}
}

func Xmpcr_add(tls *libc.TLS, r Tmpcr_ptr, s Tmpcr_srcptr, t Tmpcr_srcptr) {
	_mpcr_add_rnd(tls, r, s, t, int32(_MPFR_RNDU))
}

func Xmpcr_sub_rnd(tls *libc.TLS, r Tmpcr_ptr, s Tmpcr_srcptr, t Tmpcr_srcptr, rnd Tmpfr_rnd_t) {
	/*
	   Set r to s - t, rounded according to whether rnd is MPFR_RNDU or
	   	MPFR_RNDD; if the result were negative, it is set to infinity.
	*/
	var cmp int32
	var d Tint64_t
	_, _ = cmp, d
	cmp = Xmpcr_cmp(tls, s, t)
	if Xmpcr_inf_p(tls, s) != 0 || Xmpcr_inf_p(tls, t) != 0 || cmp < 0 {
		Xmpcr_set_inf(tls, r)
	} else {
		if cmp == 0 {
			Xmpcr_set_zero(tls, r)
		} else {
			if Xmpcr_zero_p(tls, t) != 0 {
				Xmpcr_set(tls, r, s)
			} else {
				/* Now all numbers are positive and normalised, and s > t. */
				d = (*t__mpcr_struct)(unsafe.Pointer(s)).Fexp - (*t__mpcr_struct)(unsafe.Pointer(t)).Fexp
				if d >= int64(64) {
					(*t__mpcr_struct)(unsafe.Pointer(r)).Fmant = (*t__mpcr_struct)(unsafe.Pointer(s)).Fmant
				} else {
					(*t__mpcr_struct)(unsafe.Pointer(r)).Fmant = (*t__mpcr_struct)(unsafe.Pointer(s)).Fmant - (*t__mpcr_struct)(unsafe.Pointer(t)).Fmant>>d
				}
				(*t__mpcr_struct)(unsafe.Pointer(r)).Fexp = (*t__mpcr_struct)(unsafe.Pointer(s)).Fexp
				if rnd == int32(_MPFR_RNDD) {
					(*t__mpcr_struct)(unsafe.Pointer(r)).Fmant--
				}
				_mpcr_normalise_rnd(tls, r, rnd)
			}
		}
	}
}

func Xmpcr_sub(tls *libc.TLS, r Tmpcr_ptr, s Tmpcr_srcptr, t Tmpcr_srcptr) {
	Xmpcr_sub_rnd(tls, r, s, t, int32(_MPFR_RNDU))
}

func Xmpcr_div(tls *libc.TLS, r Tmpcr_ptr, s Tmpcr_srcptr, t Tmpcr_srcptr) {
	if Xmpcr_inf_p(tls, s) != 0 || Xmpcr_inf_p(tls, t) != 0 || Xmpcr_zero_p(tls, t) != 0 {
		Xmpcr_set_inf(tls, r)
	} else {
		if Xmpcr_zero_p(tls, s) != 0 {
			Xmpcr_set_zero(tls, r)
		} else {
			(*t__mpcr_struct)(unsafe.Pointer(r)).Fmant = (*t__mpcr_struct)(unsafe.Pointer(s)).Fmant<<libc.Int32FromInt32(32)/(*t__mpcr_struct)(unsafe.Pointer(t)).Fmant + int64(1)
			(*t__mpcr_struct)(unsafe.Pointer(r)).Fexp = (*t__mpcr_struct)(unsafe.Pointer(s)).Fexp - int64(32) - (*t__mpcr_struct)(unsafe.Pointer(t)).Fexp
			_mpcr_normalise(tls, r)
		}
	}
}

func Xmpcr_div_2ui(tls *libc.TLS, r Tmpcr_ptr, s Tmpcr_srcptr, e uint64) {
	if Xmpcr_inf_p(tls, s) != 0 {
		Xmpcr_set_inf(tls, r)
	} else {
		if Xmpcr_zero_p(tls, s) != 0 {
			Xmpcr_set_zero(tls, r)
		} else {
			(*t__mpcr_struct)(unsafe.Pointer(r)).Fmant = (*t__mpcr_struct)(unsafe.Pointer(s)).Fmant
			(*t__mpcr_struct)(unsafe.Pointer(r)).Fexp = (*t__mpcr_struct)(unsafe.Pointer(s)).Fexp - libc.Int64FromUint64(e)
		}
	}
}

func Xsqrt_int64(tls *libc.TLS, n Tint64_t) (r Tint64_t) {
	/* Assuming that 2^30 <= n < 2^32, return ceil (sqrt (n*2^30)). */
	var N, s, t Tuint64_t
	var i int32
	_, _, _, _ = N, i, s, t
	/* We use the "Babylonian" method to compute an integer square root of N;
	   replacing the geometric mean sqrt (N) = sqrt (s * N/s) by the
	   arithmetic mean (s + N/s) / 2, rounded up, we obtain an upper bound
	   on the square root. */
	N = libc.Uint64FromInt64(n) << int32(30)
	s = libc.Uint64FromInt32(1) << libc.Int32FromInt32(31)
	i = 0
	for {
		if !(i < int32(5)) {
			break
		}
		t = s << int32(1)
		s = (s*s + N + t - uint64(1)) / t
		goto _1
	_1:
		;
		i++
	}
	/* Exhaustive search over all possible values of n shows that with
	   6 or more iterations, the method computes ceil (sqrt (N)) except
	   for squares N, where it stabilises on sqrt (N) + 1.
	   So we need to add a check for s-1; it turns out that then
	   5 iterations are enough. */
	if (s-uint64(1))*(s-uint64(1)) >= N {
		return libc.Int64FromUint64(s - uint64(1))
	} else {
		return libc.Int64FromUint64(s)
	}
	return r
}

func _mpcr_sqrt_rnd(tls *libc.TLS, r Tmpcr_ptr, s Tmpcr_srcptr, rnd Tmpfr_rnd_t) {
	/* Set r to the square root of s, rounded according to whether rnd is
	   MPFR_RNDU or MPFR_RNDD. */
	if Xmpcr_inf_p(tls, s) != 0 {
		Xmpcr_set_inf(tls, r)
	} else {
		if Xmpcr_zero_p(tls, s) != 0 {
			Xmpcr_set_zero(tls, r)
		} else {
			if (*t__mpcr_struct)(unsafe.Pointer(s)).Fexp%int64(2) == 0 {
				(*t__mpcr_struct)(unsafe.Pointer(r)).Fmant = Xsqrt_int64(tls, (*t__mpcr_struct)(unsafe.Pointer(s)).Fmant)
				(*t__mpcr_struct)(unsafe.Pointer(r)).Fexp = (*t__mpcr_struct)(unsafe.Pointer(s)).Fexp/int64(2) - int64(15)
			} else {
				(*t__mpcr_struct)(unsafe.Pointer(r)).Fmant = Xsqrt_int64(tls, int64(2)*(*t__mpcr_struct)(unsafe.Pointer(s)).Fmant)
				(*t__mpcr_struct)(unsafe.Pointer(r)).Fexp = ((*t__mpcr_struct)(unsafe.Pointer(s)).Fexp-int64(1))/int64(2) - int64(15)
			}
			/* Now we have 2^30 <= r < 2^31, so r is normalised;
			   if r == 2^30, then furthermore the square root was exact,
			   so we do not need to subtract 1 ulp when rounding down and
			   preserve normalisation. */
			if rnd == int32(_MPFR_RNDD) && (*t__mpcr_struct)(unsafe.Pointer(r)).Fmant != libc.Int64FromInt32(1)<<libc.Int32FromInt32(30) {
				(*t__mpcr_struct)(unsafe.Pointer(r)).Fmant--
			}
		}
	}
}

func Xmpcr_sqrt(tls *libc.TLS, r Tmpcr_ptr, s Tmpcr_srcptr) {
	_mpcr_sqrt_rnd(tls, r, s, int32(_MPFR_RNDU))
}

func _mpcr_set_d_rnd(tls *libc.TLS, r Tmpcr_ptr, d float64, rnd Tmpfr_rnd_t) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	/* Assuming that d is a positive double, set r to d rounded according
	   to rnd, which can be one of MPFR_RNDU or MPFR_RNDD. */
	var frac float64
	var _ /* e at bp+0 */ int32
	_ = frac
	frac = libc.Xfrexp(tls, d, bp)
	(*t__mpcr_struct)(unsafe.Pointer(r)).Fmant = int64(frac * float64(libc.Int64FromInt32(1)<<libc.Int32FromInt32(53)))
	(*t__mpcr_struct)(unsafe.Pointer(r)).Fexp = int64(*(*int32)(unsafe.Pointer(bp)) - int32(53))
	_mpcr_normalise_rnd(tls, r, rnd)
}

func _mpcr_f_abs_rnd(tls *libc.TLS, r Tmpcr_ptr, z Tmpfr_srcptr, rnd Tmpfr_rnd_t) {
	/*
	   Set r to the absolute value of z, rounded according to rnd, which
	   	can be one of MPFR_RNDU or MPFR_RNDD.
	*/
	var d float64
	var neg int32
	_, _ = d, neg
	neg = libmpfr.Xmpfr_sgn(tls, z)
	if neg == 0 {
		Xmpcr_set_zero(tls, r)
	} else {
		if rnd == int32(_MPFR_RNDU) {
			d = libmpfr.Xmpfr_get_d(tls, z, int32(_MPFR_RNDA))
		} else {
			d = libmpfr.Xmpfr_get_d(tls, z, int32(_MPFR_RNDZ))
		}
		if d < libc.Float64FromInt32(0) {
			d = -d
		}
		_mpcr_set_d_rnd(tls, r, d, rnd)
	}
}

func Xmpcr_add_rounding_error(tls *libc.TLS, r Tmpcr_ptr, p Tmpfr_prec_t, rnd Tmpfr_rnd_t) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	/* Replace r, radius of a complex ball, by the new radius obtained after
	   rounding both parts of the centre of the ball in direction rnd at
	   precision t.
	   Otherwise said:
	   r += ldexp (1 + r, -p) for rounding to nearest, adding 0.5ulp;
	   r += ldexp (1 + r, 1-p) for directed rounding, adding 1ulp.
	*/
	var _ /* s at bp+0 */ Tmpcr_t
	Xmpcr_set_one(tls, bp)
	Xmpcr_add(tls, bp, bp, r)
	if rnd == int32(_MPFR_RNDN) {
		Xmpcr_div_2ui(tls, bp, bp, libc.Uint64FromInt64(p))
	} else {
		Xmpcr_div_2ui(tls, bp, bp, libc.Uint64FromInt64(p-int64(1)))
	}
	Xmpcr_add(tls, r, r, bp)
}

func Xmpcr_c_abs_rnd(tls *libc.TLS, r Tmpcr_ptr, z Tmpc_srcptr, rnd Tmpfr_rnd_t) {
	bp := tls.Alloc(48)
	defer tls.Free(48)
	/* Compute a bound on mpc_abs (z) in r.
	   rnd can take either of the values MPFR_RNDU and MPFR_RNDD, and
	   the function computes an upper or a lower bound, respectively. */
	var _ /* im at bp+16 */ Tmpcr_t
	var _ /* re at bp+0 */ Tmpcr_t
	var _ /* u at bp+32 */ Tmpcr_t
	_mpcr_f_abs_rnd(tls, bp, z, rnd)
	_mpcr_f_abs_rnd(tls, bp+16, z+32, rnd)
	if Xmpcr_zero_p(tls, bp) != 0 {
		Xmpcr_set(tls, r, bp+16)
	} else {
		if Xmpcr_zero_p(tls, bp+16) != 0 {
			Xmpcr_set(tls, r, bp)
		} else {
			/* Squarings can be done exactly. */
			(*t__mpcr_struct)(unsafe.Pointer(bp + 32)).Fmant = (*t__mpcr_struct)(unsafe.Pointer(bp)).Fmant * (*t__mpcr_struct)(unsafe.Pointer(bp)).Fmant
			(*t__mpcr_struct)(unsafe.Pointer(bp + 32)).Fexp = int64(2) * (*t__mpcr_struct)(unsafe.Pointer(bp)).Fexp
			(*t__mpcr_struct)(unsafe.Pointer(r)).Fmant = (*t__mpcr_struct)(unsafe.Pointer(bp+16)).Fmant * (*t__mpcr_struct)(unsafe.Pointer(bp+16)).Fmant
			(*t__mpcr_struct)(unsafe.Pointer(r)).Fexp = int64(2) * (*t__mpcr_struct)(unsafe.Pointer(bp+16)).Fexp
			/* Additions still fit. */
			_mpcr_add_rnd(tls, r, r, bp+32, rnd)
			_mpcr_sqrt_rnd(tls, r, r, rnd)
		}
	}
}

func Xmpcb_out_str(tls *libc.TLS, f uintptr, op Tmpcb_srcptr) {
	Xmpc_out_str(tls, f, int32(10), uint64(0), op, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
	libc.Xfprintf(tls, f, __ccgo_ts+13, 0)
	Xmpcr_out_str(tls, f, op+64)
	libc.Xfprintf(tls, f, __ccgo_ts+47, 0)
}

func Xmpcb_init(tls *libc.TLS, rop Tmpcb_ptr) {
	Xmpc_init2(tls, rop, int64(2))
	Xmpcr_set_inf(tls, rop+64)
}

func Xmpcb_clear(tls *libc.TLS, rop Tmpcb_ptr) {
	Xmpc_clear(tls, rop)
}

func Xmpcb_get_prec(tls *libc.TLS, op Tmpcb_srcptr) (r Tmpfr_prec_t) {
	return Xmpc_get_prec(tls, op)
}

func Xmpcb_set_prec(tls *libc.TLS, rop Tmpcb_ptr, prec Tmpfr_prec_t) {
	Xmpc_set_prec(tls, rop, prec)
	Xmpcr_set_inf(tls, rop+64)
}

func Xmpcb_set(tls *libc.TLS, rop Tmpcb_ptr, op Tmpcb_srcptr) {
	if rop != op {
		Xmpc_set_prec(tls, rop, Xmpc_get_prec(tls, op))
		Xmpc_set(tls, rop, op, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
		Xmpcr_set(tls, rop+64, op+64)
	}
}

func Xmpcb_set_inf(tls *libc.TLS, rop Tmpcb_ptr) {
	Xmpc_set_prec(tls, rop, int64(2))
	Xmpc_set_ui_ui(tls, rop, uint64(0), uint64(0), int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
	Xmpcr_set_inf(tls, rop+64)
}

func Xmpcb_set_c(tls *libc.TLS, rop Tmpcb_ptr, op Tmpc_srcptr, prec Tmpfr_prec_t, err_re uint64, err_im uint64) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	/* Set the precision of rop to prec and assign a ball with centre op
	   to it. err_re and err_im contain potential errors in the real and
	   imaginary parts of op as multiples of a half ulp. For instance,
	   if the real part of op is exact, err_re should be set to 0;
	   if it is the result of rounding to nearest, it should be set to 1;
	   if it is the result of directed rounding, it should be set to 2.
	   The radius of the ball reflects err_re and err_im and the potential
	   additional rounding error that can occur when the precision of op
	   is higher than prec. If the real part of op is 0, then err_re
	   should be 0, since then ulp notation makes no sense, and similarly
	   for the imaginary part; otherwise the radius is set to infinity.
	   The implementation takes potential different precisions in the real
	   and imaginary parts of op into account. */
	var inex, v1, v2, v3, v4 int32
	var _ /* relerr_im at bp+16 */ Tmpcr_t
	var _ /* relerr_re at bp+0 */ Tmpcr_t
	_, _, _, _, _ = inex, v1, v2, v3, v4
	Xmpc_set_prec(tls, rop, prec)
	inex = Xmpc_set(tls, rop, op, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
	if (*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) && err_re > uint64(0) || (*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) && err_im > uint64(0) || !(libmpfr.Xmpfr_number_p(tls, op) != 0 && libmpfr.Xmpfr_number_p(tls, op+32) != 0) {
		Xmpcr_set_inf(tls, rop+64)
	} else {
		Xmpcr_set_ui64_2si64(tls, bp, err_re, -(*t__mpfr_struct)(unsafe.Pointer(op)).F_mpfr_prec)
		/* prop:relerror of algorithms.tex */
		if inex&int32(3) == int32(2) {
			v1 = -int32(1)
		} else {
			if inex&int32(3) == 0 {
				v2 = 0
			} else {
				v2 = int32(1)
			}
			v1 = v2
		}
		if v1 != 0 {
			Xmpcr_add_rounding_error(tls, bp, prec, int32(_MPFR_RNDN))
		}
		Xmpcr_set_ui64_2si64(tls, bp+16, err_im, -(*t__mpfr_struct)(unsafe.Pointer(op+32)).F_mpfr_prec)
		if inex>>int32(2) == int32(2) {
			v3 = -int32(1)
		} else {
			if inex>>int32(2) == 0 {
				v4 = 0
			} else {
				v4 = int32(1)
			}
			v3 = v4
		}
		if v3 != 0 {
			Xmpcr_add_rounding_error(tls, bp+16, prec, int32(_MPFR_RNDN))
		}
		Xmpcr_max(tls, rop+64, bp, bp+16)
		/* prop:comrelerror in algorithms.tex */
	}
}

func Xmpcb_set_ui_ui(tls *libc.TLS, z Tmpcb_ptr, re uint64, im uint64, prec Tmpfr_prec_t) {
	/*
	   Set the precision of z to prec and assign a ball with centre
	   	re+I*im to it. If prec is too small to hold the centre coordinates
	   	without rounding, use the minimal possible precision instead.
	*/
	var v1 int64
	_ = v1
	if prec > libc.Int64FromUint64(libc.Uint64FromInt32(m_CHAR_BIT)*libc.Uint64FromInt64(8)) {
		v1 = prec
	} else {
		v1 = libc.Int64FromUint64(libc.Uint64FromInt32(m_CHAR_BIT) * libc.Uint64FromInt64(8))
	}
	prec = v1
	Xmpc_set_prec(tls, z, prec)
	Xmpc_set_ui_ui(tls, z, re, im, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
	Xmpcr_set_zero(tls, z+64)
}

func Xmpcb_neg(tls *libc.TLS, z Tmpcb_ptr, z1 Tmpcb_srcptr) {
	var overlap int32
	var p Tmpfr_prec_t
	_, _ = overlap, p
	overlap = libc.BoolInt32(z == z1)
	if !(overlap != 0) {
		p = Xmpcb_get_prec(tls, z1)
		if Xmpcb_get_prec(tls, z) != p {
			Xmpcb_set_prec(tls, z, p)
		}
	}
	Xmpc_neg(tls, z, z1, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4)) /* exact */
	Xmpcr_set(tls, z+64, z1+64)
}

func Xmpcb_mul(tls *libc.TLS, z Tmpcb_ptr, z1 Tmpcb_srcptr, z2 Tmpcb_srcptr) {
	bp := tls.Alloc(80)
	defer tls.Free(80)
	var overlap int32
	var p Tmpfr_prec_t
	var v1 int64
	var _ /* r at bp+0 */ Tmpcr_t
	var _ /* zc at bp+16 */ Tmpc_t
	_, _, _ = overlap, p, v1
	if Xmpcb_get_prec(tls, z1) < Xmpcb_get_prec(tls, z2) {
		v1 = Xmpcb_get_prec(tls, z1)
	} else {
		v1 = Xmpcb_get_prec(tls, z2)
	}
	p = v1
	overlap = libc.BoolInt32(z == z1 || z == z2)
	if overlap != 0 {
		Xmpc_init2(tls, bp+16, p)
	} else {
		(*(*Tmpc_t)(unsafe.Pointer(bp + 16)))[0] = *(*t__mpc_struct)(unsafe.Pointer(z))
		Xmpc_set_prec(tls, bp+16, p)
	}
	Xmpc_mul(tls, bp+16, z1, z2, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
	if overlap != 0 {
		Xmpc_clear(tls, z)
	}
	*(*t__mpc_struct)(unsafe.Pointer(z)) = (*(*Tmpc_t)(unsafe.Pointer(bp + 16)))[0]
	/* generic error of multiplication */
	Xmpcr_mul(tls, bp, z1+64, z2+64)
	Xmpcr_add(tls, bp, bp, z1+64)
	Xmpcr_add(tls, bp, bp, z2+64)
	/* error of rounding to nearest */
	Xmpcr_add_rounding_error(tls, bp, p, int32(_MPFR_RNDN))
	Xmpcr_set(tls, z+64, bp)
}

func Xmpcb_sqr(tls *libc.TLS, z Tmpcb_ptr, z1 Tmpcb_srcptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var overlap int32
	var p Tmpfr_prec_t
	var _ /* r at bp+0 */ Tmpcr_t
	var _ /* r2 at bp+16 */ Tmpcr_t
	_, _ = overlap, p
	p = Xmpcb_get_prec(tls, z1)
	overlap = libc.BoolInt32(z == z1)
	/* Compute the error first in case there is overlap. */
	Xmpcr_mul_2ui(tls, bp+16, z1+64, uint64(1))
	Xmpcr_sqr(tls, bp, z1+64)
	Xmpcr_add(tls, bp, bp, bp+16)
	Xmpcr_add_rounding_error(tls, bp, p, int32(_MPFR_RNDN))
	if !(overlap != 0) {
		Xmpcb_set_prec(tls, z, p)
	}
	Xmpc_sqr(tls, z, z1, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
	Xmpcr_set(tls, z+64, bp)
}

func Xmpcb_pow_ui(tls *libc.TLS, z Tmpcb_ptr, z1 Tmpcb_srcptr, e uint64) {
	bp := tls.Alloc(80)
	defer tls.Free(80)
	var _ /* pow at bp+0 */ Tmpcb_t
	if e == uint64(0) {
		Xmpcb_set_ui_ui(tls, z, uint64(1), uint64(0), Xmpcb_get_prec(tls, z1))
	} else {
		if e == uint64(1) {
			Xmpcb_set(tls, z, z1)
		} else {
			/* Right to left powering is easier to implement, but requires an
			   additional variable even when there is no overlap. */
			Xmpcb_init(tls, bp)
			Xmpcb_set(tls, bp, z1)
			/* Avoid setting z to 1 and multiplying by it, instead set it to the
			   smallest 2-power multiple of z1 that is occurring. */
			for e%uint64(2) == uint64(0) {
				Xmpcb_sqr(tls, bp, bp)
				e /= uint64(2)
			}
			Xmpcb_set(tls, z, bp)
			e /= uint64(2)
			for e != uint64(0) {
				Xmpcb_sqr(tls, bp, bp)
				if e%uint64(2) == uint64(1) {
					Xmpcb_mul(tls, z, z, bp)
				}
				e /= uint64(2)
			}
			Xmpcb_clear(tls, bp)
		}
	}
}

func Xmpcb_add(tls *libc.TLS, z Tmpcb_ptr, z1 Tmpcb_srcptr, z2 Tmpcb_srcptr) {
	bp := tls.Alloc(112)
	defer tls.Free(112)
	var overlap int32
	var p Tmpfr_prec_t
	var v1 int64
	var _ /* denom at bp+32 */ Tmpcr_t
	var _ /* r at bp+0 */ Tmpcr_t
	var _ /* s at bp+16 */ Tmpcr_t
	var _ /* zc at bp+48 */ Tmpc_t
	_, _, _ = overlap, p, v1
	if Xmpcb_get_prec(tls, z1) < Xmpcb_get_prec(tls, z2) {
		v1 = Xmpcb_get_prec(tls, z1)
	} else {
		v1 = Xmpcb_get_prec(tls, z2)
	}
	p = v1
	overlap = libc.BoolInt32(z == z1 || z == z2)
	if overlap != 0 {
		Xmpc_init2(tls, bp+48, p)
	} else {
		(*(*Tmpc_t)(unsafe.Pointer(bp + 48)))[0] = *(*t__mpc_struct)(unsafe.Pointer(z))
		Xmpc_set_prec(tls, bp+48, p)
	}
	Xmpc_add(tls, bp+48, z1, z2, int32(_MPFR_RNDZ)+int32(_MPFR_RNDZ)<<libc.Int32FromInt32(4))
	/* rounding towards 0 makes the generic error easier to compute,
	   but incurs a tiny penalty for the rounding error */
	/* generic error of addition:
	   r <= (|z1|*r1 + |z2|*r2) / |z1+z2|
	     <= (|z1|*r1 + |z2|*r2) / |z| since we rounded towards 0 */
	Xmpcr_c_abs_rnd(tls, bp+32, bp+48, int32(_MPFR_RNDD))
	Xmpcr_c_abs_rnd(tls, bp, z1, int32(_MPFR_RNDU))
	Xmpcr_mul(tls, bp, bp, z1+64)
	Xmpcr_c_abs_rnd(tls, bp+16, z2, int32(_MPFR_RNDU))
	Xmpcr_mul(tls, bp+16, bp+16, z2+64)
	Xmpcr_add(tls, bp, bp, bp+16)
	Xmpcr_div(tls, bp, bp, bp+32)
	/* error of directed rounding */
	Xmpcr_add_rounding_error(tls, bp, p, int32(_MPFR_RNDZ))
	if overlap != 0 {
		Xmpc_clear(tls, z)
	}
	*(*t__mpc_struct)(unsafe.Pointer(z)) = (*(*Tmpc_t)(unsafe.Pointer(bp + 48)))[0]
	Xmpcr_set(tls, z+64, bp)
}

func Xmpcb_sqrt(tls *libc.TLS, z Tmpcb_ptr, z1 Tmpcb_srcptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	/* The function "glides over" the branch cut on the negative real axis:
	   In fact it always returns a ball with centre the square root of the
	   centre of z1, and a reasonable radius even when the input ball has a
	   crosses the negative real axis. This is inconsistent in a sense: The
	   output ball does not contain all the possible outputs of a call to
	   mpc_sqrt on an element of the input ball. On the other hand, it does
	   contain square roots of all elements of the input ball. This comes
	   handy for the alternative implementation of mpc_agm using ball
	   arithmetic, but would also cause a potential implementation of
	   mpcb_agm to ignore the branch cut. */
	var overlap int32
	var p Tmpfr_prec_t
	var _ /* r at bp+0 */ Tmpcr_t
	_, _ = overlap, p
	p = Xmpcb_get_prec(tls, z1)
	overlap = libc.BoolInt32(z == z1)
	/* Compute the error first in case there is overlap. */
	/* generic error of square root for z1->r <= 0.5:
	   0.5*epsilon1 + (sqrt(2)-1) * epsilon1^2
	   <= 0.5 * epsilon1 * (1 + epsilon1),
	   see eq:propsqrt in algorithms.tex, together with a Taylor
	   expansion of 1/sqrt(1-epsilon1) */
	if !(Xmpcr_lt_half_p(tls, z1+64) != 0) {
		Xmpcr_set_inf(tls, bp)
	} else {
		Xmpcr_set_one(tls, bp)
		Xmpcr_add(tls, bp, bp, z1+64)
		Xmpcr_mul(tls, bp, bp, z1+64)
		Xmpcr_div_2ui(tls, bp, bp, uint64(1))
		/* error of rounding to nearest */
		Xmpcr_add_rounding_error(tls, bp, p, int32(_MPFR_RNDN))
	}
	if !(overlap != 0) {
		Xmpcb_set_prec(tls, z, p)
	}
	Xmpc_sqrt(tls, z, z1, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
	Xmpcr_set(tls, z+64, bp)
}

func Xmpcb_div(tls *libc.TLS, z Tmpcb_ptr, z1 Tmpcb_srcptr, z2 Tmpcb_srcptr) {
	bp := tls.Alloc(96)
	defer tls.Free(96)
	var overlap int32
	var p Tmpfr_prec_t
	var v1 int64
	var _ /* r at bp+0 */ Tmpcr_t
	var _ /* s at bp+16 */ Tmpcr_t
	var _ /* zc at bp+32 */ Tmpc_t
	_, _, _ = overlap, p, v1
	if Xmpcb_get_prec(tls, z1) < Xmpcb_get_prec(tls, z2) {
		v1 = Xmpcb_get_prec(tls, z1)
	} else {
		v1 = Xmpcb_get_prec(tls, z2)
	}
	p = v1
	overlap = libc.BoolInt32(z == z1 || z == z2)
	if overlap != 0 {
		Xmpc_init2(tls, bp+32, p)
	} else {
		(*(*Tmpc_t)(unsafe.Pointer(bp + 32)))[0] = *(*t__mpc_struct)(unsafe.Pointer(z))
		Xmpc_set_prec(tls, bp+32, p)
	}
	Xmpc_div(tls, bp+32, z1, z2, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
	if overlap != 0 {
		Xmpc_clear(tls, z)
	}
	*(*t__mpc_struct)(unsafe.Pointer(z)) = (*(*Tmpc_t)(unsafe.Pointer(bp + 32)))[0]
	/* generic error of division */
	Xmpcr_add(tls, bp, z1+64, z2+64)
	Xmpcr_set_one(tls, bp+16)
	Xmpcr_sub_rnd(tls, bp+16, bp+16, z2+64, int32(_MPFR_RNDD))
	Xmpcr_div(tls, bp, bp, bp+16)
	/* error of rounding to nearest */
	Xmpcr_add_rounding_error(tls, bp, p, int32(_MPFR_RNDN))
	Xmpcr_set(tls, z+64, bp)
}

func Xmpcb_div_2ui(tls *libc.TLS, z Tmpcb_ptr, z1 Tmpcb_srcptr, e uint64) {
	Xmpc_div_2ui(tls, z, z1, e, int32(_MPFR_RNDN)+int32(_MPFR_RNDN)<<libc.Int32FromInt32(4))
	Xmpcr_set(tls, z+64, z1+64)
}

func Xmpcb_can_round(tls *libc.TLS, op Tmpcb_srcptr, prec_re Tmpfr_prec_t, prec_im Tmpfr_prec_t, rnd Tmpc_rnd_t) (r int32) {
	/*
	   The function returns true if it can decide that rounding the centre
	   	of op to an mpc_t variable of precision prec_re for the real and
	   	prec_im for the imaginary part returns a correctly rounded result
	   	of the ball in direction rnd for which the rounding direction value
	   	can be determined. The second condition implies that if the centre
	   	can be represented at the target precisions and the radius is small,
	   	but non-zero, the function returns false although correct rounding
	   	would be possible, while the rounding direction value could be
	   	anything.
	   	If the return value is true, then using mpcb_round with the same
	   	rounding mode sets a correct result and returns a correct rounding
	   	direction value with the usual MPC semantic.
	*/
	var exp_err, exp_im, exp_re Tmpfr_exp_t
	var im, re Tmpfr_srcptr
	var v1 int64
	_, _, _, _, _, _ = exp_err, exp_im, exp_re, im, re, v1
	if Xmpcr_inf_p(tls, op+64) != 0 {
		return 0
	} else {
		if Xmpcr_zero_p(tls, op+64) != 0 {
			return int32(1)
		}
	}
	re = op
	im = op + 32
	/* If the real or the imaginary part of the centre is 0, directed
	   rounding is impossible, and rounding to nearest is only possible
	   if the absolute error is less than the smallest representable
	   number; since rounding only once at precision p introduces an error
	   of about 2^-p, this means that the precision needs to be about as
	   big as the negative of the minimal exponent, which is astronomically
	   large. In any case, even then we could not determine the rounding
	   direction value. */
	if (*t__mpfr_struct)(unsafe.Pointer(re)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) || (*t__mpfr_struct)(unsafe.Pointer(im)).F_mpfr_exp == libc.Int64FromInt32(0)-libc.Int64FromUint64(libc.Uint64FromInt32(-libc.Int32FromInt32(1))>>libc.Int32FromInt32(1)) {
		return 0
	}
	exp_re = (*t__mpfr_struct)(unsafe.Pointer(re)).F_mpfr_exp
	exp_im = (*t__mpfr_struct)(unsafe.Pointer(im)).F_mpfr_exp
	/* Absolute error of the real part, as given in the proof of
	   prop:comrelerror of algorithms.tex:
	   |x-x~|  = |x~*theta_R - y~*theta_I|
	          <= (|x~|+|y~|) * epsilon,
	             where epsilon is the complex relative error
	          <= 2 * max (|x~|, |y~|) * epsilon
	   To call mpfr_can_round, we only need the exponent in base 2,
	   which is then bounded above by
	             1 + max (exp_re, exp_im) + exponent (epsilon) */
	if exp_re > exp_im {
		v1 = exp_re
	} else {
		v1 = exp_im
	}
	exp_err = int64(1) + v1 + Xmpcr_get_exp(tls, op+64)
	/* To check whether the rounding direction value can be determined
	   when rounding to nearest, use the trick described in the
	   documentation of mpfr_can_round to check for directed rounding
	   at precision larger by 1. */
	return libc.BoolInt32(libmpfr.Xmpfr_can_round(tls, re, exp_re-exp_err, int32(_MPFR_RNDN), int32(_MPFR_RNDZ), prec_re+libc.BoolInt64(rnd&libc.Int32FromInt32(0x0F) == int32(_MPFR_RNDN))) != 0 && libmpfr.Xmpfr_can_round(tls, im, exp_im-exp_err, int32(_MPFR_RNDN), int32(_MPFR_RNDZ), prec_im+libc.BoolInt64(rnd>>libc.Int32FromInt32(4) == int32(_MPFR_RNDN))) != 0)
}

func Xmpcb_round(tls *libc.TLS, rop Tmpc_ptr, op Tmpcb_srcptr, rnd Tmpc_rnd_t) (r int32) {
	/* Set rop to the centre of op and return the corresponding rounding
	   direction value. To make sure that this corresponds to the MPC
	   semantics of returning a correctly rounded result and a correct
	   rounding direction value, one needs to call mpcb_can_round first. */
	return Xmpc_set(tls, rop, op, rnd)
}

var __ccgo_ts = (*reflect.StringHeader)(unsafe.Pointer(&__ccgo_ts1)).Data

var __ccgo_ts1 = "1.3.1\x00%+li\x00(\x00 \x00)\x00(%s\x00(%s %s\x00∞\x000\x00±(%li, %li)\x00\n\x00"
