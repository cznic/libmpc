// Copyright 2023 The libmpc-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:generate go run generator.go

// Package libmpc is a ccgo/v4 version of libmpc.a (https://www.multiprecision.org/mpc/)
package libmpc // import "modernc.org/libmpc"
