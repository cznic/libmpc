// Copyright 2023 The libmpc-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package libmpc // import "modernc.org/libmpc"

import (
	"context"
	"flag"
	"fmt"
	"io/fs"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"
	"sync"
	"testing"
	"time"

	_ "modernc.org/ccgo/v3/lib"
	"modernc.org/ccgo/v4/lib"
)

var (
	oXTags     = flag.String("xtags", "", "passed as -tags to go build of tests") //TODO
	goos       = runtime.GOOS
	goarch     = runtime.GOARCH
	gomaxprocs = runtime.GOMAXPROCS(-1)
)

func TestMain(m *testing.M) {
	rc := m.Run()
	os.Exit(rc)
}

type test struct {
	buildok   int
	compileok int
	err       error
	fail      int
	out       []byte
	pass      int
	path      string
	testN     int
}

var skips = []string{
	"tests/tset.o.go", // fails in C as well (mpc-1.3.1)
}

func Test(t *testing.T) {
	tempDir := t.TempDir()
	libtests := filepath.Join("testdata", goos, goarch, "libmpc-tests.ago")
	var files, skip, compileok, buildok, fail, pass int
	limiter := make(chan struct{}, gomaxprocs)
	var wg sync.WaitGroup
	var tasks []*test
	filepath.Walk(filepath.Join("testdata", goos, goarch, "tests"), func(path string, info fs.FileInfo, err error) error {
		if err != nil {
			t.Fatal(err)
		}

		switch {
		case info.IsDir():
			return nil
		case
			strings.HasSuffix(path, ".dat"),
			strings.HasSuffix(path, ".dsc"):

			b, err := os.ReadFile(path)
			if err != nil {
				t.Fatal(err)
			}

			if err := os.WriteFile(filepath.Join(tempDir, filepath.Base(path)), b, 0660); err != nil {
				t.Fatal(err)
			}

			return nil
		case !strings.HasSuffix(path, ".o.go"):
			return nil
		}

		files++
		slash := filepath.ToSlash(path)
		for _, v := range skips {
			if strings.HasSuffix(slash, v) {
				skip++
				t.Logf("%s: SKIP (list)", path)
				return nil
			}
		}

		b, err := os.ReadFile(path)
		if err != nil {
			t.Fatal(err)
		}

		if !strings.Contains(string(b), "\nfunc ppmain() {") {
			skip++
			t.Logf("%s: SKIP (no main)", path)
			return nil
		}

		wg.Add(1)
		task := &test{path: path, testN: files}
		tasks = append(tasks, task)
		return nil
	})

	for i, task := range tasks {
		limiter <- struct{}{}
		t.Log(i, task.path)

		go func(task *test) {

			defer func() {
				<-limiter
				wg.Done()
			}()

			goFile := filepath.Join(tempDir, fmt.Sprintf("test%v.go", task.testN))
			bin := filepath.Join(tempDir, fmt.Sprintf("test%v", task.testN))
			if goos == "windows" {
				bin += ".exe"
			}
			if task.err = ccgo.NewTask(
				goos, goarch,
				[]string{
					"",
					"-no-main-minimize",
					"--prefix-enumerator=_",
					"--prefix-external=x_",
					"--prefix-field=F",
					"--prefix-macro=m_",
					"--prefix-static-internal=_",
					"--prefix-static-none=_",
					"--prefix-tagged-enum=_",
					"--prefix-tagged-struct=T",
					"--prefix-tagged-union=T",
					"--prefix-typename=T",
					"--prefix-undefined=_",
					"-extended-errors",
					"-o", goFile,
					task.path,
					libtests,
					"-lgmp",
					"-lmpfr",
					"-lmpc",
				},
				os.Stdout, os.Stderr,
				nil,
			).Main(); task.err != nil {
				return
			}

			task.compileok++
			if task.out, task.err = exec.Command("go", "build", "-o", bin, goFile).CombinedOutput(); task.err != nil {
				return
			}

			task.buildok++
			if task.out, task.err = run(bin, tempDir); task.err != nil {
				task.fail++
			} else {
				task.pass++
			}
		}(task)
	}

	wg.Wait()
	for _, v := range tasks {
		compileok += v.compileok
		buildok += v.buildok
		fail += v.fail
		pass += v.pass
		if v.err != nil || v.fail != 0 {
			t.Errorf("%s: %s FAIL %v", v.path, v.out, v.err)
		}
	}
	t.Logf("files=%v skip=%v compileok=%v buildok=%v fail=%v pass=%v", files, skip, compileok, buildok, fail, pass)
	// all_test.go:180: files=94 skip=21 compileok=73 buildok=73 fail=0 pass=73
}

func run(bin, inDir string) (out []byte, err error) {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Minute)

	defer cancel()

	cmd := exec.CommandContext(ctx, bin)
	cmd.Dir = inDir
	cmd.WaitDelay = 20 * time.Second
	return cmd.CombinedOutput()
}
